<?php 
function pdf_url($post_id){
	return plugins_url() . "/fudoumodule/pdf/pdf.php?post_id=" . $post_id;

}

//PDFリンクを管理画面に表示
add_filter("fudomodulecolumns",function($text,$post_id){return  $text.'<a href="'. pdf_url($post_id). '">PDFを表示</a><br/>';},20,2);
