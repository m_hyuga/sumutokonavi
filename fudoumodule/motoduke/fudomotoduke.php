<?php
/*
不動産元付モジュール
*/

/*チェック用*/
add_action("admin_head", 'suffix2console');
function suffix2console() {
global $hook_suffix;
	if (is_user_logged_in()) {
	$str = "<script type=\"text/javascript\">console.log('%s')</script>";
	//printf($str, $hook_suffix);
	}
} 


function motoduke_add_pages() {
add_submenu_page( 'tools.php', '元付PDF出力', '元付PDF出力', 'level_7', 'mododuke_pdfoutput_page', 'mododuke_pdfoutput_page' );
}
add_action('admin_menu', 'motoduke_add_pages');

add_action( 'load-tools_page_mododuke_pdfoutput_page', 'motoduke_pdfoutput' );
function motoduke_pdfoutput(){
	$motodukes = array();
	if(isset($_POST["tamplatetype"])){
		$motodukes = ($_POST["tamplatetype"]  == "a")?$_POST["motoduke"]:$_POST["motoduke2"];
	}
	if($motodukes && empty($_POST["view"])){
	
		$maxcurl = 20;
		$curlcount = 0;
		$sttime = time();
		$lists = array();
		
		foreach($motodukes as $motoduke){
			$mo = $motoduke;
			$curlcount++;
			if($curlcount == count($motodukes)){$endflag="&down=1";}else{$endflag="";}
			$lists[] = site_url() . "/wp-admin/tools.php?page=mododuke_pdfoutput_page&curl={$mo}&type={$_POST['tamplatetype']}&count={$curlcount}{$endflag}";
			if($curlcount % $maxcurl == 0){
				fetch_multi_url($lists,120);
				$lists = array();
			}
			
		}
		if($curlcount % $maxcurl){fetch_multi_url($lists,120);}
		if($curlcount>0){
			$zip = new ZipArchive();
			$zipFileName = "template_{$_POST['tamplatetype']}.zip";
			$zipFilePath = FUDOMODULEPATH . "/motoduke/temp/";
			
			$result = $zip->open($zipFilePath.$zipFileName, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
			if ($result !== true) {
			      return false;
		    }	
			
			if ($dir = opendir($zipFilePath)) {
			
			    while (($file = readdir($dir)) !== false) {
		
			        if (substr($file,-4) == ".pdf") {
			           //$zip->addFile($zipFilePath . $file,$file);
					   $zip->addFromString($file,file_get_contents($zipFilePath . $file));
					}
			    } 
			    closedir($dir);
				$zip->close();
				sleep(3);
				// ストリームに出力
				
			    header('Content-Type: application/zip; name="' . $zipFileName . '"');
			    header('Content-Disposition: attachment; filename="' . $zipFileName . '"');
			    header('Content-Length: '.filesize($zipFilePath.$zipFileName));
			    echo file_get_contents($zipFilePath.$zipFileName);
			    // 一時ファイルを削除
			    deleteData ( $zipFilePath );
				exit();
			}	
		}
		
	}
}
if(!empty($_GET["update"])){
	global $sttime;
	global $curlcount;
	$sttime = $_GET["stime"];
	$curlcount = $_GET["count"];
	add_action ('admin_notices','curl_message');
}
if(!empty($_GET["curl"])){multi_pdfoutput($_GET["curl"],$_GET["type"],$_GET["count"]);} 
function multi_pdfoutput($motoduke,$type,$count){
		global $wpdb;
		//テンプレートを読み込む（A	or B)
		require(FUDOMODULEPATH ."/template/htmltemplate_" . $type . ".php");
		
		//PDFライブラリ読み込み
		require FUDOMODULEPATH . "/mpdf/mpdf.php";

		//元付を選択
		set_time_limit(300);
		$mpdf=new mPDF('ja', 'A4-L',0,'',5,5,5,5,1,1);
		$mpdf->SetFont("meiryo");
		$path = FUDOMODULEPATH . "/motoduke/temp/";
		ob_start();
		output_pdf($motoduke);
		$html = ob_get_contents();
		ob_end_clean();
		$mpdf->WriteHTML($html);
		$filename = $path . str_pad($count, 3, "0", STR_PAD_LEFT) . replaceText(trim($motoduke)) . ".pdf";
		$filename = mb_convert_encoding($filename, MOJICODE); 
		$mpdf->Output($filename,"f");
}

function pdfoutput_message() {
	 echo '<div class="message updated"><p>PDFを保存しました。</p></div>';
}


function curl_message() {
	global $sttime;
	global $curlcount;
	$n = time() - $sttime;
	 echo '<div class="message updated"><p>' . $curlcount .'件のPDFを保存しました。経過時間：'. $n .'秒</p></div>';
	 fudomodule_log($curlcount .'件のPDFを保存しました。経過時間：'. $n .'秒');
}

function mododuke_pdfoutput_page(){
	global $wpdb;
?>

  <div class="wrap">
        <h2>元付PDF出力</h2>
		<?php 
		/*
		echo "log";
		echo get_option("fudomodule_log");
		echo "end";
	//	fudomodule_log("【ログ開始】",1);
	print_R(get_option( 'active_plugins', array() ));*/
		?>
		<?php 
		$mpdfpath =  FUDOMODULEPATH. "/mpdf/mpdf.php";
		if(!file_exists($mpdfpath)):?>
			<div class="error">
			<?php echo $mpdfpath;?>は存在しません。MPDFをインストールしてください。
			</div>
		<?php else:?>
			<?php if(!is_writable(FUDOMODULEPATH. "/motoduke/temp/")):?>
				<div class="error">
				<?php echo FUDOMODULEPATH. "/motoduke/temp/";?>は書き込みが禁止されています。パーミッションを確認してください。
				</div>			
			
			<?php endif;?>
			<?php $metaalls = $wpdb->get_results("SELECT DISTINCT M.meta_value FROM $wpdb->postmeta AS M INNER JOIN $wpdb->posts AS P ON M.post_id = P.ID WHERE M.meta_key = 'motozukemei'");?>
			<?php 
					asort($metaalls);
					if(!empty($_GET["merge"])){
						$merges = array();
						if($_GET["merge"] == "all"){
							foreach($metaalls as $metaall){$merges[] = $metaall->meta_value;}
						}else{
							$merges = array($_GET["merge"]);
						}
						foreach($merges as $merge){
							$tomerge = replaceText($merge);
							$wpdb->query("UPDATE $wpdb->postmeta SET meta_value = REPLACE(meta_value,'{$merge}','{$tomerge}')");
						}
					}
					$metaalls = $wpdb->get_results("SELECT DISTINCT M.meta_value FROM $wpdb->postmeta AS M INNER JOIN $wpdb->posts AS P ON M.post_id = P.ID WHERE M.meta_key = 'motozukemei'");
					foreach($metaalls as $metaall){
						$array1[] = $metaall->meta_value;
						$array2[] = replaceText($metaall->meta_value);
					}
					$array2 = array_unique($array2);
				  	$diffs = array();
				 	$diffs = array_diff($array1, $array2);
					asort($diffs);
				  if($diffs):
				  ?>
					<div class="error">
						<a href="tools.php?page=mododuke_pdfoutput_page&merge=all">すべてまとめる</a><br/>
						<?php foreach($diffs as $diff):?>
							<?php echo $diff;?>は類似データがあります。＞
							<a href="tools.php?page=mododuke_pdfoutput_page&merge=<?php echo $diff;?>"><?php echo replaceText($diff);?>にまとめる</a><br/>
						<?php endforeach;?>
					</div>
				  <?php endif;?>
				 			<form method="post">
				<div>
					<h3>テンプレートタイプ</h3>
					<select name="tamplatetype" id="templatetype">
						<option value="a">A</option>
						<option value="b">B</option>
					</select>
					<h3>出力元付</h3>
					<select name="motoduke[]" id="type_a" size="10" multiple style="height:200px;width:200px;">
					<?php $metas = $wpdb->get_results("SELECT DISTINCT M.meta_value FROM ($wpdb->postmeta AS M INNER JOIN $wpdb->posts AS P ON M.post_id = P.ID) INNER JOIN $wpdb->postmeta AS M2 ON P.ID = M2.post_id  WHERE M.meta_key = 'motozukemei' AND M2.meta_key = 'kaiin' AND M2.meta_value = '1' AND P.post_status = 'publish'");?>
						<?php asort($metas);?>		
						<?php foreach($metas as $meta):?>
							<option value="<?php echo $meta->meta_value;?>" selected><?php echo $meta->meta_value;?></option>
						<?php endforeach;?>
					</select>
					<select name="motoduke2[]" size="10" id="type_b" multiple style="height:200px;width:200px;display:none;">
					<?php $metas2 = $wpdb->get_results("SELECT DISTINCT M.meta_value FROM ($wpdb->postmeta AS M INNER JOIN $wpdb->posts AS P ON M.post_id = P.ID) INNER JOIN $wpdb->postmeta AS M2 ON P.ID = M2.post_id  WHERE M.meta_key = 'motozukemei' AND M2.meta_key = 'kaiin' AND M2.meta_value = '0' AND P.post_status = 'publish'");?>
						<?php asort($metas2);?>
						<?php foreach($metas2 as $meta2):?>
							<option value="<?php echo $meta2->meta_value;?>" selected><?php echo $meta2->meta_value;?></option>
						<?php endforeach;?>
					</select>
					<input type="checkbox" name="view" value="1">ViewMode<br />
					<input type="submit" value="PDF出力">
				</div>
			</form>
			<script>
				jQuery(function(){
					if(jQuery("#templatetype").val() == "a"){jQuery("#type_a").css("display","inline");jQuery("#type_b").css("display","none");}
					if(jQuery("#templatetype").val() == "b"){jQuery("#type_b").css("display","inline");jQuery("#type_a").css("display","none");}
				});
				jQuery("#templatetype").change(function(){
				
					if(jQuery(this).val() == "a"){jQuery("#type_a").css("display","inline");jQuery("#type_b").css("display","none");}
					if(jQuery(this).val() == "b"){jQuery("#type_b").css("display","inline");jQuery("#type_a").css("display","none");}
				});
			</script>
			<?php
			/*Viewモードの場合*/
			$motodukes=array();
			if(isset($_POST["tamplatetype"])){
				$motodukes = ($_POST["tamplatetype"]  == "a")?$_POST["motoduke"]:$_POST["motoduke2"];
			}
			if($motodukes && $_POST["view"]):
				require(FUDOMODULEPATH ."/template/htmltemplate_" . $_POST["tamplatetype"] . ".php");
				foreach($motodukes as $motoduke){
					output_pdf($motoduke);

				}
			?>
			
			<?php endif;?>
		<?php endif;?>
  </div>





<?php

}

//物件種別
function pdf_bukkenshubetsu($post_id){
	global $work_bukkenshubetsu;
	$bukkenshubetsu = get_post_meta($post_id,"bukkenshubetsu",true);
	$shubetu_work = $work_bukkenshubetsu[$bukkenshubetsu];
	return $shubetu_work["name"];
}

//所在地
function pdf_custom_shozaichi_print($post_id) {
	global $wpdb;

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = pdfLeft($shozaichiken_data,2);

	if($shozaichiken_data=="")
		$shozaichiken_data = get_post_meta($post_id,'shozaichiken',true);

		$sql = "SELECT `middle_area_name` FROM `".$wpdb->prefix."area_middle_area` WHERE `middle_area_id`=".$shozaichiken_data."";
		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) echo $metas->middle_area_name;

	$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichicode_data = pdfLeft($shozaichicode_data,5);
	$shozaichicode_data = pdfRight($shozaichicode_data,3);

	if($shozaichiken_data !="" && $shozaichicode_data !=""){
		$sql = "SELECT narrow_area_name FROM ".$wpdb->prefix."area_narrow_area WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) echo $metas->narrow_area_name;
	}
	
	echo get_post_meta($post_id,"shozaichimeisho",true);
}

//交通１
function pdf_custom_koutsu1_print($post_id) {
	global $wpdb;

	$rosen_name = '';
	$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
	$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = pdfLeft($shozaichiken_data,2);


	if($koutsurosen_data !=""){
		$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)){
			$rosen_name = $metas->rosen_name;
			echo "".$rosen_name;
		}
	}

	if($koutsurosen_data !="" && $koutsueki_data !=""){
		$sql = "SELECT DTS.station_name";
		$sql = $sql . " FROM ".$wpdb->prefix."train_rosen AS DTR";
		$sql = $sql . " INNER JOIN ".$wpdb->prefix."train_station AS DTS ON DTR.rosen_id = DTS.rosen_id";
		$sql = $sql . " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)){
			if($metas->station_name != '＊＊＊＊') 	echo $metas->station_name.'駅';
		}
	}

	$koutsubusstei=get_post_meta($post_id, 'koutsubusstei1', true);
	$koutsubussfun=get_post_meta($post_id, 'koutsubussfun1', true);
	$koutsutohob1f=get_post_meta($post_id, 'koutsutohob1f', true);
	if($koutsubusstei !="" || $koutsubussfun !=""){
		$koutsubussfun_new="";
		if($koutsubussfun){$koutsubussfun_new = $koutsubussfun .'分';}
		if($rosen_name == 'バス'){
			echo '('.$koutsubusstei.' '.$koutsubussfun_new;
		}else{
			echo ' バス('.$koutsubusstei.' '.$koutsubussfun_new;
		}

		if($koutsutohob1f !="" )
			echo ' 停歩'.$koutsutohob1f.'分';
		echo ')';
	}

	if(get_post_meta($post_id, 'koutsutoho1', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1', true).'m';

	if(get_post_meta($post_id, 'koutsutoho1f', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1f', true).'分';
}

//交通２
function pdf_custom_koutsu2_print($post_id) {
	global $wpdb;

	$rosen_name = '';
	$koutsurosen_data = get_post_meta($post_id, 'koutsurosen2', true);
	$koutsueki_data = get_post_meta($post_id, 'koutsueki2', true);

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = pdfLeft($shozaichiken_data,2);

	if($koutsurosen_data !=""){
		$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)){
			$rosen_name = $metas->rosen_name;
			echo "<br />".$rosen_name;
		}
	}

	if($koutsurosen_data !="" && $koutsueki_data !=""){
		$sql = "SELECT DTS.station_name";
		$sql = $sql . " FROM ".$wpdb->prefix."train_rosen AS DTR";
		$sql = $sql . " INNER JOIN ".$wpdb->prefix."train_station AS DTS ON DTR.rosen_id = DTS.rosen_id";
		$sql = $sql . " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)){
			if($metas->station_name != '＊＊＊＊') 	echo $metas->station_name.'駅';
		}
	}

	$koutsubusstei=get_post_meta($post_id, 'koutsubusstei2', true);
	$koutsubussfun=get_post_meta($post_id, 'koutsubussfun2', true);
	$koutsutohob2f=get_post_meta($post_id, 'koutsutohob2f', true);
	if($koutsubusstei !="" || $koutsubussfun !=""){

		if($rosen_name == 'バス'){
			echo '('.$koutsubusstei.' '.$koutsubussfun.'分';
		}else{
			echo ' バス('.$koutsubusstei.' '.$koutsubussfun.'分';
		}

		if($koutsutohob2f !="" )
			echo ' 停歩'.$koutsutohob2f.'分';
		echo ')';
	}

	if(get_post_meta($post_id, 'koutsutoho2', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2', true).'m';

	if(get_post_meta($post_id, 'koutsutoho2f', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2f', true).'分';
}

//価格
function pdf_kakaku($post_id){
	return number_format(get_post_meta($post_id,"kakaku",true)/10000);
}

//間取り
function pdf_madori($post_id) {
	$madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);
	echo get_post_meta($post_id,'madorisu',true);
	if($madorisyurui_data=="10")	echo 'R';
	if($madorisyurui_data=="20")	echo 'K';
	if($madorisyurui_data=="25")	echo 'SK';
	if($madorisyurui_data=="30")	echo 'DK';
	if($madorisyurui_data=="35")	echo 'SDK';
	if($madorisyurui_data=="40")	echo 'LK';
	if($madorisyurui_data=="45")	echo 'SLK';
	if($madorisyurui_data=="50")	echo 'LDK';
	if($madorisyurui_data=="55")	echo 'SLDK';
}


//築年数
function tikunensu($post_id){
	if(get_post_meta($post_id, 'tatemonochikunenn', true)){echo date("Y年m月",strtotime(get_post_meta($post_id, 'tatemonochikunenn', true) . "/01"));}else{echo "-";}
}

//機種依存文字を変換
function replaceText($str){
        //変換前の文字
        $search = Array('Ⅰ','Ⅱ','Ⅲ','Ⅳ','Ⅴ','Ⅵ','Ⅶ','Ⅷ','Ⅸ','Ⅹ','①','②','③','④','⑤','⑥','⑦','⑧','⑨','⑩','№','㈲','㈱','(有)','(株)'," ","　");
        //変換後の文字
        $replace = Array('I','II','III','IV','V','VI','VII','VIII','IX','X','(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)','(9)','(10)','No.','（有）','（株）','（有）','（株）',"","");

        $ret = str_replace($search, $replace, $str);

        return $ret;
}


//マルチスレッド処理

function fetch_multi_url($url_list,$timeout=0) {

    $mh = curl_multi_init();
 
    foreach ($url_list as $i => $url) {
        $conn[$i] = curl_init($url);
        curl_setopt($conn[$i],CURLOPT_RETURNTRANSFER,1);
        curl_setopt($conn[$i],CURLOPT_FAILONERROR,1);
        curl_setopt($conn[$i],CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($conn[$i],CURLOPT_MAXREDIRS,100);
        curl_setopt($conn[$i],CURLOPT_MAXCONNECTS,100);
		
        //SSL証明書を無視
        curl_setopt($conn[$i],CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($conn[$i],CURLOPT_SSL_VERIFYHOST,false);
        
        //タイムアウト
        if ($timeout){
            curl_setopt($conn[$i],CURLOPT_TIMEOUT,$timeout);
        }
       
        curl_multi_add_handle($mh,$conn[$i]);
    }
   
    //URLを取得
    //すべて取得するまでループ
    $active = null;
    do {
        $mrc = curl_multi_exec($mh,$active);
    } while ($mrc == CURLM_CALL_MULTI_PERFORM);
   
    while ($active and $mrc == CURLM_OK) {
        if (curl_multi_select($mh) != -1) {
            do {
                $mrc = curl_multi_exec($mh,$active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        }
    }
   
    if ($mrc != CURLM_OK) {
      //  echo '読み込みエラーが発生しました:'.$mrc;
	  add_action ('admin_notices','err1');
    }
   
    //ソースコードを取得
    $res = array();
    foreach ($url_list as $i => $url) {
        if (($err = curl_error($conn[$i])) == '') {
            $res[$i] = curl_multi_getcontent($conn[$i]);
        } else {
			global $errlist;
			$errlist = $url_list[$i] . $err;
			add_action ('admin_notices','err2');
        //    echo '取得に失敗しました:'.$url_list[$i].'<br />';
        }
        curl_multi_remove_handle($mh,$conn[$i]);
        curl_close($conn[$i]);
    }
    curl_multi_close($mh);
  
    return $res;
}

function deleteData ( $dir ) {
    if ( $dirHandle = opendir ( $dir )) {
        while ( false !== ( $fileName = readdir ( $dirHandle ) ) ) {
            if ( $fileName != "." && $fileName != ".." ) {
                unlink ( $dir.$fileName );
            }
        }
        closedir ( $dirHandle );
    }
}

function err1($err){
	echo "ERROR:001 {$err}";
}

function err2($err){
global $errlist;
	echo "ERROR:002 {$err}" . $errlist. "<br/>";
}
// Left関数//左からn文字取得して返す 
function pdfLeft($str,$n){
        if(mb_strlen($str,"utf-8") <= $n){
		return $str;
	}else{
		return mb_substr($str,0,(mb_strlen($str,"utf-8")-$n)*-1,"utf-8");
	}
}


// Right関数//右からn文字取得して返す 
function pdfRight($str,$n){
	return mb_substr($str,($n)*-1);
}