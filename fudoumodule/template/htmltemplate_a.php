<?php 
/*テンプレートA　公開済み会員公開物件*/
function output_pdf($motoduke){
global $wpdb;
require(dirname(__FILE__) ."/motoduke.css");

$tel = $wpdb->get_row("SELECT COUNT(*) as CNT,M2.meta_value FROM ($wpdb->postmeta AS M INNER JOIN $wpdb->posts AS P ON M.post_id = P.ID) INNER JOIN $wpdb->postmeta AS M2 ON P.ID = M2.post_id WHERE M.meta_key = 'motozukemei' AND M.meta_value='{$motoduke}' AND P.post_status = 'private' AND M2.meta_key =  'motozuketel'
order by CNT desc");
$fax = $wpdb->get_row("SELECT COUNT(*) as CNT,M2.meta_value FROM ($wpdb->postmeta AS M INNER JOIN $wpdb->posts AS P ON M.post_id = P.ID) INNER JOIN $wpdb->postmeta AS M2 ON P.ID = M2.post_id WHERE M.meta_key = 'motozukemei' AND M.meta_value='{$motoduke}' AND P.post_status = 'private' AND M2.meta_key =  'motozukefax'
order by CNT desc");
?>
<!-- container  -->
<div id="container">

<!-- header  -->
<div id="header">

<!-- left-Column  -->
<div id="left-Column">
<h1 class="header_title">不動産物件掲載についてのお願い</h1>
<table class="header_left_table">
<tr>
<td colspan="3" class="destination">
<h2><?php echo $motoduke;?>　御中</h2>
</td>
</tr>
<tr>
<td class="tantou">ご担当者様</td>
<td class="to_tel">ＴＥＬ <?php echo $tel->meta_value;?></td>
<td class="to_fax">ＦＡＸ <?php echo $fax->meta_value;?></td>
</tr>
</table>



<p class="cl01">
平素は格別のご厚誼を賜り厚くお礼申し上げます。<br/>
弊社は松本市を中心とした不動産物件検索サイトを自社で運営しております。<br/>
日々着々とインターネットから訪れるお客様が増えてきておりますが、お客様から
ご要望の多い「松本市およびその周辺エリア」の物件の掲載数が不足しており、
お客様にご迷惑をおかけしております。 つきましては、御社お取扱いの下記物件を
是非、弊社の不動産物件検索サイトへ掲載させていただきたく、掲載許可お願いの
FAXを送らせていただいた次第です。 何卒ご理解のほどよろしくお願い申し上げます。<br/>
大変お手数ではございますが、下記物件それぞれについて掲載許可[OK・NO]に○をつけて
いただき、本紙をそのまま
FAX：0263-27-9911までご返信のほどお願いいたします。
</p>
</div><!-- end of left-Column  -->

<!-- right-Column  -->
<div id="right-Column">
<p class="t_right mb30 mt15"><?php echo date("Y.m.d");?></p>
<table class="header_right_table">
<tr>
<th scope="row">返<br>信<br>先<br>
</th>
<td class="w275">
<p>発信元：</p>
<h3 class="f23 bold">株式会社セイブ</h3>
<h4>担当者 平林学</h4>
<p class="lh1_3 cl01">〒390-0833<br>
長野県松本市双葉24-10</p>
<p class="lh1_3 cl01">TEL：0263-27-9910</p>
<p class="f23 bold">FAX：0263-27-9911</p>
<p>e-mail：info@smileland.net </p>
</td>
<td class="va_m bdr f14"></td>
</tr>
</table>


</div><!-- end of right-Column  -->





</div><!-- end of header  -->
    
    
    
    
    
<!-- content  -->
<div id="content">



<?php
$ids = $wpdb->get_results("SELECT M.post_id FROM  ($wpdb->postmeta AS M INNER JOIN $wpdb->posts AS P ON M.post_id = P.ID) INNER JOIN $wpdb->postmeta AS M2 ON P.ID = M2.post_id WHERE M.meta_key = 'motozukemei' AND M.meta_value='{$motoduke}'AND M2.meta_key = 'kaiin' AND M2.meta_value = '1' AND P.post_status = 'publish'");
?>

<table class="cnt_table">
<tr>
<th scope="col" class="w01">物件種目</th>
<th scope="col" class="w02">物件番号</th>
<th scope="col" class="w03">住所</th>
<th scope="col" class="w04">交通／名称</th>
<th scope="col" class="w05">価格（万円）</th>
<th scope="col" class="w05">価格変更</th>
<th scope="col" class="w08">土地（㎡）</th>
<th scope="col" class="w06">間取り</th>
<th scope="col" class="w08">建物（㎡）</th>
<th scope="col" class="w06">築年月</th>
<th scope="col" class="w07">物件について</th>
</tr>
<?php 
foreach($ids as $id):
$post_id = $id->post_id;
?>
<tr>
<td><?php echo pdf_bukkenshubetsu($post_id);?></td>
<td><?php echo get_post_meta($post_id,"shikibesu",true);?></td>
<td class="td_left"><?php echo pdf_custom_shozaichi_print($post_id);?><br></td>
<td class="td_left"><?php pdf_custom_koutsu1_print($post_id);?><br/><?php pdf_custom_koutsu2_print($post_id);?></td>
<td><?php echo pdf_kakaku($post_id);?><br></td>
<td>&nbsp;</td>
<td><?php echo get_post_meta($post_id, 'tochikukaku', true);?></td>
<td><?php pdf_madori($post_id);?><br></td>
<td><?php echo get_post_meta($post_id, 'tatemonomenseki', true);?><br></td>
<td><?php echo tikunensu($post_id);?></td>
<td>ＯＫ・ＮＯ</td>
</tr>
<?php endforeach;?>
</table>

</div><!-- end of content  -->   
    
    






<!-- footer  -->
<div id="footer">

<ul>
<li class="cl01 f18">※この中には貴社から直前に資料を頂いた物件が含まれていることがあります。</li>
<li class="cl01 f18">※他に物件をお持ちでしたら資料をお送りください。</li>
</ul>
<hr>
<address>FAXのご返信先　→　株式会社セイブ　FAX番号：<span class="fax">0263-27-9911</span>までお願いします</address>
</div><!-- end of footer  -->

</div><!-- end of container  -->



<?php
}


