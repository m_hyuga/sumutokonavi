<?php
/*土地用PDF出力テンプレート*/
?>
<!-- container  -->
<div id="container">

<!-- upper  -->
<div id="upper">

<!-- left-Column  -->
<div id="left-Column">


<!-- header  -->
<div id="header">
<h1 class="header_title"><?php echo $title;?></h1>
<h2 class="header_access mb10"><?php my_custom_koutsu1_print($post_id); ?>
     <?php my_custom_koutsu2_print($post_id); ?></h2>
</div><!-- end of header  -->
    
    
    
    
    
<!-- content  -->
<div id="content" class="lcon">

<div id="main-img">
<div class="img-left">
<p class="mb5"><?php if(!empty($img_out[2])){echo $img_out[2];}?></p>
<p class="bd"><img src="<?php echo $mapsrc;?>" alt="*"></p>
</div>
<div class="img-right">
<p class="bd01 menseki">土地面積　<span class="specialspan"><?php echo get_post_meta($post_id, 'tochikukaku', true);?> m&sup2;(<?php echo tubo(get_post_meta($post_id, 'tochikukaku', true));?>)</span></p>
<p class="landmadori"><?php if(!empty($img_out[1])){echo $img_out[1];}?></p></div>
</div>


    
<div class="comment_bd">
<table id="comment">
<tr>
<th rowspan="2" scope="row" class="kaeru"><img src="<?php echo IMGPATH;?>/img/commentlogo.png" alt="コメントロゴ"></th>
<td class="sales_comment">セールスコメント</td>
</tr>
<tr>
<td>
<p class="comment_space">
<?php echo comment_line($pdf->post_content,8);?>
</p>
</td>
</tr>
</table>
</div>
 
</div><!-- end of content  -->

</div><!-- end of left-Column  -->
    
    
    
    
<!-- right-Column  -->
<div id="right-Column">

<table id="right_table">
<tr>
<th scope="row">物件種別</th>
<td colspan="3" class="font1"><?php my_custom_bukkenshubetsu_print($post_id); ?></td>
</tr>
<tr>
<th scope="row">価格</th>
<td colspan="3" class="font1"><?php my_custom_kakaku_print($post_id);?></td>
</tr>
<tr>
<th scope="row">土地面積</th>
<td colspan="3" class="font1"><?php echo get_post_meta($post_id, 'tochikukaku', true);?> m&sup2;</td>
</tr>
<tr>
<th scope="row">所在地</th>
<td colspan="3"><?php my_custom_shozaichi_print($post_id); ?><?php echo get_post_meta($post_id, 'shozaichimeisho', true); ?><?php echo get_post_meta($post_id, 'shozaichimeisho2', true); ?></td>
</tr>
<tr>
<th scope="row">現況</th>
<td><?php my_custom_nyukyogenkyo_print($post_id);?></td>
<th scope="row">建ぺい率</th>
<td class="right-Column-w02"><?php echo get_post_meta($post_id, 'tochikenpei', true);?>％</td>
</tr>
<tr>
<th scope="row">容積率</th>
<td colspan="3"><?php echo get_post_meta($post_id, 'tochiyoseki', true);?>％</td>
</tr>
<tr>
<th scope="row"><p>土地権利</p></th>
<td colspan="3"><?php my_custom_tochikenri_print($post_id);?></td>
</tr>
<tr>
<th scope="row">接道状況</th>
<td colspan="3"><?php my_custom_tochisetsudo_print($post_id);?> <?php  my_custom_tochisetsudohouko1_print($post_id);?> <?php my_custom_fukuin1($post_id);?> <?php my_custom_maguchi1($post_id);?>
 <?php  my_custom_tochisetsudohouko2_print($post_id);?> <?php my_custom_fukuin2($post_id);?> <?php my_custom_maguchi2($post_id);?></tr>
<tr>
<th scope="row">都市計画</th>
<td><?php  my_custom_tochikeikaku_print($post_id) ;?></td>
<th scope="row">地目</th>
<td><?php my_custom_tochichimoku_print($post_id);?></td>
</tr>
<tr>
<th scope="row">用途地域</th>
<td><?php my_custom_tochiyouto_print($post_id);?></td>
<th scope="row">取引態様</th>
<td><?php my_custom_torihikitaiyo_print($post_id);?></td>
</tr>
<tr>
<th scope="row">引渡し</th>
<td colspan="3"><?php my_custom_nyukyogenkyo_print($post_id);?><?php my_custom_nyukyojiki_print($post_id);?></td>
</tr>
<tr>
<th scope="row">小学校</th>
<td><?php echo get_post_meta($post_id,"shuuhenshougaku",true);?></td>
<th scope="row">中学校</th>
<td><?php echo get_post_meta($post_id,"shuuhenchuugaku",true);?></td>
</tr>
<tr class="setubi">
<th scope="row">備考</th>
<td colspan="3"><?php my_custom_setsubi_print($post_id);?>
</tr>
</table>



</div><!-- end of right-Column  -->
</div><!-- end of upper  -->

<?php pdf_footer();?>

</div><!-- end of container  -->