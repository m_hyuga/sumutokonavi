<?php
/*町名関連*/

add_action( 'wp_ajax_get_townname', 'get_townname' );
add_action( 'wp_ajax_nopriv_get_townname', 'get_townname' );
//学区選択AJAX返信
function get_townname() {
	global $wpdb;
	//町名
	$ken = $_POST["kenid"];
	$sik = $_POST["sikid"];
	$sikcode = $_POST["kenid"] . $_POST["sikid"] . "000000";
	
	$sql  =  " SELECT DISTINCT TWN.town_name ";
	$sql .=  " FROM ".$wpdb->prefix."area_town_area as TWN";
	$sql .=  " WHERE TWN.middle_area_id=".$ken ." AND TWN.narrow_area_id=".$sik ." ";
	$sql = $wpdb->prepare($sql,'');
	$metas = $wpdb->get_results( $sql,  ARRAY_A );		

	$sql = $wpdb->prepare($sql,'');
	$metas = $wpdb->get_results( $sql,  ARRAY_A );
	
	$sql2  = "SELECT DISTINCT M1.meta_value FROM (($wpdb->posts AS P ";
	$sql2 .= "INNER JOIN $wpdb->postmeta AS M1 ON M1.post_id = P.ID) ";
	$sql2 .= "INNER JOIN $wpdb->postmeta AS M2 ON M2.post_id = P.ID) ";
	$sql2 .= "INNER JOIN $wpdb->postmeta AS M3 ON M3.post_id = P.ID ";
	$sql2 .= "WHERE M1.meta_key = 'shozaichimeisho' ";
	$sql2 .= "AND M2.meta_key = 'shozaichiken' AND M2.meta_value = '$ken' ";
	$sql2 .= "AND M3.meta_key = 'shozaichicode' AND M3.meta_value = '$sikcode' ";
	$sql2 .= "AND P.post_status = 'publish' AND P.post_type = 'fudo'"; 
	echo '<option value="">町名を指定してください</option>';
	$sql2 = $wpdb->prepare($sql2,'');
	$metas2 = $wpdb->get_results( $sql2,  ARRAY_A );
	$shozaichimeisho = "|";
	foreach($metas2 as $meta2){
		$shozaichimeisho .= $meta2["meta_value"] . "|";
	}
	foreach($metas as $meta){
		if(preg_match('/'.$meta['town_name'].'/',$shozaichimeisho)){echo '<option value="' . $meta["town_name"]. '">' .$meta["town_name"] .  '</option>';}

	}
	
	die();
}

//町名検索表示
function fudo_townlist_selectbox(){
			echo '<select name="town" id="town">';
			echo '<option value="">町名を指定してください</option>';
			echo '</select>'

?>
	<script>
		//<![CDATA[
		ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
		jQuery(function($){
			$("#sik").change(function(){
				jQuery.ajax({
					type: 'POST',
					url: ajaxurl,
					data: {
						"action": "get_townname",
						"kenid":$("#ken").val(),
						"sikid":$("#sik").val(),
					},
					success: function(data){
						if(data != 0){
							$("#town").html(data);
						}
					},
					error: function(){
						alert('error');
					}
				});
				return false;
			});
			return false;
		});
		//]]>
	</script>
<?php
}

//メタボックスの追加
add_action('admin_menu', 'dash_town_meta_box');
function dash_town_meta_box() {
	add_meta_box('town_data', '町名データ', 'town_dashboard', 'dashboard','side');
}
function town_dashboard(){
global $wpdb;
$table_name = $wpdb->prefix . 'user_town_meta';
$table2_name = $wpdb->prefix . 'area_town_area';
$metas = $wpdb->get_results("SELECT COUNT(TA.town_id) as cnt,TA.town_name FROM $table_name as TM INNER JOIN $table2_name as TA on TM.town_area_id = TA.town_id GROUP BY TA.town_id");
echo "町内データ";
?>
<div id="townlist_table" class="table table_content">
<table>
<thead>
<tr><th style="width:100px;">登録数</th><th>町名</th></tr>
</thead>
<tbody>
<?php foreach($metas as $meta):?>
<tr>
<td><?php echo $meta->cnt;?>件</td><td><?php echo $meta->town_name;?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
</div>
<?php 
}
?>