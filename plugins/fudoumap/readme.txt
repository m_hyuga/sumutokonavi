=== fudou ===
Contributors: nendeb
Tags: fudousan,map,bukken,estate,fudou,GoogleMaps
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 1.3.5

Fudousan Map Plugin for Real Estate

== Description ==

Fudousan Map Plugin for Real Estate

== Installation ==

Installing the plugin:
1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `fudoumap` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.

== Requirements ==

* WordPress 3.5 or later
* PHP 5.2 or later (NOT support PHP 4.x!!)


== Credits ==

This plugin uses GoogleMaps API.

== Upgrade Notice ==

The required WordPress version has been changed and now requires WordPress 3.5 or higher

== Frequently Asked Questions ==

Use these support channels appropriately.

1. [Docs](http://nendeb.jp/)

== Changelog ==
= v1.3.5 =
* Add twentythirteen(WordPress 3.6beta2) Support

= v1.3.4 =
* Fixed gsearch

= v1.3.3 =
* Fixed WordPress3.5

= v1.3.2 =
* Fixed eki json.
* Add twentytwelve Support

= v1.3.0 =
* Fixed css items.

= v1.2.8 =
* Fixed jyoken items.

= v1.2.7 =
* Fixed other items.

= v1.2.5 =
* Fixed other items.

= v 1.2.1 =
* Fixed unpc template.

= v 1.2.0 =
* Added: unpc template.
* Fixed database Item.

= v 1.1.4 =
* Added: kaiin2 Item.

= v 1.1.2 =
* Fixed: bukken Item.

= v 1.1.0 =
* Fixed: SSL MODE .

= v 1.0.9 =
* Fixed: thumbnail image .

= v1.0.7 =
* Fixed: search Item .

= v1.0.5 =
* Added: Member Item.

= v1.0.4 =
* Fixed: search Item .

= v1.0.3 =
* Fixed: search Item .

= v1.0.2 =
* Fixed: header Item .

= v1.0.1 =
* Fixed: myfunction Item .

= v1.0.0 =
* Initial version of the plugin

