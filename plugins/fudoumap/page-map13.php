<?php
/**
 * The Template for displaying fudou page posts.
 *
 * Template Name: page-map13.php
 * 
 * @package WordPress
 * @subpackage Fudousan Plugin
 * Fudousan Map Plugin
 * Version: 1.3.5
 */


	//CSS追加
	function add_post_type_template_map2() {
		if ( !is_multisite() ) {
			echo '<link rel="stylesheet" type="text/css" media="all" href="'.WP_PLUGIN_URL.'/fudoumap/gmap213.css" />';
		}
	}
	add_action('wp_head', 'add_post_type_template_map2');


	//半角数字チェック   
	if (!function_exists('myIsNum_m')) {
		function myIsNum_m($value) {
			if (preg_match("/^[0-9]+$/", $value)) {
				return $value;
			}
			return '';
		}
	}

	global $wpdb;


		//条件表示 0 無し
		$int_jyo = isset($_GET['jyo']) ? $_GET['jyo'] : '';
		if($int_jyo == '') $int_jyo = '1';

		//初期値
		$int_zoom = isset($_GET['zom']) ? myIsNum_m($_GET['zom']) : '';

		$int_lat = isset($_GET['lat']) ? $_GET['lat'] : '';		//座標初期値
		$int_lng = isset($_GET['lng']) ? $_GET['lng'] : '';		//座標初期値

		$bukken_shubetsu = isset($_GET['shu']) ? $_GET['shu'] : '';	//種別
		$madori_id = isset($_GET['mad']) ? $_GET['mad'] : '';		//間取り
		$set_id = isset($_GET['set']) ? $_GET['set'] : '';		//設備

		$ros_id = isset($_GET['ros']) ? myIsNum_m($_GET['ros']) : '';	//路線
		$eki_id = isset($_GET['eki']) ? myIsNum_m($_GET['eki']) : '';	//駅
		$ken_id = isset($_GET['ken']) ? myIsNum_m($_GET['ken']) : '';	//県
		$sik_id = isset($_GET['sik']) ? myIsNum_m($_GET['sik']) : '';	//市区

		$ken_id=sprintf("%02d",$ken_id);


		$kalb_data = isset($_GET['kalb']) ? myIsNum_m($_GET['kalb']) : '';	//価格下限
		$kahb_data = isset($_GET['kahb']) ? myIsNum_m($_GET['kahb']) : '';	//価格上限
		$kalc_data = isset($_GET['kalc']) ? myIsNum_m($_GET['kalc']) : '';	//賃料下限
		$kahc_data = isset($_GET['kahc']) ? myIsNum_m($_GET['kahc']) : '';	//賃料上限
		$hof_data =  isset($_GET['hof'])  ? myIsNum_m($_GET['hof'])  : '';	//歩分


		$tik_data = isset($_GET['tik']) ? myIsNum_m($_GET['tik']) : '';		//築年数
		$mel_data = isset($_GET['mel']) ? myIsNum_m($_GET['mel']) : '';		//面積下限
		$meh_data = isset($_GET['meh']) ? myIsNum_m($_GET['meh']) : '';		//面積上限

		$geocode =  isset($_GET['geo']) ? myIsNum_m($_GET['geo']) : '';		//地域・駅(text)
	//	if( $geocode != '' )
	//		$geocode =mb_convert_encoding($geocode, 'UTF-8', 'SJIS') ;

		$rimaw_data = isset($_GET['rim']) ? $_GET['rim'] : ''; 	//表面利回り

		//url生成

		//間取り
		$madori_url = '';
		if(!empty($madori_id)) {
			$i=0;
			foreach($madori_id as $meta_box){
				$madori_url .= '&mad[]='.$madori_id[$i];
				$i++;
			}
		}


		//設備条件
		$setsubi_url = '';
		if(!empty($set_id)) {
			$i=0;
			foreach($set_id as $meta_box){
				$setsubi_url .= '&set[]='.$set_id[$i];
				$i++;
			}
		}

		$add_url  = '';

		//複数種別
		if (is_array($bukken_shubetsu)) {
			$i=0;
			foreach($bukken_shubetsu as $meta_set){
				$add_url  .= '&shu[]='.$bukken_shubetsu[$i];
				$i++;
			}

		} else {
			$add_url  .= '&shu='.$bukken_shubetsu;
		}


		$add_url .= '&ros='.$ros_id;
		$add_url .= '&eki='.$eki_id;
		$add_url .= '&ken='.$ken_id;
		$add_url .= '&sik='.$sik_id;
		$add_url .= '&kalc='.$kalc_data;
		$add_url .= '&kahc='.$kahc_data;
		$add_url .= '&kalb='.$kalb_data;
		$add_url .= '&kahb='.$kahb_data;

		$add_url .= '&rim='.$rimaw_data;

		$add_url .= '&hof='.$hof_data;
		$add_url .= $madori_url;
		$add_url .= '&tik='.$tik_data;
		$add_url .= '&mel='.$mel_data;
		$add_url .= '&meh='.$meh_data;
		$add_url .= $setsubi_url;




		if( $ros_id > 0 && $eki_id > 0){


			$sql  = "SELECT TA.lat , TA.lng ";
			$sql .=  " FROM ".$wpdb->prefix."train_latlng AS TA";
			$sql .=  " WHERE TA.rosen_id = ".$ros_id." AND TA.station_id = ".$eki_id." ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_row( $sql );

			if($metas){
				$int_lat = $metas->lat;
				$int_lng = $metas->lng;
			}else{

				$sql  = "SELECT TR.rosen_name, TS.station_name , MA.middle_area_name ";
				$sql .= "FROM (".$wpdb->prefix."train_rosen AS TR ";
				$sql .= "INNER JOIN ".$wpdb->prefix."train_station AS TS ON TR.rosen_id = TS.rosen_id) ";
				$sql .= "INNER JOIN ".$wpdb->prefix."area_middle_area AS MA ON TS.middle_area_id = MA.middle_area_id ";
				$sql .= "WHERE TR.rosen_id=".$ros_id." AND TS.station_id=".$eki_id." ";

				$sql = $wpdb->prepare($sql,'');
				$metas = $wpdb->get_row( $sql );

				if($metas){
					$geocode = $metas->middle_area_name . ' ';
				//	$geocode = $metas->rosen_name . ' ';
					$geocode .= $metas->station_name . '駅';
				}

			}

			if($int_zoom == '' )
				$int_zoom = '15';

		}


		if( $ken_id > 0 && $sik_id > 0){

			$sql  = "SELECT MA.middle_area_name, NA.narrow_area_name ";
			$sql .= "FROM ".$wpdb->prefix."area_middle_area AS MA ";
			$sql .= "INNER JOIN ".$wpdb->prefix."area_narrow_area AS NA ON MA.middle_area_id = NA.middle_area_id ";
			$sql .= "WHERE MA.middle_area_id=".$ken_id." AND NA.narrow_area_id=".$sik_id." ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_row( $sql );
			$geocode = $metas->middle_area_name;
			$geocode .= $metas->narrow_area_name;

			if($int_zoom == '' )
				$int_zoom = '15';
		}



		if($int_zoom == '' )
			$int_zoom = get_option('fudoumap_zoom');
		if($int_zoom == '' )
			$int_zoom = '15';



		//座標初期値
		if($int_lat == '' || $int_lng == '' ){
			$int_lat = get_option('fudoumap_lat');
			$int_lng = get_option('fudoumap_lng');
		}

		if($int_lat == '' || $int_lng == '' ){
			$int_lat = '35.681323';	
			$int_lng = '139.767650';
		}


		//ヘッダ
		$fudoumap_title = get_option('fudoumap_title');
		$fudoumap_keywords = get_option('fudoumap_keywords');
		$fudoumap_description = get_option('fudoumap_description');


		$web_property_id = get_option('web_property_id');


		$tenpo_form = get_option('tenpo_form');
		$tenpo_form = str_replace(array("\r\n","\r","\n"), '', $tenpo_form);
		$tenpo_lat = get_option('tenpo_lat');
		$tenpo_lng = get_option('tenpo_lng');
		$tenpo_img = get_option('tenpo_img');

		get_header(); 

		wp_deregister_script('googlemapapi');
		wp_enqueue_script('googlemapapi', 'http://maps.googleapis.com/maps/api/js?&sensor=false');
?>
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">
				<div id="map_canvas"></div>
				<div id="map_copy"> </div>
				<div id="map_adv"> </div>

			</div><!-- .#content -->
		</div><!-- .#primary -->


		<div id="tertiary" class="sidebar-container" role="complementary">
			<div class="sidebar-inner">
			<div class="widget-area">
			<aside id="fudo_inmap" class="widget">
				<div id="map_menu">
					<div id="map_search"><?php require_once WP_PLUGIN_DIR . '/fudoumap/gsearch2.php'; ?></div>
				</div>
			</aside>
			</div>
			</div><!-- .sidebar-inner -->
		</div><!-- #tertiary -->






		<script type="text/javascript">
		<!-- <![CDATA[
			window.onload=function(){ initialize();}

			var tenpo_form = '<?php echo $tenpo_form;?>';
			var tenpo_lat =  '<?php echo $tenpo_lat;?>';
			var tenpo_lng =  '<?php echo $tenpo_lng;?>';
			var tenpo_img =  '<?php echo $tenpo_img;?>';
			var address = "<?php echo $geocode;?>";
			var int_lat = '<?php echo $int_lat;?>';
			var int_lng = '<?php echo $int_lng;?>';
			var int_zoom = <?php echo $int_zoom;?>;
			var add_url = '<?php echo $add_url;?>';
			var int_jyo= <?php echo $int_jyo;?>;
			var getsite='<?php echo WP_PLUGIN_URL;?>/fudoumap/';
			function setTenpomarker2() {}

		// ]]> -->
		</script>
		<script type="text/javascript" src="<?php echo WP_PLUGIN_URL;?>/fudoumap/js/util.js"></script>
		<script type="text/javascript" src="<?php echo WP_PLUGIN_URL;?>/fudoumap/js/gmap2b_121min.js"></script>
		<script type="text/javascript" src="<?php echo WP_PLUGIN_URL;?>/fudoumap/js/jmapsearch.js"></script>

		<!-- //in_map12 -->

		<?php get_footer(); ?>

