<?php
/*
Plugin Name: Fudousan Map Plugin
Plugin URI: http://nendeb.jp/
Description: Fudousan Map Plugin for Real Estate
Version: 1.3.5
Author: nendeb
Author URI: http://nendeb.jp/
*/

// Define current version constant
define( 'FUDOU_M_VERSION', '1.3.5' );

/*  Copyright 2013 nendeb (email : nendeb@gmail.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


// Define 
if (!defined('FUDOU_MAP'))
	define( 'FUDOU_MAP', true );

if (!defined('WP_CONTENT_URL'))
      define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');
if (!defined('WP_CONTENT_DIR'))
      define('WP_CONTENT_DIR', ABSPATH.'wp-content');
if (!defined('WP_PLUGIN_URL'))
      define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');
if (!defined('WP_PLUGIN_DIR'))
      define('WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins');


//データーベース設定
register_activation_hook(__FILE__,'init_data_tables_fudoumap');
function init_data_tables_fudoumap() {
	include_once( dirname(__FILE__).'/data/fudoumap-configdatabase.php');
	databaseinstallation_fudoumap(0);
	init_filecopy_fudo();
}
function init_filecopy_fudo() {
	$mapfile = 'fmap.php';
	if( file_exists( ABSPATH  ."/" .$mapfile ) === false ) {
		if (!copy(WP_PLUGIN_DIR . "/fudoumap/" . $mapfile ,  ABSPATH  ."/" .$mapfile)) {
		}
	}
}


//データーベースチェック
add_action('admin_init', 'map_databaseinstallation_warnings');
function map_databaseinstallation_warnings() {
	global $wpdb;

	$table_name = $wpdb->prefix . "train_latlng";

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
		function databaseinstallation_notices() {
			echo '<div class="error" style="text-align: center;"><p>データーベースを登録できませんでした。サーバーに問題があるのかも知れません。[Fudousan GoogleMaps Plugin]</p></div>';
		}
		add_action('admin_notices', 'databaseinstallation_notices');
	}
}


//不動産プラグインチェック
add_action('init', 'fudou_active_plugins_check_map');
function fudou_active_plugins_check_map(){
	global $is_fudouktai,$is_fudoumap,$is_fudoukaiin,$is_fudoumail,$is_fudourains,$is_fudoucsv;
	$fudo_active_plugins = get_option('active_plugins');
	if(is_array($fudo_active_plugins)) {
		foreach($fudo_active_plugins as $meta_box){
			if( $meta_box == 'fudouktai/fudouktai.php') $is_fudouktai=true;
			if( $meta_box == 'fudoumap/fudoumap.php') $is_fudoumap=true;
			if( $meta_box == 'fudoukaiin/fudoukaiin.php') $is_fudoukaiin=true;
			if( $meta_box == 'fudoumail/fudoumail.php') $is_fudoumail=true;
			if( $meta_box == 'fudourains/fudourains.php') $is_fudourains=true;
			if( $meta_box == 'fudoucsv/fudoucsv.php') $is_fudoucsv=true;
		}
	}
}


//ページテンプレート切替
add_filter('template_include', 'get_post_type_template_map');
function get_post_type_template_map($template = '') {
	if ( is_front_page() ) {
    		if( isset( $_GET['page'] ) && $_GET['page'] == 'map'){

		if ( function_exists('wp_get_theme') ) {
			$theme_ob = wp_get_theme();
			$template_name = $theme_ob->template;
		}else{
			$template_name = get_option('template');
		}

		switch ( $template_name ) {
				case "twentythirteen" :
					//twentytwelve 
					$template = locate_template(array('../../plugins/fudoumap/page-map13.php', 'index.php'));
					break;
				case "twentytwelve" :
					//twentytwelve 
					$template = locate_template(array('../../plugins/fudoumap/page-map12.php', 'index.php'));
					break;
				case "twentyeleven" :
					//twentyeleven 
					$template = locate_template(array('../../plugins/fudoumap/page-map11.php', 'index.php'));
					break;
				case "twentyten" :
					//twentyeleven 
					$template = locate_template(array('../../plugins/fudoumap/page-map.php', 'index.php'));
					break;
				default:
					//他
					$template = locate_template(array('../../plugins/fudoumap/page-map_unpc.php', 'index.php'));
					break;
			}
		}
	}
	return $template;
}


//thickbox
function fudou_map_active_thickbox(){
//	if ( is_front_page() ) {
    		if( isset( $_GET['page'] ) && $_GET['page'] == 'map'){
			if (function_exists('add_thickbox')) add_thickbox();
		}
//	}
}
add_action('init', 'fudou_map_active_thickbox');




//プラグイン基本設定
add_action('admin_menu', 'fudoumap_admin_menu');
function fudoumap_admin_menu() {
	require_once ABSPATH . '/wp-admin/admin.php';
	$plugin = new FudouMapPlugin;
	add_management_page('edit.php', '不動産マップ設定', 'edit_pages', __FILE__, array($plugin, 'form'));
}

class FudouMapPlugin {

	function process_option($name, $default, $params) {
		if (array_key_exists($name, $params)) {
			$value = stripslashes($params[$name]);
		} elseif (array_key_exists('_'.$name, $params)) {
			// unchecked checkbox value
			$value = stripslashes($params['_'.$name]);
		} else {
			$value = null;
		}
		$stored_value = get_option($name);
		if ($value == null) {
			if ($stored_value === false) {
				if (is_callable($default) && 
					method_exists($default[0], $default[1])) {
					$value = call_user_func($default);
				} else {
					$value = $default;
				}
				add_option($name, $value);
				} else {
					$value = $stored_value;
				}
			} else {
			if ($stored_value === false) {
				add_option($name, $value);
			} elseif ($stored_value != $value) {
				update_option($name, $value);
			}
		}
		return $value;
	}

	function process_option2($name, $default, $params) {
		$value = stripslashes($params[$name]);
		$stored_value = get_option($name);

			if ($stored_value === false) {
				add_option($name, $value);
			} else {
				update_option($name, $value);
			}

		return $value;
	}




	//プラグイン設定フォーム
	function form() {
		global $post;
		global $wpdb;

		$shozaichiken_data = '';
		$result1 = '';

		if( isset($_POST['fudoumap']) && $_POST['fudoumap'] == 'init'){
		        $fudoumap_lat = $this->process_option2('fudoumap_lat','', $_POST);
		        $fudoumap_lng = $this->process_option2('fudoumap_lng','', $_POST);
		        $fudoumap_zoom = $this->process_option2('fudoumap_zoom','', $_POST);
		}

		if( isset($_POST['fudoumap']) && $_POST['fudoumap'] == 'title'){
		        $fudoumap_title = $this->process_option2('fudoumap_title','', $_POST);
		        $fudoumap_keywords = $this->process_option2('fudoumap_keywords','', $_POST);
		        $fudoumap_description = $this->process_option2('fudoumap_description','', $_POST);
		}

		if( isset($_POST['fudoumap']) && $_POST['fudoumap'] == 'tenpo'){
		        $tenpo_form = $this->process_option2('tenpo_form','', $_POST);
		        $tenpo_lat = $this->process_option2('tenpo_lat','', $_POST);
		        $tenpo_lng = $this->process_option2('tenpo_lng','', $_POST);
		        $tenpo_img = $this->process_option2('tenpo_img','', $_POST);
		}

		$fudoumap_lat = get_option('fudoumap_lat');
		$fudoumap_lng = get_option('fudoumap_lng');
		$fudoumap_zoom = get_option('fudoumap_zoom');
		$fudoumap_title = get_option('fudoumap_title');
		$fudoumap_keywords = get_option('fudoumap_keywords');
		$fudoumap_description = get_option('fudoumap_description');


		$tenpo_form = get_option('tenpo_form');
		$tenpo_lat = get_option('tenpo_lat');
		$tenpo_lng = get_option('tenpo_lng');
		$tenpo_img = get_option('tenpo_img');


		if(empty($fudoumap_zoom)) $fudoumap_zoom = 15;

		$ros = isset($_POST['koutsurosen1']) ? $_POST['koutsurosen1'] : '';
		$eki = isset($_POST['koutsueki1']) ? $_POST['koutsueki1'] : '';
		$lat = isset($_POST['bukkenido']) ? $_POST['bukkenido'] : '';
		$lng = isset($_POST['bukkenkeido']) ? $_POST['bukkenkeido'] : '';


		//GeoCode 代替座標登録
		if($ros != '' && $eki != '' && $lat != '' &&  $lng  != '' ){ 

			$sql  = "SELECT COUNT(rosen_id) AS CO ";
			$sql .=  " FROM ".$wpdb->prefix."train_latlng AS TA";
			$sql .=  " WHERE TA.rosen_id = ".$ros." AND TA.station_id = ".$eki." ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_row( $sql );
			$co = $metas->CO ;

			if ($co == 0 ){
				$sql = "INSERT INTO ".$wpdb->prefix."train_latlng (`rosen_id`, `station_id`, `lat`, `lng`) VALUES ";
				$sql.= " (".$ros.",".$eki.",'".$lat."','".$lng."')";
				$sql = $wpdb->prepare($sql,'');
				$wpdb->query( $sql );

			}else{
				$sql  = "UPDATE ".$wpdb->prefix."train_latlng SET lat='".$lat."' , lng = '".$lng."' ";
				$sql .= " WHERE rosen_id = ".$ros." AND station_id = ".$eki." ";

				$sql = $wpdb->prepare($sql,'');
				$wpdb->query( $sql );
			}

			//登録チェック

			$sql  =  "SELECT TR.rosen_name, TS.station_name, TA.lat, TA.lng";
			$sql .=  " FROM (".$wpdb->prefix."train_latlng AS TA ";
			$sql .=  " INNER JOIN ".$wpdb->prefix."train_rosen AS TR ON TA.rosen_id = TR.rosen_id) ";
			$sql .=  " INNER JOIN ".$wpdb->prefix."train_station AS TS ON (TA.rosen_id = TS.rosen_id) AND (TA.station_id = TS.station_id)";
			$sql .=  " WHERE TA.rosen_id = ".$ros." AND TA.station_id = ".$eki." ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_row( $sql );

			if(!empty($metas)) {
				$rosen_name = $metas->rosen_name ;
				$station_name = $metas->station_name ;
				$lat = $metas->lat ;
				$lng = $metas->lng ;
				$result1 = '　('. $rosen_name.$station_name.'　緯度:'.$lat.' 経度:'.$lng .')';
			}

		}

			$maps_url = WP_PLUGIN_URL . "/fudoumap/googlemaps/GoogleMaps2.php";




	if ( is_multisite() ) {
			echo '<div class="error" style="text-align: center;"><p>マルチサイトでは利用できません。</p></div>';
	}else{
?>
		<div class="wrap">
			<div id="icon-tools" class="icon32"><br /></div>
			<h2>不動産マップ 基本設定</h2>
			<div id="poststuff">

			<div id="post-body">
			<div id="post-body-content">

				<?php if ( !empty($result1) || isset($_POST['fudoumap_lat']) || isset($_POST['fudoumap_title']) || isset($_POST['tenpo_lat'])  ) : ?>
				<div id="message" class="updated fade"><p><strong><?php _e('Options saved.') ?><?php echo $result1;?></strong></p></div>
				<?php endif; ?> 
				<br />

			        <h3>サイト</h3>
				　　公開サイト インマップ版(サイト内版) <a href="<?php echo home_url() . '/?page=map'; ?>" target="_blank"><?php echo home_url() . '/?page=map'; ?></a><br />
				　　公開サイト フルマップ版 <a href="<?php echo site_url() . '/fmap.php'; ?>" target="_blank"><?php echo site_url() . '/fmap.php'; ?></a><br />
				　　公開サイト フルマップ版(条件検索無し) <a href="<?php echo site_url() . '/fmap.php?jyo=0'; ?>" target="_blank"><?php echo site_url() . '/fmap.php?jyo=0'; ?></a><br />
				　　公開サイト フルマップ版(条件検索無し [トップ・ヘルプ]ボタン無し) <a href="<?php echo site_url() . '/fmap.php?jyo2=0'; ?>" target="_blank"><?php echo site_url(). '/fmap.php?jyo2=0'; ?></a><br />
				　　*表示しない場合は「fmap.phpファイル」をサイトルートへコピーしてください。

				<br />
				<br />

			        <h3>タイトル</h3>
				<div id="postdivrich" class="postarea">
				<form class="add:the-list: validate" method="post" name="postseo">
				<input type="hidden" name="fudoumap" value="title" />
				　　title <input type="text" name="fudoumap_title" value="<?php echo $fudoumap_title;?>" size="50" /><br />
				　　keywords <input type="text" name="fudoumap_keywords" value="<?php echo $fudoumap_keywords;?>" size="50" /> ( <font color="#ff0000">,</font> 区切り)<br />
				　　description <input type="text" name="fudoumap_description" value="<?php echo $fudoumap_description;?>" size="50" /><br />
				<p class="submit" style="padding:0 15px;"><input type="submit" name="submit" id="submit" class="button-primary" value="変更を保存"  /></p>
				</form>
				</div>
				<br />
				<br />


			        <h3>初期位置</h3>
				<div id="postdivrich" class="postarea">
				<form class="add:the-list: validate" method="post" name="fudoumap">
				<input type="hidden" name="fudoumap" value="init" />
				　　最初にMAPを開いた時に表示させる位置設定です。<br />
				　　緯度 <input type="text" name="fudoumap_lat" value="<?php echo $fudoumap_lat;?>" size="25" />
				　　経度 <input type="text" name="fudoumap_lng" value="<?php echo $fudoumap_lng;?>" size="25" />

				<input type="button" value="座標設定" onclick="openSubWindow( '<?php echo $maps_url;?>?lat=<?php echo $fudoumap_lat;?>&lng=<?php echo $fudoumap_lng;?>&t=1');"><br />

				　　ズーム レベル <select name="fudoumap_zoom" id="fudoumap_zoom">
					<option value="12"<?php if($fudoumap_zoom == 12 ) echo ' selected="selected"'?>>12</option>
					<option value="13"<?php if($fudoumap_zoom == 13 ) echo ' selected="selected"'?>>13</option>
					<option value="14"<?php if($fudoumap_zoom == 14 ) echo ' selected="selected"'?>>14</option>
					<option value="15"<?php if($fudoumap_zoom == 15 ) echo ' selected="selected"'?>>15</option>
					<option value="16"<?php if($fudoumap_zoom == 16 ) echo ' selected="selected"'?>>16</option>
					<option value="17"<?php if($fudoumap_zoom == 17 ) echo ' selected="selected"'?>>17</option>
					<option value="18"<?php if($fudoumap_zoom == 18 ) echo ' selected="selected"'?>>18</option>
				</select>


				　<p class="submit" style="padding:0 15px;"><input type="submit" name="submit" id="submit" class="button-primary" value="座標保存"  /></p>
				</form>
				</div>

				<br />
				<br />


			        <h3>店舗</h3>
				<div id="postdivrich" class="postarea">
				<form class="add:the-list: validate" method="post" name="posttenpo">
				<input type="hidden" name="fudoumap" value="tenpo" />

				　　緯度 <input type="text" name="tenpo_lat" value="<?php echo $tenpo_lat;?>" size="25" />　
				　　経度 <input type="text" name="tenpo_lng" value="<?php echo $tenpo_lng;?>" size="25" /> 
				<input type="button" value="座標設定" onclick="openSubWindow( '<?php echo $maps_url;?>?lat=<?php echo $tenpo_lat;?>&lng=<?php echo $tenpo_lng;?>&t=3');"><br />
				<br />


				<table border="0" style="padding:0 15px;">
				<tr>
					<td>
				　　ふきだしの内容<br />
				　　<textarea rows='10' cols='40' name='tenpo_form' tabindex='2' id='fudo_form'><?php echo $tenpo_form; ?></textarea>
					</td>
					<td>

				　　店舗マーカー<br />
				<table border="0" style="padding:0 15px;">
				<tr>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp01.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp01.png') echo 'checked="checked"';?> value="gmap_sp01.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp02.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp02.png') echo 'checked="checked"';?> value="gmap_sp02.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp03.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp03.png') echo 'checked="checked"';?> value="gmap_sp03.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp04.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp04.png') echo 'checked="checked"';?> value="gmap_sp04.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp05.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp05.png') echo 'checked="checked"';?> value="gmap_sp05.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp06.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp06.png') echo 'checked="checked"';?> value="gmap_sp06.png" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp07.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp07.png') echo 'checked="checked"';?> value="gmap_sp07.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp08.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp08.png') echo 'checked="checked"';?> value="gmap_sp08.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp09.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp09.png') echo 'checked="checked"';?> value="gmap_sp09.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp10.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp10.png') echo 'checked="checked"';?> value="gmap_sp10.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp11.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp11.png') echo 'checked="checked"';?> value="gmap_sp11.png" /></td>
					<td width="51" align="center"><img src="../wp-content/plugins/fudoumap/img/gmap_sp12.png" /><br /><input type="radio" name="tenpo_img" id="radio" <?php if($tenpo_img == 'gmap_sp12.png') echo 'checked="checked"';?> value="gmap_sp12.png" /></td>
				</tr>
				</table>

					</td>
				</tr>
				</table>

				　<p class="submit" style="padding:0 15px;"><input type="submit" name="submit" id="submit" class="button-primary" value="店舗保存"  /></p>
				</form>
				</div>




				<br />
				<br />



				<h3>駅座標</h3>
				<div id="postdivrich" class="postarea">
				<form class="add:the-list: validate" method="post" name="post">
				<input type="hidden" name="fudoumap" value="eki" />
				　　路線駅へのマップ移動は「GeoCode」を利用しています。<br />
				　　「GeoCode」で移動できない、違う場所に移動してしまう 場合は、こちらで座標を登録をしてください。<br /><br />
	<?php
				//営業県
				if($shozaichiken_data == ""){
					$shozaichiken_data = '0';
					for( $i=1; $i<48 ; $i++ ){
						if( get_option('ken'.$i) != ''){
							$shozaichiken_data .= ','.get_option('ken'.$i);
						}
					}
				}


				if(!empty($shozaichiken_data)){ 

					$sql = "SELECT DISTINCT DTR.rosen_id, DTR.rosen_name";
					$sql = $sql . " FROM ".$wpdb->prefix."train_area_rosen AS DTAR";
					$sql = $sql . " INNER JOIN ".$wpdb->prefix."train_rosen AS DTR ON DTAR.rosen_id = DTR.rosen_id";
					$sql = $sql . " WHERE DTAR.middle_area_id in (".$shozaichiken_data.") ";
					$sql = $sql . " ORDER BY DTR.rosen_name";


					$sql = $wpdb->prepare($sql,'');
					$metas = $wpdb->get_results( $sql, ARRAY_A );
					if(!empty($metas)) {
						echo '　　路線<select name="koutsurosen1" id="koutsurosen1" onchange="SEki(this)">';
						echo '<option value="">路線を選択してください</option>';
						foreach ( $metas as $meta ) {
							$meta_id = $meta['rosen_id'];
							$meta_valu = $meta['rosen_name'];

							echo '<option value="'.$meta_id.'"';
								
						//	if($koutsurosen_data==$meta_id){ 
						//	 	 echo ' selected="selected"';
						//	}
							echo '>'.$meta_valu.'</option>';
						}
						echo '</select>';
					}

				}

				echo '　駅<select name="koutsueki1" id="koutsueki1" onchange="consent_check()">';
				echo '<option value="0">駅を選択してください</option>';
				echo '</select>';

				$maps_url = WP_PLUGIN_URL . "/fudoumap/googlemaps/GoogleMaps2.php";

	?>
				<input type="button" value="座標設定" name="btn" onclick="openSubWindow( '<?php echo $maps_url;?>?ros=' + document.post.koutsurosen1.options[document.post.koutsurosen1.selectedIndex].value  + '&eki=' + document.post.koutsueki1.options[document.post.koutsueki1.selectedIndex].value );">
				<br />
				　　緯度 <input type="text" name="bukkenido" value="" size="25" />　
				　　経度 <input type="text" name="bukkenkeido" value="" size="25" />
				　　<p class="submit" style="padding:0 15px;"><input type="submit" name="submit" id="submit" class="button-primary" value="座標保存"  /></p>


				<style type="text/css">
				<!--
				table#rosenekimap {
					margin: 0px 0px 0px 15px;
					border-top: 1px solid #999;
					border-left: 1px solid #999;
				}

				#rosenekimap td {
					padding: 5px;
					margin: 0px;
					background: #FFF;
					border-bottom: 1px solid #999;
					border-right: 1px solid #999;
					text-align: left;
					vertical-align: top;
				}

				#rosenekimap th {
					margin: 0;
					padding: 5px 0px 0px 5px;
					border-bottom: 1px solid #999;
					color: #666;
					text-align: left;
					vertical-align: top;
					border-right: 1px solid #999;
				}
				-->
				</style>

	<?php

				$sql  =  "SELECT TR.rosen_name, TS.station_name, TA.lat, TA.lng";
				$sql .=  " FROM (".$wpdb->prefix."train_latlng AS TA ";
				$sql .=  " INNER JOIN ".$wpdb->prefix."train_rosen AS TR ON TA.rosen_id = TR.rosen_id) ";
				$sql .=  " INNER JOIN ".$wpdb->prefix."train_station AS TS ON (TA.rosen_id = TS.rosen_id) AND (TA.station_id = TS.station_id)";
				$sql .=  " ORDER BY TR.rosen_name";

				$sql = $wpdb->prepare($sql,'');
				$metas = $wpdb->get_results( $sql, ARRAY_A );

				if(!empty($metas)) {
					echo '　　登録済み座標';
					echo '<table border="0" cellpadding="0" cellspacing="0" id="rosenekimap">';
						echo '<tr>';
						echo '<th>路線名</th>' ;
						echo '<th>駅名</th>' ;
						echo '<th>緯度</th>' ;
						echo '<th>経度</th>' ;
						echo '</tr>';
					foreach ( $metas as $meta ) {
						echo '<tr>';
						echo '<td>' . $meta['rosen_name'] . '</td>' ;
						echo '<td>' . $meta['station_name'] . '</td>' ;
						echo '<td>' . $meta['lat'] . '</td>' ;
						echo '<td>' . $meta['lng'] . '</td>' ;
						echo '</tr>';
					}
					echo '</table>';
				}
				echo '　　修正は同じ路線・駅で座標保存をしてください。';

	?>

				</form>
				</div>



<SCRIPT language="JavaScript">
// <![CDATA[
var request;
var getsite="<?php echo WP_PLUGIN_URL;?>/fudou/json/";

consent_check();

function SEki(slct) {

	var syoki1="路線を選択してください";
	var syoki2="駅を選択してください";

	var postDat = encodeURI("shozaichiken=<?php echo $shozaichiken_data; ?>") + encodeURI("&koutsurosen="+document.post.koutsurosen1.options[slct.selectedIndex].value);
	request = new XMLHttpRequest();
	request.open("POST", getsite+"jsoneki.php", true);
	request.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
	request.send(postDat);
	request.onreadystatechange = function() {
	if (request.readyState == 4 && request.status == 200) {
        var id = null;
        var name = null;
        var val = null;
        ekicodecrea();
        var jsDat = request.responseText;
        var data = eval("("+jsDat+")");
	if (data.eki.length>0) {
		document.post.koutsueki1.options[0]=new Option(syoki2,"0",false,false);
	}else{
		document.post.koutsueki1.options[0]=new Option(syoki1,"0",false,false);
	}
        for(var i=0; i<data.eki.length; i++) {
          id = data.eki[i].id;
          name = data.eki[i].name;
	  val = false;
          document.post.koutsueki1.options[i+1] = new Option(name,id,false,val);
        }
      }
    }
}
function ekicodecrea(){
    var cnt = document.post.koutsueki1.length;
    for(var i=cnt; i>=0; i--) {
      document.post.koutsueki1.options[i] = null;
    }
}

function update(obj,obj2) {
	window.document.post.bukkenido.value = obj.album;
	window.document.post.bukkenkeido.value = obj2.album2;
}

function update2(obj,obj2) {
	window.document.fudoumap.fudoumap_lat.value = obj.album;
	window.document.fudoumap.fudoumap_lng.value = obj2.album2;
}

function update3(obj,obj2) {
	window.document.posttenpo.tenpo_lat.value = obj.album;
	window.document.posttenpo.tenpo_lng.value = obj2.album2;
}




function openSubWindow(url){
	var sub = window.open(url,'','width=650,height=550,status=0,location=0,resizable=0,scrollbars=1,toolbar=0');
	sub.focus();
}

function closeWithoutAlert(){
	if( document.all ){
		window.opener = true;
	}
	window.close();
}

//ボタン
function consent_check() {
	if (document.post.koutsueki1.options[document.post.koutsueki1.selectedIndex].value == '0')
		document.post.btn.disabled = true;
	else 
		document.post.btn.disabled = false;
}


// ]]>
</SCRIPT>


			<br />



		</div>
		</div>



	</div>

<?php
	}
	}
}

//ヘッダ埋め込みマップ
function header_fudoumap($post_id) {

	$head_map_width = intval(HEADER_IMAGE_WIDTH)+0;		//マップ幅
	$head_map_height = intval(HEADER_IMAGE_HEIGHT)+80;	//マップ高さ

	$map_url_m = site_url();
	$map_url_m .= "/fmap.php?jyo=0&amp;jyo2=0&amp;shu=" . $_GET['shu'];


	$map_url = '';
	if($_GET['bukken'] == "station"){
		$map_url .= "&amp;ros=" . $_GET['mid'];
		$map_url .= "&amp;eki=" . $_GET['nor'];
	}else{
		$map_url .= "&amp;ros=" . $_GET['ros'];
		$map_url .= "&amp;eki=" . $_GET['eki'];
	}
	if($_GET['bukken'] == "shiku"){
		$map_url .= "&amp;ken=" . $_GET['mid'];
		$map_url .= "&amp;sik=" . $_GET['nor'];
	}else{
		$map_url .= "&amp;ken=" . $_GET['ken'];
		$map_url .= "&amp;sik=" . $_GET['sik'];
	}

	$map_url .= "&amp;kalc=" . $_GET['kalc'];
	$map_url .= "&amp;kahc=" . $_GET['kahc'];
	$map_url .= "&amp;kalb=" . $_GET['kalb'];
	$map_url .= "&amp;kahb=" . $_GET['kahb'];
	$map_url .= "&amp;hof=" . $_GET['hof'];
	$map_url .= "&amp;tik=" . $_GET['tik'];
	$map_url .= "&amp;mel=" . $_GET['mel'];
	$map_url .= "&amp;meh=" . $_GET['meh'];


	$map_url = $map_url_m . $map_url ;

	//シングルページ
	if (is_single() && $post_id != ''){
		$bukkenido = get_post_meta($post_id,'bukkenido',true);
		$bukkenkeido = get_post_meta($post_id,'bukkenkeido',true);

		if( $bukkenido != '' && $bukkenkeido != '') {
			$map_url = $map_url_m . "&amp;lat=$bukkenido&amp;lng=$bukkenkeido" ;
		}
	}

	if ( ( $post_id =='' && $_GET['shu'] !='' && ( $_GET['nor'] !='' || $_GET['eki'] !='' || $_GET['sik'] !='') )  || ( $post_id !='' && !is_home() && $bukkenido !='' && $bukkenkeido !='' )  ) {
		echo '<iframe src="'.$map_url.'" width="' .$head_map_width. '" height="' .$head_map_height.'" frameborder="0"></iframe>';
	}else{
		echo '<img src="' . get_header_image() . '" width="' .  HEADER_IMAGE_WIDTH . '" height="' . HEADER_IMAGE_HEIGHT . '" alt="" />';
	}

}

//アーカイブ埋め込みマップ
function archives_fudoumap() {

	$head_map_width = '700';	//マップ幅
	$head_map_height = '400';	//マップ高さ

	$map_url_m = site_url();
	$map_url_m .= "/fmap.php?jyo=0&amp;jyo2=0&amp;shu=" . $_GET['shu'];

	$map_url = '';
	if($_GET['bukken'] == "station"){
		$map_url .= "&amp;ros=" . $_GET['mid'];
		$map_url .= "&amp;eki=" . $_GET['nor'];
	}else{
		$map_url .= "&amp;ros=" . $_GET['ros'];
		$map_url .= "&amp;eki=" . $_GET['eki'];
	}
	if($_GET['bukken'] == "shiku"){
		$map_url .= "&amp;ken=" . $_GET['mid'];
		$map_url .= "&amp;sik=" . $_GET['nor'];
	}else{
		$map_url .= "&amp;ken=" . $_GET['ken'];
		$map_url .= "&amp;sik=" . $_GET['sik'];
	}

	$map_url .= "&amp;kalc=" . $_GET['kalc'];
	$map_url .= "&amp;kahc=" . $_GET['kahc'];
	$map_url .= "&amp;kalb=" . $_GET['kalb'];
	$map_url .= "&amp;kahb=" . $_GET['kahb'];
	$map_url .= "&amp;hof=" . $_GET['hof'];
	$map_url .= "&amp;tik=" . $_GET['tik'];
	$map_url .= "&amp;mel=" . $_GET['mel'];
	$map_url .= "&amp;meh=" . $_GET['meh'];
	$map_url = $map_url_m . $map_url ;


	//必要なデーターが揃うと表示
	if ( $_GET['shu'] !='' && ( $_GET['nor'] !='' || $_GET['eki'] !='' || $_GET['sik'] !='') ) {
		echo '<iframe src="'.$map_url.'" width="' .$head_map_width. '" height="' .$head_map_height.'" frameborder="0"></iframe>';
	}
}



?>
