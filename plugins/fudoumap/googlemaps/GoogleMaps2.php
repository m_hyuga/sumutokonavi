<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 * Version: 1.2.5
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require_once '../../../../wp-blog-header.php';

//$wpdb->show_errors();

	status_header( 200 );

	global $wpdb;

	$rosen_name = '';
	$eki_name = '';

	$geomode = true;

	$maptype = isset($_GET['t']) ? $_GET['t'] : '';

	$lat = isset($_GET['lat']) ? $_GET['lat'] : '';
	$lng = isset($_GET['lng']) ? $_GET['lng'] : '';

	if($lat==""){
		$lat = "35.689488";
	}else{
		$geomode = false;
	}

	if($lng==""){
		$lng = "139.691706";
	}

	$ros_id = isset($_GET['ros']) ? myIsNum($_GET['ros']) : '';
	$eki_id = isset($_GET['eki']) ? myIsNum($_GET['eki']) : '';


	if($ros_id !='' && $eki_id != '' ){
		$sql  = " SELECT TR.rosen_name, TS.station_name";
		$sql .= " FROM ".$wpdb->prefix."train_rosen AS TR";
		$sql .= " INNER JOIN ".$wpdb->prefix."train_station AS TS ON TR.rosen_id = TS.rosen_id";
		$sql .= " WHERE TR.rosen_id = ".$ros_id." AND TS.station_id = ".$eki_id." ";

		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		$rosen_name = $metas->rosen_name;
		$eki_name = $metas->station_name;
		if($eki_name != '' )
			$eki_name .= '駅';
	}



	//半角数字チェック   
	function myIsNum($value) {
		if (preg_match("/^[0-9]+$/", $value)) {
			return $value;
		}
		return '';
	}



?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title></title>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">


	var geocoder;
	var map;
	function initialize() {
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(<?php echo $lat;?>, <?php echo $lng;?>);
		var myOptions = {
		zoom: 16,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

		var centerIcon = new google.maps.Marker({
			icon: image
		});
		var image = new google.maps.MarkerImage(
			'centerMark.gif'
			, new google.maps.Size(39, 39)
			, new google.maps.Point(0,0)
			, new google.maps.Point(19,19)
		);
		var centerIcon = new google.maps.Marker({
			position: latlng,
			icon: image,
			map: map
		});


		function drawMarker(centerLocation){
			centerIcon.setPosition(centerLocation);
		}

		var centerd = map.getCenter();
		document.frm.lat.value=centerd.lat().toFixed(6); 
		document.frm.lng.value=centerd.lng().toFixed(6); 

		google.maps.event.addListener(map, 'center_changed', function(event) {
			var center = map.getCenter();
			document.frm.lat.value=center.lat().toFixed(6); 
			document.frm.lng.value=center.lng().toFixed(6); 
			drawMarker(map.getCenter());

		});

<?php if($geomode){ ?>
		codeAddress();
<?php } ?>

	}

	function codeAddress() {
	//	var address = '<?php echo $rosen_name;?> <?php echo $eki_name;?>';
		var address = document.getElementById("address").value;
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
			} else {
				alert("座標が見つかりませんでした。\n「 " + address + "」\n手動で座標を設定してください。" );
			}
		});
	}


</script>
</head>
<style>
	html { height: 100% }
	body { height: 100%; margin: 0px; padding: 0px }
	body,input{
		font-family: "メイリオ", Meiryo,"ヒラギノ角ゴ Pro W3", "Hiragino Kaku Gothic Pro",  Osaka, "ＭＳ Ｐゴシック", "MS PGothic", sans-serif;
		font-size: 12px;
	}
	
</style>
<body onload="initialize()">
  <div>
	<form name="frm">
	<input id="address" type="textbox" size="20" value="<?php echo $rosen_name;?> <?php echo $eki_name;?>">
	<input type="button" value="Geo" onclick="codeAddress()">

	緯度：<input type="text" name="lat" size="6">
	経度：<input type="text" name="lng" size="6">
	<input type="button" value="決定" onclick="notifyParent();">
	<div style="padding: 1; display:none" id="msgPreview"> </div>
	</form>
  </div>
<div id="map_canvas" style="width:100%; height:90%; z-index:1"></div>


<script type="text/javascript">
// <![CDATA[


function notifyParent(){
	var obj  = {"album"  : document.frm.lat.value };
	var obj2 = {"album2" : document.frm.lng.value};
	var obj3 = {"album3" : document.frm.lng.value};

<?php if($maptype == 1){ ?>
	window.opener.update2(obj,obj2);
<?php }else{ ?>
	<?php if($maptype == 3){ ?>
		window.opener.update3(obj,obj2);
	<?php }else{ ?>
		window.opener.update(obj,obj2);

	<?php } ?>
<?php } ?>
	window.close();

}


// ]]>
</script>

</body>
</html>

