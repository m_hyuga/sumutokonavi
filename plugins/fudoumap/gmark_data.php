<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 * @subpackage Fudousan Plugin
 * Fudousan Map Plugin
 * Version: 1.2.7
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */

define('WP_USE_THEMES', false);

/** Loads the WordPress Environment and Template */
require_once '../../../wp-blog-header.php';

//$wpdb->show_errors();

//半角数字チェック   
if (!function_exists('myIsNum_m')) {
	function myIsNum_m($value) {
		if (preg_match("/^[0-9]+$/", $value)) {
			return $value;
		}
		return '';
	}
}

	status_header( 200 );
	header("Content-Type: text/plain; charset=utf-8");

	global $wpdb;


	$j_kaiin = '';
	$GetDat = '';

	$shu_data = " > 0 ";

	//POST
	$mlat = isset($_POST['mlat']) ? $_POST['mlat'] : '';
	$mlng = isset($_POST['mlng']) ? $_POST['mlng'] : '';
	$latNE = isset($_POST['latNE']) ? $_POST['latNE'] : '';
	$lngNE = isset($_POST['lngNE']) ? $_POST['lngNE'] : '';
	$latSW = isset($_POST['latSW']) ? $_POST['latSW'] : '';
	$lngSW = isset($_POST['lngSW']) ? $_POST['lngSW'] : '';


	//種別
	$bukken_shubetsu = isset($_POST['shu']) ? $_POST['shu'] : '';
	if($bukken_shubetsu == '1') 
		$shu_data = '< 3000' ;	//売買
	if($bukken_shubetsu == '2') 
		$shu_data = '> 3000' ;	//賃貸
	if(intval($bukken_shubetsu) > 3 ) 
		$shu_data = '= ' .$bukken_shubetsu ;



	$madori_id = isset($_POST['mad']) ? $_POST['mad'] : '';	//間取り
	$set_id = isset($_POST['set']) ? $_POST['set'] : '';	//設備
	$ros_id = isset($_POST['ros']) ? myIsNum_m($_POST['ros']) : '';	//路線
	$eki_id = isset($_POST['eki']) ? myIsNum_m($_POST['eki']) : '';	//駅
	$ken_id = isset($_POST['ken']) ? myIsNum_m($_POST['ken']) : '';	//県
	$sik_id = isset($_POST['sik']) ? myIsNum_m($_POST['sik']) : '';	//市区

	$kalb_data = isset($_POST['kalb']) ? $_POST['kalb'] : '';	//価格下限
	$kahb_data = isset($_POST['kahb']) ? $_POST['kahb'] : '';	//価格上限
	$kalc_data = isset($_POST['kalc']) ? $_POST['kalc'] : '';	//賃料下限
	$kahc_data = isset($_POST['kahc']) ? $_POST['kahc'] : '';	//賃料上限

	$ken_id=sprintf("%02d",$ken_id);


	//売買
	if($bukken_shubetsu == '1' || intval($bukken_shubetsu) < 3000 ) {
		$kal_data =$kalb_data*10000 ;
		$kah_data =$kahb_data*10000 ;
	}
	//賃貸
	if($bukken_shubetsu == '2' || intval($bukken_shubetsu) > 3000 ) {
		$kal_data =$kalc_data*10000 ;
		$kah_data =$kahc_data*10000 ;
	}

	$hof_data = isset($_POST['hof']) ? $_POST['hof'] : '';	//歩分
	$tik_data = isset($_POST['tik']) ? $_POST['tik'] : '';	//築年数
	$mel_data = isset($_POST['mel']) ? $_POST['mel'] : '';	//面積下限
	$meh_data = isset($_POST['meh']) ? $_POST['meh'] : '';	//面積上限


	if( $latNE !='' && $lngNE !='' && $latSW !='' && $lngSW !='' ){

		$limit_from = 0 ;
		$limit_to   = 200;

		//ユーザー別会員物件リスト
		$kaiin_users_rains_register = get_option('kaiin_users_rains_register');

		$sql = "SELECT P.ID";
		$sql .=  " FROM (($wpdb->posts AS P";
		$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
		$sql .=  " INNER JOIN $wpdb->postmeta AS PM_I ON P.ID = PM_I.post_id) ";
		$sql .=  " INNER JOIN $wpdb->postmeta AS PM_K ON P.ID = PM_K.post_id";
		$sql .=  " WHERE P.post_status='publish' AND P.post_password = ''  AND P.post_type ='fudo' ";
		$sql .=  " AND PM.meta_key='bukkenshubetsu' AND CAST(PM.meta_value AS SIGNED)".$shu_data."";
		$sql .=  " AND PM_I.meta_key='bukkenido' AND PM_I.meta_value < ".$latNE." AND PM_I.meta_value > ".$latSW."";
		$sql .=  " AND PM_K.meta_key='bukkenkeido' AND PM_K.meta_value < ".$lngNE." AND PM_K.meta_value > ".$lngSW."";
		$sql .=  " GROUP BY P.ID";
		$sql .=  " ORDER BY P.post_date DESC";
		$sql .=  " LIMIT ".$limit_from.",".$limit_to."";

	//	$sql = $wpdb->prepare($sql);
		$metas = $wpdb->get_results( $sql, ARRAY_A );
		$meta_dat = '';

		if(!empty($metas)) {
			$i=0;
			foreach ( $metas as $meta ) {
				if($i!=0) $meta_dat .= ",";
				$meta_dat .= $meta['ID'];
				$i++;
			}
		}


		if($meta_dat != '' && ( ($kal_data > 0 || $kah_data > 0 ) || ($hof_data > 0) || ($mel_data > 0 || $meh_data > 0 ) || ($tik_data > 0 ) || !empty($set_id) || !empty($madori_id) )){

			$sql  = "SELECT P.ID ";
			$sql .= " FROM ((((( (( $wpdb->posts AS P";

			//価格
			if($kal_data > 0 || $kah_data > 0 ){
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";	//価格
			}else{
				$sql .=  " )";	
			}

			//歩分
			if($hof_data > 0){
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id)";		//歩分
			}else{
				$sql .=  " )";	
			}

			//面積
			if($mel_data > 0 || $meh_data > 0 ){
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_3 ON P.ID = PM_3.post_id)";		//面積
			}else{
				$sql .=  " )";	
			}

			//築年数
			if($tik_data > 0 ){
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_4 ON P.ID = PM_4.post_id)";		//築年数
			}else{
				$sql .=  " )";	
			}

			//設備
			if(!empty($set_id)) {
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_5 ON P.ID = PM_5.post_id)";		//設備
			}else{
				$sql .=  " )";	
			}

			//間取
			if(!empty($madori_id) ) {
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_6 ON P.ID = PM_6.post_id)";		//間取
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_7 ON P.ID = PM_7.post_id)";		//間取
			}else{
				$sql .=  ") )";	
			}

			$sql .=  " WHERE  ";
			$sql .=  " P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";

			if($meta_dat != ''){
				$sql .=  " AND P.ID IN (".$meta_dat.") ";
			}

			//価格
			if($kal_data > 0 || $kah_data > 0 ){
				$sql .=  " AND PM_1.meta_key='kakaku' ";
				if( $kal_data > 0 )
					$sql .=  " AND CAST(PM_1.meta_value AS SIGNED) >= $kal_data ";
				if( $kah_data > 0 )
					$sql .=  " AND CAST(PM_1.meta_value AS SIGNED) <= $kah_data ";
			}

			//歩分
			if($hof_data > 0){
				$sql .=  " AND (PM_2.meta_key='koutsutoho1f' OR PM_2.meta_key='koutsutoho2f' )";
				$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) > 0 ";
				$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) <= $hof_data ";
			}

			//面積
			if($mel_data > 0 || $meh_data > 0 ){
				$sql .=  " AND PM_3.meta_key='tatemonomenseki' ";
				if( $mel_data > 0 )
					$sql .=  " AND CAST(PM_3.meta_value AS SIGNED) >= $mel_data ";
				if( $meh_data > 0 )
					$sql .=  " AND CAST(PM_3.meta_value AS SIGNED) <= $meh_data ";
				$sql .=  " AND PM_3.meta_value !='' ";
			}

			//築年数
			$nowym= date('Ym');
			if($tik_data > 0 ){
				$sql .=  " AND PM_4.meta_key='tatemonochikunenn' ";
				if( $tik_data > 0 )
				//	$sql .=  " AND ( CAST(LEFT(PM_4.meta_value,4) AS SIGNED)  *100 + CAST(RIGHT(PM_4.meta_value,2) AS SIGNED) ) >= ($nowym- $tik_data * 100) ";
					$sql .=  " AND ( CAST(LEFT(PM_4.meta_value,4) AS SIGNED)  *100 + CASE WHEN LENGTH(PM_4.meta_value)>5 THEN CAST(RIGHT(PM_4.meta_value,2) AS SIGNED) ELSE 0 END ) >= ($nowym- $tik_data * 100) ";
			}

			//設備
			if(!empty($set_id)) {
				$sql .=  " AND (PM_5.meta_key='setsubi' AND ( ";
				$i=0;
				foreach($set_id as $meta_box){
					if($i!=0) $sql .= " AND ";
					$sql .= " PM_5.meta_value LIKE '%".$set_id[$i]."%'";
					$i++;
				}
				$sql .=  " ))";
			}

			//間取
			if(!empty($madori_id)) {
				$sql .=  " AND ( ";
				$i=0;
				foreach($madori_id as $meta_box){
					$madorisu_data = $madori_id[$i];
					if($i!=0) $sql .= " OR ";
					$sql .= " (PM_6.meta_key='madorisu' AND PM_6.meta_value ='".myLeft($madorisu_data,1)."' ";
					$sql .= " AND PM_7.meta_key='madorisyurui' AND PM_7.meta_value ='".myRight($madorisu_data,2)."')";
					$i++;
				}
				$sql .=  " ) ";
			}

			$sql .=  " GROUP BY P.ID ";
			$sql .=  " ORDER BY P.post_date DESC";
			$sql .=  " LIMIT ".$limit_from.",".$limit_to."";

			//$sql = $wpdb->prepare($sql);
			$metas2 = $wpdb->get_results( $sql, ARRAY_A );

			$meta_dat = '';
			if(!empty($metas2)) {
				$i=0;
				foreach ( $metas2 as $meta ) {
					if($i!=0) $meta_dat .= ",";
					$meta_dat .= $meta['ID'];
					$i++;
				}
			}

		}


		if($meta_dat != ''){

			$sql  = "SELECT P.ID ,PM_I.meta_value AS bukkenido , PM_K.meta_value AS bukkenkeido,PM.meta_value AS bukkenshubetsu";
			$sql .= " FROM (( $wpdb->posts AS P";

			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_I ON P.ID = PM_I.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_K ON P.ID = PM_K.post_id";


			$sql .=  " WHERE  ";
			$sql .=  " P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND P.ID IN (".$meta_dat.") ";
			$sql .=  " AND PM.meta_key='bukkenshubetsu'";
			$sql .=  " AND PM_I.meta_key='bukkenido' AND PM_K.meta_key='bukkenkeido'";
			$sql .=  " GROUP BY P.ID , PM_I.meta_value, PM_K.meta_value ";
			$sql .=  " ORDER BY P.post_date DESC";
			$sql .=  " LIMIT ".$limit_from.",".$limit_to."";


			$sql = $wpdb->prepare($sql);
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$rstCount = 0;
			$GetDat = '';


			if(!empty($metas)) {
			
				foreach ( $metas as $meta ) {

					$post_id =  $meta['ID'];

					$j_url =  get_permalink($post_id);
					$j_kakaku = '';
					$j_menseki = '';
					$j_shozaichi = '';
					$j_koutsui = '';
					$j_tsubo = '';

					//会員
					$kaiin = 0;
					if( !is_user_logged_in() && get_post_meta($post_id, 'kaiin', true) == 1 ) $kaiin = 1;

					//ユーザー別会員物件リスト
					$kaiin2 = users_kaiin_bukkenlist($post_id,$kaiin_users_rains_register,get_post_meta($post_id, 'kaiin', true));


					//価格
						if ( !my_custom_kaiin_view_m('kaiin_kakaku',$kaiin,$kaiin2) ){
							$j_kakaku2 = '会員限定';
						}else{
							//非公開の場合
							if( get_post_meta($post_id, 'seiyakubi', true) != "" ){
								$j_kakaku = 'ご成約済';
							}else{

								if(get_post_meta($post_id,'kakakukoukai',true) == "0"){
									$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
									if($kakakujoutai_data_data=="1")	$j_kakaku = '相談';
									if($kakakujoutai_data_data=="2")	$j_kakaku = '確定';
									if($kakakujoutai_data_data=="3")	$j_kakaku = '入札';

								}else{
									$kakaku_data = get_post_meta($post_id,'kakaku',true);
									if(is_numeric($kakaku_data)){
										$j_kakaku =  floatval($kakaku_data)/10000;
										$j_kakaku .= "万円 ";
									}
								}					
							}
						}



					//間取り・土地面積

						//土地面積
						if ( my_custom_kaiin_view_m('kaiin_menseki',$kaiin,$kaiin2) ){
							if ( get_post_meta($post_id,'bukkenshubetsu',true) < 1200 ) {
								if( get_post_meta($post_id, 'tochikukaku', true) !="" ) 
									$j_menseki = ''.get_post_meta($post_id, 'tochikukaku', true).'m&sup2;';
							}
						}


					//所在地
						if ( my_custom_kaiin_view_m('kaiin_shozaichi',$kaiin,$kaiin2) ){
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);
							$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichicode_data = myLeft($shozaichicode_data,5);
							$shozaichicode_data = myRight($shozaichicode_data,3);

							if($shozaichiken_data !="" && $shozaichicode_data !=""){
								$sql = "SELECT narrow_area_name FROM ".$wpdb->prefix."area_narrow_area WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

								$sql = $wpdb->prepare($sql);
								$metas = $wpdb->get_row( $sql );
								$j_shozaichi = "".$metas->narrow_area_name."";
							}
							$j_shozaichi .= get_post_meta($post_id, 'shozaichimeisho', true);
						}


					//交通路線
						if ( my_custom_kaiin_view_m('kaiin_kotsu',$kaiin,$kaiin2) ){
							$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
							$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);

							if($koutsurosen_data !=""){
								$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql);
								$metas = $wpdb->get_row( $sql );
								$j_koutsui = "".$metas->rosen_name;
							}

							//交通駅
							if($koutsurosen_data !="" && $koutsueki_data !=""){
								$sql = "SELECT DTS.station_name";
								$sql = $sql . " FROM ".$wpdb->prefix."train_rosen AS DTR";
								$sql = $sql . " INNER JOIN ".$wpdb->prefix."train_station as DTS ON DTR.rosen_id = DTS.rosen_id";
								$sql = $sql . " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql);
								$metas = $wpdb->get_row( $sql );
								if($metas->station_name != '＊＊＊＊')
								$j_koutsui .= $metas->station_name.'駅';
							}
						}

					//坪単価
						$j_tsubo = get_post_meta($post_id, 'kakakutsubo', true);


					/*
					if( $kaiin == 1 ) {
						$j_kaiin = '<br />この物件は、「会員様にのみ限定公開」している物件です';
					 } else { 
						//ユーザー別会員物件リスト
						if (!$kaiin2 && get_option('kaiin_users_rains_register') == 1 && get_post_meta($post_id, 'kaiin', true) == 1 ) {
							$j_kaiin = '<br />この物件は、「閲覧条件」に 該当していない物件です。';
						}
					}
					*/

					//$GetDat .= "{'j_domain':'". $j_domain . "','j_kaiin':'". $j_kaiin . "','j_url':'".$j_url ."','j_img':'". $j_img . "','j_title':'". $j_title. "','j_kakaku':'". $j_kakaku. "','j_madori':'". $j_madori. "','j_menseki' :'". $j_menseki . "','j_shozaichi' :'". $j_shozaichi . "','j_koutsui' :'". $j_koutsui . "'}";
					$GetDat .= '<tr><td>' .$j_shozaichi. '</td><td>' .$j_koutsui. '</td><td>' .$j_kakaku. '</td><td>' .$j_menseki. '</td><td>' .$j_tsubo. '</td></tr>';

				}	//foreach


				if( !empty($GetDat) ){
					$GetDat = '<table id="myTable" class="tablesorter"><thead><tr><th>所在地</th><th>交通・バス・徒歩</th><th>価格</th><th>面積</th><th>坪単価</th></tr></thead><tfoot><tr><th>所在地</th><th>交通・バス徒歩</th><th>価格</th><th>面積</th><th>坪単価</th></tr></tfoot><tbody>' . $GetDat . '</tbody></table>';
				}

				$SetDat = "{'Bukken':[{'j_data': '".$GetDat."'}]}";
			}else{
				$SetDat = "{'Bukken':'','Err':'Err1'}";
			}



		}else{
				$SetDat = "{'Bukken':'','Err':'Err2'}";
		
		}
		
		echo $SetDat;
	}





//$wpdb->print_error();


/*
	//GET

	$latNE	=  $_GET['latNE'];
	$lngNE	=  $_GET['lngNE'];
	$latSW	=  $_GET['latSW'];
	$lngSW	=  $_GET['lngSW'];


	$bukken_shubetsu = $_GET['shu'];	//種別

	$shu_data = " > 0 ";
	if($bukken_shubetsu == '1') 
		$shu_data = '< 3000' ;	//売買
	if($bukken_shubetsu == '2') 
		$shu_data = '> 3000' ;	//賃貸

	if(intval($bukken_shubetsu) > 3 ) 
		$shu_data = '= ' .$bukken_shubetsu ;



	$madori_id = $_GET['mad'];		//間取り
	$set_id = $_GET['set'];			//設備

	$ros_id = myIsNum_m($_GET['ros']);	//路線
	$eki_id = myIsNum_m($_GET['eki']);	//駅
	$ken_id = myIsNum_m($_GET['ken']);	//県
	$sik_id = myIsNum_m($_GET['sik']);	//市区


		$ken_id=sprintf("%02d",$ken_id);


	$kalb_data = myIsNum_m($_GET['kalb']);	//価格下限
	$kahb_data = myIsNum_m($_GET['kahb']);	//価格上限
	$kalc_data = myIsNum_m($_GET['kalc']);	//賃料下限
	$kahc_data = myIsNum_m($_GET['kahc']);	//賃料上限

		//売買
		if($bukken_shubetsu == '1' || intval($bukken_shubetsu) < 3000 ) {
			$kal_data =$kalb_data*10000 ;
			$kah_data =$kahb_data*10000 ;
		}
		//賃貸
		if($bukken_shubetsu == '2' || intval($bukken_shubetsu) > 3000 ) {
			$kal_data =$kalc_data*10000 ;
			$kah_data =$kahc_data*10000 ;
		}

	$hof_data = myIsNum_m($_GET['hof']);	//歩分
	$tik_data = myIsNum_m($_GET['tik']);	//築年数
	$mel_data = myIsNum_m($_GET['mel']);	//面積下限
	$meh_data = myIsNum_m($_GET['meh']);	//面積上限

*/




/*
 * 不動産会員2チェック
 * @package WordPress3.1
 * @subpackage Fudousan Plugin
 * Fudousan mail Plugin
*/

//ユーザー別会員物件リスト
function users_kaiin_bukkenlist_m($post_id,$kaiin_users_rains_register,$kaiin){

	global $is_fudouktai,$is_fudoumap,$is_fudoukaiin,$is_fudoumail,$is_fudourains;

	$id_data = '';

	if($kaiin_users_rains_register == 1 && $kaiin == 1 && $is_fudoumail ){

		global $wpdb;

		global $userdata; 
		get_currentuserinfo();   
		$user_mail_ID = $userdata->ID;


		//条件種別
			$user_mail_shu = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_shu', true) );

			if (is_array($user_mail_shu)) {
				$i=0;
				$shu_data = ' IN ( 0 ';
				foreach($user_mail_shu as $meta_set){
					$shu_data .= ','. $user_mail_shu[$i] . '';
					$i++;
				}
				$shu_data .= ') ';

				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
				$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=  " AND PM.meta_key='bukkenshubetsu' AND PM.meta_value ".$shu_data."";
				$sql .=  " AND P.ID = ".$post_id."";

				$sql = $wpdb->prepare($sql);
				$metas = $wpdb->get_row( $sql );
				if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';
			}

		//echo '<br />条件種別 ';
		//echo $id_data;


		//条件エリア
			$user_mail_sik = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_sik', true) );

			if(is_array( $user_mail_sik ) && $id_data !='' ){
				$i=0;
				$sik_data = ' IN ( 0 ';
				foreach($user_mail_sik as $meta_set){
					$sik_data .= ','. $user_mail_sik[$i] . '';
					$i++;
				}
				$sik_data .= ') ';

				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
				$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=  " AND PM.meta_key='shozaichicode' AND PM.meta_value ".$sik_data."";
				$sql .=  " AND P.ID = ".$post_id."";

				$sql = $wpdb->prepare($sql);
				$metas = $wpdb->get_row( $sql );
				if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';
			}


		//echo '<br />条件エリア ';
		//echo $id_data;

		//条件路線駅
			$user_mail_eki = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_eki', true) );


			if(is_array( $user_mail_eki ) && $id_data !='' ){
				$i=0;
				$eki_data = ' IN ( 0 ';
				foreach($user_mail_eki as $meta_set){
					$eki_data .= ',' . intval(myLeft($user_mail_eki[$i],6)) . intval(myRight($user_mail_eki[$i],6));
					$i++;
				}
				$eki_data .= ') ';

				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM ($wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
				$sql .=  " WHERE  P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=  " AND PM.meta_key='koutsurosen1' AND PM_1.meta_key='koutsueki1' ";
				$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $eki_data . "";
				$sql .=  " AND P.ID = ".$post_id."";

				$sql = $wpdb->prepare($sql);
				$metas = $wpdb->get_row( $sql );
				if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';

				//交通2 try
				if($id_data == ''){ 

					$sql = "SELECT DISTINCT( P.ID )";
					$sql .=  " FROM ($wpdb->posts AS P";
					$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
					$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
					$sql .=  " WHERE  P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
					$sql .=  " AND PM.meta_key='koutsurosen2' AND PM_1.meta_key='koutsueki2' ";
					$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $eki_data . "";
					$sql .=  " AND P.ID = ".$post_id."";

					$sql = $wpdb->prepare($sql);
					$metas = $wpdb->get_row( $sql );
					if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';
				}

			}

		//echo '<br />条件路線駅 ';
		//echo $id_data;


		//条件価格
			$kalb_data = get_user_meta( $user_mail_ID, 'user_mail_kalb', true);
			$kahb_data = get_user_meta( $user_mail_ID, 'user_mail_kahb', true);
			$kalc_data = get_user_meta( $user_mail_ID, 'user_mail_kalc', true);
			$kahc_data = get_user_meta( $user_mail_ID, 'user_mail_kahc', true);

			if($kalb_data+$kahb_data+$kalc_data+$kahc_data >0 && $id_data !=''){

				$kalb_data =$kalb_data*10000 ;

				if($kahb_data == '0' ){
					$kahb_data = 1000000000 ;
				}else{
					$kahb_data =$kahb_data*10000 ;
				}

				$kalc_data =$kalc_data*10000 ;

				if($kahc_data == '0' ){
					$kahc_data = 9990000 ;
				}else{
					$kahc_data =$kahc_data*10000 ;
				}


				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM ($wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id )";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
				$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=  " AND PM_1.meta_key='bukkenshubetsu' AND CAST(PM_1.meta_value AS SIGNED) < 3000";
				$sql .=  " AND PM.meta_key='kakaku'";
				$sql .=  " AND CAST(PM.meta_value AS SIGNED) >= $kalb_data AND CAST(PM.meta_value AS SIGNED) <= $kahb_data  ";
				$sql .=  " AND P.ID = ".$post_id."";

				$sql = $wpdb->prepare($sql);
				$metas = $wpdb->get_row( $sql );
				if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';


				if($id_data == ''){ 
					$sql = "SELECT DISTINCT( P.ID )";
					$sql .=  " FROM ($wpdb->posts AS P";
					$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id )";
					$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
					$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
					$sql .=  " AND PM_1.meta_key='bukkenshubetsu' AND CAST(PM_1.meta_value AS SIGNED) > 3000";
					$sql .=  " AND PM.meta_key='kakaku'";
					$sql .=  " AND CAST(PM.meta_value AS SIGNED) >= $kalc_data  AND CAST(PM.meta_value AS SIGNED) <= $kahc_data ";
				//	$sql .=  " OR ( P.ID " . $id_data2 . ")";

					$sql .=  " AND P.ID = ".$post_id."";

					$sql = $wpdb->prepare($sql);
					$metas = $wpdb->get_row( $sql );
					if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';
				}

			}

		//echo '<br />価格 ';
		//echo $id_data;



		//専有面積
			$tatemo_l_data = get_user_meta( $user_mail_ID, 'user_mail_tatemonomenseki_l', true);
			$tatemo_h_data = get_user_meta( $user_mail_ID, 'user_mail_tatemonomenseki_h', true);

			if( !empty($tatemo_l_data) || !empty($tatemo_h_data) ){

				if( $tatemo_h_data == '0' ) $tatemo_h_data = 9999 ;

				if(( $tatemo_l_data != 0 || $tatemo_h_data != 9999 ) && $id_data !='' ){
					$sql = "SELECT DISTINCT( P.ID )";
					$sql .=  " FROM $wpdb->posts AS P";
					$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
					$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
					$sql .=  " AND PM_2.meta_key='tatemonomenseki'";
					$sql .=  " AND PM_2.meta_value *100 >= $tatemo_l_data*100 ";
					$sql .=  " AND PM_2.meta_value *100 <= $tatemo_h_data*100 ";
					$sql .=  " AND P.ID = ".$post_id."";
					$sql = $wpdb->prepare($sql);
					$metas = $wpdb->get_row( $sql );
					if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';
				}
			}

		//echo '<br />専有面積 ';
		//echo $metas->ID;


		//土地面積
			$tochim_l_data = get_user_meta( $user_mail_ID, 'user_mail_tochikukaku_l', true);
			$tochim_h_data = get_user_meta( $user_mail_ID, 'user_mail_tochikukaku_h', true);

			if( !empty($tochim_l_data) || !empty($tochim_h_data) ){

				if( $tochim_h_data  == '0' ) $tochim_h_data = 9999 ;

				if(( $tochim_l_data != 0 || $tochim_h_data != 9999 ) && $id_data !='' ){
					$sql = "SELECT DISTINCT( P.ID )";
					$sql .=  " FROM $wpdb->posts AS P";
					$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
					$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
					$sql .=  " AND PM_2.meta_key='tochikukaku'";
					$sql .=  " AND PM_2.meta_value *100 >= $tochim_l_data*100 ";
					$sql .=  " AND PM_2.meta_value *100 <= $tochim_h_data*100 ";
					$sql .=  " AND P.ID = ".$post_id."";
					$sql = $wpdb->prepare($sql);
					$metas = $wpdb->get_row( $sql );
					if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';
				}
			}
		//echo '<br />土地面積 ';
		//echo $metas->ID;




		//条件間取り
			$user_mail_madori = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_madori', true) );

			if(is_array( $user_mail_madori ) && $id_data !='' ){
				$i=0;
				$madori_data = ' IN ( 0 ';
				foreach($user_mail_madori as $meta_set){
					$madori_data .= ','. $user_mail_madori[$i] . '';
					$i++;
				}
				$madori_data .= ') ';

				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM ($wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
				$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=  " AND PM.meta_key='madorisu' AND PM_1.meta_key='madorisyurui' ";
				$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $madori_data . "";
				$sql .=  " AND P.ID = ".$post_id."";

				$sql = $wpdb->prepare($sql);
				$metas = $wpdb->get_row( $sql );
				if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';

			}

		//echo '<br />間取り ';
		//echo $id_data;

		//駅歩分
			$hof_data = get_user_meta( $user_mail_ID, 'user_mail_hohun', true);

			if( $hof_data != 0  && $id_data !='' ){

				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2   ON P.ID = PM_2.post_id ";
				$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=  " AND (PM_2.meta_key='koutsutoho1f' OR PM_2.meta_key='koutsutoho2f' )";
				$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) > 0 ";
				$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) <= $hof_data ";

				$sql .=  " AND P.ID = ".$post_id."";

				$sql = $wpdb->prepare($sql);
				$metas = $wpdb->get_row( $sql );
				if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';
			}

		//echo '<br />歩分 ';
		//echo $id_data;


		//条件設備
			$user_mail_setsubi = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_setsubi', true) );

			if(is_array( $user_mail_setsubi ) && $id_data !='' ){
				$i=0;
				$setsubi_data = " AND (";
				foreach($user_mail_setsubi as $meta_set){
				//	if($i!=0) $setsubi_data .= " OR ";
					if($i!=0) $setsubi_data .= " AND ";
					$setsubi_data .= " PM.meta_value LIKE '%/". $user_mail_setsubi[$i] . "%' ";
					$i++;
				}
				$setsubi_data .= ")";


				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
				$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=  " AND PM.meta_key='setsubi' ".$setsubi_data."";
				$sql .=  " AND P.ID = ".$post_id."";

			//	$sql = $wpdb->prepare($sql);
				$metas = $wpdb->get_row( $sql );
				if( !empty($metas->ID) ) $id_data = $metas->ID; else $id_data ='';

			}

		//echo '<br />設備 ';
		//echo $id_data;

		if( $id_data != ''){
			return  true;	//表示
		}else{
			return  false;	//非表示
		}
	}else{
		return  true;	//表示
	}
}

//会員項目表示判定
function my_custom_kaiin_view_m($koumoku,$kaiin,$kaiin2) {
	$koumoku = get_option($koumoku);
	if( ( $koumoku != 1 && $kaiin == 1 )  || ( $koumoku != 1 && $kaiin == 0 && !$kaiin2 ) ) {
		return  false;	//非表示
	}else{
		return  true;	//表示
	}
}


?>

