<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 * @subpackage Fudousan Plugin
 * Fudousan Map Plugin
 * Version: 1.3.4
 */

	global $is_fudouktai,$is_fudoumap,$is_fudoukaiin;

	$shu_data = '';
	$madori_dat = '';
	$setsubi_dat ='';
	$tmp_eki = '';

	//種別
	$shu_id = isset($_GET['shu']) ? $_GET['shu'] : '';

	if($bukken_shubetsu == 2 || $bukken_shubetsu > 3000 ) {
		echo '<style type="text/css">';
		echo "\n<!--\n";
		echo '#kakaku_c { display:block; }';
		echo '#kakaku_b { display:none; }';
		echo "\n-->\n";
		echo '</style>';
	}

	if($bukken_shubetsu != 2 && $bukken_shubetsu < 3000 ) {
		echo '<style type="text/css">';
		echo "\n<!--\n";
		echo '#kakaku_b { display:block; }';
		echo '#kakaku_c { display:none; }';
		echo "\n-->\n";
		echo '</style>';
	}


	if($bukken_shubetsu == '1') 
		$shu_data = '< 3000' ;	//売買
	if($bukken_shubetsu == '2') 
		$shu_data = '> 3000' ;	//賃貸

	if(intval($bukken_shubetsu) > 3 ) 
		$shu_data = '= ' .$bukken_shubetsu ;
	//	$shu_data = " > 0 ";


		echo '<div id="sub">';
?>
<h3 class="sub_nav_heading toggle">
<?php  if(!strstr($template, 'page-map_unsp')):?>
		<span id="menu_botton"><a href="<?php echo WP_PLUGIN_URL;?>/fudoumap/help/help2.php?height=470&width=450"  class="thickbox"><img src="<?php echo WP_PLUGIN_URL;?>/fudoumap/img/gmap_sb2.png" alt="ヘルプページ" title="ヘルプページ" border="0" width="85" /></a></span>
マップ検索</h3>
<?php endif;?>
<?php
//		echo '<h3 class="sub_nav_heading toggle">マップ検索</h3>';

		echo '<div class="sub_nav">';
		echo '<form method="get" id="searchmapitem" name="searchmapitem" action="" >';

		echo '<input type="hidden" name="page" value="map">';

		echo 'ご希望の種別を選択して下さい<br />';

		//種別選択
		echo '<div id="shubetsu" class="shubetsu">';
		echo '<select name="shu" id="shu" onchange="SShu(this)">';
		echo '<option value="0">種別選択</option>';



	//	if($view1 < 2 ){

			$sql  =  " SELECT PM.meta_value AS bukkenshubetsu";
			$sql .=  " FROM ($wpdb->posts as P ";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_G ON P.ID = PM_G.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM.meta_key='bukkenshubetsu' ";
			$sql .=  " AND CAST( PM.meta_value AS SIGNED ) < 3000 ";
			$sql .=  " AND PM_G.meta_key='bukkenido' AND PM_G.meta_value != '' ";
			$sql .=  " GROUP BY PM.meta_value ";
			$sql .=  " ORDER BY PM.meta_value";
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			if(!empty($metas)) {

				echo '<option value="1"';
				if($shu_id == '1' )
					echo ' selected="selected"';
				echo '>売買　全て</option>';

				foreach ( $metas as $meta ) {
					$bukkenshubetsu_id = $meta['bukkenshubetsu'];

					foreach($work_bukkenshubetsu as $meta_box){
						if( $bukkenshubetsu_id ==  $meta_box['id'] ){
							echo '<option value="'.$meta_box['id'].'"';
							if($shu_id == $meta_box['id'] )
								echo ' selected="selected"';
							echo '>'.$meta_box['name'].'</option>';
						}
					}
				}
			}
	//	}

	//	if($view1 != 1 ){

			$sql  =  " SELECT PM.meta_value AS bukkenshubetsu";
			$sql .=  " FROM ($wpdb->posts as P ";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_G ON P.ID = PM_G.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM.meta_key='bukkenshubetsu' ";
			$sql .=  " AND CAST( PM.meta_value AS SIGNED ) > 3000 ";
			$sql .=  " AND PM_G.meta_key='bukkenido' AND PM_G.meta_value != '' ";
			$sql .=  " GROUP BY PM.meta_value ";
			$sql .=  " ORDER BY PM.meta_value";
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			if(!empty($metas)) {
				echo '<option value="2"';
				if($shu_id == '2' )
					echo ' selected="selected"';
				echo '>賃貸　全て</option>';

				foreach ( $metas as $meta ) {
					$bukkenshubetsu_id = $meta['bukkenshubetsu'];

					foreach($work_bukkenshubetsu as $meta_box){
						if( $bukkenshubetsu_id ==  $meta_box['id'] ){
							echo '<option value="'.$meta_box['id'].'"';
							if($shu_id == $meta_box['id'] )
								echo ' selected="selected"';
							echo '>'.$meta_box['name'].'</option>';
						}
					}
				}
			}
	//	}



		echo '</select>';
		echo '</div>';


		echo '<div id="caution1">以下ご希望の条件を選択して<br />物件検索ボタンを押して下さい</div>';

		//路線選択
		echo '<div id="roseneki" class="roseneki">';

		echo '駅選択<br />';
		echo '<select name="ros" id="ros" onchange="SEki(this)">';
		echo '<option value="0">路線選択</option>';
		if( $shu_data !='' && $ros_id !=''){

			$sql  =  "SELECT DTR.rosen_name, PM.meta_value AS rosen_id";
			$sql .=  " FROM ((($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_G ON P.ID = PM_G.post_id) ";

			$sql .=  " INNER JOIN ".$wpdb->prefix."train_rosen as DTR ON CAST( PM.meta_value AS SIGNED ) = DTR.rosen_id";
			$sql .=  " WHERE (PM.meta_key='koutsurosen1' Or PM.meta_key='koutsurosen2') ";
			$sql .=  " AND P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";

			$sql .=  " AND PM_G.meta_key='bukkenido' AND PM_G.meta_value != '' ";

			$sql .=  " GROUP BY DTR.rosen_name, PM.meta_value";
			$sql .=  " ORDER BY DTR.rosen_name";
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {

				foreach ( $metas as $meta ) {
					//バス除外
					if($meta['rosen_id'] != "6000"){
						echo '<option value="'.$meta['rosen_id'].'"';
						if($ros_id == $meta['rosen_id'] ) echo ' selected="selected"';
						echo '>'.$meta['rosen_name'].'</option>';
					}
				}
			}
		}
		echo '</select><br />';


		//駅選択
		echo '<select name="eki" id="eki">';
		echo '<option value="0">駅選択</option>';
		if( $shu_data !='' && $ros_id !='' && $eki_id !='' ){

			$sql  =  " SELECT DISTINCT PM.meta_value AS station_id ";
			$sql .=  " FROM ((( $wpdb->posts as P ";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM ON P.ID = PM.post_id )";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id )";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id )";
			$sql .=  " WHERE";
			$sql .=  " (";
			$sql .=  " 	P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " 	AND PM.meta_key='koutsueki1'";
			$sql .=  " 	AND PM_1.meta_key='koutsurosen1' AND PM_1.meta_value = ".$ros_id."";
			$sql .=  " 	AND PM_2.meta_key='bukkenshubetsu' AND PM_2.meta_value ".$shu_data."";
			$sql .=  " )";
			$sql .=  " or";
			$sql .=  " (";
			$sql .=  " 	P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " 	AND PM.meta_key='koutsueki2'";
			$sql .=  " 	AND PM_1.meta_key='koutsurosen2' AND PM_1.meta_value = ".$ros_id."";
			$sql .=  " 	AND PM_2.meta_key='bukkenshubetsu' AND PM_2.meta_value ".$shu_data."";
			$sql .=  " )";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			if(!empty($metas)) {
				$tmp_eki = '0';
				foreach ( $metas as $meta ) {
						$tmp_eki .= ','. $meta['station_id'];
				}
			}

			$sql  =  " SELECT DISTINCT DTS.station_name , DTS.station_id ";
			$sql .=  " FROM ".$wpdb->prefix."train_station as DTS";
			$sql .=  " WHERE DTS.rosen_id=".$ros_id." AND DTS.station_id in (".$tmp_eki.") ";
			$sql .=  " ORDER BY DTS.station_ranking";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {

				foreach ( $metas as $meta ) {
					echo '<option value="'.$meta['station_id'].'"';
					if($eki_id == $meta['station_id'] )
						echo ' selected="selected"';
					echo '>'.$meta['station_name'].'</option>';
				}
			}
		}
		echo '</select>';
		echo '</div>';


		//県選択
		echo '<div id="chiiki" class="chiiki">';
		echo '市区選択<br />';
		echo '<select name="ken" id="ken" onchange="SSik(this)">';
		echo '<option value="0">県選択</option>';
		if( $shu_data !='' && $ken_id !='' ){
			$sql  =  "SELECT MA.middle_area_name, PM.meta_value AS middle_area_id";
			$sql .=  " FROM ((($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_G ON P.ID = PM_G.post_id) ";

			$sql .=  " INNER JOIN ".$wpdb->prefix."area_middle_area as MA ON CAST( PM.meta_value AS SIGNED ) = MA.middle_area_id";
			$sql .=  " WHERE PM.meta_key='shozaichiken' ";
			$sql .=  " AND P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
			$sql .=  " AND PM_G.meta_key='bukkenido' AND PM_G.meta_value != '' ";
			$sql .=  " GROUP BY MA.middle_area_name, PM.meta_value";
			$sql .=  " ORDER BY CAST( PM.meta_value AS SIGNED )";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {

				foreach ( $metas as $meta ) {
					echo '<option value="'.$meta['middle_area_id'].'"';
					if($ken_id == $meta['middle_area_id'] )
						echo ' selected="selected"';
					echo '>'.$meta['middle_area_name'].'</option>';
				}
			}
		}
		echo '</select><br />';


		//市区選択
		echo '<select name="sik" id="sik">';
		echo '<option value="0">市区選択</option>';
		if( $shu_data !='' && $ken_id !='' && $sik_id !='' ){

			$sql  =  "SELECT NA.narrow_area_name, CAST( RIGHT(LEFT(PM.meta_value,5),3) AS SIGNED ) as narrow_area_id";
			$sql .=  " FROM ((($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_G ON P.ID = PM_G.post_id) ";

			$sql .=  " INNER JOIN ".$wpdb->prefix."area_narrow_area as NA ON CAST( RIGHT(LEFT(PM.meta_value,5),3) AS SIGNED ) = NA.narrow_area_id";
			$sql .=  " WHERE PM.meta_key='shozaichicode' ";
			$sql .=  " AND P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
			$sql .=  " AND CAST( LEFT(PM.meta_value,2) AS SIGNED ) = ". $ken_id;
			$sql .=  " AND PM_G.meta_key='bukkenido' AND PM_G.meta_value != '' ";
			$sql .=  " AND NA.middle_area_id = ". $ken_id;
			$sql .=  " GROUP BY NA.narrow_area_name, PM.meta_value";
			$sql .=  " ORDER BY CAST( PM.meta_value AS SIGNED )";
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {
				foreach ( $metas as $meta ) {
					echo '<option value="'.$meta['narrow_area_id'].'"';
					if($sik_id == $meta['narrow_area_id'] )
						echo ' selected="selected"';
					echo '>'.$meta['narrow_area_name'].'</option>';
				}
			}
		}
		echo '</select>';
		echo '</div>';


		//価格選択
		echo '<div id="kakaku_c" class="kakaku_c">';
		echo '賃料<br />';
		echo '<select name="kalc" id="kalc">';
		echo '<option value="0">下限なし</option>';
		echo '<option value="3"'; 			if ($kalc_data == '3') echo ' selected="selected"';			echo '>3万円</option>';
		echo '<option value="4"';			if ($kalc_data == '4') echo ' selected="selected"';			echo '>4万円</option>';
		echo '<option value="5"';			if ($kalc_data == '5') echo ' selected="selected"';			echo '>5万円</option>';
		echo '<option value="6"';			if ($kalc_data == '6') echo ' selected="selected"';			echo '>6万円</option>';
		echo '<option value="7"';			if ($kalc_data == '7') echo ' selected="selected"';			echo '>7万円</option>';
		echo '<option value="8"';			if ($kalc_data == '8') echo ' selected="selected"';			echo '>8万円</option>';
		echo '<option value="9"';			if ($kalc_data == '9') echo ' selected="selected"';			echo '>9万円</option>';
		echo '<option value="10"';			if ($kalc_data == '10') echo ' selected="selected"';			echo '>10万円</option>';
		echo '<option value="11"';			if ($kalc_data == '11') echo ' selected="selected"';			echo '>11万円</option>';
		echo '<option value="12"';			if ($kalc_data == '12') echo ' selected="selected"';			echo '>12万円</option>';
		echo '<option value="13"';			if ($kalc_data == '13') echo ' selected="selected"';			echo '>13万円</option>';
		echo '<option value="14"';			if ($kalc_data == '14') echo ' selected="selected"';			echo '>14万円</option>';
		echo '<option value="15"';			if ($kalc_data == '15') echo ' selected="selected"';			echo '>15万円</option>';
		echo '<option value="16"';			if ($kalc_data == '16') echo ' selected="selected"';			echo '>16万円</option>';
		echo '<option value="17"';			if ($kalc_data == '17') echo ' selected="selected"';			echo '>17万円</option>';
		echo '<option value="18"';			if ($kalc_data == '18') echo ' selected="selected"';			echo '>18万円</option>';
		echo '<option value="19"';			if ($kalc_data == '19') echo ' selected="selected"';			echo '>19万円</option>';
		echo '<option value="20"';			if ($kalc_data == '20') echo ' selected="selected"';			echo '>20万円</option>';
		echo '<option value="30"';			if ($kalc_data == '30') echo ' selected="selected"';			echo '>30万円</option>';
		echo '<option value="50"';			if ($kalc_data == '50') echo ' selected="selected"';			echo '>50万円</option>';
		echo '<option value="100"';			if ($kalc_data == '100') echo ' selected="selected"';			echo '>100万円</option>';
		echo '</select>～';

		echo '<select name="kahc" id="kahc">';
		echo '<option value="3"';			if ($kahc_data == '3') echo ' selected="selected"';			echo '>3万円</option>';
		echo '<option value="4"';			if ($kahc_data == '4') echo ' selected="selected"';			echo '>4万円</option>';
		echo '<option value="5"';			if ($kahc_data == '5') echo ' selected="selected"';			echo '>5万円</option>';
		echo '<option value="6"';			if ($kahc_data == '6') echo ' selected="selected"';			echo '>6万円</option>';
		echo '<option value="7"';			if ($kahc_data == '7') echo ' selected="selected"';			echo '>7万円</option>';
		echo '<option value="8"';			if ($kahc_data == '8') echo ' selected="selected"';			echo '>8万円</option>';
		echo '<option value="9"';			if ($kahc_data == '9') echo ' selected="selected"';			echo '>9万円</option>';
		echo '<option value="10"';			if ($kahc_data == '10') echo ' selected="selected"';			echo '>10万円</option>';
		echo '<option value="11"';			if ($kahc_data == '11') echo ' selected="selected"';			echo '>11万円</option>';
		echo '<option value="12"';			if ($kahc_data == '12') echo ' selected="selected"';			echo '>12万円</option>';
		echo '<option value="13"';			if ($kahc_data == '13') echo ' selected="selected"';			echo '>13万円</option>';
		echo '<option value="14"';			if ($kahc_data == '14') echo ' selected="selected"';			echo '>14万円</option>';
		echo '<option value="15"';			if ($kahc_data == '15') echo ' selected="selected"';			echo '>15万円</option>';
		echo '<option value="16"';			if ($kahc_data == '16') echo ' selected="selected"';			echo '>16万円</option>';
		echo '<option value="17"';			if ($kahc_data == '17') echo ' selected="selected"';			echo '>17万円</option>';
		echo '<option value="18"';			if ($kahc_data == '18') echo ' selected="selected"';			echo '>18万円</option>';
		echo '<option value="19"';			if ($kahc_data == '19') echo ' selected="selected"';			echo '>19万円</option>';
		echo '<option value="20"';			if ($kahc_data == '20') echo ' selected="selected"';			echo '>20万円</option>';
		echo '<option value="30"';			if ($kahc_data == '30') echo ' selected="selected"';			echo '>30万円</option>';
		echo '<option value="50"';			if ($kahc_data == '50') echo ' selected="selected"';			echo '>50万円</option>';
		echo '<option value="100"';			if ($kahc_data == '100') echo ' selected="selected"';			echo '>100万円</option>';
		echo '<option value="0"';			if ($kahc_data == '0' ||$kahc_data == '' ) echo ' selected="selected"';			echo '>上限なし</option>';
		echo '</select>';
		echo '</div>';



		echo '<div id="kakaku_b" class="kakaku_b">';
		echo '価格<br />';
		echo '<select name="kalb" id="kalb">';
		echo '<option value="0">下限なし</option>';
		echo '<option value="300"'; 			if ($kalb_data == '300') echo ' selected="selected"';			echo '>300万円</option>';
		echo '<option value="400"';			if ($kalb_data == '400') echo ' selected="selected"';			echo '>400万円</option>';
		echo '<option value="500"';			if ($kalb_data == '500') echo ' selected="selected"';			echo '>500万円</option>';
		echo '<option value="600"';			if ($kalb_data == '600') echo ' selected="selected"';			echo '>600万円</option>';
		echo '<option value="700"';			if ($kalb_data == '700') echo ' selected="selected"';			echo '>700万円</option>';
		echo '<option value="800"';			if ($kalb_data == '800') echo ' selected="selected"';			echo '>800万円</option>';
		echo '<option value="900"';			if ($kalb_data == '900') echo ' selected="selected"';			echo '>900万円</option>';
		echo '<option value="1000"';			if ($kalb_data == '1000') echo ' selected="selected"';			echo '>1000万円</option>';
		echo '<option value="1100"';			if ($kalb_data == '1100') echo ' selected="selected"';			echo '>1100万円</option>';
		echo '<option value="1200"';			if ($kalb_data == '1200') echo ' selected="selected"';			echo '>1200万円</option>';
		echo '<option value="1300"';			if ($kalb_data == '1300') echo ' selected="selected"';			echo '>1300万円</option>';
		echo '<option value="1400"';			if ($kalb_data == '1400') echo ' selected="selected"';			echo '>1400万円</option>';
		echo '<option value="1500"';			if ($kalb_data == '1500') echo ' selected="selected"';			echo '>1500万円</option>';
		echo '<option value="1600"';			if ($kalb_data == '1600') echo ' selected="selected"';			echo '>1600万円</option>';
		echo '<option value="1700"';			if ($kalb_data == '1700') echo ' selected="selected"';			echo '>1700万円</option>';
		echo '<option value="1800"';			if ($kalb_data == '1800') echo ' selected="selected"';			echo '>1800万円</option>';
		echo '<option value="1900"';			if ($kalb_data == '1900') echo ' selected="selected"';			echo '>1900万円</option>';
		echo '<option value="2000"';			if ($kalb_data == '2000') echo ' selected="selected"';			echo '>2000万円</option>';
		echo '<option value="3000"';			if ($kalb_data == '3000') echo ' selected="selected"';			echo '>3000万円</option>';
		echo '<option value="5000"';			if ($kalb_data == '5000') echo ' selected="selected"';			echo '>5000万円</option>';
		echo '<option value="7000"';			if ($kalb_data == '7000') echo ' selected="selected"';			echo '>7000万円</option>';
		echo '<option value="10000"';			if ($kalb_data == '10000') echo ' selected="selected"';			echo '>1億円</option>';
		echo '</select>～';

		echo '<select name="kahb" id="kahb">';
		echo '<option value="300"';			if ($kahb_data == '300') echo ' selected="selected"';			echo '>300万円</option>';
		echo '<option value="400"';			if ($kahb_data == '400') echo ' selected="selected"';			echo '>400万円</option>';
		echo '<option value="500"';			if ($kahb_data == '500') echo ' selected="selected"';			echo '>500万円</option>';
		echo '<option value="600"';			if ($kahb_data == '600') echo ' selected="selected"';			echo '>600万円</option>';
		echo '<option value="700"';			if ($kahb_data == '700') echo ' selected="selected"';			echo '>700万円</option>';
		echo '<option value="800"';			if ($kahb_data == '800') echo ' selected="selected"';			echo '>800万円</option>';
		echo '<option value="900"';			if ($kahb_data == '900') echo ' selected="selected"';			echo '>900万円</option>';
		echo '<option value="1000"';			if ($kahb_data == '1000') echo ' selected="selected"';			echo '>1000万円</option>';
		echo '<option value="1100"';			if ($kahb_data == '1100') echo ' selected="selected"';			echo '>1100万円</option>';
		echo '<option value="1200"';			if ($kahb_data == '1200') echo ' selected="selected"';			echo '>1200万円</option>';
		echo '<option value="1300"';			if ($kahb_data == '1300') echo ' selected="selected"';			echo '>1300万円</option>';
		echo '<option value="1400"';			if ($kahb_data == '1400') echo ' selected="selected"';			echo '>1400万円</option>';
		echo '<option value="1500"';			if ($kahb_data == '1500') echo ' selected="selected"';			echo '>1500万円</option>';
		echo '<option value="1600"';			if ($kahb_data == '1600') echo ' selected="selected"';			echo '>1600万円</option>';
		echo '<option value="1700"';			if ($kahb_data == '1700') echo ' selected="selected"';			echo '>1700万円</option>';
		echo '<option value="1800"';			if ($kahb_data == '1800') echo ' selected="selected"';			echo '>1800万円</option>';
		echo '<option value="1900"';			if ($kahb_data == '1900') echo ' selected="selected"';			echo '>1900万円</option>';
		echo '<option value="2000"';			if ($kahb_data == '2000') echo ' selected="selected"';			echo '>2000万円</option>';
		echo '<option value="3000"';			if ($kahb_data == '3000') echo ' selected="selected"';			echo '>3000万円</option>';
		echo '<option value="5000"';			if ($kahb_data == '5000') echo ' selected="selected"';			echo '>5000万円</option>';
		echo '<option value="7000"';			if ($kahb_data == '7000') echo ' selected="selected"';			echo '>7000万円</option>';
		echo '<option value="10000"';			if ($kahb_data == '10000') echo ' selected="selected"';			echo '>1億円</option>';
		echo '<option value="0"';			if ($kahb_data == '0' ||$kahb_data == '' ) echo ' selected="selected"';			echo '>上限なし</option>';
		echo '</select>';
		echo '</div>';


		//駅歩分
		echo '<div id="hofun" class="hofun">';
		echo '駅歩分<br />';
		echo '<select name="hof" id="hof">';
		echo '<option value="0">指定なし</option>';
		echo '<option value="1"';
			if ($hof_data == '1') echo ' selected="selected"';
			echo '>1分以内</option>';
		echo '<option value="3"';
			if ($hof_data == '3') echo ' selected="selected"';
			echo '>3分以内</option>';
		echo '<option value="5"';
			if ($hof_data == '5') echo ' selected="selected"';
			echo '>5分以内</option>';
		echo '<option value="10"';
			if ($hof_data == '10') echo ' selected="selected"';
			echo '>10分以内</option>';
		echo '<option value="15"';
			if ($hof_data == '15') echo ' selected="selected"';
			echo '>15分以内</option>';
		echo '</select>';
		echo '</div>';




		//間取り
		if( $shu_data !='' ){

			//間取り
			$sql  =  "SELECT PM.meta_value AS madorisu,PM_2.meta_value AS madorisyurui";
			$sql .=  " FROM (((($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id)) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_G ON P.ID = PM_G.post_id ";

			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
			$sql .=  " AND PM.meta_key='madorisu'";
			$sql .=  " AND PM_2.meta_key='madorisyurui'";
			$sql .=  " AND PM_G.meta_key='bukkenido' AND PM_G.meta_value != '' ";

			$sql .=  " GROUP BY PM.meta_value,PM_2.meta_value";
			$sql .=  " ORDER BY CAST( PM.meta_value AS SIGNED ),CAST( PM_2.meta_value AS SIGNED )";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			$madori_dat = '';

			if(!empty($metas)) {

				foreach ( $metas as $meta ) {
					$madorisu_data = $meta['madorisu'];
					$madorisyurui_data = $meta['madorisyurui'];

					$madori_code = $madorisu_data;
					$madori_code .= $madorisyurui_data;

					if( $madorisu_data < 10 ){
						foreach( $work_madori as $meta_box ){
							if( $madorisyurui_data == $meta_box['code'] ){
								$madori_dat .= '<span style="display: inline-block"><input name="mad[]" value="'.$madori_code.'" id="mad'.$madori_code.'" type="checkbox"';
								if(is_array($madori_id)) {
									foreach($madori_id as $meta_box4)
										if( $meta_box4 == $madori_code ) $madori_dat .= ' checked="checked"';
								}
								$madori_dat .= ' /> <label for="mad'.$madori_code.'">'.$madorisu_data.$meta_box['name'].'</label></span> ';
							}
						}
					}
				}
			}
		}

		if($madori_dat != '' ){
			echo '<div id="madori_cb" class="madori_cb">間取り<br />'. $madori_dat .'</div>';
		}else{
			echo '<div id="madori_cb" class="madori_cb"></div>';
		}


		//築年数
		echo '<div id="chikunen" class="chikunen">';
		echo '築年数<br />';
		echo '<select name="tik" id="tik">';
		echo '<option value="0">指定なし</option>';
		echo '<option value="1"';			if ($tik_data == '1') echo ' selected="selected"';			echo '>1年以内</option>';
		echo '<option value="3"';			if ($tik_data == '3') echo ' selected="selected"';			echo '>3年以内</option>';
		echo '<option value="5"';			if ($tik_data == '5') echo ' selected="selected"';			echo '>5年以内</option>';
		echo '<option value="10"';			if ($tik_data == '10') echo ' selected="selected"';			echo '>10年以内</option>';
		echo '<option value="15"';			if ($tik_data == '15') echo ' selected="selected"';			echo '>15年以内</option>';
		echo '<option value="20"';			if ($tik_data == '20') echo ' selected="selected"';			echo '>20年以内</option>';
		echo '</select>';
		echo '</div>';


		echo '<div id="memseki" class="memseki">';
		//面積
		echo '面積<br />';
		echo '<select name="mel" id="mel">';
		echo '<option value="0">下限なし</option>';
		echo '<option value="10"';			if ($mel_data == '10') echo ' selected="selected"';			echo '>10m&sup2;</option>';
		echo '<option value="15"';			if ($mel_data == '15') echo ' selected="selected"';			echo '>15m&sup2;</option>';
		echo '<option value="20"';			if ($mel_data == '20') echo ' selected="selected"';			echo '>20m&sup2;</option>';
		echo '<option value="25"';			if ($mel_data == '25') echo ' selected="selected"';			echo '>25m&sup2;</option>';
		echo '<option value="30"';			if ($mel_data == '30') echo ' selected="selected"';			echo '>30m&sup2;</option>';
		echo '<option value="35"';			if ($mel_data == '35') echo ' selected="selected"';			echo '>35m&sup2;</option>';
		echo '<option value="40"';			if ($mel_data == '40') echo ' selected="selected"';			echo '>40m&sup2;</option>';
		echo '<option value="50"';			if ($mel_data == '50') echo ' selected="selected"';			echo '>50m&sup2;</option>';
		echo '<option value="60"';			if ($mel_data == '60') echo ' selected="selected"';			echo '>60m&sup2;</option>';
		echo '<option value="70"';			if ($mel_data == '70') echo ' selected="selected"';			echo '>70m&sup2;</option>';
		echo '<option value="80"';			if ($mel_data == '80') echo ' selected="selected"';			echo '>80m&sup2;</option>';
		echo '<option value="90"';			if ($mel_data == '90') echo ' selected="selected"';			echo '>90m&sup2;</option>';
		echo '<option value="100"';			if ($mel_data == '100') echo ' selected="selected"';			echo '>100m&sup2;</option>';
		echo '<option value="200"';			if ($mel_data == '200') echo ' selected="selected"';			echo '>200m&sup2;</option>';
		echo '<option value="300"';			if ($mel_data == '300') echo ' selected="selected"';			echo '>300m&sup2;</option>';
		echo '<option value="400"';			if ($mel_data == '400') echo ' selected="selected"';			echo '>400m&sup2;</option>';
		echo '<option value="500"';			if ($mel_data == '500') echo ' selected="selected"';			echo '>500m&sup2;</option>';
		echo '<option value="600"';			if ($mel_data == '600') echo ' selected="selected"';			echo '>600m&sup2;</option>';
		echo '<option value="700"';			if ($mel_data == '700') echo ' selected="selected"';			echo '>700m&sup2;</option>';
		echo '<option value="800"';			if ($mel_data == '800') echo ' selected="selected"';			echo '>800m&sup2;</option>';
		echo '<option value="900"';			if ($mel_data == '900') echo ' selected="selected"';			echo '>900m&sup2;</option>';
		echo '<option value="1000"';			if ($mel_data == '1000') echo ' selected="selected"';			echo '>1000m&sup2;</option>';
		echo '</select>～';

		echo '<select name="meh" id="meh">';
		echo '<option value="10"';			if ($meh_data == '10') echo ' selected="selected"';			echo '>10m&sup2;</option>';
		echo '<option value="15"';			if ($meh_data == '15') echo ' selected="selected"';			echo '>15m&sup2;</option>';
		echo '<option value="20"';			if ($meh_data == '20') echo ' selected="selected"';			echo '>20m&sup2;</option>';
		echo '<option value="25"';			if ($meh_data == '25') echo ' selected="selected"';			echo '>25m&sup2;</option>';
		echo '<option value="30"';			if ($meh_data == '30') echo ' selected="selected"';			echo '>30m&sup2;</option>';
		echo '<option value="35"';			if ($meh_data == '35') echo ' selected="selected"';			echo '>35m&sup2;</option>';
		echo '<option value="40"';			if ($meh_data == '40') echo ' selected="selected"';			echo '>40m&sup2;</option>';
		echo '<option value="50"';			if ($meh_data == '50') echo ' selected="selected"';			echo '>50m&sup2;</option>';
		echo '<option value="60"';			if ($meh_data == '60') echo ' selected="selected"';			echo '>60m&sup2;</option>';
		echo '<option value="70"';			if ($meh_data == '70') echo ' selected="selected"';			echo '>70m&sup2;</option>';
		echo '<option value="80"';			if ($meh_data == '80') echo ' selected="selected"';			echo '>80m&sup2;</option>';
		echo '<option value="90"';			if ($meh_data == '90') echo ' selected="selected"';			echo '>90m&sup2;</option>';
		echo '<option value="100"';			if ($meh_data == '100') echo ' selected="selected"';			echo '>100m&sup2;</option>';
		echo '<option value="200"';			if ($meh_data == '200') echo ' selected="selected"';			echo '>200m&sup2;</option>';
		echo '<option value="300"';			if ($meh_data == '300') echo ' selected="selected"';			echo '>300m&sup2;</option>';
		echo '<option value="400"';			if ($meh_data == '400') echo ' selected="selected"';			echo '>400m&sup2;</option>';
		echo '<option value="500"';			if ($meh_data == '500') echo ' selected="selected"';			echo '>500m&sup2;</option>';
		echo '<option value="600"';			if ($meh_data == '600') echo ' selected="selected"';			echo '>600m&sup2;</option>';
		echo '<option value="700"';			if ($meh_data == '700') echo ' selected="selected"';			echo '>700m&sup2;</option>';
		echo '<option value="800"';			if ($meh_data == '800') echo ' selected="selected"';			echo '>800m&sup2;</option>';
		echo '<option value="900"';			if ($meh_data == '900') echo ' selected="selected"';			echo '>900m&sup2;</option>';
		echo '<option value="1000"';			if ($meh_data == '1000') echo ' selected="selected"';			echo '>1000m&sup2;</option>';
		echo '<option value="0"';			if ($meh_data == '0' ||$meh_data == '' ) echo ' selected="selected"';			echo '>上限なし</option>';
		echo '</select>';
		echo '</div>';


		//設備
		if( $shu_data !='' ){

			$widget_seach_setsubi = maybe_unserialize( get_option('widget_seach_setsubi') );

			$sql  =  "SELECT PM.meta_value as setsubi";
			$sql .=  " FROM (($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_G ON P.ID = PM_G.post_id ";

			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
			$sql .=  " AND PM.meta_key='setsubi'";
			$sql .=  " AND PM_G.meta_key='bukkenido' AND PM_G.meta_value != '' ";

			$sql .=  " GROUP BY PM.meta_value";
			$sql .=  " ORDER BY CAST( PM.meta_value AS SIGNED )";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$array_setsubi = array();

			if(!empty($metas)) {
				foreach($work_setsubi as $meta_box){

					foreach ( $metas as $meta ) {
						$setsubi_data = $meta['setsubi'];

						if( strpos($setsubi_data,$meta_box['code']) ){

							$setsubi_code = $meta_box['code'];
							$setsubi_name = $meta_box['name'];

							$data = array( $setsubi_code => array("code" => $setsubi_code,"name" => $setsubi_name));

							foreach($array_setsubi as $meta_box2){
								if ( $setsubi_code == $meta_box2['code'])
									$data = '';
							}
							if(!empty($data))
							$array_setsubi = array_merge( $data , $array_setsubi);
						}
					}
				}
			}

			if(!empty($array_setsubi)) {

				krsort($array_setsubi);

				$setsubi_dat ='';

				foreach($array_setsubi as $meta_box3){


					//$widget_seach_setsubi
					if(is_array($widget_seach_setsubi)) {
						$k=0;
						foreach($widget_seach_setsubi as $meta_box5){
							if($widget_seach_setsubi[$k] == $meta_box3['code']){

								$setsubi_dat .= '<span style="display: inline-block">';
								$setsubi_dat .= '<input type="checkbox" name="set[]"  value="'.$meta_box3['code'].'" id="'.$meta_box3['code'].'"';
									if(is_array($set_id)) {
										foreach($set_id as $meta_box4)
											if( $meta_box4 == $meta_box3['code'] ) $setsubi_dat .= ' checked="checked"';
									}
								$setsubi_dat .= '">';
								$setsubi_dat .= ' <label for="'.$meta_box3['code'].'">'.$meta_box3['name'].'</label>';
								$setsubi_dat .= '</span> ';
							}
							$k++;
						}
					}else{

								$setsubi_dat .= '<span style="display: inline-block">';
								$setsubi_dat .= '<input type="checkbox" name="set[]"  value="'.$meta_box3['code'].'" id="'.$meta_box3['code'].'"';
									if(is_array($set_id)) {
										foreach($set_id as $meta_box4)
											if( $meta_box4 == $meta_box3['code'] ) $setsubi_dat .= ' checked="checked"';
									}
								$setsubi_dat .= '">';
								$setsubi_dat .= ' <label for="'.$meta_box3['code'].'">'.$meta_box3['name'].'</label>';
								$setsubi_dat .= '</span> ';
					}
				}
			}
		}

		if( $setsubi_dat != '' ){
			echo '<div id="setsubi_cb" class="setsubi_cb">設備・条件<br />'. $setsubi_dat .'</div>';
		}else{
			echo '<div id="setsubi_cb" class="setsubi_cb"></div>';
		}

		echo '<input type="submit" id="btn" name="btn" value="物件検索" />';
	//	echo '　<input type="button" id="btn2" name="btn2" value="マップトップへ" onClick="location.href=\'?page=map\';" />';
		
		echo '</form>';

	if ( $is_fudoukaiin && get_option('kaiin_users_can_register') ) { 
		if (!is_user_logged_in() ) { 
?>
		<br />
		<a href="<?php echo WP_PLUGIN_URL;?>/fudoukaiin/wp-login.php?action=login&KeepThis=true&TB_iframe=true&height=500&width=400" class="thickbox">ログイン</a> | 
		<a href="<?php echo WP_PLUGIN_URL;?>/fudoukaiin/wp-login.php?action=lostpassword&KeepThis=true&TB_iframe=true&height=270&width=400" class="thickbox">パスワード忘れた</a>
<?php
		}
	}

		echo '</div>';
		echo '</div>';

?>
