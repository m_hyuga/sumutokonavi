<?php
/**
 * The Template for displaying fudou page posts.
 *
 * Template Name: page-map.php
 * 
 * @package WordPress3.0
 * @subpackage Fudousan Plugin
 * Fudousan Map Plugin
 * Version: 1.3.5
 */


	//半角数字チェック   
	if (!function_exists('myIsNum_m')) {
		function myIsNum_m($value) {
			if (preg_match("/^[0-9]+$/", $value)) {
				return $value;
			}
			return '';
		}
	}

	global $wpdb;


		//条件表示 0 無し
		$int_jyo = isset($_GET['jyo']) ? $_GET['jyo'] : '';
		if($int_jyo == '') $int_jyo = '1';

		//初期値
		$int_zoom = isset($_GET['zom']) ? myIsNum_m($_GET['zom']) : '';

		$int_lat = isset($_GET['lat']) ? $_GET['lat'] : '';		//座標初期値
		$int_lng = isset($_GET['lng']) ? $_GET['lng'] : '';		//座標初期値

		$bukken_shubetsu = isset($_GET['shu']) ? $_GET['shu'] : '';	//種別
		$madori_id = isset($_GET['mad']) ? $_GET['mad'] : '';		//間取り
		$set_id = isset($_GET['set']) ? $_GET['set'] : '';		//設備

		$ros_id = isset($_GET['ros']) ? myIsNum_m($_GET['ros']) : '';	//路線
		$eki_id = isset($_GET['eki']) ? myIsNum_m($_GET['eki']) : '';	//駅
		$ken_id = isset($_GET['ken']) ? myIsNum_m($_GET['ken']) : '';	//県
		$sik_id = isset($_GET['sik']) ? myIsNum_m($_GET['sik']) : '';	//市区

		$ken_id=sprintf("%02d",$ken_id);


		$kalb_data = isset($_GET['kalb']) ? myIsNum_m($_GET['kalb']) : '';	//価格下限
		$kahb_data = isset($_GET['kahb']) ? myIsNum_m($_GET['kahb']) : '';	//価格上限
		$kalc_data = isset($_GET['kalc']) ? myIsNum_m($_GET['kalc']) : '';	//賃料下限
		$kahc_data = isset($_GET['kahc']) ? myIsNum_m($_GET['kahc']) : '';	//賃料上限
		$hof_data =  isset($_GET['hof'])  ? myIsNum_m($_GET['hof'])  : '';	//歩分


		$tik_data = isset($_GET['tik']) ? myIsNum_m($_GET['tik']) : '';		//築年数
		$mel_data = isset($_GET['mel']) ? myIsNum_m($_GET['mel']) : '';		//面積下限
		$meh_data = isset($_GET['meh']) ? myIsNum_m($_GET['meh']) : '';		//面積上限

		$geocode =  isset($_GET['geo']) ? myIsNum_m($_GET['geo']) : '';		//地域・駅(text)
	//	if( $geocode != '' )
	//		$geocode =mb_convert_encoding($geocode, 'UTF-8', 'SJIS') ;

		//url生成

		//間取り
		$madori_url = '';
		if(!empty($madori_id)) {
			$i=0;
			foreach($madori_id as $meta_box){
				$madori_url .= '&mad[]='.$madori_id[$i];
				$i++;
			}
		}


		//設備条件
		$setsubi_url = '';
		if(!empty($set_id)) {
			$i=0;
			foreach($set_id as $meta_box){
				$setsubi_url .= '&set[]='.$set_id[$i];
				$i++;
			}
		}

		$add_url  = '';

		//複数種別
		if (is_array($bukken_shubetsu)) {
			$i=0;
			foreach($bukken_shubetsu as $meta_set){
				$add_url  .= '&shu[]='.$bukken_shubetsu[$i];
				$i++;
			}

		} else {
			$add_url  .= '&shu='.$bukken_shubetsu;
		}


		$add_url .= '&ros='.$ros_id;
		$add_url .= '&eki='.$eki_id;
		$add_url .= '&ken='.$ken_id;
		$add_url .= '&sik='.$sik_id;
		$add_url .= '&kalc='.$kalc_data;
		$add_url .= '&kahc='.$kahc_data;
		$add_url .= '&kalb='.$kalb_data;
		$add_url .= '&kahb='.$kahb_data;
		$add_url .= '&hof='.$hof_data;
		$add_url .= $madori_url;
		$add_url .= '&tik='.$tik_data;
		$add_url .= '&mel='.$mel_data;
		$add_url .= '&meh='.$meh_data;
		$add_url .= $setsubi_url;




		if( $ros_id > 0 && $eki_id > 0){


			$sql  = "SELECT TA.lat , TA.lng ";
			$sql .=  " FROM ".$wpdb->prefix."train_latlng AS TA";
			$sql .=  " WHERE TA.rosen_id = ".$ros_id." AND TA.station_id = ".$eki_id." ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_row( $sql );

			if($metas){
				$int_lat = $metas->lat;
				$int_lng = $metas->lng;
			}else{

				$sql  = "SELECT TR.rosen_name, TS.station_name , MA.middle_area_name ";
				$sql .= "FROM (".$wpdb->prefix."train_rosen AS TR ";
				$sql .= "INNER JOIN ".$wpdb->prefix."train_station AS TS ON TR.rosen_id = TS.rosen_id) ";
				$sql .= "INNER JOIN ".$wpdb->prefix."area_middle_area AS MA ON TS.middle_area_id = MA.middle_area_id ";
				$sql .= "WHERE TR.rosen_id=".$ros_id." AND TS.station_id=".$eki_id." ";

				$sql = $wpdb->prepare($sql,'');
				$metas = $wpdb->get_row( $sql );

				if($metas){
					$geocode = $metas->middle_area_name . ' ';
				//	$geocode = $metas->rosen_name . ' ';
					$geocode .= $metas->station_name . '駅';
				}

			}

			if($int_zoom == '' )
				$int_zoom = '15';

		}


		if( $ken_id > 0 && $sik_id > 0){

			$sql  = "SELECT MA.middle_area_name, NA.narrow_area_name ";
			$sql .= "FROM ".$wpdb->prefix."area_middle_area AS MA ";
			$sql .= "INNER JOIN ".$wpdb->prefix."area_narrow_area AS NA ON MA.middle_area_id = NA.middle_area_id ";
			$sql .= "WHERE MA.middle_area_id=".$ken_id." AND NA.narrow_area_id=".$sik_id." ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_row( $sql );
			$geocode = $metas->middle_area_name;
			$geocode .= $metas->narrow_area_name;

			if($int_zoom == '' )
				$int_zoom = '15';
		}



		if($int_zoom == '' )
			$int_zoom = get_option('fudoumap_zoom');
		if($int_zoom == '' )
			$int_zoom = '15';



		//座標初期値
		if($int_lat == '' || $int_lng == '' ){
			$int_lat = get_option('fudoumap_lat');
			$int_lng = get_option('fudoumap_lng');
		}

		if($int_lat == '' || $int_lng == '' ){
			$int_lat = '35.681323';	
			$int_lng = '139.767650';
		}


		//ヘッダ
		$fudoumap_title = get_option('fudoumap_title');
		$fudoumap_keywords = get_option('fudoumap_keywords');
		$fudoumap_description = get_option('fudoumap_description');


		$web_property_id = get_option('web_property_id');


		$tenpo_form = get_option('tenpo_form');
		$tenpo_form = str_replace(array("\r\n","\r","\n"), '', $tenpo_form);
		$tenpo_lat = get_option('tenpo_lat');
		$tenpo_lng = get_option('tenpo_lng');
		$tenpo_img = get_option('tenpo_img');


//get_header(); 

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;


	if( $fudoumap_title !='' ){
		echo '' . $fudoumap_title . '';
	}else{
		wp_title( '|', true, 'right' );
	}

	echo '</title>';


	if($fudoumap_keywords !='') 
		echo '<meta name="keywords" content="' . $fudoumap_keywords . '" />' ;
	if($fudoumap_description !='') 
		echo '<meta name="description" content="' . $fudoumap_description . '" />' ;

	?>


<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo WP_PLUGIN_URL;?>/fudoumap/gmap2.css" media="all" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();

	wp_deregister_script('googlemapapi');
	wp_enqueue_script('googlemapapi', 'http://maps.googleapis.com/maps/api/js?&sensor=false');

?>
<script type="text/javascript">
<!-- <![CDATA[
	var tenpo_form = '<?php echo $tenpo_form;?>';
	var tenpo_lat =  '<?php echo $tenpo_lat;?>';
	var tenpo_lng =  '<?php echo $tenpo_lng;?>';
	var tenpo_img =  '<?php echo $tenpo_img;?>';
	var address = "<?php echo $geocode;?>";
	var int_lat = '<?php echo $int_lat;?>';
	var int_lng = '<?php echo $int_lng;?>';
	var int_zoom = <?php echo $int_zoom;?>;
	var add_url = '<?php echo $add_url;?>';
	var int_jyo= <?php echo $int_jyo;?>;
	var getsite='<?php echo WP_PLUGIN_URL;?>/fudoumap/';
// ]]> -->
</script>
<script type="text/javascript" src="<?php echo WP_PLUGIN_URL;?>/fudoumap/js/util.js"></script>
<script type="text/javascript" src="<?php echo WP_PLUGIN_URL;?>/fudoumap/js/gmap2b_121min.js"></script>

</head>

<body <?php body_class(); ?> onload="initialize()">
<div id="wrapper" class="hfeed">
	<div id="header">
		<div id="masthead">
			<div id="branding" role="banner">
				<?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>
				<<?php echo $heading_tag; ?> id="site-title">
					<span>
						<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
					</span>
				</<?php echo $heading_tag; ?>>
				<div id="site-description"><?php bloginfo( 'description' ); ?></div>

				<?php
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID );
					elseif ( get_header_image() ) : ?>
						<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="" />
					<?php endif; ?>
			</div><!-- #branding -->

			<div id="access" role="navigation">
			  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
				<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
				<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div><!-- #access -->
		</div><!-- #masthead -->
	</div><!-- #header -->

	<div id="main">

		<div id="container">
			<div id="content" role="main">
				<div id="map_canvas"></div>
				<div id="map_copy"> </div>
				<div id="map_adv"> </div>

			</div><!-- .#content -->
		</div><!-- .#container -->

		<div id="primary" class="widget-area" role="complementary">
			<div id="map_menu">
				<div id="map_search"><?php require_once 'gsearch2.php'; ?></div>
				<script type="text/javascript" src="<?php echo WP_PLUGIN_URL;?>/fudoumap/js/jmapsearch.js"></script>
			</div>
<!--
			<div id="menu_botton">
				<a href="<?php echo WP_PLUGIN_URL;?>/fudoumap/help/help2.html?height=460&width=450"  class="thickbox"><img src="<?php echo WP_PLUGIN_URL;?>/fudoumap/img/gmap_sb2.png" alt="ヘルプページ" title="ヘルプページ" border="0" /></a>
			</div>
-->
		</div>

<?php get_footer(); ?>


