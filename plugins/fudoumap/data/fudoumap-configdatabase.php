<?php
/*
 * 不動産マッププラグインデーターベース設定
 * @package WordPress3.3
 * @subpackage Fudousan Plugin
 * Version: 1.2.6
*/

function databaseinstallation_fudoumap($state){
	global $wpdb;

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	$char = defined("DB_CHARSET") ? DB_CHARSET : "utf8";

	//テーブル train_latlng
	$table_name = $wpdb->prefix . "train_latlng";
	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
		$table2 =  "CREATE TABLE IF NOT EXISTS " . $table_name . " (";
		$table2 .= "	rosen_id int(11) NOT NULL,";
		$table2 .= "	station_id int(11) NOT NULL,";
		$table2 .= "	lat text NOT NULL,";
		$table2 .= "	lng text NOT NULL,";
		$table2 .= "	KEY rosen_id (rosen_id,station_id)";
		$table2 .= "	) DEFAULT CHARSET=$char;";
		$result = $wpdb->query($table2);
		dbDelta($table2);
	}
}
?>
