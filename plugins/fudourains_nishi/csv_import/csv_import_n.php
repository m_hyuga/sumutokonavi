<?php
/** 
 * @author    nendeb <nendeb@gmail.com>
 * @copyright 2014 nendeb
 * @package WordPress4.0
 * @Version: 1.5.0
 */

function csv_admin_menu_nishi() {
	require_once ABSPATH . '/wp-admin/admin.php';
	$plugin = new CSVImporterPlugin_nishi;
	add_management_page('n_rains', '西日本レインズCSV取込', 'edit_pages', __FILE__, array($plugin, 'form'));
}
add_action('admin_menu', 'csv_admin_menu_nishi');


class CSVImporterPlugin_nishi {

	public function __construct(){
		$log = array();
		$skipped = 0;
		$imported = 0;
		$comments = 0;
		$updated = 0;
		$addnew = true;
		$err_log = '';
	}

	function process_option($name, $default, $params) {

		if (array_key_exists($name, $params)) {
			$value = stripslashes($params[$name]);
		} elseif (array_key_exists('_'.$name, $params)) {
			// unchecked checkbox value
			$value = stripslashes($params['_'.$name]);
		} else {
			$value = null;
		}
		$stored_value = get_option($name);
		if ($value == null) {
			if ($stored_value === false) {
				if (is_callable($default) && method_exists($default[0], $default[1])) {
					$value = call_user_func($default);
				} else {
					$value = $default;
				}
			add_option($name, $value);
			} else {
				$value = $stored_value;
			}
		} else {
			if ($stored_value === false) {
				add_option($name, $value);
			} elseif ($stored_value != $value) {
				update_option($name, $value);
			}
		}
		return $value;
	}



	// Plugin's interface
	public function form() {

		global $opt_csv,$opt_overwrite,$opt_draft,$opt_kaiin,$opt_shurui;

		$opt_draft = $this->process_option('csv_importer_import_as_draft','publish', $_POST);
		$opt_type = $this->process_option('csv_importer_import_post_type','post', $_POST);
		$opt_csv = $this->process_option('csv_importer_import_csv_type', 'csv', $_POST);
		$opt_overwrite = $this->process_option('csv_importer_import_csv_overwrite', 'no', $_POST);

		$opt_kaiin = $this->process_option('csv_importer_import_csv_kaiin', '', $_POST);
		$opt_shurui = $this->process_option('csv_importer_import_csv_shurui', '', $_POST);


		if ('POST' == $_SERVER['REQUEST_METHOD']) {
			$this->post( compact('opt_draft','opt_type','opt_csv','$opt_overwrite') );
		} 

		?>
		<style type="text/css"> 
		<!--
		.k_checkbox {
			display: inline-block;
			margin: 0 1em 0 0;
		}
		#post-body-content {
			margin: 0 auto;;
			line-height: 1.5;
			padding:16px 16px 30px;
			border-radius:11px;
			background:#fff;
			border:1px solid #e5e5e5;
			box-shadow:rgba(200, 200, 200, 1) 0 4px 18px;
			width: 90%;
			font-size: 12px;
		}
		// -->
		</style>

		<div class="wrap">
			<div id="icon-tools" class="icon32"><br /></div>
			<h2>西日本レインズCSV取込</h2>
			<div id="poststuff">

			<div id="post-body">
			<div id="post-body-content">


			<form class="add:the-list: validate" method="post" enctype="multipart/form-data">

			<!-- Import as draft -->
			<p>下書き
			<input name="_csv_importer_import_as_draft" type="hidden" value="publish" />
			<label><input name="csv_importer_import_as_draft" type="checkbox" <?php if ('draft' == $opt_draft) { echo 'checked="checked"'; } ?> value="draft" /> インポート時に全て「下書き」として登録</label>
			</p>

			<!-- Import pages or posts -->
			<input name="_csv_importer_import_post_type" type="hidden" value="fudo" />
			<input name="csv_importer_import_post_type" type="hidden" value="fudo" />

			<!-- Import as update or skip -->
			<input name="_csv_importer_import_csv_overwrite" type="hidden" value="no" />
			<p>物件番号重複処理
			<select name="csv_importer_import_csv_overwrite">
				<option value="no" <?php if ('no' == $opt_overwrite) { echo 'selected="selected"'; } ?>>スキップする</option>
				<option value="yes" <?php if ('yes' == $opt_overwrite) { echo 'selected="selected"'; } ?>>上書きする</option>
			</select> *「上書きする」は時間がかかります。
			</p>

			<!-- Import as kaiin or ippan -->
			<input name="_csv_importer_import_csv_kaiin" type="hidden" value="0" />
			<p>会員物件
			<select name="csv_importer_import_csv_kaiin">
				<option value="0" <?php if ('0' == $opt_kaiin) { echo 'selected="selected"'; } ?>>一般</option>
				<option value="1" <?php if ('1' == $opt_kaiin) { echo 'selected="selected"'; } ?>>会員</option>
			</select> *会員にする場合は「不動産会員プラグイン」が必要です。
			</p>


			<!-- Import as csv type -->
			<p>CSVタイプ
			<select name="csv_importer_import_csv_type">
				<option value="n_rains">西日本レインズ</option>
			</select>
			</p>

			<p>種類/種別選択
			<select name="csv_importer_import_csv_shurui">
				<option>物件種別を選択</option>
				<option value="11" <?php if ('11' == $opt_shurui) { echo 'selected="selected"'; } ?>>【売買】土地</option>
				<option value="12" <?php if ('12' == $opt_shurui) { echo 'selected="selected"'; } ?>>【売買】一戸建て</option>
				<option value="13" <?php if ('13' == $opt_shurui) { echo 'selected="selected"'; } ?>>【売買】マンション等</option>
				<option value="14" <?php if ('14' == $opt_shurui) { echo 'selected="selected"'; } ?>>【売買】住宅以外の建物全部</option>
				<option value="15" <?php if ('15' == $opt_shurui) { echo 'selected="selected"'; } ?>>【売買】住宅以外の建物一部</option>
				<option value="31" <?php if ('31' == $opt_shurui) { echo 'selected="selected"'; } ?>>【賃貸】土地</option>
				<option value="32" <?php if ('32' == $opt_shurui) { echo 'selected="selected"'; } ?>>【賃貸】一戸建</option>
				<option value="33" <?php if ('33' == $opt_shurui) { echo 'selected="selected"'; } ?>>【賃貸】マンション</option>
				<option value="34" <?php if ('34' == $opt_shurui) { echo 'selected="selected"'; } ?>>【賃貸】住宅以外建物全部</option>
				<option value="35" <?php if ('35' == $opt_shurui) { echo 'selected="selected"'; } ?>>【賃貸】住宅以外建物一部</option>
			</select>
			</p>

		        <!-- File input -->
		        <p>CSVアップロードファイル<br/>
		            <input name="csv_import" id="csv_import" type="file" value="" aria-required="true" size="40" /></p>
			<p class="submit"><input type="submit" class="button" name="submit" value="取込み" /></p>
			</form>
			</div>
			</div>
			</div>


		</div><!-- end wrap -->
	<?php

	}



	function print_messages() {

		if (!empty($this->log)) {
			?>
			<div class="wrap">
				<?php if (!empty($this->log['error'])): ?>
					<div class="error">    
						<?php foreach ($this->log['error'] as $error): ?>
						<p><?php echo $error; ?></p>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php if (!empty($this->log['notice'])): ?>
					<div class="updated fade">
					<?php foreach ($this->log['notice'] as $notice): ?>
					<p><?php echo $notice; ?></p>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div><!-- end wrap -->
			<?php

			$this->log = array();
		}
	}


	// Handle POST submission
	function post( $options ) {

		global $imported,$updated,$skipped;
		global $addnew,$opt_overwrite;
		global $err_log;
		global $opt_shurui;

		$local_upload_filename = isset($_FILES['csv_import']['name']) ? $_FILES['csv_import']['name'] : '';

		//set_time_limit(120);

		require_once 'datasource.php';

		if (empty($_FILES['csv_import']['tmp_name'])) {
			$this->log['error'][] = 'No file uploaded, aborting.';
			$this->print_messages();
			return;
		}

		$time_start = microtime(true);
		$csv = new File_CSV_DataSource;
		$file = $_FILES['csv_import']['tmp_name'];

		//UTF-8
		//$this->stripBOM($file);

		if (!$csv->load($file)) {
			$this->log['error'][] = 'Failed to load file, aborting.';
			$this->print_messages();
			return;
		}

		// pad shorter rows with empty values
		$csv->symmetrize();

		// WordPress sets the correct timezone for date functions somewhere
		// in the bowels of wp_insert_post(). We need strtotime() to return
		// correct time before the call to wp_insert_post().
		$tz = get_option('timezone_string');
		if ($tz && function_exists('date_default_timezone_set')) {
			date_default_timezone_set($tz);
		}


		echo '<div id="running">';
		foreach ($csv->connect() as $csv_data) {

			if ($post_id = $this->create_post($csv_data, $options)) {

				//上書き許可
				if( $addnew == true ){
					$this->create_custom_fields($post_id, $csv_data);
						echo str_pad('',256);
						echo '*';
						flush();

				}else{
					if( $opt_overwrite == 'yes' ){
						$this->create_custom_fields($post_id, $csv_data);
						echo str_pad('',256);
						echo '*';
						flush();
					}else{
				                $skipped++;
				                $imported--;
					}
				}

			} else {
				$skipped++;
				$imported--;
			}
		}
		echo '</div>';

		if (file_exists($file)) {
			@unlink($file);
		}

		$exec_time = microtime(true) - $time_start;

		if ($skipped) {
			$this->log['notice'][] = "<b>{$skipped} 件、登録できませんでした。</b>";
		}

		if($imported =='') $imported=0;
		if($updated =='') $updated=0;

		$this->log['notice'][] = sprintf("<b>%s</b>　新規 {$imported} 件、更新 {$updated} 件、 登録しました。( %.2f sec )  %s", $local_upload_filename , $exec_time , $err_log);
		$this->print_messages();

		echo "\n";
		echo '<style type="text/css">';
		echo '<!--';
		echo "\n";
		echo 'div#running { display:none; }';
		echo "\n";
		echo '// -->';
		echo '</style>';
		echo "\n";
	}

	function create_post($data, $options) {

		global $wpdb,$imported,$updated;
		global $opt_csv,$opt_shurui;
		global $addnew;
		global $opt_overwrite,$opt_draft;
		global $user_ID;

		global $shubetsu_data;
		global $err_log;

		extract($options);

		if($opt_csv == 'n_rains'){

			$tmp_opt_draft_h = $opt_draft;

			if( strpos($data['備考'], '不可') !== false) 			$tmp_opt_draft_h = "draft";
			if( strpos($data['備考'], '厳禁') !== false)			$tmp_opt_draft_h = "draft";
			if( strpos($data['備考'], '掲載禁止') !== false)		$tmp_opt_draft_h = "draft";

			if( strpos($data['条件等'], '不可') !== false) 			$tmp_opt_draft_h = "draft";
			if( strpos($data['条件等'], '厳禁') !== false)			$tmp_opt_draft_h = "draft";
			if( strpos($data['条件等'], '掲載禁止') !== false)		$tmp_opt_draft_h = "draft";

			if( $data['登録日'] !='' ){
				$tmp_post_date_h = $this->parse_date4($data['登録日']);
			}else{
				$tmp_post_date_h = '';
			}


			//タイトル
			$shubetsu_data = '';
			$bukken_shum = $data['種目'];
			$bukkenshubetsu_data = '';

			if( $opt_shurui && $bukken_shum ){

				switch ( $opt_shurui ) {

					//【売買】土地
					case 11:
						switch ($bukken_shum) {
									case "売地":
										$shubetsu_data = '1101'; $bukkenshubetsu_data = '売地'; 	break;
									case "借地権":
										$shubetsu_data = '1102'; $bukkenshubetsu_data = '借地権'; 	break;
									case "底地権":
										$shubetsu_data = '1103'; $bukkenshubetsu_data = '底地権'; 	break;
						}
						break;
					//【売買】一戸建て
					case 12:
						switch ($bukken_shum) {
							//戸建
									case "新築一戸建":
										$shubetsu_data = '1201'; $bukkenshubetsu_data = '新築戸建'; 	break;
									case "中古一戸建":
										$shubetsu_data = '1202'; $bukkenshubetsu_data = '中古戸建'; 	break;
									case "新築テラスハウス":
										$shubetsu_data = '1203'; $bukkenshubetsu_data = '新築テラス'; 	break;
									case "中古テラスハウス":
										$shubetsu_data = '1204'; $bukkenshubetsu_data = '中古テラス'; 	break;
						}
						break;
					//【売買】マンション等
					case 13:
						switch ($bukken_shum) {
									case "新築マンション":
										$shubetsu_data = '1301'; $bukkenshubetsu_data = '新築マンション'; 	break;
									case "中古マンション":
										$shubetsu_data = '1302'; $bukkenshubetsu_data = '中古マンション'; 	break;
									case "新築タウンハウス":
										$shubetsu_data = '1307'; $bukkenshubetsu_data = '新築タウン'; 	break;
									case "中古タウンハウス":
										$shubetsu_data = '1308'; $bukkenshubetsu_data = '中古タウン'; 	break;
									case "新築リゾートマンション":
									case "中古リゾートマンション":
										$shubetsu_data = '1309'; $bukkenshubetsu_data = 'リゾートマンション'; 	break;
									case "その他":
										$shubetsu_data = '1399'; $bukkenshubetsu_data = 'マンションその他'; 	break;
						}
						break;
					//【【売買】住宅以外の建物全部
					case 14:
						switch ($bukken_shum) {
									case "店舗":
										$shubetsu_data = '1401'; $bukkenshubetsu_data = '店舗'; 	break;
									case "店舗付き住宅":
										$shubetsu_data = '1403'; $bukkenshubetsu_data = '店付住宅'; 	break;
									case "住宅付き店舗":
										$shubetsu_data = '1404'; $bukkenshubetsu_data = '住付店舗'; 	break;
									case "事務所":
										$shubetsu_data = '1405'; $bukkenshubetsu_data = '事務所'; 	break;
									case "店舗事務所":
										$shubetsu_data = '1406'; $bukkenshubetsu_data = '店舗事務'; 	break;
									case "ビル":
										$shubetsu_data = '1407'; $bukkenshubetsu_data = 'ビル'; 	break;
									case "工場":
										$shubetsu_data = '1408'; $bukkenshubetsu_data = '工場'; 	break;
									case "マンション":
										$shubetsu_data = '1409'; $bukkenshubetsu_data = 'マンション'; 	break;
									case "倉庫":
										$shubetsu_data = '1410'; $bukkenshubetsu_data = '倉庫'; 	break;
									case "アパート":
										$shubetsu_data = '1411'; $bukkenshubetsu_data = 'アパート'; 	break;
									case "寮":
										$shubetsu_data = '1412'; $bukkenshubetsu_data = '寮'; 	break;
									case "旅館":
										$shubetsu_data = '1413'; $bukkenshubetsu_data = '旅館'; 	break;
									case "ホテル":
										$shubetsu_data = '1414'; $bukkenshubetsu_data = 'ホテル'; 	break;
									case "別荘":
										$shubetsu_data = '1415'; $bukkenshubetsu_data = '別荘'; 	break;
									case "リゾートマンション":
										$shubetsu_data = '1416'; $bukkenshubetsu_data = 'リゾートマンション'; 	break;
									case "文化住宅":
										$shubetsu_data = '1421'; $bukkenshubetsu_data = '文化住宅'; 	break;
									case "その他":
										$shubetsu_data = '1499'; $bukkenshubetsu_data = '外全その他'; 	break;
						}
						break;
					//【【売買】住宅以外の建物一部
					case 15:
						switch ($bukken_shum) {
									case "店舗":
										$shubetsu_data = '1502'; $bukkenshubetsu_data = '店舗'; 	break;
									case "事務所":
										$shubetsu_data = '1505'; $bukkenshubetsu_data = '事務所'; 	break;
									case "店舗事務所":
										$shubetsu_data = '1506'; $bukkenshubetsu_data = '店舗事務'; 	break;
									case "その他":
										$shubetsu_data = '1599'; $bukkenshubetsu_data = '外一その他'; 	break;
						}
						break;


					//【賃貸】土地
					case 31:
						switch ($bukken_shum) {
									case "居住用地":
									case "事業用地":
										$shubetsu_data = '3212'; $bukkenshubetsu_data = '賃貸土地'; 	break;
						}
						break;
					//【賃貸】一戸建
					case 32:
						switch ($bukken_shum) {
									case "貸家";
										$shubetsu_data = '3103'; $bukkenshubetsu_data = '貸家'; 	break;
									case "テラスハウス";
										$shubetsu_data = '3104'; $bukkenshubetsu_data = 'テラスハウス'; 	break;
						}
						break;
					//【賃貸】マンション
					case 33:
						switch ($bukken_shum) {
									case "マンション";
										$shubetsu_data = '3101'; $bukkenshubetsu_data = 'マンション'; 	break;
									case "アパート";
										$shubetsu_data = '3102'; $bukkenshubetsu_data = 'アパート'; 	break;
									case "タウンハウス";
										$shubetsu_data = '3105'; $bukkenshubetsu_data = 'タウンハウス'; 	break;
									case "間借り";
										$shubetsu_data = '3106'; $bukkenshubetsu_data = '間借り'; 	break;
									case "文化住宅";
										$shubetsu_data = '3124'; $bukkenshubetsu_data = '文化住宅'; 	break;
						}
						break;
					//【賃貸】住宅以外建物全部
					case 34:
						switch ($bukken_shum) {
									case "店舗戸建":
										$shubetsu_data = '3201'; $bukkenshubetsu_data = '店舗戸建'; 	break;
									case "事務所":
										$shubetsu_data = '3203'; $bukkenshubetsu_data = '事務所'; 	break;
									case "工場":
										$shubetsu_data = '3205'; $bukkenshubetsu_data = '工場'; 	break;
									case "倉庫":
										$shubetsu_data = '3206'; $bukkenshubetsu_data = '倉庫'; 	break;
									case "マンション（一括）":
										$shubetsu_data = '3208'; $bukkenshubetsu_data = '事業用マンション（一括）'; 	break;
									case "旅館等":
										$shubetsu_data = '3209'; $bukkenshubetsu_data = '旅館'; 	break;
									case "寮":
										$shubetsu_data = '3210'; $bukkenshubetsu_data = '事業用寮'; 	break;
									case "別荘":
										$shubetsu_data = '3211'; $bukkenshubetsu_data = '事業用別荘'; 	break;
									case "ビル":
										$shubetsu_data = '3213'; $bukkenshubetsu_data = '事業用ビル'; 	break;
									case "住宅付き店舗戸建":
										$shubetsu_data = '3214'; $bukkenshubetsu_data = '住店舗戸建'; 	break;
									case "店舗事務所":
										$shubetsu_data = '3204'; $bukkenshubetsu_data = '店舗・事務所'; 	break;
									case "その他":
										$shubetsu_data = '3299'; $bukkenshubetsu_data = '事業用その他'; 	break;
						}
						break;
					//【賃貸】住宅以外建物一部
					case 35:
						switch ($bukken_shum) {
									case "店舗一部":
										$shubetsu_data = '3202'; $bukkenshubetsu_data = '店舗一部'; 	break;
									case "事務所":
										$shubetsu_data = '3203'; $bukkenshubetsu_data = '事務所'; 	break;
									case "店舗事務所":
										$shubetsu_data = '3204'; $bukkenshubetsu_data = '店舗・事務所'; 	break;
									case "住宅付き店舗一部":
										$shubetsu_data = '3215'; $bukkenshubetsu_data = '住店舗一部'; 	break;
									case "マンション（一室）":
										$shubetsu_data = '3208'; $bukkenshubetsu_data = '事業用マンション（一室）'; 	break;
									case "その他":
										$shubetsu_data = '3299'; $bukkenshubetsu_data = '事業用その他'; 	break;
						}
						break;
				}

			}

			//種別チェック
			if( $bukkenshubetsu_data ){

				$post_title_data = convert_chars($data['所在地名１']).''.convert_chars($data['所在地２']);
				$post_title_data = str_replace( " " ,"" , $post_title_data);
				$post_title_data = str_replace( "　" ,"" , $post_title_data);

				$post_title_data .= ' '.$bukkenshubetsu_data ;

				if($post_title_data=='')
					$post_title_data=' ';


				$post_content = convert_chars( $data['備考'] );

				//備考データに「手数料」「問合せ先」「 問）」「ＦＡＸ」がある場合は無
				if( strpos( $post_content, '手数料' ) !== false ) $post_content = '';
				if( strpos( $post_content, '問合せ先' ) !== false )  $post_content = '';
				if( strpos( $post_content, '問）' ) !== false )  $post_content = '';
				if( strpos( $post_content, '担当' ) !== false )  $post_content = '';
				if( strpos( $post_content, 'ＦＡＸ' ) !== false )  $post_content = '';


				if( $data['雑費'] )
					$post_content .= '　[雑費]' . convert_chars( $data['雑費'] );

				if( $data['建築確認'] )
					$post_content .= '　[建築確認]' . convert_chars( $data['建築確認'] );

				if( $data['造作譲渡（万円）'] )
					$post_content .= '　[造作譲渡]' . convert_chars( $data['造作譲渡（万円）'] ) . '万円';



				//西日本レインズチェック
				if( isset($data['物件番号']) && isset($data['建築確認']) && isset($data['造作譲渡（万円）']) && isset($data['最寄沿線']) ){
				        $new_post = array(
				            'post_title' => $post_title_data,						//記事のタイトル 
				            'post_content' => $post_content,						//記事の内容
				            'post_status' => $tmp_opt_draft_h, 						//投稿ステータス 
				            'post_type' => $opt_type,							//投稿種別
				            'post_date' => $tmp_post_date_h,						//記事の投稿日
				            'post_excerpt' =>' ',							//記事の要約
				            'post_name' => '',								//投稿スラッグ 
				            'post_author' => $user_ID							//投稿者のユーザID 
				        );
				}else{
					$err_log .= '<br />※CSVのフォーマットが違います。';
				}
			}else{
				$err_log .= '<br />※[' . trim( $data['物件番号'] ) . '] 種目がありませんでした。';
				return false;
			}

		}	//n_rains


		//重複チェック
		$check_shikibesu= trim( $data['物件番号'] );

		if( $check_shikibesu ){

			$sql = "SELECT post_id FROM $wpdb->postmeta WHERE `meta_key` = 'shikibesu' AND `meta_value` ='" .$check_shikibesu."'";
		//	$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_row( $sql );

			//重複の場合は上書き
			if( !empty($metas) ){
				$addnew = false;
				//上書き許可
				if($opt_overwrite == 'yes'){
					$new_post['ID'] = $metas->post_id;
				        // update
				        $id = wp_update_post($new_post);
				        $updated++;
			        }

			}else{
				// create
				$id = wp_insert_post($new_post);
				$addnew = true;
				$imported++;
			}
		}else{
			// create
			$id = wp_insert_post($new_post);
			$imported++;
		}

	        return $id;

	}



	//カスタムフィールド登録
	function create_custom_fields($post_id, $data) {

		global $work_n_rains,$work_n_rains_rosen,$work_n_rains_eki;
		global $opt_kaiin;
		global $shubetsu_data;

	  	global $opt_csv;
		global $wpdb;
		global $addnew;
		global $err_log;

		$shikibesu = '';
		$shanaimemo = '';
		$nyukyonengetsu = '';

		$kakakushikikin = '';
		$kakakuhoshoukin = '';
		$kakakushikibiki = '';
		$kakakukyouekihi = '';
		$tatemonochikunenn = '';

		$tochikenri = '';
		$tochisetsudohouko1 = '';
		$tochisetsudoshurui1 = '';
		$tochisetsudoichishitei1 = '';
		$tochisetsudofukuin1 = '';
		$tochisetsudomaguchi1 = '';

		$tochisetsudohouko2 = '';
		$tochisetsudoshurui2 = '';
		$tochisetsudoichishitei2 = '';
		$tochisetsudofukuin2 = '';
		$tochisetsudomaguchi2 = '';

		$koutsurosen_dat= '';

		//設備
		$setsubi_data = "99900";

		//CSVタイプ
		if($addnew==true){
			$sql_txt = "(".$post_id.",'csvtype','".$opt_csv."')";
		}else{
			update_post_meta($post_id,'csvtype', $opt_csv);
		}

		//会員設定
		if( $opt_kaiin != '' ){
			if($addnew==true){
				$sql_txt .= ",(".$post_id.",'kaiin','".$opt_kaiin."')";
			}else{
				update_post_meta($post_id,'kaiin', $opt_kaiin);
			}
		}else{
			if($addnew==true){
				$sql_txt .= ",(".$post_id.",'kaiin','')";
			}
		}


		//7種別	物件種別
		if($shubetsu_data != ''){
			if($addnew==true){
				$sql_txt .= ",(".$post_id.",'bukkenshubetsu','".$shubetsu_data."')";
			}else{
				update_post_meta($post_id, 'bukkenshubetsu', $shubetsu_data);
			}
		}


	        foreach ($data as $k => $v) {

			$v = esc_sql( esc_attr($v) );

			//西日本レインズ
			if($opt_csv == 'n_rains'){

				if($k=="物件番号" && $v!=""){ $shikibesu = $v;}

				//設備
				if($k=="備考" && $v!=""){
					if( strpos( $v, '建築条件付' ) !== false ) $setsubi_data .= "/11001";
					if( strpos( $v, '建築条件無' ) !== false ) $setsubi_data .= "/11002";
					if( strpos( $v, '建築条件なし' ) !== false ) $setsubi_data .= "/11002";
				}
				if($k=="条件等" && $v!=""){
					if( strpos( $v, '建築条件付' ) !== false ) $setsubi_data .= "/11001";
					if( strpos( $v, '建築条件無' ) !== false ) $setsubi_data .= "/11002";
					if( strpos( $v, '建築条件なし' ) !== false ) $setsubi_data .= "/11002";
				}

				//社内用メモ
				if( $k=="特記事項1" && $v!="" )			$shanaimemo .= '' . $v . "　";
				if( $k=="特記事項2" && $v!="" )			$shanaimemo .= '' . $v . "　";
				if( $k=="特記事項3" && $v!="" )			$shanaimemo .= '' . $v . "　";
				if( $k=="特記事項4" && $v!="" )			$shanaimemo .= '' . $v . "　";
				if( $k=="特記事項5" && $v!="" )			$shanaimemo .= '' . $v . "　";
				if( $k=="手数料負担（貸主）" && $v!="" )	$shanaimemo .= '[手数料負担（貸主）]' . $v . "　";
				if( $k=="手数料負担（借主）" && $v!="" )	$shanaimemo .= '[手数料負担（借主）]' . $v . "　";
				if( $k=="手数料配分（元付）" && $v!="" )	$shanaimemo .= '[手数料配分（元付）]' . $v . "　";
				if( $k=="手数料配分（客付）" && $v!="" )	$shanaimemo .= '[手数料配分（客付）]' . $v . "　";
				if( $k=="手数料（率）" && $v!="" )		$shanaimemo .= '[手数料（率）]' . $v . "　";
				if( $k=="手数料（金額）" && $v!="" )		$shanaimemo .= '[手数料（金額）]' . $v . "　";
				if( $k=="条件等" && $v!="" )			$shanaimemo .= '[条件等]' . $v . "　";
				if( $k=="管理費" && $v!="" )			$shanaimemo .= '[管理費]' . $v . "　";
				if( $k=="共益費" && $v!="" )			$shanaimemo .= '[共益費]' . $v . "　";
				if( $k=="雑費" && $v!="" )			$shanaimemo .= '[雑費]' . $v . "　";
				if( $k=="建物賃貸借区分" && $v!="" )			$shanaimemo .= '[建物賃貸借区分]' . $v . "　";


				//備考データに「手数料」「問合せ先」「 問）」「ＦＡＸ」がある場合
				if( $k=="備考" && $v!="" ){
					if( strpos( $v, '手数料' ) !== false 
						|| strpos( $v, '問合せ先' ) !== false 
						|| strpos( $v, '問）' ) !== false 
						|| strpos( $v, '担当' ) !== false 
						|| strpos( $v, 'ＦＡＸ' ) !== false  ){

							$shanaimemo .= '[備考]' . $v . "　";
					}
				}


				if( $k=="坪単価（万円）" && $v!="" ){ $v=$v * 10000;}


				//校区
				if( $k=="学区（小学校）" && $v!="" ){ if( strpos( $v, '小学校' ) === false ) $v= $v.'小学校'; }
				if( $k=="学区（中学校）" && $v!="" ){ if( strpos( $v, '中学校' ) === false ) $v= $v.'中学校'; }

				//0対策
				if( $k=="土地面積" && $v!="" ){ if( $v=="0" ) $v="";}
				if( $k=="建ぺい率" && $v!="" ){ if( $v=="0" ) $v="";}
				if( $k=="容積率" && $v!="" ){ if( $v=="0" ) $v="";}

				if( $k=="取引態様" && $v!="" ){ if( $v!="その他" ) $v="6";}
				if( $k=="物件番号" && $v!="" ) $v=trim($v);

				//入居時期
				if( $k=="引渡し（年）" && $v!="" ) { if( $v!="0000" ) $nyukyonengetsu = $v;}
				if( $k=="引渡し（月）" && $v!="" && $nyukyonengetsu ){ if( $v!="00" )  $nyukyonengetsu .= '/' . $v;}

				//金銭面
					if( $k=="保証金（ヶ月）" && $v!='' ) $kakakuhoshoukin = $v;
					if( $k=="保証金（万円）" && $v!='' ) $kakakuhoshoukin = $v * 10000;

					if( $k=="敷金（ヶ月）" && $v!='' ) $kakakushikikin = $v;
					if( $k=="敷金（万円）" && $v!='' ) $kakakushikikin = $v * 10000;

				//償却・敷引金 
					if( $k=="敷引（万円）" && $v!='' ) $kakakushikibiki = $v * 10000;
					if( $k=="敷引（ヶ月）" && $v!='' ) $kakakushikibiki = $v;
					if( $k=="敷引（実費）" && $v!='' ) $kakakushikibiki = $v;
					//(1～99:ヶ月、101～200:100を引いて % 、201以上の場合単位は円)
					if( $k=="保証金償却" && $v!='' ) $kakakushikibiki = $v;
					if( $k=="保証金償却（％）" && $v!='' ) $kakakushikibiki = $v+100;

				//合算
					if( $k=="管理費" && $v!='' ) $kakakukyouekihi = $v;
					if( $k=="共益費" && $v!='' ) $kakakukyouekihi = $kakakukyouekihi + $v;
				//	if( $k=="雑費" && $v!='' ) $kakakukyouekihi = $kakakukyouekihi + $v;


				//築年月
					if( $k=="築年月（年）" && $v!='' ) { if( (int)$v!=0 ) $tatemonochikunenn = $v; }
					if( $k=="築年月（月）" && $v!='' ) { if( $tatemonochikunenn && $v!="00" ) $tatemonochikunenn = $tatemonochikunenn . '/' . $v;}


				//土地権利/借地権種類
					if( $k=="土地権利" && $v!='' ) $tochikenri =  $v;
					if( $k=="借地権種類" && $v!='' ) $tochikenri =  $v;


				//接道1		北4m　私道　接面9.7m　位置指定有
				if( $k=="接道1" && $v!='' ){

					if( $v != "0m" ){
						//接道方向1
						$tochisetsudohouko1 = $this->hougaku_check($v);

						// 接道種別1	1:公道 2:私道 
						if( strpos( $v, '公道' ) !== false ) $tochisetsudoshurui1 = 1;
						if( strpos( $v, '私道' ) !== false ) $tochisetsudoshurui1 = 2;

						// 位置指定道路1
						if( strpos( $v, '位置指定有' ) !== false ) $tochisetsudoichishitei1 = 1;

						//接道幅員1
						$tochisetsudofukuin1 = $this->str_cut_l($v,'　');
						$tochisetsudofukuin1 = str_replace( $tochisetsudohouko1,"" , $tochisetsudofukuin1);
						$tochisetsudofukuin1 = str_replace( "m","" , $tochisetsudofukuin1);
						if( $tochisetsudofukuin1 == "0" ) $tochisetsudofukuin1 = '';
						if( strpos( $tochisetsudofukuin1, '接面' ) !== false )  $tochisetsudofukuin1 = '';

						//接道間口1
						if( strpos( $v, '面' ) !== false ) {
							$tochisetsudomaguchi1 = $this->str_cut_r($v,"面");
							$tochisetsudomaguchi1 = str_replace( "　位置指定有","" , $tochisetsudomaguchi1);
							$tochisetsudomaguchi1 = str_replace( "m","" , $tochisetsudomaguchi1);
							if( $tochisetsudomaguchi1 == "0" ) $tochisetsudomaguchi1 = '';
						}
					}
				}


				//接道2
				if( $k=="接道2" && $v!='' ){

					if( $v != "0m" ){
						//接道方向2
						$tochisetsudohouko2 = $this->hougaku_check($v);

						// 接道種別2	1:公道 2:私道 
						if( strpos( $v, '公道' ) !== false ) $tochisetsudoshurui2 = 1;
						if( strpos( $v, '私道' ) !== false ) $tochisetsudoshurui2 = 2;

						// 位置指定道路2
						if( strpos( $v, '位置指定有' ) !== false ) $tochisetsudoichishitei2 = 1;

						//接道幅員2
						$tochisetsudofukuin2 = $this->str_cut_l($v,'　');
						$tochisetsudofukuin2 = str_replace( $tochisetsudohouko2,"" , $tochisetsudofukuin2);
						$tochisetsudofukuin2 = str_replace( "m","" , $tochisetsudofukuin2);
						if( $tochisetsudofukuin2 == "0" ) $tochisetsudofukuin2 = '';
						if( strpos( $tochisetsudofukuin2, '接面' ) !== false )  $tochisetsudofukuin2 = '';

						//接道間口2
						if( strpos( $v, '面' ) !== false ) {
							$tochisetsudomaguchi2 = $this->str_cut_r($v,"面");
							$tochisetsudomaguchi2 = str_replace( "　位置指定有","" , $tochisetsudomaguchi2);
							$tochisetsudomaguchi2 = str_replace( "m","" , $tochisetsudomaguchi2);
							if( $tochisetsudomaguchi2 == "0" ) $tochisetsudomaguchi2 = '';
						}
					}
				}

				//価格
				if($k =='価格' && $v!='') $v= $v*10000; 

				//権利金（万円）
				if($k =='権利金（万円）' && $v!='') $v= $v*10000; 

				//間取
				if($k =='間取り'){
					if( $v!=""){
					if( $v!="0"){
						$madorisyurui = '';
						$madorisu = intval($v);

						if( strpos($v, "ルーム") !== false )    $madorisyurui = '10';
						if( strpos($v, "R") !== false )    $madorisyurui = '10';
						if( strpos($v, "K") !== false )    $madorisyurui = '20';
						if( strpos($v, "SK") !== false )   $madorisyurui = '25';
						if( strpos($v, "DK") !== false )   $madorisyurui = '30';
						if( strpos($v, "SDK") !== false )  $madorisyurui = '35';
						if( strpos($v, "LK") !== false )   $madorisyurui = '40';
						if( strpos($v, "SLK") !== false )  $madorisyurui = '45';
						if( strpos($v, "LDK") !== false )  $madorisyurui = '50';
						if( strpos($v, "SLDK") !== false ) $madorisyurui = '55';
					}else{
						$madorisyurui = '';
						$madorisu = '';
					}

					if($addnew==true){
						$sql_txt .= ",(".$post_id.",'madorisu','".$madorisu."')";
					}else{
						update_post_meta($post_id, 'madorisu', $madorisu);
					}
					if($addnew==true){
						$sql_txt .= ",(".$post_id.",'madorisyurui','".$madorisyurui."')";
					}else{
						update_post_meta($post_id, 'madorisyurui', $madorisyurui);
					}
					}
				}


				//県
				if($k=="所在地コード" && $v!="" ){

					$shozaichiken_code = (int)myLeft($v , 2 );

					if($addnew==true){
						$sql_txt .= ",(".$post_id.",'shozaichiken','".$shozaichiken_code."')";
					}else{
						update_post_meta($post_id, 'shozaichiken', $shozaichiken_code);
					}
				}


				//所在地
				if($k=="所在地名１" && $v!="" && $shozaichiken_code ){

						$v = str_replace( " " ,"" , $v);
						$v = str_replace( "　" ,"" , $v);

						$sql = "SELECT narrow_area_id FROM " . $wpdb->prefix . DB_SHIKU_TABLE . " WHERE middle_area_id=".$shozaichiken_code." AND narrow_area_name = '".$v."'";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );
						if(!empty($metas)){
							$shozaichi_code = $metas->narrow_area_id;
							$shozaichi = $shozaichiken_code.$shozaichi_code."000000" ;


							if($addnew==true){
								$sql_txt .= ",(".$post_id.",'shozaichicode','".$shozaichi."')";
							}else{
								update_post_meta($post_id, 'shozaichicode', $shozaichi);
							}
							$shozaichi ="";
						}else{
								$err_log .= '<br />※['. $shikibesu . '] ' . $shozaichiken_code . ' ' . $v . 'が見つかりませんでした。';
						}

				}

				//路線
				if($k=="最寄沿線" && $v!=""){

					$koutsurosen_dat= $v;

					if( $v == "平成筑豊鉄道" ){
					}else{

						foreach($work_n_rains_rosen as $meta_box){
							if($meta_box['name'] == $v){
								$koutsurosen1 = $meta_box['code'];

								if($addnew==true){
									$sql_txt .= ",(".$post_id.",'koutsurosen1','".$koutsurosen1."')";
								}else{
									update_post_meta($post_id, 'koutsurosen1', $koutsurosen1);
								}
							}
						}
					}
				}


				if($k=="最寄駅" && $v!="" ){

					//平成筑豊鉄道 例外
					if( $koutsurosen_dat == '平成筑豊鉄道' ){

						foreach( $work_n_rains_eki as $meta_box ){
							if($meta_box['ekiname'] == $v){
								$koutsurosen1 = $meta_box['rosen'];
								$koutsueki1 = $meta_box['eki'];
								if($addnew==true){
									$sql_txt .= ",(".$post_id.",'koutsurosen1','".$koutsurosen1."')";
									$sql_txt .= ",(".$post_id.",'koutsueki1','".$koutsueki1."')";
								}else{
									update_post_meta($post_id, 'koutsurosen1', $koutsurosen1);
									update_post_meta($post_id, 'koutsueki1', $koutsueki1);
								}
							}
						}
					}else{
						if( $koutsurosen1 !="" ){

							$v_sub = '';
							$v_sub2 = '';
							$v_sub3 = '';

							$findme = 'ヶ';
							$findme2 = 'ケ';

							$pos = strpos($v, $findme);
							if ($pos === false) {
							} else {
								$v_sub = str_replace( $findme ,$findme2 , $v);
							}
							$pos = strpos($v, $findme2);
							if ($pos === false) {
							} else {
								$v_sub2 = str_replace( $findme2 ,$findme , $v);
							}

							//例外
							if( strpos($v, "１丁目") !== false ) $v_sub3 = str_replace( "１丁目" , "一丁目" , $v);
							if( strpos($v, "２丁目") !== false ) $v_sub3 = str_replace( "２丁目" , "二丁目" , $v);
							if( strpos($v, "３丁目") !== false ) $v_sub3 = str_replace( "３丁目" , "三丁目" , $v);
							if( strpos($v, "４丁目") !== false ) $v_sub3 = str_replace( "４丁目" , "四丁目" , $v);
							if( strpos($v, "５丁目") !== false ) $v_sub3 = str_replace( "５丁目" , "五丁目" , $v);
							if( strpos($v, "６丁目") !== false ) $v_sub3 = str_replace( "６丁目" , "六丁目" , $v);
							if( strpos($v, "７丁目") !== false ) $v_sub3 = str_replace( "７丁目" , "七丁目" , $v);
							if( strpos($v, "８丁目") !== false ) $v_sub3 = str_replace( "８丁目" , "八丁目" , $v);
							if( strpos($v, "９丁目") !== false ) $v_sub3 = str_replace( "９丁目" , "九丁目" , $v);

							if( strpos($v, "一丁目") !== false ) $v_sub3 = str_replace( "一丁目" , "１丁目" , $v);
							if( strpos($v, "二丁目") !== false ) $v_sub3 = str_replace( "二丁目" , "２丁目" , $v);
							if( strpos($v, "三丁目") !== false ) $v_sub3 = str_replace( "三丁目" , "３丁目" , $v);
							if( strpos($v, "四丁目") !== false ) $v_sub3 = str_replace( "四丁目" , "４丁目" , $v);
							if( strpos($v, "五丁目") !== false ) $v_sub3 = str_replace( "五丁目" , "５丁目" , $v);
							if( strpos($v, "六丁目") !== false ) $v_sub3 = str_replace( "六丁目" , "６丁目" , $v);
							if( strpos($v, "七丁目") !== false ) $v_sub3 = str_replace( "七丁目" , "７丁目" , $v);
							if( strpos($v, "八丁目") !== false ) $v_sub3 = str_replace( "八丁目" , "８丁目" , $v);
							if( strpos($v, "九丁目") !== false ) $v_sub3 = str_replace( "九丁目" , "９丁目" , $v);

							if( strpos($v, "（") !== false ) $v_sub3 = mb_convert_kana($v, 'a') ;;
							if( $koutsurosen_dat == '伊予鉄道環状線' && $v == "大手町" ) $v_sub3 = '大手町駅前';
							if( $koutsurosen_dat == '広島電鉄横川線' && $v == "横川駅" ) $v_sub3 = '横川';


							$sql = "SELECT DTS.station_id";
							$sql = $sql . " FROM " . $wpdb->prefix . DB_EKI_TABLE . " AS DTS";
							$sql = $sql . " WHERE DTS.rosen_id=".$koutsurosen1." AND DTS.middle_area_id=".$shozaichiken_code."";
							$sql = $sql . " AND ( DTS.station_name='".$v."'";
							if($v_sub  != '') $sql = $sql . " OR DTS.station_name='".$v_sub."' ";
							if($v_sub2 != '') $sql = $sql . " OR DTS.station_name='".$v_sub2."' ";
							if($v_sub3 != '') $sql = $sql . " OR DTS.station_name='".$v_sub3."' ";
							$sql = $sql . " )";
						//	$sql = $wpdb->prepare($sql,'');
							$metas = $wpdb->get_row( $sql );

							if(!empty($metas)){
								$meta = $metas->station_id;

								if($meta!=''){
									if($addnew==true){
										$sql_txt .= ",(".$post_id.",'koutsueki1','".$meta."')";
									}else{
										update_post_meta($post_id, 'koutsueki1', $meta);
									}
								}
							}else{
								$err_log .= '<br />※['. $shikibesu . '] ' .$koutsurosen_dat . ' ' . $v . 'が見つかりませんでした。';
							}

							//$shozaichiken_code = '';
						}
					}
				}


				//$work_n_rains
				foreach($work_n_rains as $meta_box){

					if($k == $meta_box['h_name'] && $meta_box['d_name'] !=''){

						if($addnew==true){
							$sql_txt .= ",(".$post_id.",'". $meta_box['d_name']."','".$v."')";
						}else{
							update_post_meta($post_id, $meta_box['d_name'], $v);
						}
					}
				}

			}	//西日本レインズ

		} //loop end


		if($setsubi_data != "99900"){
			if($addnew==true){
				$sql_txt .= ",(".$post_id.",'setsubi','".$setsubi_data."')";
			}else{
				update_post_meta($post_id, 'setsubi', $setsubi_data);
			}
		}

		if($addnew==true){
			if( $shanaimemo !='' )	$sql_txt .= ",(".$post_id.",'shanaimemo','".$shanaimemo."')";
			if( $nyukyonengetsu !='' )	$sql_txt .= ",(".$post_id.",'nyukyonengetsu','".$nyukyonengetsu."')";
			if( $kakakushikikin !='' )	$sql_txt .= ",(".$post_id.",'kakakushikikin','".$kakakushikikin."')";
			if( $kakakuhoshoukin !='' )	$sql_txt .= ",(".$post_id.",'kakakuhoshoukin','".$kakakuhoshoukin."')";
			if( $kakakushikibiki !='' )	$sql_txt .= ",(".$post_id.",'kakakushikibiki','".$kakakushikibiki."')";
			if( $kakakukyouekihi !='' )	$sql_txt .= ",(".$post_id.",'kakakukyouekihi','".$kakakukyouekihi."')";
			if( $tatemonochikunenn !='' )	$sql_txt .= ",(".$post_id.",'tatemonochikunenn','".$tatemonochikunenn."')";

			if( $tochikenri !='' )	$sql_txt .= ",(".$post_id.",'tochikenri','".$tochikenri."')";
			if( $tochisetsudohouko1 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudohouko1','".$tochisetsudohouko1."')";
			if( $tochisetsudoshurui1 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudoshurui1','".$tochisetsudoshurui1."')";
			if( $tochisetsudoichishitei1 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudoichishitei1','".$tochisetsudoichishitei1."')";
			if( $tochisetsudofukuin1 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudofukuin1','".$tochisetsudofukuin1."')";
			if( $tochisetsudomaguchi1 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudomaguchi1','".$tochisetsudomaguchi1."')";
			if( $tochisetsudohouko2 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudohouko2','".$tochisetsudohouko2."')";
			if( $tochisetsudoshurui2 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudoshurui2','".$tochisetsudoshurui2."')";
			if( $tochisetsudoichishitei2 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudoichishitei2','".$tochisetsudoichishitei2."')";
			if( $tochisetsudofukuin2 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudofukuin2','".$tochisetsudofukuin2."')";
			if( $tochisetsudomaguchi2 !='' )	$sql_txt .= ",(".$post_id.",'tochisetsudomaguchi2','".$tochisetsudomaguchi2."')";

		}else{
			update_post_meta($post_id, 'shanaimemo'	,$shanaimemo);
			update_post_meta($post_id, 'nyukyonengetsu'	,$nyukyonengetsu);
			update_post_meta($post_id, 'kakakushikikin'	,$kakakushikikin);
			update_post_meta($post_id, 'kakakuhoshoukin'	,$kakakuhoshoukin);
			update_post_meta($post_id, 'kakakushikibiki'	,$kakakushikibiki);
			update_post_meta($post_id, 'kakakukyouekihi'	,$kakakukyouekihi);
			update_post_meta($post_id, 'tatemonochikunenn'	,$tatemonochikunenn);

			update_post_meta($post_id, 'tochikenri'	,$tochikenri);
			update_post_meta($post_id, 'tochisetsudohouko1'	,$tochisetsudohouko1);
			update_post_meta($post_id, 'tochisetsudoshurui1'	,$tochisetsudoshurui1);
			update_post_meta($post_id, 'tochisetsudoichishitei1'	,$tochisetsudoichishitei1);
			update_post_meta($post_id, 'tochisetsudofukuin1'	,$tochisetsudofukuin1);
			update_post_meta($post_id, 'tochisetsudomaguchi1'	,$tochisetsudomaguchi1);
			update_post_meta($post_id, 'tochisetsudohouko2'	,$tochisetsudohouko2);
			update_post_meta($post_id, 'tochisetsudoshurui2'	,$tochisetsudoshurui2);
			update_post_meta($post_id, 'tochisetsudoichishitei2'	,$tochisetsudoichishitei2);
			update_post_meta($post_id, 'tochisetsudofukuin2'	,$tochisetsudofukuin2);
			update_post_meta($post_id, 'tochisetsudomaguchi2'	,$tochisetsudomaguchi2);
		}


		if( $addnew==true ){
			$sql = "INSERT INTO $wpdb->postmeta (`post_id`, `meta_key`, `meta_value`) VALUES ";
			$sql .= $sql_txt;
			$wpdb->query( $sql );
		}

	}

	//投稿者のユーザID
	function get_auth_id($author) {
	        if (is_numeric($author)) {
	            return $author;
	        }
	        $author_data = get_userdatabylogin($author);
	        return ($author_data) ? $author_data->ID : 0;
	} 


	// Convert date in CSV file to 20130912 format
	function parse_date4($data) {
		$year = myLeft($data,4);
		$month = myRight( myLeft($data,6),2 );
		$day = myRight($data,2);
	        return $year . '-' . $month . '-' . $day . ' 00:00:00';
	}

	// Convert date in CSV file to 2010/11 format
	function parse_date3($data) {
		$data = str_replace ('年', '/', $data);
		$data = str_replace ('月', '', $data);
	        return $data;
	}

	// Convert date in CSV file to 2010/11 format
	function parse_date2($data) {
	    $timestamp = strtotime($data);
	    if (false === $timestamp) {
	        return '';
	    } else {
	        return date('Y/m', $timestamp);
	    }
	}

	// Convert date in CSV file to 1999-12-31 23:52:00 format
	function parse_date($data) {
		$data = str_replace ('.', '/', $data);
	    $timestamp = strtotime($data);
	    if (false === $timestamp) {
		$timestamp = '00:00:00';
	        return date('Y-m-d H:i:s', $timestamp);
	    } else {
	        return date('Y-m-d H:i:s', $timestamp);
	    }
	}

	//方角チェック
	function hougaku_check( $value ) {
		if( strpos( $value, '北東' ) !== false ) return "北東";
		if( strpos( $value, '南東' ) !== false ) return "南東";
		if( strpos( $value, '南西' ) !== false ) return "南西";
		if( strpos( $value, '北西' ) !== false ) return "北西";
		if( strpos( $value, '東' ) !== false ) return "東";
		if( strpos( $value, '西' ) !== false ) return "西";
		if( strpos( $value, '南' ) !== false ) return "南";
		if( strpos( $value, '北' ) !== false ) return "北";

		return false;
	}




	function str_cut_l($str,$options) {
		$pos = mb_strpos($str , $options);
		if($pos !== false )
		$str= myLeft($str,$pos);
	        return $str;
	}

	function str_cut_r($str,$options) {
		$all = mb_strlen( $str, 'utf-8');
		$pos = mb_strpos($str , $options);
		if($pos !== false )
		$str= myRight($str,$all - $pos -1 );
	        return $str;
	}

	//$options offset
	function str_cut_map_fun($str,$options) {
		$all = mb_strlen( $str, 'utf-8');
		$str = myRight($str,$all - $options );
		$str = $this->str_cut_l($str , '.' );
	        return $str;
	}

/*
	// Left関数//左からn文字取得して返す 
	if (!function_exists('myLeft')) {
	function myLeft($str,$n){
		return mb_substr($str,0,(mb_strlen($str)-$n)*-1);
	}
	}

	// Right関数//右からn文字取得して返す 
	if (!function_exists('myRight')) {
	function myRight($str,$n){
		return mb_substr($str,($n)*-1);
	}
	}

*/



	// delete BOM from UTF-8 file
	function stripBOM($fname) {
	    $res = fopen($fname, 'rb');
	    if (false !== $res) {
	        $bytes = fread($res, 3);
	        if ($bytes == pack('CCC', 0xef, 0xbb, 0xbf)) {
	            $this->log['notice'][] = 'Getting rid of byte order mark...';
	            fclose($res);

	            $contents = file_get_contents($fname);
	            if (false === $contents) {
	                trigger_error('Failed to get file contents.', E_USER_WARNING);
	            }
	            $contents = substr($contents, 3);
	            $success = file_put_contents($fname, $contents);
	            if (false === $success) {
	                trigger_error('Failed to put file contents.', E_USER_WARNING);
	            }
	        } else {
	            fclose($res);
	        }
	    } else {
	        $this->log['error'][] = 'Failed to open file, aborting.';
	    }
	}

}	//class CSVImporterPlugin__nishi




	//n_rains csv format
	$work_n_rains =
	array(	
		"1"    =>  array("d_name" => "shikibesu" ,		"h_name" => "物件番号" ),
	//	"2"    =>  array("d_name" => "" ,			"h_name" => "登録日" ),
		"3"    =>  array("d_name" => "motozukemei" ,		"h_name" => "会員商号" ),
		"4"    =>  array("d_name" => "motozuketel" ,		"h_name" => "会員電話番号" ),
	//	"5"    =>  array("d_name" => "" ,			"h_name" => "種目" ),
		"6"    =>  array("d_name" => "tatemonoshinchiku" ,	"h_name" => "新築中古区分" ),
	//	"7"    =>  array("d_name" => "" ,			"h_name" => "所在地コード" ),
	//	"8"    =>  array("d_name" => "" ,			"h_name" => "所在地名１" ),
		"9"    =>  array("d_name" => "shozaichimeisho" ,	"h_name" => "所在地２" ),
		"10"   =>  array("d_name" => "shozaichimeisho2" ,	"h_name" => "番地" ),
		"11"   =>  array("d_name" => "bukkenmei" ,		"h_name" => "建物名" ),
		"12"   =>  array("d_name" => "bukkennaiyo" ,		"h_name" => "部屋番号" ),
	//	"13"   =>  array("d_name" => "" ,			"h_name" => "最寄沿線" ),
	//	"14"   =>  array("d_name" => "" ,			"h_name" => "最寄駅" ),
		"15"   =>  array("d_name" => "koutsutoho1" ,		"h_name" => "徒歩（ｍ）" ),
		"16"   =>  array("d_name" => "koutsutoho1f" ,		"h_name" => "徒歩徒歩（分）" ),	//2
	//	"17"   =>  array("d_name" => "" ,			"h_name" => "最寄バス" ),
		"18"   =>  array("d_name" => "koutsubusstei1" ,		"h_name" => "最寄バス停" ),
	//	"19"   =>  array("d_name" => "" ,			"h_name" => "バス（ｍ）" ),
		"20"   =>  array("d_name" => "koutsutohob1f" ,		"h_name" => "徒歩（分）" ),	//2 停歩
		"21"   =>  array("d_name" => "shuuhenshougaku" ,	"h_name" => "学区（小学校）" ),
		"22"   =>  array("d_name" => "shuuhenchuugaku" ,	"h_name" => "学区（中学校）" ),
		"23"   =>  array("d_name" => "kakaku" ,			"h_name" => "価格" ),
		"24"   =>  array("d_name" => "kakakuzei" ,		"h_name" => "消費税区分" ),
	//	"25"   =>  array("d_name" => "" ,			"h_name" => "敷金（ヶ月）" ),
	//	"26"   =>  array("d_name" => "" ,			"h_name" => "敷金（万円）" ),
	//	"27"   =>  array("d_name" => "" ,			"h_name" => "敷引（万円）" ),
	//	"28"   =>  array("d_name" => "" ,			"h_name" => "敷引（ヶ月）" ),
	//	"29"   =>  array("d_name" => "" ,			"h_name" => "敷引（実費）" ),
	//	"30"   =>  array("d_name" => "" ,			"h_name" => "保証金（ヶ月）" ),
	//	"31"   =>  array("d_name" => "" ,			"h_name" => "保証金（万円）" ),
	//	"32"   =>  array("d_name" => "" ,			"h_name" => "保証金償却" ),
	//	"33"   =>  array("d_name" => "" ,			"h_name" => "保証金償却（％）" ),
		"34"   =>  array("d_name" => "kakakukenrikin" ,		"h_name" => "権利金（万円）" ),
	//	"35"   =>  array("d_name" => "" ,			"h_name" => "造作譲渡（万円）" ),
	//	"36"   =>  array("d_name" => "" ,			"h_name" => "管理費" ),
		"37"   =>  array("d_name" => "kakakutsumitate" ,	"h_name" => "積立金" ),
	//	"38"   =>  array("d_name" => "" ,			"h_name" => "共益費" ),
	//	"39"   =>  array("d_name" => "" ,			"h_name" => "雑費" ),
		"40"   =>  array("d_name" => "shakuchiryo" ,		"h_name" => "借地料" ),
		"41"   =>  array("d_name" => "kakakutsubo" ,		"h_name" => "坪単価（万円）" ),
	//	"42"   =>  array("d_name" => "tochikenri" ,		"h_name" => "土地権利" ),
		"43"   =>  array("d_name" => "tochikukaku" ,		"h_name" => "土地面積" ),
		"44"   =>  array("d_name" => "tochishido" ,		"h_name" => "私道面積" ),
		"45"   =>  array("d_name" => "tatemonohosiki" ,		"h_name" => "計測方式" ),
		"46"   =>  array("d_name" => "tatemonomenseki" ,	"h_name" => "建物専有面積" ),
		"47"   =>  array("d_name" => "heyabarukoni" ,		"h_name" => "バルコニー面積" ),
		"48"   =>  array("d_name" => "heyamuki" ,		"h_name" => "バルコニー方向" ),
	//	"49"   =>  array("d_name" => "tochikenri" ,		"h_name" => "借地権種類" ),
	//	"50"   =>  array("d_name" => "" ,			"h_name" => "建物賃貸借区分" ),
		"51"   =>  array("d_name" => "tatemonokozo" ,		"h_name" => "建物構造" ),
		"52"   =>  array("d_name" => "tatemonokaisu1" ,		"h_name" => "地上階層" ),
		"53"   =>  array("d_name" => "tatemonokaisu2" ,		"h_name" => "地下階層" ),
		"54"   =>  array("d_name" => "heyakaisu" ,		"h_name" => "所在階" ),
		"55"   =>  array("d_name" => "bukkensoukosu" ,		"h_name" => "戸数" ),
	//	"56"   =>  array("d_name" => "" ,			"h_name" => "築年月（年）" ),
	//	"57"   =>  array("d_name" => "" ,			"h_name" => "築年月（月）" ),
	//	"58"   =>  array("d_name" => "" ,			"h_name" => "間取り" ),
		"59"   =>  array("d_name" => "madoribiko" ,		"h_name" => "間取り内訳" ),
		"60"   =>  array("d_name" => "tochikenpei" ,		"h_name" => "建ぺい率" ),
		"61"   =>  array("d_name" => "tochiyoseki" ,		"h_name" => "容積率" ),
		"62"   =>  array("d_name" => "tochichimoku" ,		"h_name" => "地目" ),
		"63"   =>  array("d_name" => "tochikeikaku" ,		"h_name" => "都市計画" ),
		"64"   =>  array("d_name" => "tochiyouto" ,		"h_name" => "用途地域" ),
	//	"65"   =>  array("d_name" => "" ,			"h_name" => "最適用途" ),
		"66"   =>  array("d_name" => "tochikokudohou" ,		"h_name" => "国土法" ),
		"67"   =>  array("d_name" => "nyukyogenkyo" ,		"h_name" => "現況" ),
		"68"   =>  array("d_name" => "nyukyojiki" ,		"h_name" => "引渡し" ),
	//	"69"   =>  array("d_name" => "" ,			"h_name" => "引渡し（年）" ),
	//	"70"   =>  array("d_name" => "" ,			"h_name" => "引渡し（月）" ),
		"71"   =>  array("d_name" => "nyukyosyun" ,		"h_name" => "引渡し（旬）" ),
	//	"72"   =>  array("d_name" => "" ,			"h_name" => "接道1" ),
	//	"73"   =>  array("d_name" => "" ,			"h_name" => "接道2" ),
	//	"74"   =>  array("d_name" => "" ,			"h_name" => "接道3" ),
		"75"   =>  array("d_name" => "tochisetsudo" ,		"h_name" => "接道状況" ),
		"76"   =>  array("d_name" => "chushajokubun" ,		"h_name" => "駐車場有無" ),
		"77"   =>  array("d_name" => "chushajoryokin" ,		"h_name" => "駐車場料金（円）" ),
	//	"78"   =>  array("d_name" => "" ,			"h_name" => "駐車場敷金（ヶ月）" ),
	//	"79"   =>  array("d_name" => "" ,			"h_name" => "駐車場敷金（万円）" ),
	//	"80"   =>  array("d_name" => "" ,			"h_name" => "備考" ),
	//	"81"   =>  array("d_name" => "" ,			"h_name" => "条件等" ),
	//	"82"   =>  array("d_name" => "" ,			"h_name" => "建築確認" ),
	//	"83"   =>  array("d_name" => "" ,			"h_name" => "特記事項1" ),
	//	"84"   =>  array("d_name" => "" ,			"h_name" => "特記事項2" ),
	//	"85"   =>  array("d_name" => "" ,			"h_name" => "特記事項3" ),
	//	"86"   =>  array("d_name" => "" ,			"h_name" => "特記事項4" ),
	//	"87"   =>  array("d_name" => "" ,			"h_name" => "特記事項5" ),
	//	"88"   =>  array("d_name" => "" ,			"h_name" => "保険加入義務" ),
		"89"   =>  array("d_name" => "kakakuhokenkikan" ,	"h_name" => "保険期間（年）" ),
		"90"   =>  array("d_name" => "kakakuhoken" ,		"h_name" => "保険金額（円）" ),
		"91"   =>  array("d_name" => "torihikitaiyo" ,		"h_name" => "取引態様" ),
	//	"92"   =>  array("d_name" => "" ,			"h_name" => "手数料負担（貸主）" ),
	//	"93"   =>  array("d_name" => "" ,			"h_name" => "手数料負担（借主）" ),
	//	"94"   =>  array("d_name" => "" ,			"h_name" => "手数料配分（元付）" ),
	//	"95"   =>  array("d_name" => "" ,			"h_name" => "手数料配分（客付）" ),
		"96"   =>  array("d_name" => "houshoukeitai" ,		"h_name" => "報酬形態" ),
	//	"97"   =>  array("d_name" => "" ,			"h_name" => "手数料（率）" ),
	//	"98"   =>  array("d_name" => "" ,			"h_name" => "手数料（金額）" ),
		"99"   =>  array("d_name" => "shakuchikikan" ,		"h_name" => "契約期間" ),		//借地
		"100"  =>  array("d_name" => "seiyakubi" ,		"h_name" => "成約日" ),
	//	"101"  =>  array("d_name" => "" ,			"h_name" => "成約価格" )
	);









	//n_rains csv format
	$work_n_rains_rosen =
	array(	
		"12"  => array("name"=> "東海道・山陽新幹線","code" =>"12"),
		"254" => array("name"=> "山陽本線","code" =>"254"),
		"257" => array("name"=> "加古川線","code" =>"257"),
		"261" => array("name"=> "姫新線","code" =>"261"),
		"262" => array("name"=> "赤穂線","code" =>"262"),
		"263" => array("name"=> "津山線","code" =>"263"),
		"264" => array("name"=> "吉備線","code" =>"264"),
		"265" => array("name"=> "宇野線","code" =>"265"),
		"266" => array("name"=> "本四備讃線","code" =>"266"),
		"267" => array("name"=> "伯備線","code" =>"267"),
		"271" => array("name"=> "芸備線","code" =>"271"),
		"273" => array("name"=> "福塩線","code" =>"273"),
		"275" => array("name"=> "呉線","code" =>"275"),
		"277" => array("name"=> "可部線","code" =>"277"),
		"278" => array("name"=> "岩徳線","code" =>"278"),
		"279" => array("name"=> "山口線","code" =>"279"),
		"280" => array("name"=> "宇部線","code" =>"280"),
		"281" => array("name"=> "小野田線","code" =>"281"),
		"283" => array("name"=> "美祢線","code" =>"283"),
		"293" => array("name"=> "山陰本線","code" =>"293"),
		"298" => array("name"=> "因美線","code" =>"298"),
		"299" => array("name"=> "境線","code" =>"299"),
		"300" => array("name"=> "木次線","code" =>"300"),
		"301" => array("name"=> "三江線","code" =>"301"),
		"306" => array("name"=> "関西本線","code" =>"306"),
		"321" => array("name"=> "博多南線","code" =>"321"),
		"325" => array("name"=> "予讃線","code" =>"325"),
		"327" => array("name"=> "内子線","code" =>"327"),
		"328" => array("name"=> "予土線","code" =>"328"),
		"329" => array("name"=> "高徳線","code" =>"329"),
		"330" => array("name"=> "鳴門線","code" =>"330"),
		"333" => array("name"=> "土讃線","code" =>"333"),
		"335" => array("name"=> "徳島線","code" =>"335"),
		"337" => array("name"=> "牟岐線","code" =>"337"),
		"341" => array("name"=> "鹿児島本線","code" =>"341"),
		"344" => array("name"=> "香椎線","code" =>"344"),
		"345" => array("name"=> "篠栗線","code" =>"345"),
		"346" => array("name"=> "三角線","code" =>"346"),
		"349" => array("name"=> "肥薩線","code" =>"349"),
		"351" => array("name"=> "指宿枕崎線","code" =>"351"),
		"352" => array("name"=> "長崎本線","code" =>"352"),
		"353" => array("name"=> "唐津線","code" =>"353"),
		"355" => array("name"=> "筑肥線","code" =>"355"),
		"356" => array("name"=> "佐世保線","code" =>"356"),
		"357" => array("name"=> "大村線","code" =>"357"),
		"358" => array("name"=> "久大本線","code" =>"358"),
		"361" => array("name"=> "豊肥本線","code" =>"361"),
		"365" => array("name"=> "日豊本線","code" =>"365"),
		"366" => array("name"=> "日田彦山線","code" =>"366"),
		"367" => array("name"=> "日南線","code" =>"367"),
		"368" => array("name"=> "吉都線","code" =>"368"),
		"369" => array("name"=> "筑豊本線","code" =>"369"),
		"371" => array("name"=> "後藤寺線","code" =>"371"),
		"416" => array("name"=> "阿佐海岸阿佐東線","code" =>"416"),
		"570" => array("name"=> "西鉄天神大牟田線","code" =>"570"),
		"572" => array("name"=> "西日本鉄道太宰府線","code" =>"572"),
		"573" => array("name"=> "西日本鉄道甘木線","code" =>"573"),
		"574" => array("name"=> "西日本鉄道貝塚線","code" =>"574"),
		"612" => array("name"=> "福岡市空港線","code" =>"612"),
		"613" => array("name"=> "福岡市箱崎線","code" =>"613"),
		"616" => array("name"=> "熊本市健軍線","code" =>"616"),
		"617" => array("name"=> "熊本市上熊本線","code" =>"617"),
		"618" => array("name"=> "鹿児島市谷山線","code" =>"618"),
		"619" => array("name"=> "鹿児島市唐湊線","code" =>"619"),
		"636" => array("name"=> "アストラムライン","code" =>"636"),
		"672" => array("name"=> "タンゴ鉄道宮津線","code" =>"672"),
		"673" => array("name"=> "タンゴ鉄道宮福線","code" =>"673"),
		"683" => array("name"=> "若桜鉄道","code" =>"683"),
		"684" => array("name"=> "錦川鉄道錦川清流線","code" =>"684"),
		"685" => array("name"=> "土佐くろしお宿毛線","code" =>"685"),
		"686" => array("name"=> "北九州高速鉄道","code" =>"686"),
	//	"687" => array("name"=> "","code" =>"687"),
	//	"688" => array("name"=> "","code" =>"688"),
	//	"689" => array("name"=> "平成筑豊鉄道","code" =>"689"),
		"690" => array("name"=> "甘木鉄道","code" =>"690"),
		"692" => array("name"=> "南阿蘇鉄道","code" =>"692"),
		"693" => array("name"=> "くま川鉄道","code" =>"693"),
		"712" => array("name"=> "松浦鉄道西九州線","code" =>"712"),
		"784" => array("name"=> "一畑電気鉄道松江線","code" =>"784"),
		"785" => array("name"=> "一畑電気鉄道大社線","code" =>"785"),
		"786" => array("name"=> "岡山電軌東山本線","code" =>"786"),
		"787" => array("name"=> "岡山電軌清輝橋線","code" =>"787"),
		"788" => array("name"=> "水島臨海鉄道","code" =>"788"),
		"789" => array("name"=> "広島電鉄宇品線","code" =>"789"),
		"790" => array("name"=> "広島電鉄本線","code" =>"790"),
	//	"791" => array("name"=> "","code" =>"791"),
		"792" => array("name"=> "広島電鉄皆実線","code" =>"792"),
	//	"793" => array("name"=> "","code" =>"793"),
		"794" => array("name"=> "広島電鉄江波線","code" =>"794"),
		"7941" => array("name"=> "広島電鉄横川線","code" =>"794"),
		"795" => array("name"=> "広島電鉄宮島線","code" =>"795"),
		"796" => array("name"=> "高松琴平電鉄琴平線","code" =>"796"),
		"797" => array("name"=> "高松琴平電鉄志度線","code" =>"797"),
		"798" => array("name"=> "高松琴平電鉄長尾線","code" =>"798"),
		"799" => array("name"=> "伊予鉄道高浜線","code" =>"799"),
		"800" => array("name"=> "伊予鉄道郡中線","code" =>"800"),
		"801" => array("name"=> "伊予鉄道横河原線","code" =>"801"),
		"802" => array("name"=> "伊予鉄道環状線","code" =>"802"),
		"805" => array("name"=> "伊予鉄道城南線","code" =>"805"),
		"806" => array("name"=> "土佐電気鉄道桟橋線","code" =>"806"),
		"807" => array("name"=> "土佐電気鉄道後免線","code" =>"807"),
		"808" => array("name"=> "土佐電気鉄道伊野線","code" =>"808"),
		"809" => array("name"=> "筑豊電気鉄道","code" =>"809"),
		"810" => array("name"=> "島原鉄道","code" =>"810"),
		"811" => array("name"=> "長崎電軌本線","code" =>"811"),
		"812" => array("name"=> "長崎電軌蛍茶屋支線","code" =>"812"),
		"813" => array("name"=> "長崎電軌桜町支線","code" =>"813"),
		"814" => array("name"=> "長崎電軌大浦支線","code" =>"814"),
		"815" => array("name"=> "熊本電気鉄道","code" =>"815"),
		"823" => array("name"=> "広島電鉄白島線","code" =>"823"),
		"824" => array("name"=> "伊予鉄道本町線","code" =>"824"),
		"999" => array("name"=> "井原鉄道","code" =>"999"),
		"1018"=> array("name"=> "土佐くろしおなはり","code" =>"1018"),
		"1021"=> array("name"=> "智頭急行","code" =>"1021"),
		"1022"=> array("name"=> "宮崎空港線","code" =>"1022"),
		"2012"=> array("name"=> "沖縄都市モノレール","code" =>"2012"),
		"2014"=> array("name"=> "九州新幹線","code" =>"2014"),
		"2015"=> array("name"=> "肥薩おれんじ鉄道","code" =>"2015"),
		"2019"=> array("name"=> "福岡市七隈線","code" =>"2019"),
		"2023"=> array("name"=> "スカイレール","code" =>"2023"),
		"2034"=> array("name"=> "門司港レトロ観光線","code" =>"2034"),

	);

	//n_rains csv format
	$work_n_rains_eki =
	array(	
		"1"  => array("ekiname"=> "糒","rosen" =>"687","eki" =>"7376"),
		"2"  => array("ekiname"=> "田川伊田","rosen" =>"687","eki" =>"4086"),
		"3"  => array("ekiname"=> "田川市立病院","rosen" =>"687","eki" =>"9829"),
		"4"  => array("ekiname"=> "上金田","rosen" =>"687","eki" =>"7375"),
		"5"  => array("ekiname"=> "金田","rosen" =>"687","eki" =>"7374"),
		"6"  => array("ekiname"=> "人見","rosen" =>"687","eki" =>"7373"),
		"7"  => array("ekiname"=> "ふれあい生力","rosen" =>"687","eki" =>"9081"),
		"8"  => array("ekiname"=> "市場","rosen" =>"687","eki" =>"7371"),
		"9"  => array("ekiname"=> "中泉","rosen" =>"687","eki" =>"7370"),
		"10" => array("ekiname"=> "藤棚","rosen" =>"687","eki" =>"7369"),
		"11" => array("ekiname"=> "あかぢ","rosen" =>"687","eki" =>"7368"),
		"12" => array("ekiname"=> "南直方御殿口","rosen" =>"687","eki" =>"9722"),
		"13" => array("ekiname"=> "直方","rosen" =>"687","eki" =>"3758"),
		"14" => array("ekiname"=> "下伊田","rosen" =>"687","eki" =>"7377"),
		"15" => array("ekiname"=> "赤池","rosen" =>"687","eki" =>"7372"),
		"16" => array("ekiname"=> "松山","rosen" =>"688","eki" =>"9529"),
		"17" => array("ekiname"=> "田川後藤寺","rosen" =>"688","eki" =>"4087"),
		"18" => array("ekiname"=> "糸田","rosen" =>"688","eki" =>"7379"),
		"19" => array("ekiname"=> "豊前大熊","rosen" =>"688","eki" =>"7378"),
		"20" => array("ekiname"=> "金田","rosen" =>"688","eki" =>"7374"),
		"21" => array("ekiname"=> "大藪","rosen" =>"688","eki" =>"7380"),
		"22" => array("ekiname"=> "田川伊田","rosen" =>"689","eki" =>"4086"),
		"23" => array("ekiname"=> "油須原","rosen" =>"689","eki" =>"7387"),
		"24" => array("ekiname"=> "上伊田","rosen" =>"689","eki" =>"9725"),
		"25" => array("ekiname"=> "美夜古泉","rosen" =>"689","eki" =>"7381"),
		"26" => array("ekiname"=> "勾金","rosen" =>"689","eki" =>"7389"),
		"27" => array("ekiname"=> "柿下温泉口","rosen" =>"689","eki" =>"6625"),
		"28" => array("ekiname"=> "内田","rosen" =>"689","eki" =>"7388"),
		"29" => array("ekiname"=> "赤","rosen" =>"689","eki" =>"9836"),
		"30" => array("ekiname"=> "崎山","rosen" =>"689","eki" =>"7386"),
		"31" => array("ekiname"=> "犀川","rosen" =>"689","eki" =>"7385"),
		"32" => array("ekiname"=> "東犀川三四郎","rosen" =>"689","eki" =>"6626"),
		"33" => array("ekiname"=> "新豊津","rosen" =>"689","eki" =>"7384"),
		"34" => array("ekiname"=> "今川河童","rosen" =>"689","eki" =>"7382"),
		"35" => array("ekiname"=> "行橋","rosen" =>"689","eki" =>"400"),
		"36" => array("ekiname"=> "源じいの森","rosen" =>"689","eki" =>"9082"),
		"37" => array("ekiname"=> "豊津","rosen" =>"689","eki" =>"7383"),

	);

