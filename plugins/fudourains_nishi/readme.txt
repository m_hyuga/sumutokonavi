=== fudou ===
Contributors: nendeb
Tags: fudousan,map,bukken,estate,fudou,GoogleMaps
Requires at least: 3.9.2
Tested up to: 4.0
Stable tag: 1.5.0

Fudousan Nishinihon Rains CSV Import

== Description ==

Fudousan Nishinihon Rains CSV Import

== Installation ==

Installing the plugin:
1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `fudourains_nishi` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.

== Requirements ==

* WordPress 3.9.2 or later
* PHP 5.2 or later (NOT support PHP 4.x!!)

== Credits ==

This plugin uses  Fudousan Plugin

== Upgrade Notice ==

The required WordPress version has been changed and now requires WordPress 3.9.2 or higher

== Frequently Asked Questions ==

Use these support channels appropriately.

1. [Docs](http://nendeb.jp/)

== Changelog ==
= v1.5.0 =
* Fixed WordPress4.0.

= v1.0.1 =
* Fixed WordPress3.9a

= v1.0.0 =
* Initial version of the plugin

