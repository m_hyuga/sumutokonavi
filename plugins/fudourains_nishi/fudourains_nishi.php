<?php
/*
Plugin Name: Fudousan Nishinihon Rains
Plugin URI: http://nendeb.jp/
Description: Fudousan Plugin for n_rains
Version: 1.5.0
Author: nendeb
Author URI: http://nendeb.jp/
License: GPLv2
*/

// Define current version constant
define( 'FUDOU_N_RAINS_VERSION', '1.5.0' );

/*  Copyright 2014 nendeb (email : nendeb@gmail.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


if( !defined('WP_CONTENT_URL') )	define('WP_CONTENT_URL', get_option('siteurl').'/wp-content');
if( !defined('WP_CONTENT_DIR') )	define('WP_CONTENT_DIR', ABSPATH.'wp-content');
if( !defined('WP_PLUGIN_URL') )		define('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');
if( !defined('WP_PLUGIN_DIR') )		define('WP_PLUGIN_DIR', WP_CONTENT_DIR.'/plugins');

require_once  'csv_import/csv_import_n.php' ;

/**
 *
 * View Version in Footer
 *
 */
function fudou_n_rains_footer_version() {
	echo "<!-- FUDOU N_RAINS VERSION " . FUDOU_N_RAINS_VERSION . " -->\n";
}
add_filter( 'wp_footer', 'fudou_n_rains_footer_version' );

function database_initialization_fudounrains() {
	$fudou_version = '';
	if ( defined('FUDOU_VERSION') ){
		$fudou_version = FUDOU_VERSION;
	}	
	if ( version_compare( $fudou_version , '1.5', '<') ) { 
		if( !defined('DB_KEN_TABLE') )		define('DB_KEN_TABLE'	,'area_middle_area');
		if( !defined('DB_SHIKU_TABLE') )	define('DB_SHIKU_TABLE'	,'area_narrow_area');
		if( !defined('DB_ROSENKEN_TABLE') )	define('DB_ROSENKEN_TABLE','train_area_rosen');
		if( !defined('DB_ROSEN_TABLE') )	define('DB_ROSEN_TABLE'	,'train_rosen');
		if( !defined('DB_EKI_TABLE') )		define('DB_EKI_TABLE'	,'train_station');
	} 
}
add_action('plugins_loaded', 'database_initialization_fudounrains');
