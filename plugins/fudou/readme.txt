=== fudou ===
Contributors: nendeb
Tags: fudousan,csv,import,bukken,estate,fudou
Requires at least: 3.9.3
Tested up to: 4.2
Stable tag: 1.6.3

Fudousan Plugin for Real Estate

== Description ==
Fudousan Plugin for Real Estate


== Installation ==
* Installing the plugin:
1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `fudou` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.


== Requirements ==
* WordPress 3.9.3 or later
* PHP 5.2 or later (NOT support PHP 4.x!!)


== Credits ==
* This plugin uses [php-csv-parser] by Kazuyoshi Tlacaelel.  
* This plugin uses [CSV Importer] by Denis Kobozev.  
* This plug-in is not guaranteed.  


== Upgrade Notice ==
The required WordPress version has been changed and now requires WordPress 3.9.3 or higher


== Changelog ==
= 1.6.3 =
* Fixed inc-archive-fudo.php
* Fixed WordPress4.2

= 1.6.1 =
* Fixed archive-fudo.php
* Fixed inc-archive-fudo.php

= 1.6.0 =
* Support Bus-Lines Plugin.
* Update Rosen,Eki data.
* Fixed single-fudo.php
* Fixed inc-single-fudo.php
* Fixed archive-fudo.php
* Fixed inc-archive-fudo.php
* Fixed themes css.
* Fixed Other Files.

= 1.5.6 =
* Fixed inc-archive-fudo.php
* Fixed archive-fudo.php

= 1.5.5 =
* Fixed WordPress4.1b
* Add TwentyFifteen Support.
* Fixed fudo-widget2.php
* Fixed archive-fudo.php
* Fixed single-fudo.php

= 1.5.3 =
* Fixed Permalinks Check.
* Update city data.
* Update database Item.

= 1.5.1 =
* Fixed IsNum Check.

= 1.5.0 =
* Fixed WordPress4.0.
* Update database Item.

= 1.4.6 =
* Fixed contact-form-7 ver3.7 or later.

= 1.4.5 =
* Fixed WordPress3.9b
* Fixed EkiName.
* Fixed media-uploader.

= 1.4.4 =
* Add n_rains format.

= 1.4.3 =
* Update station data.

= 1.4.2 =
* Fixed WordPress3.8.
* Fixed twentyfourteen css.

= 1.4.1 =
* Fixed twentyfourteen css.

= 1.4.0 =
* Fixed WordPress3.7.
* Fixed function names.
* Add twentyfourteen Support.
* Fixed admin login user_check.

= 1.3.5 =
* Add admin login user_check.

= 1.3.4 =
* Fixed archive-fudo.
* Fixed WordPress3.6.
* Fixed style20XX.css.

= 1.3.3 =
* Fixed archive-fudo.
* Fixed single-fudo.

= 1.3.2 =
* Fixed Item Name.

= 1.3.1 =
* Add body class.

= 1.3.0 =
* Fixed fudo-widget.
* Fixed archive-fudo.
* Fixed contactform7 3.4
* Add twentythirteen(beta1) Support.

= 1.2.1 =
* Fixed Twitter API Ver1.1
* Update area/station db.

= 1.2.0 =
* Fixed WordPress3.5

= 1.1.3 =
* Fixed single-fudo.
* Fixed fudou_admin_too.l
* Fixed widget2.
* Fixed csv import.
* Add bukken Item.
* Add image Item.
* Add widget4.
* Add twentytwelve Support.

= 1.1.2 =
* Add bukken Item.
* Fixed csv_import.

= 1.1.1 =
* Fixed single-fudo.
* Fixed contact.php.

= 1.1.0 =
* Fixed single-fudo.
* Fixed archive-fudo.
* Fixed page-jyoken.

= 1.0.9 =
* Fixed top items.
* Fixed archive-fudo
* Fixed single-fudo
* Fixed widget3

= 1.0.8 =
* Fixed csv Import.
* Fixed jyoken items.
* Update area station db.
* Fixed admin bukkenlist.

= 1.0.7 =
* Fixed kaiin items.
* Add   kaiin in csv.

= 1.0.6 =
* Fixed kaiin items.

= 1.0.5 =
* Fixed other items.

= 1.0.4 =
* Fixed fudo-widget2
* Fixed archive-fudo

= 1.0.3 =
* Fixed admin_phpex.
* Fixed admin_fudou.

= 1.0.2 =
* Fixed database Item.
* Fixed: twentyeleven css.
* Fixed: Output xls.

= 1.0.1 =
* Fixed: for WordPress3.3.

= 1.0.0 =
* Initial version of the plugin.

