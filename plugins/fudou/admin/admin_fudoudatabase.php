<?php
/*
 * 不動産プラグイン地域・路線データーメンテナンス
 * @package WordPress4.1
 * @subpackage Fudousan Plugin
 * Version: 1.6.0
*/


//地域・路線データーメンテナンス
function fudou_admin_database_maintenance() {
	require_once ABSPATH . '/wp-admin/admin.php';
	$plugin = new FudouPluginDatabaseMaintenance;
	add_options_page( 'admin_menu_riyo', '地域・路線データーメンテナンス', 'activate_plugins', __FILE__, array($plugin, 'form' ) );
}
add_action( 'admin_menu', 'fudou_admin_database_maintenance' );

//submenu hidden
function remove_fudou_admin_database_maintenance() {
	remove_submenu_page('options-general.php', 'fudou/admin/admin_fudoudatabase.php' );
}
add_action( 'admin_menu', 'remove_fudou_admin_database_maintenance', 11 );


class FudouPluginDatabaseMaintenance {

	function form() {
		global $wpdb;

		$updated_fade = '';

		//地域・路線データー更新
		$db_renew = isset($_POST['db_renew']) ? $_POST['db_renew'] : 0;

		if( $db_renew ){

			check_admin_referer( 'fudou_roseneki_db_renew' );

			//テーブル
			$table_name1 = $wpdb->prefix . DB_KEN_TABLE;
			$table_name2 = $wpdb->prefix . DB_SHIKU_TABLE;
			$table_name3 = $wpdb->prefix . DB_ROSENKEN_TABLE;
			$table_name4 = $wpdb->prefix . DB_ROSEN_TABLE;
			$table_name5 = $wpdb->prefix . DB_EKI_TABLE;

			//削除
			$sql = "DROP TABLE IF EXISTS $table_name1 , $table_name2 , $table_name3 , $table_name4 , $table_name5";
			$result = $wpdb->query( $sql );

			//登録
			if( $result ){
				require_once ( ABSPATH . '/wp-content/plugins/fudou/data/fudo-configdatabase.php'  );
				databaseinstallation_fudo(0);

				$results1 = $wpdb->get_var("show tables like '$table_name1'") ;
				$results2 = $wpdb->get_var("show tables like '$table_name2'") ;
				$results3 = $wpdb->get_var("show tables like '$table_name3'") ;
				$results4 = $wpdb->get_var("show tables like '$table_name4'") ;
				$results5 = $wpdb->get_var("show tables like '$table_name5'") ;

				if( empty( $results1 ) || empty( $results2 ) || empty( $results3 ) || empty( $results4 ) || empty( $results5 ) ){
					$updated_fade = '地域・路線データーの更新に失敗しました。';
				}else{
					$updated_fade = '地域・路線データーを更新しました。';
				}
			}else{
					$updated_fade = '地域・路線データーの削除に失敗しました。';
			}
		}

		?>

		<div class="wrap">
			<div id="icon-tools" class="icon32"><br /></div>
			<h2>不動産プラグイン 地域・路線データーメンテナンス</h2>
			<div id="poststuff">

			<div id="post-body">
			<div id="post-body-content">

				<?php if( $updated_fade ) : ?>
				<div id="message" class="updated fade"><p><strong><?php echo $updated_fade; ?></strong></p></div>
				<?php endif; ?> 


			        <h2>地域・路線データー</h2>
				<div style="margin:0 0 0 20px;">
				<?php
					include_once( WP_PLUGIN_DIR . '/fudou/data/fudo_database.php');

					$fudo_area_db_version = '';
					$fudo_train_db_version = '';

					if( defined('FUDOU_AREA_DB_VERSION') )  $fudo_area_db_version  = FUDOU_AREA_DB_VERSION;
					if( defined('FUDOU_TRAIN_DB_VERSION') ) $fudo_train_db_version = FUDOU_TRAIN_DB_VERSION;

					echo '地域データーバージョン ' . get_option("fudo_area_db_version") ;
					if( get_option("fudo_area_db_version") != $fudo_area_db_version && $fudo_area_db_version ){
						echo  '　　<font color="#ff3300">※新バージョンのデーターがあります。 (' . $fudo_area_db_version . ')</font>';
					}
					echo '<br />';

					echo '路線データーバージョン ' . get_option("fudo_train_db_version");
					if( get_option("fudo_train_db_version") != $fudo_train_db_version && $fudo_train_db_version ){
						echo  '　　<font color="#ff3300">※新バージョンのデーターがあります。 (' . $fudo_train_db_version . ')</font>';
					}

					//テーブルチェック
					$table_name1 = $wpdb->prefix . DB_KEN_TABLE;
					$table_name2 = $wpdb->prefix . DB_SHIKU_TABLE;
					$table_name3 = $wpdb->prefix . DB_ROSENKEN_TABLE;
					$table_name4 = $wpdb->prefix . DB_ROSEN_TABLE;
					$table_name5 = $wpdb->prefix . DB_EKI_TABLE;

					$results1 = $wpdb->get_var("show tables like '$table_name1'") ;
					$results2 = $wpdb->get_var("show tables like '$table_name2'") ;
					$results3 = $wpdb->get_var("show tables like '$table_name3'") ;
					$results4 = $wpdb->get_var("show tables like '$table_name4'") ;
					$results5 = $wpdb->get_var("show tables like '$table_name5'") ;

					if( empty($results1) || empty($results2) || empty($results3) || empty($results4) || empty($results5) ){
						$db_table_check = false;
					}else{
						$db_table_check = true;
					}

					if( !$db_table_check || get_option("fudo_area_db_version") != $fudo_area_db_version || get_option("fudo_train_db_version") != $fudo_train_db_version ){
						echo '<form name="db_renew" id="db_renew" method="post">';
						wp_nonce_field( 'fudou_roseneki_db_renew' ); 
						echo '<input type="hidden" name="db_renew" value="1" />';
						echo '<p class="submit"><input type="submit" name="submit" id="submit" class="button-primary" value="地域・路線データー更新"  /></p>';
						echo '</form>';
						echo '※地域・路線データーを更新する際には必ず事前にデーターベースのバックアップを取ってください。';
					}
				?>
				</div>
				<br />
				<br />
			</div><!-- post-body-content  -->
			</div><!-- post-body -->
			</div><!-- poststuff -->

		</div><!-- wrap -->

		<script type="text/javascript">
			jQuery("#db_renew").submit(function(){
				if(confirm("地域・路線データー更新します。\nよろしいですか?")){return true; }else{ return false; }
			});
		</script>
		<?php
	}

}

