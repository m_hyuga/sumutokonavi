<?php




// 坪単価
function mylist_custom_kakakutsubo_print($post_id) {
	$kakakutsubo_data = get_post_meta($post_id,'kakakutsubo',true);
	if(is_numeric($kakakutsubo_data)){
		echo floatval($kakakutsubo_data)/10000;
		echo "万円";
	}
}


//7 種別 物件種別
function mylist_custom_bukkenshubetsu_print($post_id) {

	$bukkenshubetsu_data = get_post_meta($post_id,'bukkenshubetsu',true);

	if($bukkenshubetsu_data=="1101")  echo '売地';
	if($bukkenshubetsu_data=="1102")  echo '借地権譲渡';
	if($bukkenshubetsu_data=="1103")  echo '底地権譲渡';
	if($bukkenshubetsu_data=="1201")  echo '新築戸建';
	if($bukkenshubetsu_data=="1202")  echo '中古戸建';
	if($bukkenshubetsu_data=="1203")  echo '新築テラスハウス';
	if($bukkenshubetsu_data=="1204")  echo '中古テラスハウス';
	if($bukkenshubetsu_data=="1301")  echo '新築マンション';
	if($bukkenshubetsu_data=="1302")  echo '中古マンション';
	if($bukkenshubetsu_data=="1303")  echo '新築公団';
	if($bukkenshubetsu_data=="1304")  echo '中古公団';
	if($bukkenshubetsu_data=="1305")  echo '新築公社';
	if($bukkenshubetsu_data=="1306")  echo '中古公社';
	if($bukkenshubetsu_data=="1307")  echo '新築タウン';
	if($bukkenshubetsu_data=="1308")  echo '中古タウン';
	if($bukkenshubetsu_data=="1309")  echo 'リゾートマン';
	if($bukkenshubetsu_data=="1401")  echo '店舗';
	if($bukkenshubetsu_data=="1403")  echo '店舗付住宅';
	if($bukkenshubetsu_data=="1404")  echo '住宅付店舗';
	if($bukkenshubetsu_data=="1405")  echo '事務所';
	if($bukkenshubetsu_data=="1406")  echo '店舗・事務所';
	if($bukkenshubetsu_data=="1407")  echo 'ビル';
	if($bukkenshubetsu_data=="1408")  echo '工場';
	if($bukkenshubetsu_data=="1409")  echo 'マンション';
	if($bukkenshubetsu_data=="1410")  echo '倉庫';
	if($bukkenshubetsu_data=="1411")  echo 'アパート';
	if($bukkenshubetsu_data=="1412")  echo '寮';
	if($bukkenshubetsu_data=="1413")  echo '旅館';
	if($bukkenshubetsu_data=="1414")  echo 'ホテル';
	if($bukkenshubetsu_data=="1415")  echo '別荘';
	if($bukkenshubetsu_data=="1416")  echo 'リゾートマン';
	if($bukkenshubetsu_data=="1420")  echo '社宅';
	if($bukkenshubetsu_data=="1499")  echo 'その他';
	if($bukkenshubetsu_data=="1502")  echo '店舗';
	if($bukkenshubetsu_data=="1505")  echo '事務所';
	if($bukkenshubetsu_data=="1506")  echo '店舗・事務所';
	if($bukkenshubetsu_data=="1507")  echo 'ビル';
	if($bukkenshubetsu_data=="1509")  echo 'マンション';
	if($bukkenshubetsu_data=="1599")  echo 'その他';
	if($bukkenshubetsu_data=="3101")  echo 'マンション';
	if($bukkenshubetsu_data=="3102")  echo 'アパート';
	if($bukkenshubetsu_data=="3103")  echo '一戸建';
	if($bukkenshubetsu_data=="3104")  echo 'テラスハウス';
	if($bukkenshubetsu_data=="3105")  echo 'タウンハウス';
	if($bukkenshubetsu_data=="3106")  echo '間借り';
	if($bukkenshubetsu_data=="3110")  echo '寮・下宿';
	if($bukkenshubetsu_data=="3201")  echo '店舗(建物全部)';
	if($bukkenshubetsu_data=="3202")  echo '店舗(建物一部)';
	if($bukkenshubetsu_data=="3203")  echo '事務所';
	if($bukkenshubetsu_data=="3204")  echo '店舗・事務所';
	if($bukkenshubetsu_data=="3205")  echo '工場';
	if($bukkenshubetsu_data=="3206")  echo '倉庫';
	if($bukkenshubetsu_data=="3207")  echo '一戸建';
	if($bukkenshubetsu_data=="3208")  echo 'マンション';
	if($bukkenshubetsu_data=="3209")  echo '旅館';
	if($bukkenshubetsu_data=="3210")  echo '寮';
	if($bukkenshubetsu_data=="3211")  echo '別荘';
	if($bukkenshubetsu_data=="3212")  echo '土地';
	if($bukkenshubetsu_data=="3213")  echo 'ビル';
	if($bukkenshubetsu_data=="3214")  echo '住宅付店舗(一戸建)';
	if($bukkenshubetsu_data=="3215")  echo '住宅付店舗(建物一部)';
	if($bukkenshubetsu_data=="3282")  echo '駐車場';
	if($bukkenshubetsu_data=="3299")  echo 'その他';

	if($bukkenshubetsu_data=="3122")  echo 'コーポ';
	if($bukkenshubetsu_data=="3123")  echo 'ハイツ';
	if($bukkenshubetsu_data=="3124")  echo '文化住宅';
	if($bukkenshubetsu_data=="1104")  echo '建付土地';

}


// 17所在地 所在地コード
function mylist_custom_shozaichi_print($post_id) {
	global $wpdb;

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = myLeft($shozaichiken_data,2);

	if($shozaichiken_data=="")
		$shozaichiken_data = get_post_meta($post_id,'shozaichiken',true);

		$sql = "SELECT `middle_area_name` FROM `".$wpdb->prefix."area_middle_area` WHERE `middle_area_id`=".$shozaichiken_data."";
		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_row( $sql );
		echo $metas->middle_area_name;

	$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichicode_data = myLeft($shozaichicode_data,5);
	$shozaichicode_data = myRight($shozaichicode_data,3);

	if($shozaichiken_data !="" && $shozaichicode_data !=""){
		$sql = "SELECT narrow_area_name FROM ".$wpdb->prefix."area_narrow_area WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_row( $sql );
		echo $metas->narrow_area_name;
	}
}

// 22交通路線1

function mylist_custom_koutsu1_print($post_id) {
	global $wpdb;

	$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
	$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = myLeft($shozaichiken_data,2);

	if($koutsurosen_data !=""){
		$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) $rosen_name = $metas->rosen_name;
		echo "".$rosen_name;
	}

	if($koutsurosen_data !="" && $koutsueki_data !=""){
		$sql = "SELECT DTS.station_name";
		$sql .=  " FROM ".$wpdb->prefix."train_rosen AS DTR";
		$sql .=  " INNER JOIN ".$wpdb->prefix."train_station as DTS ON DTR.rosen_id = DTS.rosen_id";
		$sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) {
			if($metas->station_name != '＊＊＊＊') 	echo $metas->station_name.'駅';
		}
	}

	$koutsubusstei=get_post_meta($post_id, 'koutsubusstei1', true);
	$koutsubussfun=get_post_meta($post_id, 'koutsubussfun1', true);
	$koutsutohob1f=get_post_meta($post_id, 'koutsutohob1f', true);
	if($koutsubusstei !="" || $koutsubussfun !=""){

		if($rosen_name == 'バス'){
			echo '('.$koutsubusstei.' '.$koutsubussfun.'分';
		}else{
			echo ' バス('.$koutsubusstei.' '.$koutsubussfun.'分';
		}

		if($koutsutohob1f !="" )
			echo ' 停歩'.$koutsutohob1f.'分';
		echo ')';
	}


	if(get_post_meta($post_id, 'koutsutoho1', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1', true).'m';

	if(get_post_meta($post_id, 'koutsutoho1f', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho1f', true).'分';

}
function mylist_custom_koutsu2_print($post_id) {
	global $wpdb;

	$koutsurosen_data = get_post_meta($post_id, 'koutsurosen2', true);
	$koutsueki_data = get_post_meta($post_id, 'koutsueki2', true);

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = myLeft($shozaichiken_data,2);

	if($koutsurosen_data !=""){
		$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) $rosen_name = $metas->rosen_name;
		echo "<br />".$rosen_name;
	}

	if($koutsurosen_data !="" && $koutsueki_data !=""){
		$sql = "SELECT DTS.station_name";
		$sql .=  " FROM ".$wpdb->prefix."train_rosen AS DTR";
		$sql .=  " INNER JOIN ".$wpdb->prefix."train_station AS DTS ON DTR.rosen_id = DTS.rosen_id";
		$sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) {
			if($metas->station_name != '＊＊＊＊') 	echo $metas->station_name.'駅';
		}
	}

	$koutsubusstei=get_post_meta($post_id, 'koutsubusstei2', true);
	$koutsubussfun=get_post_meta($post_id, 'koutsubussfun2', true);
	$koutsutohob2f=get_post_meta($post_id, 'koutsutohob2f', true);
	if($koutsubusstei !="" || $koutsubussfun !=""){

		if($rosen_name == 'バス'){
			echo '('.$koutsubusstei.' '.$koutsubussfun.'分';
		}else{
			echo ' バス('.$koutsubusstei.' '.$koutsubussfun.'分';
		}

		if($koutsutohob2f !="" )
			echo ' 停歩'.$koutsutohob2f.'分';
		echo ')';
	}


	if(get_post_meta($post_id, 'koutsutoho2', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2', true).'m';

	if(get_post_meta($post_id, 'koutsutoho2f', true) !="")
		echo ' 徒歩'.get_post_meta($post_id, 'koutsutoho2f', true).'分';

	echo '';

}


// 71建物	建物構造
function mylist_custom_tatemonokozo_print($post_id) {
	$tatemonokozo_data = get_post_meta($post_id,'tatemonokozo',true);
	if($tatemonokozo_data=="1") 	echo '木造';
	if($tatemonokozo_data=="2") 	echo 'ブロック';
	if($tatemonokozo_data=="3") 	echo '鉄骨造';
	if($tatemonokozo_data=="4") 	echo 'RC';
	if($tatemonokozo_data=="5") 	echo 'SRC';
	if($tatemonokozo_data=="6") 	echo 'PC';
	if($tatemonokozo_data=="7") 	echo 'HPC';
	if($tatemonokozo_data=="9") 	echo 'その他';
	if($tatemonokozo_data=="10") 	echo '軽量鉄骨';
	if($tatemonokozo_data=="11") 	echo 'ALC';
	if($tatemonokozo_data=="12") 	echo '鉄筋ブロック';
	if($tatemonokozo_data=="13") 	echo 'CFT(コンクリート充填鋼管)';

	//text
	if( $tatemonokozo_data !='' && !is_numeric($tatemonokozo_data) ) echo $tatemonokozo_data;

}

// 72建物	建物面積計測方式
function mylist_custom_tatemonohosiki_print($post_id) {
	if(get_post_meta($post_id,'tatemonohosiki',true)=="1")	echo '壁芯';
	if(get_post_meta($post_id,'tatemonohosiki',true)=="2")	echo '内法';
	//text
	if( get_post_meta($post_id,'tatemonohosiki',true) !='' && !is_numeric(get_post_meta($post_id,'tatemonohosiki',true)) ) echo get_post_meta($post_id,'tatemonohosiki',true);

}

// 80建物	新築・未入居
function mylist_custom_tatemonoshinchiku_print($post_id) {
	if(get_post_meta($post_id,'tatemonoshinchiku',true)=="0") echo '中古　';
	if(get_post_meta($post_id,'tatemonoshinchiku',true)=="1") echo '新築未入居　';
	//text
	if( get_post_meta($post_id,'tatemonoshinchiku',true) !='' && !is_numeric(get_post_meta($post_id,'tatemonoshinchiku',true)) ) echo get_post_meta($post_id,'tatemonoshinchiku',true).'　';
}



// 88	間取り	間取部屋数
// 89	間取り	間取部屋種類
function mylist_custom_madorisu_print($post_id) {
	$madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);
	echo get_post_meta($post_id,'madorisu',true);
	if($madorisyurui_data=="10")	echo 'R';
	if($madorisyurui_data=="20")	echo 'K';
	if($madorisyurui_data=="25")	echo 'SK';
	if($madorisyurui_data=="30")	echo 'DK';
	if($madorisyurui_data=="35")	echo 'SDK';
	if($madorisyurui_data=="40")	echo 'LK';
	if($madorisyurui_data=="45")	echo 'SLK';
	if($madorisyurui_data=="50")	echo 'LDK';
	if($madorisyurui_data=="55")	echo 'SLDK';
}


//139	金銭面	賃料・価格 ※ 単位：円
function mylist_custom_kakaku_print($post_id) {
	//非公開の場合
	if(get_post_meta($post_id,'kakakukoukai',true) == "0"){

		$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
		if($kakakujoutai_data=="1")	echo '相談';
		if($kakakujoutai_data=="2")	echo '確定';
		if($kakakujoutai_data=="3")	echo '入札';

	}else{
		$kakaku_data = get_post_meta($post_id,'kakaku',true);
		if(is_numeric($kakaku_data)){
			echo floatval($kakaku_data)/10000;
			echo "万円";
		}
	}
}

// 147	金銭面	礼金・月数 ※ 100以上の場合は単位は"円"、それ以外は"ヶ月"
function mylist_custom_kakakureikin_print($post_id) {
	$kakakureikin_data = get_post_meta($post_id,'kakakureikin',true);
//	if(is_numeric($kakakureikin_data)){
		if($kakakureikin_data >= 100) {
			echo floatval($kakakureikin_data)/10000;
			echo "万円";
		}else{
			echo $kakakureikin_data;
			echo "ヶ月";
		}
//	}
}

// 149	金銭面	敷金・月数 ※ 100以上の場合は単位は"円"、それ以外は"ヶ月" 
function mylist_custom_kakakushikikin_print($post_id) {
	$kakakushikikin_data = get_post_meta($post_id,'kakakushikikin',true);
//	if(is_numeric($kakakushikikin_data)){
		if($kakakushikikin_data >= 100) {
			echo floatval($kakakushikikin_data)/10000;
			echo "万円";
		}else{
			echo $kakakushikikin_data;
			echo "ヶ月";
		}
//	}
}

// 150	金銭面	保証金・月数 ※ 100以上の場合は単位は"円"、それ以外は"ヶ月"
function mylist_custom_kakakuhoshoukin_print($post_id) {
	$kakakuhoshoukin_data = get_post_meta($post_id,'kakakuhoshoukin',true);
//	if(is_numeric($kakakuhoshoukin_data)){
		if($kakakuhoshoukin_data >= 100) {
			echo floatval($kakakuhoshoukin_data)/10000;
			echo "万円";
		}else{
			echo $kakakuhoshoukin_data;
			echo "ヶ月";
		}
//	}
}

// 151	金銭面	権利金 100以上の場合は単位は"円"、それ以外は"ヶ月"
function mylist_custom_kakakukenrikin_print($post_id) {
	$kakakukenrikin_data = get_post_meta($post_id,'kakakukenrikin',true);
	if(is_numeric($kakakukenrikin_data)){
		if($kakakukenrikin_data >= 100) {
			echo floatval($kakakukenrikin_data)/10000;
			echo "万円";
		}else{
			echo $kakakukenrikin_data;
			echo "ヶ月";
		}
	}
}

// 155	金銭面	償却・敷引金 1～99:"ヶ月"、101～200:100を引いて"%" 201以上の場合単位は"円"
function mylist_custom_kakakushikibiki_print($post_id) {
	$kakakushikibiki_data = get_post_meta($post_id,'kakakushikibiki',true);
//	if(is_numeric($kakakushikibiki_data)){
		if($kakakushikibiki_data < 100) {
			echo $kakakushikibiki_data;
			echo "ヶ月";
		}elseif($kakakushikibiki_data>100 && $kakakushikibiki_data<=200){
			echo floatval($kakakushikibiki_data)-100;
			echo "%";
		}elseif($kakakushikibiki_data>200){
			echo floatval($kakakushikibiki_data)/10000;
			echo "万円";
		}
//	}
}

// 178	駐車場	駐車場料金
// 180	駐車場	駐車場区分
function mylist_custom_chushajo_print($post_id) {
	$chushajoryokin_data = get_post_meta($post_id,'chushajoryokin',true);
	if($chushajoryokin_data !="")
		echo $chushajoryokin_data.'円　';
	$chushajokubun_data = get_post_meta($post_id,'chushajokubun',true);
	if($chushajokubun_data=="1")	echo '空有';
	if($chushajokubun_data=="2")	echo '空無';
	if($chushajokubun_data=="3")	echo '近隣';
	if($chushajokubun_data=="4")	echo '無';
	//text
	if( $chushajokubun_data !='' && !is_numeric($chushajokubun_data) ) echo $chushajokubun_data;


}

//ページナビゲション
// function f_page_navi($fw_record_count,$fw_page_size,$fw_page_count,$bukken_sort,$bukken_order,$bukken_page_data,$s,$joken_url){

// 	$navi_max = 5;
// 	$k = 0;

// 	$move_str = $fw_record_count.'件 ';

// 	if($fw_page_count=="")
// 		$fw_page_count =1;

// 	if ($fw_record_count > $fw_page_size){

// 		$w_max_page = intval($fw_record_count / $fw_page_size);

// 		if( ($fw_record_count % $fw_page_size) <> 0 )
// 			$w_max_page = $w_max_page + 1;

// 		if( intval($fw_page_count) >= intval($navi_max)){
// 			$w_loop_start = $fw_page_count - intval($navi_max/2);
// 		}else{
// 			$w_loop_start = 1;
// 		}

// 		if( $w_max_page < ($fw_page_count + intval($navi_max/2)))
// 			$w_loop_start = $w_max_page-$navi_max + 1;

// 		if( $w_loop_start < 1)
// 			$w_loop_start =  1;


// 		if( $fw_page_count > 1){
// 			$move_str .='<a href="'.$joken_url.'&amp;paged='.($fw_page_count-1).'&amp;so='.$bukken_sort.'&amp;ord='.$bukken_order.'&amp;s='.$s.'">&laquo;</a> ';
// 		}


// 		if( $w_loop_start <> 1)
// 			$move_str .='<a href="'.$joken_url.'&amp;paged=&amp;so='.$bukken_sort.'&amp;ord='.$bukken_order.'&amp;s='.$s.'">1</a> ';

// 		if( $w_loop_start > 2)
// 			$move_str .='.. ';


// 		for ($j=$w_loop_start; $j<$w_max_page+1;$j++){

// 			if ($j == $fw_page_count){
// 				$move_str .='<b>'.$j.'</b> ';
// 			}else{
// 				$move_str .='<a href="'.$joken_url.'&amp;paged='.$j.'&amp;so='.$bukken_sort.'&amp;ord='.$bukken_order.'&amp;s='.$s.'">'.$j.'</a> ';
// 			}
			
// 			$k++;
// 			if ($k >= $navi_max)
// 				break;
// 		}

// 		if($w_max_page > $j)
// 			$move_str .='.. ';

// 		if($w_max_page > $j ){
// 			if( $w_max_page > ($fw_page_count + intval($navi_max/2)) )
// 				$move_str .='<a href="'.$joken_url.'&amp;paged='.($w_max_page).'&amp;so='.$bukken_sort.'&amp;ord='.$bukken_order.'&amp;s='.$s.'">'.$w_max_page.'</a> ';
// 		}

// 		if( $fw_record_count > $fw_page_size * $fw_page_count){
// 			$move_str .='<a href="'.$joken_url.'&amp;paged='.($fw_page_count+1).'&amp;so='.$bukken_sort.'&amp;ord='.$bukken_order.'&amp;s='.$s.'">&raquo;</a>';
// 		}


// 		if( $fw_page_count > 1){
// 			$w_first_page = ($fw_page_count - 1) * $fw_page_size;
// 		}else{
// 			$w_first_page = 1;
// 		}

// 		return $move_str;
// 	}


// }

// 225物件画像タイプ

function mylist_custom_fudoimgtype_print($imgtype) {

	switch ($imgtype) {
		case "1" :
			$imgtype = "(間取)"; break;
		case "2" :
			$imgtype = "(外観)"; break;
		case "3" :
			$imgtype = "(地図)"; break;
		case "4" :
			$imgtype = "(周辺)"; break;
		case "5" :
			$imgtype = "(内装)"; break;
		case "9" :
			$imgtype = ""; break;	//(その他画像)
		case "10" :
			$imgtype = "(玄関)"; break;
		case "11" :
			$imgtype = "(居間)"; break;
		case "12" :
			$imgtype = "(キッチン)"; break;
		case "13" :
			$imgtype = "(寝室)"; break;
		case "14" :
			$imgtype = "(子供部屋)"; break;
		case "15" :
			$imgtype = "(風呂)"; break;
	}

	return $imgtype;
}



