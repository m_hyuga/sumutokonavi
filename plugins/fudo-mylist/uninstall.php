<?php

if( !defined( 'ABSPATH') && !defined('WP_UNINSTALL_PLUGIN') )
    exit();

$pid = get_option('fudo-mylist-mylist');
 wp_delete_post( $pid );

delete_option('fudo-mylist-mylist'); // あと片付け
delete_option('fudo-mylist-theme');
delete_option('fudo-mylist-expires');
delete_option('fudo-mylist-css');


?>