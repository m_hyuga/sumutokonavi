﻿=== Fudousn Share Buttons ===
Contributors: nendeb
Tags: Fudousn Share Buttons
Requires at least: 4.1.5
Tested up to: 4.2
Stable tag: 0.5.3

Share Buttons for Wordpress.

== Description ==
The Jetpack-like Share Buttons for Wordpress.


== Installation ==
Installing the plugin:
1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `fudou-share-bottons` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.


== Credits ==
SVG Icon  
https://icomoon.io/


= Other Notes =
* If you would set with other place you can set do_action code .
	//For Auto
	<?php do_action( 'fudou_share_buttons_do' ); ?>
	//For Fixation Single Page
	<?php do_action( 'fudou_share_buttons_do', $post->ID ); ?>
	//For Fixation Toppage
	<?php do_action( 'fudou_share_buttons_do', 0 ); ?>
	//For Count Cache Onry
	<?php do_action( 'fudou_share_buttons_do', $post->ID, 1 ); ?>

* If you want to change the transient time.
	add_filter( 'fudou_share_buttons_transient_time', function(){ return {hours}; }  );

* If you want to sort or change  the button items.
	add_filter( 'fudou_share_buttons_list', function(){ return array( 'twitter', 'facebook', 'googleplus', 'hatena', 'feedly', 'pocket', 'evernote', 'line', 'rss' ); }  );

* If you want the CSS file in inert.
	if( isset( $fudou_share_buttons ) ){
		remove_action( 'wp_enqueue_scripts', array( $fudou_share_buttons, 'fudou_svgicons_css' ) );
	}

* If user_profile_fields 'Twitter' suffered
	if( isset( $fudou_share_buttons ) ){
		remove_filter( 'user_contactmethods', array( $fudou_share_buttons, 'fudou_share_buttons_user_profile_fields' ) );
	}

* You can add a short code to Content
	[fudou_share_buttons_in]

* If you want to erase from Content
	if( isset( $fudou_share_buttons ) ){
		remove_filter( 'the_content', array( $fudou_share_buttons, 'fudou_share_the_content' ) );
	}

* If you want to change the minifty Files.
	add_filter( 'fudou_share_buttons_read_js_files', function(){ return 1; }  );

* If you want to change the transient time.
	add_filter( 'fudou_share_buttons_facebook_appid', function(){ return 'xxxxxxxx'; }  );	//Your Facebook APP ID

== Upgrade Notice ==
The required WordPress version has been changed and now requires WordPress 4.1.5 or higher  


== Changelog ==
= v0.5.3 =
** Fixed fudou_share_buttons();
** Fixed FaceBook Script.

= v0.5.2 =
** Fixed Twitter Script.
** Fixed FaceBook Script.
** Fixed Footer Script.
** Fixed do_action code.
** Added minifty Files.

= v0.4.0 =
** Beta version of the plugin.

