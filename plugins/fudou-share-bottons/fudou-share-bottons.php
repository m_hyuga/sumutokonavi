<?php
/*
Plugin Name: Fudousn Share Buttons
Plugin URI: http://nendeb.jp/
Description: Share Buttons for Wordpress
Version: 0.5.3
Author: nendeb
Author URI: http://nendeb.jp/
License: GPLv2
*/


// Define current version constant
define( 'FUDOU_SHARA_BUTTONS_VERSION', '0.5.3' );

/*  Copyright 2015 nendeb ( email : nendeb@gmail.com )

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/

if ( !class_exists( 'Fudou_Share_Buttons' ) ):
/**
 * Fudousn Share Buttons. 
 * 
 * @author nendeb
 * @copyright 2015
 * @version 0.5.3
 * @access public
 */
class Fudou_Share_Buttons {

	public function __construct() {

		add_filter( 'user_contactmethods',	array( $this, 'fudou_share_buttons_user_profile_fields' ) );	//For Via Twitter
		add_action( 'wp_enqueue_scripts',	array( $this, 'fudou_share_buttons_enqueue_scripts' ) );	//Read Evernote,Hatena scripts
		add_action( 'wp_enqueue_scripts',	array( $this, 'fudou_svgicons_css' ) );				//svgicons_css

		add_filter( 'the_content',		array( $this, 'fudou_share_the_content' ) );			//For Content
		add_action( 'fudou_share_buttons_do',	array( $this, 'fudou_share_buttons_do_action' ), 10, 2 );	//For Templates
		add_action( 'single-fudo6',		array( $this, 'fudou_share_buttons_do_action' ) );		//For Fudousan Plugin
		add_shortcode('fudou_share_buttons_in',	array( $this, 'fudou_share_buttons' ) );			//For Shortcode

		add_filter( 'script_loader_tag', function ( $tag, $handle ) {						//Add async tag. WordPress4.1+
			if ( 'hatena-script' !== $handle && 'evernote-script' !== $handle ) {
				return $tag;
			}
			return str_replace( ' src', ' async="async" src', $tag );
		}, 10, 2 );

		if ( function_exists( 'register_uninstall_hook') ){
			register_deactivation_hook( __FILE__, array( $this, 'fudou_share_bottons_deactivation' ) );
		}
	}


	/**
	 * Transient Time :hours
	 * 
	 * If you want to change the transient time.
	 * add_filter( 'fudou_share_buttons_transient_time', function(){ return 10; }  );	//10 hours
	 */
	public function fudou_share_buttons_transient_time() {
		return apply_filters( 'fudou_share_buttons_transient_time', 3 );
	}


	/**
	 * Facebook APP ID
	 * 
	 * If you want to change the transient time.
	 * add_filter( 'fudou_share_buttons_facebook_appid', function(){ return 'xxxxxxxx'; }  );	//Your Facebook APP ID
	 */
	public function fudou_share_buttons_facebook_appid() {
		return apply_filters( 'fudou_share_buttons_facebook_appid', '662393910437306' );
	}


	/**
	 * List of valid button.
	 *
	 * If you want to sort or change  the button items.
	 * add_filter( 'fudou_share_buttons_list', function(){ return array( 'twitter', 'facebook', 'googleplus', 'hatena', 'feedly', 'pocket', 'evernote', 'line', 'rss' ); }  );
	 */
	public function fudou_share_buttons_list() {
		return apply_filters( 'fudou_share_buttons_list', array( 'twitter', 'facebook', 'googleplus', 'hatena', 'feedly', 'pocket', 'evernote', 'line', 'rss' ) );
	}


	/**
	 * Twitter count
	 * 
	 */
	public function fudou_twitter_shares( $url='', $post_id='', $count=false ) {

		if( ! $url ) {
			return '';
		}

		if( $count !== false ){
			$count = get_option( '_transient_fudou_twitter_count' . $post_id );
		}else{

			$transient_time = $this->fudou_share_buttons_transient_time();
			if( !empty( $transient_time ) ) {
				$count = get_transient( 'fudou_twitter_count' . $post_id );
			}

			if( $count === false ) {

				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				if( !$user_agent ){
				    $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko';
				}
				$args = array(
				    'user-agent'  => $user_agent
				);

				$results = wp_remote_get( 'http://urls.api.twitter.com/1/urls/count.json?url=' . $url, $args );

				// wp_error
				if ( ! is_wp_error( $results ) && $results['response']['code'] === 200 ) {
					$parsed_results= isset( $results['body'] ) ? json_decode( $results['body'], true ) : '';
					$count = isset( $parsed_results['count'] ) ? $parsed_results['count'] : 0;
				} else {
					$count = '-';
				}

				if( !empty( $transient_time ) ) {
					set_transient( 'fudou_twitter_count' . $post_id, $count , 3600 * $transient_time );
				}
			}
		}
		return $count;
	}


	/**
	 * Facebook count // html
	 */
	public function fudou_facebook_shares( $url='', $post_id='', $count=false ) {

		if( ! $url ) {
			return '';
		}

		if( $count !== false ){
			$count = get_option( '_transient_fudou_facebook_count' . $post_id );
		}else{

			$appId = $this->fudou_share_buttons_facebook_appid();
			$transient_time = $this->fudou_share_buttons_transient_time();

			if( !empty( $transient_time ) ) {
				$count = get_transient( 'fudou_facebook_count' . $post_id );
			}
			if( $count === false ) {

				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				if( !$user_agent ){
				    $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko';
				}
				$args = array(
				    'user-agent'  => $user_agent
				);

				$results = wp_remote_get( 'http://www.facebook.com/plugins/like.php?href=' . $url . '&width&layout=button_count&action=like&show_faces=true&share=false&height=21&appId='. $appId, $args );

				// wp_error
				if ( ! is_wp_error( $results ) && $results['response']['code'] === 200 ) {
					$parsed_results = $results['body'];
					preg_match( '/<span class="pluginCountTextDisconnected">(\d+)<\/span>/i', $parsed_results, $match );
					$count = isset( $match[1] ) ? $match[1] : 0;
				} else {
					$count = '-';
				}

				if( !empty( $transient_time ) ) {
					set_transient( 'fudou_facebook_count' . $post_id, $count , 3600 * $transient_time );
				}
			}
		}
		return $count;
	}


	/**
	 * Google+1 count
	 */
	public function fudou_google_shares( $url='', $post_id='', $count=false ) {

		if( ! $url ) {
			return '';
		}

		if( $count !== false ){
			$count = get_option( '_transient_fudou_google_count' . $post_id );
		}else{

			$transient_time = $this->fudou_share_buttons_transient_time();
			if( !empty( $transient_time ) ) {
				$count = get_transient( 'fudou_google_count' . $post_id );
			}

			if( $count === false ) {
				$ch = curl_init();   
				curl_setopt( $ch, CURLOPT_URL, "https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ" ); 
				curl_setopt( $ch, CURLOPT_POST, 1);
				curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]' );
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
				curl_setopt ($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json') );
				$curl_results = curl_exec ( $ch );
				curl_close ($ch);

				$parsed_results = json_decode( $curl_results, true );
				$count = isset( $parsed_results[0]['result']['metadata']['globalCounts']['count'] ) ? $parsed_results[0]['result']['metadata']['globalCounts']['count']  : 0;

				if( !empty( $transient_time ) ) {
					set_transient( 'fudou_google_count' . $post_id, $count , 3600 * $transient_time );
				}
			}
		}
		return $count;
	}


	/**
	 * hatena count
	 */
	public function fudou_hatena_shares( $url='', $post_id='', $count=false ) {

		if( ! $url ) {
			return '';
		}

		if( $count !== false ){
			$count = get_option( '_transient_fudou_hatena_count' . $post_id );
		}else{

			$transient_time = $this->fudou_share_buttons_transient_time();
			if( !empty( $transient_time ) ) {
				$count = get_transient( 'fudou_hatena_count' . $post_id );
			}

			if( $count === false ) {

				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				if( !$user_agent ){
				    $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko';
				}
				$args = array(
				    'user-agent'  => $user_agent
				);

				$results = wp_remote_get( 'http://api.b.st-hatena.com/entry.count?url=' . $url, $args );

				// wp_error
				if ( ! is_wp_error( $results ) && $results['response']['code'] === 200 ) {
					$count = isset( $results['body'] ) ? $results['body'] : '';
					if( empty( $count ) ){
						$count = 0;
					}
				} else {
					$count = '-';
				}

				if( !empty( $transient_time ) ) {
					set_transient( 'fudou_hatena_count' . $post_id, $count , 3600 * $transient_time );
				}
			}
		}
		return $count;
	}


	/**
	 * Feedly count
	 */
	public function fudou_feedly_shares( $feed_url='', $post_id='', $count=false ) {

		if( ! $feed_url ) {
			return '';
		}

		if( $count !== false ){
			$count = get_option( '_transient_fudou_feedly_count' . $post_id );
		}else{

			$transient_time = $this->fudou_share_buttons_transient_time();
			if( !empty( $transient_time ) ) {
				$count = get_transient( 'fudou_feedly_count' );
			}

			if( $count === false ) {

				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				if( !$user_agent ){
				    $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko';
				}
				$args = array(
				    'user-agent'  => $user_agent
				);

			//	$feed_url = urlencode( get_bloginfo( 'rss2_url' ) );
				$results = wp_remote_get( 'http://cloud.feedly.com/v3/feeds/feed%2F' . $feed_url, $args );

				// wp_error
				if ( ! is_wp_error( $results ) && $results['response']['code'] === 200 ) {
					$parsed_results = json_decode( $results['body'], true );
					$count = isset( $parsed_results['subscribers'] ) ? $parsed_results['subscribers']: 0;
				} else {
					$count = '-';
				}

				if( !empty( $transient_time ) ) {
					set_transient( 'fudou_feedly_count', $count , 3600 * $transient_time );
				}
			}
		}
		return $count;
	}


	/**
	 * Pocket count
	 */
	public function fudou_pocket_shares( $url='', $post_id='', $count=false ) {

		if( ! $url ) {
			return '';
		}

		if( $count !== false ){
			$count = get_option( '_transient_fudou_pocket_count' . $post_id );
		}else{

			$transient_time = $this->fudou_share_buttons_transient_time();
			if( !empty( $transient_time ) ) {
				$count = get_transient( 'fudou_pocket_count' . $post_id );
			}

			if( $count === false ) {

				$user_agent = $_SERVER['HTTP_USER_AGENT'];
				if( !$user_agent ){
				    $user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko';
				}
				$args = array(
				    'user-agent'  => $user_agent
				);

				$results = wp_remote_get( 'http://widgets.getpocket.com/v1/button?v=1&count=horizontal&url=' . $url, $args );

				// wp_error
				if ( ! is_wp_error( $results ) && $results['response']['code'] === 200 ) {
					$parsed_results = $results['body'];
					preg_match( '/<em id="cnt">(\d+)<\/em>/i', $parsed_results, $match );
					$count = isset( $match[1] ) ? $match[1] : 0;
				} else {
					$count = '-';
				}

				if( !empty( $transient_time ) ) {
					set_transient( 'fudou_pocket_count' . $post_id, $count , 3600 * $transient_time );
				}
			}
		}
		return $count;
	}


	/**
	 * SNS Buttons
	 */
	public function fudou_share_buttons( $post_id = '', $count=false ) {

		$sns_buttons = '';

		$fudou_share_buttons_list = $this->fudou_share_buttons_list();

		if( $post_id == '0' ){
			//TopPage
			$url = esc_url( home_url() );
			$title = urlencode( esc_attr( get_bloginfo('name') ) );
			$post_id = '';
		}else{

			if( $post_id == '' ){
				global $wp_query;
				$object = $wp_query->get_queried_object();
				$post_id = isset( $object->ID ) ? $object->ID : '';
			}

			$url = '';
			$title = '';


			//タイトル・URL
			if( is_home() || is_front_page() ) {
				//TopPage
				$url = esc_url( home_url() );
				$title = urlencode( esc_attr( get_bloginfo('name') ) );

			} elseif ( is_category() ) {
				$cat = get_queried_object();
				$url = esc_url( get_category_link( $cat -> term_id ) );
				$title = urlencode( esc_attr(  $cat -> cat_name ) );

			} elseif ( is_tag() ){
				global $tag_id;
				$tag = get_tag( $tag_id );
				$url = esc_url( get_tag_link( $tag_id ) );
				$title = urlencode( esc_attr( $tag->name ) );

			}elseif( is_archive() ) {
				//WordPress4.1+ or Theme Support
				if ( function_exists( 'get_the_archive_title' ) ) {
					$url = esc_url( ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
					$title = urlencode( trim( get_the_archive_title() ) );	
				}else{
					$url = esc_url( home_url() );
					$title = urlencode( esc_attr( get_bloginfo('name') ) );
				}
			}else{
				if( $post_id ){
					$url = esc_url( get_permalink( $post_id ) );
					$title = urlencode( esc_attr( get_the_title( $post_id ) ) );
				}else{
					$url = esc_url( get_permalink() );
					$title = urlencode( esc_attr( get_the_title() ) );
				}
			}

			if( $post_id ){
				$url = esc_url( get_permalink( $post_id ) );
				$title = urlencode( esc_attr( get_the_title( $post_id ) ) );
			}

			//物件リスト
			global $wp_query;
			$cat = $wp_query->get_queried_object();
			$cat_name = isset( $cat->taxonomy ) ? $cat->taxonomy : '';
			if ( isset( $_GET['bukken'] ) || isset( $_GET['bukken_tag'] ) || $cat_name == 'bukken' || $cat_name =='bukken_tag' ) {
				global $org_title;
				$url = esc_url( ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
				$title = urlencode( esc_attr( $org_title ) );
			}
		}


		$feed_url = esc_url( get_bloginfo( 'rss2_url' ) );

		$via = get_the_author_meta( 'twitter' );
		/*
		 * Fix a Twitter User
		*/
		//if( !$via ) {
		//	$via = get_user_meta( 1, 'twitter', true );
		//}
		if( $via ) {
			$via = '&via='. str_replace( "@" ,"" , trim( $via ) );
		}

		//Fix & -> &#038;
		$url =  str_replace('&#038;','&', $url );


		if( $title && $url ) {

			$fudou_share_buttons_list = $this->fudou_share_buttons_list();

			$sns_buttons .= "\n<!-- fudou_share_buttons " . FUDOU_SHARA_BUTTONS_VERSION . " -->\n";
			$sns_buttons .= '<div class="sharedaddy">';
			$sns_buttons .= '<ul id="snscomm">';


			foreach( $fudou_share_buttons_list as $buttons_list ){	//Selection and Sort

				//Twitter
				if( $buttons_list == 'twitter' ) {

					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-twitter-alt" rel="nofollow" href="http://twitter.com/share?url=' . urlencode( $url ) . '&text=' . $title . $via . '&related=&hashtags=" title="ツィートする" onclick="window.open(this.href, \'window_twitter\', \'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes\'); return false;">';
					$sns_buttons .= '<span class="share-count">' . $this->fudou_twitter_shares( urlencode( $url ) , $post_id, $count ) . '</span>';
					$sns_buttons .= '<span class="screen-reader-text">ツィートする</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//Facebook &amp;
				if( $buttons_list == 'facebook' ) {
					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-facebook" rel="nofollow" href="http://www.facebook.com/share.php?u=' . urlencode( $url ) . '" title="Facebookでシェアする" onclick="window.open(this.href, \'window_fb\', \'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes\'); return false;">';
					//Like
					//$sns_buttons .= '<a class="icomoon icon-facebook" rel="nofollow" href="http://www.facebook.com/plugins/like.php?href=' . urlencode( $url ) . '&action=like" title="Facebookで「いいね」する" onclick="window.open(this.href, \'window_fb\', \'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes\'); return false;">';
					$sns_buttons .= '<span class="share-count">' . $this->fudou_facebook_shares( urlencode( $url ), $post_id, $count ) . '</span>';
					$sns_buttons .= '<span class="screen-reader-text">Facebook</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//Google+
				if( $buttons_list == 'googleplus' ) {
					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-google" rel="nofollow" href="https://plus.google.com/share?url=' . $url . '" title="Google+ でシェアする" onclick="window.open(this.href, \'window_google\', \'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes\'); return false;">';
					$sns_buttons .= '<span class="share-count">' . $this->fudou_google_shares( $url, $post_id, $count ) . '</span>';
					$sns_buttons .= '<span class="screen-reader-text">Google Plus</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//hatena
				if( $buttons_list == 'hatena' ) {
					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-hatebu hatena-bookmark-button" rel="nofollow" href="http://b.hatena.ne.jp/entry/' . $url . '" data-hatena-bookmark-layout="simple" title="はてなブックマークに追加">';
					$sns_buttons .= '<span class="share-count">' . $this->fudou_hatena_shares( $url , $post_id, $count ) . '</span>';
					$sns_buttons .= '<span class="screen-reader-text">はてなブックマークに追加</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//Feedly
				if( $buttons_list == 'feedly' ) {
					$sns_buttons .= '<li>';
				//	$sns_buttons .= '<a class="icomoon icon-feedly" rel="nofollow" href="http://cloud.feedly.com/#subscription%2Ffeed%2F'. urlencode( $feed_url ) . '" target="_blank" title="Feedlyで購読する">';
					$sns_buttons .= '<a class="icomoon icon-feedly" rel="nofollow" href="http://feedly.com/i/subscription/feed/'. urlencode( $feed_url ) . '" target="_blank" title="Feedlyで購読する">';
					$sns_buttons .= '<span class="share-count">' . $this->fudou_feedly_shares( urlencode( $feed_url ), $post_id, $count ) . '</span>';
					$sns_buttons .= '<span class="screen-reader-text">Feedly</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//Pocket
				if( $buttons_list == 'pocket' ) {
					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-pocket" rel="nofollow" href="http://getpocket.com/edit?url=' . urlencode( $url ) . '&title=' . $title . '" title="Pocketに保存"  onclick="window.open(this.href, \'window_pocket\', \'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes\'); return false;">';
					$sns_buttons .= '<span class="share-count">' . $this->fudou_pocket_shares( $url, $post_id, $count ) . '</span>';
					$sns_buttons .= '<span class="screen-reader-text">Pocketに保存</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//Evernote
				if( $buttons_list == 'evernote' ) {
					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-Evernote_logo" rel="nofollow" href="#" title="Evernote" onclick="Evernote.doClip({}); return false;">';
					$sns_buttons .= '<span class="screen-reader-text">Evernote</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//Line
				if( $buttons_list == 'line' ) {
					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-line" rel="nofollow" href="http://line.me/R/msg/text/?' . $title . '%0D%0A' . urlencode( $url ) . '" title="LINEで送る" onclick="window.open(this.href, \'window_line\', \'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes\'); return false;">';
					$sns_buttons .= '<span class="screen-reader-text">LINEで送る</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '</li>';
				}

				//rss
				if( $buttons_list == 'rss' ) {
					$sns_buttons .= '<li>';
					$sns_buttons .= '<a class="icomoon icon-feed2" href="' . $feed_url . '" title="RSS" target="_blank">';
					$sns_buttons .= '<span class="screen-reader-text">rss</span>';
					$sns_buttons .= '</a>';
					$sns_buttons .= '<li>';
				}
			}

			$sns_buttons .= '</ul>';
			$sns_buttons .= '</div>';
			$sns_buttons .= "\n<!-- .fudou_share_buttons -->\n";

		}
		return $sns_buttons;
	}


	/**
	 * Add Twitter in User Profile Fields.
	 *
	 * @param array $contactmethods
	 * @return array
	 */
	public function fudou_share_buttons_user_profile_fields( $contactmethods ) {
		$contactmethods["twitter"] = "Twitter";
		return $contactmethods;
	}


	/**
	 *
	 * Footer Script
	 *
	 */
	public function fudou_share_buttons_enqueue_scripts() {

		/**
		 * Read js files
		 * 
		 * If you want to change the minifty Files.
		 * add_filter( 'fudou_share_buttons_read_js_files', function(){ return 1; }  );	//Read minifty js Files
		 */
		if( apply_filters( 'fudou_share_buttons_read_js_files', 0 ) ){
			//evernote
			wp_enqueue_script( 'evernote-script', plugins_url( 'js/noteit.min.js', __FILE__ ), array(), '', true );
			//hatena
			wp_enqueue_script( 'hatena-script', plugins_url( 'js/bookmark_button.min.js', __FILE__ ), array(), '', true );
		}else{
			//evernote
			wp_enqueue_script( 'evernote-script', 'http://static.evernote.com/noteit.js', array(), '', true );
			//hatena
			wp_enqueue_script( 'hatena-script', 'http://b.st-hatena.com/js/bookmark_button.js', array(), '', true );
		}
	}


	/**
	 * Icon Styles.
	 */
	public function fudou_svgicons_css() {
		// Add svgicons, used in the stylesheet.
		wp_enqueue_style( 'svg-icons', plugins_url( 'css/svgicons.css', __FILE__ ), array(), '' );
	}


	/**
	 * the_content
	 */
	public function fudou_share_the_content( $contents ) {
		if ( ! is_singular() ) {
			return $contents;
		}
		global $wp_query;
		$object = $wp_query->get_queried_object();
		if( isset( $object->post_type ) && $object->post_type == 'fudo' ){
			return $contents;
		}else{
			$sns_buttons = $this->fudou_share_buttons();
			return apply_filters( 'fudou_share_the_content', $contents . $sns_buttons, $contents, $sns_buttons );
		}
	}


	/**
	 * do_action
	 *
	 * If you would set with other place you can set do_action code .
	 * <?php do_action( 'fudou_share_buttons_do', $post_id, $count ); ?>
	 */
	public function fudou_share_buttons_do_action( $post_id = '', $count=false )  {
		echo $this->fudou_share_buttons( $post_id, $count );
	}


	/**
	 * deactivation
	 *
	 * Delete transient data .
	 */
	public function fudou_share_bottons_deactivation(){

		if ( ! current_user_can( 'activate_plugins' ) ){
			return;
		}
		$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
		if( $plugin ){
			check_admin_referer( "deactivate-plugin_{$plugin}" );
		}

		global $wpdb;
		$sql = "SELECT DISTINCT option_name FROM $wpdb->options WHERE option_name LIKE '_transient_fudou_%' ";
		$metas = $wpdb->get_results( $sql, ARRAY_A );
		if( ! empty( $metas ) ) {
			foreach ( $metas as $meta ) {
				$transient = str_replace( '_transient_', '', $meta['option_name'] );
				delete_transient( $transient );
			}
		}
	}
}
global $fudou_share_buttons;
$fudou_share_buttons = new Fudou_Share_Buttons();
endif;
