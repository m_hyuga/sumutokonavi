<?php

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) )
	exit();

function fudou_share_bottons_delete_plugin() {

	global $wpdb;

	$sql = "SELECT DISTINCT option_name FROM $wpdb->options WHERE option_name LIKE '_transient_fudou_%' ";
	$metas = $wpdb->get_results( $sql, ARRAY_A );

	if( ! empty( $metas ) ) {
		foreach ( $metas as $meta ) {
			$transient = str_replace( '_transient_', '', $meta['option_name'] );
			delete_transient( $transient );
		}
	}
}

fudou_share_bottons_delete_plugin();

?>