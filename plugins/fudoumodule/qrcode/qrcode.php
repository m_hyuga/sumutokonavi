<?php
/*QRコード関連モジュール*/



//投稿編集画面にQRコードを追加
add_action( 'admin_init', 'qr_admin_init' );

function qr_admin_init() {
	add_meta_box( 'qr_meta_box_fudo', 'QRコード', 'qr_metabox', 'fudo' ,'side');
}

function qr_metabox( $param ) {
	$post_id = $param->ID;
	// $paramは投稿情報
	echo '<img src="http://chart.apis.google.com/chart?chs=250x250&amp;cht=qr&amp;chl=' .urlencode( get_permalink($post_id)) . '">';
}

//QRコードを管理画面に表示
add_filter("fudomodulecolumns",function($text,$post_id){return $text .  '<a href="http://chart.apis.google.com/chart?chs=500x500&amp;cht=qr&amp;chl=' . urlencode( get_permalink($post_id)) . '&amp;TB_iframe=true&height=520&width=500" class="thickbox" title="' . get_the_title($post_id). '">QRコードを表示</a><br/>';},10,2);















?>