<?php
//CSVアップロード

//add_management_page( '町名CSVアップロード', '町名CSVアップロード', 'level_10', 'csv_upload', 'csv_upload_func' ); 
function mt_add_pages() {
add_submenu_page( 'tools.php', '町名CSVアップロード', '町名CSVアップロード', 'level_7', 'csv_upload', 'csv_upload' );
}
add_action('admin_menu', 'mt_add_pages');
function csv_upload(){
global $wpdb;
$table_name = $wpdb->prefix . 'area_town_area';
//DBのバージョン
$area_town_area_db_version = '1.001';
//現在のDBバージョン取得
$installed_ver = get_option( 'area_town_area_version' );
$info="";
$table_name = $wpdb->prefix . 'area_town_area';
$errors = new WP_Error();
$update = empty($_POST["update"])?false:$_POST["update"];
$action = empty($_POST["action"])?false:$_POST["action"];
	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
		$info .= $table_name . "は存在しません。新しく作成します。<br/>";
  		// DBバージョンが違ったら作成
  		if( $area_town_area_db_version != $installed_ver ) {
  			$sql = "CREATE TABLE " . $table_name . " (
			  town_id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
			  town_name text,
			  narrow_area_id bigint(20) UNSIGNED DEFAULT '0' NOT NULL,
			  middle_area_id bigint(20) UNSIGNED DEFAULT '0' NOT NULL,
			  UNIQUE KEY town_id (town_id)
			)
			CHARACTER SET 'utf8';";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		//オプションにDBバージョン保存
		//update_option('area_town_area_version', $area_town_area_db_version);
  		}		
	}else{
		$info .= $table_name . "は存在します。";
	}
if($action == "delete"){delete_csv($action);}
$count = $wpdb->get_var("SELECT COUNT(*) FROM $table_name");
if(empty($_FILES["upcsv"]["name"]) && $update){$errors->add( 'no_files','アップロードファイルが指定されていません');}
if(!empty($_FILES["upcsv"]["name"]) && $update && substr($_FILES["upcsv"]["name"],-3) != "csv"){$errors->add( 'no_csv','CSVファイルではありません。');}

?>
  <div class="wrap">
        <h2>町名CSVのアップロード</h2>
		<?php err_check_msg($errors);?>
		<div id="message" class="updated">
			<?php if($count):echo $count;?>件の町名が登録されています<?php endif;?>
			CSVファイルは市区コード（五桁）、名前の順番のCSVファイルをご用意ください。
			<?php echo $info;?>
		</div>
        <form id="createuser" method="post" enctype="multipart/form-data">
            <?php wp_nonce_field('update-options'); ?>			
			<?php if($update && !$errors->get_error_codes()):?>
				<?php echo $_FILES["upcsv"]["name"];?>をアップロードします。<br/>
				<?php
				$upcnt=0;
				$fp = fopen($_FILES[ 'upcsv' ][ 'tmp_name' ],"r");
				while($data = fgetcsv($fp)){
					if(!empty($data[0])&&!empty($data[1])){
						$town = mb_convert_encoding($data[1], "UTF8", "auto");
						$ken = floor($data[0]/1000);
						$sik = substr($data[0],-3);
						if(!empty($town) && is_numeric($ken) && is_numeric($sik)){
							//echo $ken . ":" . $sik;
							$tck = $wpdb->get_var("SELECT COUNT(town_name) FROM $table_name WHERE town_name = '$town' and middle_area_id = $ken and narrow_area_id = $sik");
							//echo "cnt" . $tck;
							if(!$tck){
									echo "町名:" . $town .  "を登録します<br/>";
									$set_arr = array(
										'town_name'=> $town,
										'narrow_area_id' => $sik,
										'middle_area_id' => $ken
									);
									$wpdb->insert( $table_name, $set_arr);
									$upcnt++;
							}
						}
					}
				}
				echo $upcnt . "件アップロードしました";
				?>
			<?php else:?>
				<input type="file" name="upcsv" size="30" accept="text/comma-separated-values" />
				<input type="hidden" name="update" value="1" />

	            <p class="submit">
	                <input type="submit" class="button-primary" value="ファイルのアップロード" />
	            </p>
			<?php endif;?>
		</form>
		<form id="seachform" method="get">
			<?php
			
			$town_list_table = new TOWN_List_Table();
			$town_list_table->prepare_items();
			 //Table of elements
		
			$town_list_table->search_box("検索する","serach_town");
	?>
		<input type="hidden" name="page" value="csv_upload" />
		<input type="hidden" name="action" value="search" />
		</form>
		<form id="town_table" method="post">
		<?php
			$town_list_table->display();
			?>
		</form>
        <script>
			jQuery("#cb-select-all-1").click(function(){
	            //attr()ではなくprop()を使う
	            jQuery('tbody .cb input').prop('checked', jQuery(this).prop('checked'));
	        });
			jQuery("#cb-select-all-2").click(function(){
	            //attr()ではなくprop()を使う
	            jQuery('tbody .cb input').prop('checked', jQuery(this).prop('checked'));
	        });
		</script>

    </div>
<?php
}

function err_check_msg($errors){
	if ( $errors->get_error_codes() ){
		$er="";
		foreach($errors->get_error_messages() as $errmsg){
			$er .= $errmsg . "<br />";
		}
		echo '<div id="message" class="error"><p>' . $er . '</p></div>';
	}
}


//CSVデータテーブルカラム
if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
class TOWN_List_Table extends WP_List_Table {
	//初期
	 function __construct() {
		 parent::__construct( array(
		'singular'=> 'wp_list_text_town', //Singular label
		'plural' => 'wp_list_test_towns', //plural label, also this well be one of the table css class
		'ajax'	=> false //We won't support Ajax for this table
		) );
	 }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item->town_id                //The value of the checkbox should be the record's id
        );
    }
	
	//カラムデータ
	function get_columns() {
		return $columns= array(
			'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
			'town_id'=>'town_id',
			'town_name'=>'町名',
			'middle_area_id'=>'県名',
			'narrow_area_id'=>'市区名',
		);
	}
	
	//ソートカラム
	public function get_sortable_columns() {
		return $sortable = array(
			'town_id'=>array('town_id',false),
			'town_name'=>array('town_name',false),
			'middle_area_id'=>array('middle_area_id',false),
			'narrow_area_id'=>array('narrow_area_id',false)			
		);
	}
	
	//アイテムデータ
	function prepare_items() {
	global $wpdb, $_wp_column_headers;
	$screen = get_current_screen();
	$table_name = $wpdb->prefix . 'area_town_area';
	
	$mtb = $wpdb->prefix . "area_middle_area";
	$ntb = $wpdb->prefix . "area_narrow_area";
	$word = !empty($_REQUEST["s"])?$_REQUEST["s"]:"";
	/* -- Preparing your query -- */
		if($_REQUEST["action"]="search" && $wpdb->get_var("SHOW TABLES LIKE '$mtb'") == $mtb && $wpdb->get_var("SHOW TABLES LIKE '$ntb'") == $ntb && $word){
			$query  = "SELECT t.town_id,t.town_name,t.narrow_area_id,t.middle_area_id FROM $table_name as t,$mtb as m,$ntb as n ";
			$query .= "WHERE t.middle_area_id=m.middle_area_id AND t.middle_area_id=n.middle_area_id AND  t.narrow_area_id = n.narrow_area_id AND ";
			$query .= "(t.town_name LIKE '%$word%' OR m.middle_area_name LIKE '%$word%' OR n.narrow_area_name LIKE '%$word%')";
		}else{

			 $query = "SELECT * FROM $table_name";
		}
		


	/* -- Ordering parameters -- */
	    //Parameters that are going to be used to order the result
	    $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'ASC';
	    $order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : '';
	    if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }

	/* -- Pagination parameters -- */
        //Number of elements in your table?
        $totalitems = $wpdb->query($query); //return the total number of affected rows
        //How many to display per page?
        $perpage = 20;
		$paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
		if($totalitems < $perpage*$paged){$paged=1;}
        //Page Number
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
        //How many pages do we have in total?
        $totalpages = ceil($totalitems/$perpage);
        //adjust the query to take pagination into account
	    if(!empty($paged) && !empty($perpage)){
		    $offset=($paged-1)*$perpage;
    		$query.=' LIMIT '.(int)$offset.','.(int)$perpage;
	    }

	/* -- Register the pagination -- */
		$this->set_pagination_args( array(
			"total_items" => $totalitems,
			"total_pages" => $totalpages,
			"per_page" => $perpage,
		) );
		//The pagination links are automatically built according to those parameters

	/* -- Register the Columns -- */
	//	$columns = $this->get_columns();
	//	$_wp_column_headers[$screen->id]=$columns;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);



	/* -- Fetch the items -- */
		$this->items = $wpdb->get_results($query);
	}
	
	function get_bulk_actions() {
        $actions = array(
            'delete'    => '削除'
        );
        return $actions;
    }
	
	//描画
	function display_rows() {
	global $wpdb;
	//Get the records registered in the prepare_items method
	$records = $this->items;

	//Get the columns registered in the get_columns and get_sortable_columns methods
	list( $columns, $hidden ) = $this->get_column_info();
	//Loop for each record
	if(!empty($records)){foreach($records as $rec){

		//Open the line
        echo '<tr id="record_'.$rec->town_id.'">';
		foreach ( $columns as $column_name => $column_display_name ) {

			//Style attributes for each col
			$class = "class='$column_name column-$column_name'";
			$style = "";
			if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
			$attributes = $class . $style;

			//edit link
			$editlink  = '/wp-admin/link.php?action=edit&link_id='.(int)$rec->town_id;

$mtb = $wpdb->prefix . "area_middle_area";
$ntb = $wpdb->prefix . "area_narrow_area";
if($wpdb->get_var("SHOW TABLES LIKE '$mtb'") == $mtb){
	$middle = $wpdb->get_var("SELECT middle_area_name FROM $mtb WHERE middle_area_id = $rec->middle_area_id");	
}
if($wpdb->get_var("SHOW TABLES LIKE '$ntb'") == $ntb){
	$narrow = $wpdb->get_var("SELECT narrow_area_name FROM $ntb WHERE middle_area_id = $rec->middle_area_id and narrow_area_id = $rec->narrow_area_id");
}

			//Display the cell
			switch ( $column_name ) {
				case "cb":	echo '<td '.$attributes.'>'. $this->column_cb($rec).'</td>';	break;
				case "town_id":	echo '<td '.$attributes.'>'.stripslashes($rec->town_id).'</td>';	break;
				case "town_name": echo '<td '.$attributes.'>'.stripslashes($rec->town_name).'</td>'; break;
				case "middle_area_id": echo '<td '.$attributes.'>'.$middle . "(" . stripslashes($rec->middle_area_id).')</td>'; break;
				case "narrow_area_id": echo '<td '.$attributes.'>'.$narrow . "(" .  $rec->narrow_area_id.')</td>'; break;
			}
		}

		//Close the line
		echo'</tr>';
	}}
}
	
	public function column_default($item, $column_name){
		return $item -> $column_name;
	}	
	
}



function delete_csv($csv){
	global $wpdb;
	if(!empty($_REQUEST["wp_list_text_town"])){
		$table_name = $wpdb->prefix . 'area_town_area';
		$sql  = "DELETE FROM $table_name WHERE town_id IN (0,";
		$sql .= join(",",$_REQUEST["wp_list_text_town"]);
		$sql .= ")";
		$wpdb->query($sql);
	}

}