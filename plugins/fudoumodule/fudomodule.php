<?php
/*
Plugin Name: 不動産プラグイン追加モジュール
Plugin URI: 55w.jp
Description: 不動産プラグインに追加機能を追加する。
Version: 2.0
Author: gogoweb
Author URI: http://techblog.55w.jp/

2015/05/25 更新 PDFを出力させる
2014/02/19 更新 外部に共通ディレクトリcommonがある場合読み込むように変更
2014/02/18 更新 県と市からのAJAX検索を該当する町名だけに変更
2014/01/23 更新 ダッシュボードに町名リストを追加
2014/01/23 更新 町名CSVアップロードを追加
*/
//define("FUDOMODULEPATH",dirname(__FILE__));
//define("MODULETEMPLATE",dirname(__FILE__) . "/template");
define("FUDOMODULEPATH",ABSPATH . "../fudoumodule");
define("MODULETEMPLATE",ABSPATH . "../fudoumodule/template");

require(FUDOMODULEPATH ."/fudoconfig.php");

//ダッシュボードに町名メタボックスを追加
require_once(FUDOMODULEPATH ."/dashboard/fudo_dashboard.php");

//町名をCSVでアップロード
require_once(FUDOMODULEPATH."/towncsv/fudo_csv.php");

//元付を表示
require_once(FUDOMODULEPATH ."/motoduke/fudomotoduke.php");

//QRコードを管理画面に追加
require_once(FUDOMODULEPATH."/qrcode/qrcode.php");

//物件詳細のPDFを表示
require_once(FUDOMODULEPATH ."/pdf/pdf_function.php");


//会員プラグイン埋め込み
require_once(FUDOMODULEPATH ."/kaiin/fudoukaiin.php");

//メール配信プラグイン埋め込み
require_once(FUDOMODULEPATH ."/mail/fudoumail.php");

/*不動産プラグイン関連関数*/

//管理画面に追加カラム
function add_fudomodule_columns($columns) {		/* ←関数・変数は自由に */
	$columns['module'] = '追加';		/* ←アイキャッチ */
	return $columns;
}
function add_posts_columns_list($column_name, $post_id) {		/* ←関数・変数は自由に */

	if ( 'module' == $column_name ) {
			echo apply_filters("fudomodulecolumns","",$post_id);
	}

}
add_filter( 'manage_edit-fudo_columns', 'add_fudomodule_columns' );		/* ←◯◯◯はカスタム投稿のスラッグ */
add_action( 'manage_fudo_posts_custom_column', 'add_posts_columns_list', 10, 2 );

//設定画面
function fudo_module_add_pages() {
add_submenu_page( 'tools.php', '不動産モジュール設定', '不動産モジュール設定', 'level_10', 'fudo_module_setthing_page', 'fudo_module_setthing_page' );
}
add_action('admin_menu', 'fudo_module_add_pages');


function fudo_module_setthing_page(){
$update = empty($_POST["update"])?false:$_POST["update"];
if($update){
	$modules = empty($_POST["module"])?array():$_POST["module"];
	foreach($modules as $key => $val){
		update_option($key,$val);
	}
}

$fudo_active_plugins = get_option('active_plugins');
fudou_login_page();
?>
  <div class="wrap">
        <h2>不動産モジュール設定</h2>
        <form id="module_setthing" method="post">
			<input type="hidden" name="update" value="1">
            <?php wp_nonce_field('update-options'); ?>	
			<table>
				<tbody>
					<tr>
						<th>元付発信元</th>
						<td><input type="text" value="<?php echo get_option("fudomodule_source");?>" name="module[fudomodule_source]"></td>
					</tr>
					<tr>
						<th>元付担当者</th>
						<td><input type="text" value="<?php echo get_option("fudomodule_responsible");?>" name="module[fudomodule_responsible]"></td>
					</tr>					
					<tr>
						<th>元付郵便番号</th>
						<td><input type="text" value="<?php echo get_option("fudomodule_postcode");?>" name="module[fudomodule_postcode]"></td>
					</tr>
					<tr>
						<th>元付住所</th>
						<td><input type="text" value="<?php echo get_option("fudomodule_address");?>" name="module[fudomodule_address]"></td>
					</tr>
					<tr>
						<th>元付TEL</th>
						<td><input type="text" value="<?php echo get_option("fudomodule_tel");?>" name="module[fudomodule_tel]"></td>
					</tr>
					<tr>
						<th>元付FAX</th>
						<td><input type="text" value="<?php echo get_option("fudomodule_fax");?>" name="module[fudomodule_fax]"></td>
					</tr>
					<tr>
						<th>元付Email</th>
						<td><input type="text" value="<?php echo get_option("fudomodule_email");?>" name="module[fudomodule_email]"></td>
					</tr>
					<tr>
						<th>テンプレートA説明</th>
						<td><textarea name="module[fudomodule_template_a_info]"><?php echo get_option("fudomodule_template_a_info");?></textarea></td>
					</tr>
					<tr>
						<th>テンプレートB説明</th>
						<td><textarea name="module[fudomodule_template_b_info]"><?php echo get_option("fudomodule_template_b_info");?></textarea></td>
					</tr>
				</tbody>
			</table>		
            <p class="submit">
                <input type="submit" class="button-primary" value="更新" />
            </p>
		</form>
  </div>
<?php
}



//ログ
function fudomodule_log($text,$del=0){
	$log = get_option("fudomodule_log");
	if($del){$log="";}
	$log .= date("[Y.m.d H:i:s]:") . $text . "<br/>\n";
	update_option("fudomodule_log",$log); 
}




























?>