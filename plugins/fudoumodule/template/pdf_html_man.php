<?php
/*マンションPDF出力用テンプレート*/
?>
<!-- container  -->
<div id="container">
<!-- upper  -->
<div id="upper">

<!-- left-Column  -->
<div id="left-Column">


<!-- header  -->
<div id="header">
<h1 class="header_title"><?php echo $title;?></h1>
<h2 class="header_access"><?php my_custom_koutsu1_print($post_id); ?>
     <?php my_custom_koutsu2_print($post_id); ?></h2>
</div><!-- end of header  -->
    
    
    
    
    
<!-- content  -->
<div id="content">


<table border="0" class="special">
<tr>
<td><div class="special01">間取り<span  class="specialspan">&nbsp;<?php if( get_post_meta($post_id, 'madorisu', true) !=""){ ;?><?php my_custom_madorisu_print($post_id); ?><?php } ?> <?php echo get_post_meta($post_id, 'madoribiko', true);?></span></div></td>
<td><div class="special02">専有面積<span  class="specialspan">&nbsp;<?php echo get_post_meta($post_id, 'tatemonomenseki', true);?>m&sup2;</span></div></td>
<td><div class="special03">バルコニー面積<span  class="specialspan">&nbsp;<?php echo get_post_meta($post_id, 'heyabarukoni', true);?>m&sup2;</span></div></td>
</tr>
</table>



<div id="main-img">
<div class="img-left">
<p class="mb5"><?php if(!empty($img_out[2])){echo $img_out[2];}?></p>
<p class="bd"><img src="<?php echo $mapsrc;?>" alt="*"></p>
</div>
<div class="img-right"><p><?php if(!empty($img_out[1])){echo $img_out[1];}?></p></div>
</div>


    
<div class="comment_bd">
<table id="comment">
<tr>
<th rowspan="2" scope="row" class="kaeru"><img src="<?php echo IMGPATH;?>/img/commentlogo.png" alt="コメントロゴ"></th>
<td class="sales_comment">セールスコメント</td>
</tr>
<tr>
<td>
<p class="comment_space">
<?php echo comment_line($pdf->post_content);?>
</p>
</td>
</tr>
</table>
</div>
 
</div><!-- end of content  -->

</div><!-- end of left-Column  -->
    
    
    
    
<!-- right-Column  -->
<div id="right-Column">

<table id="right_table">
<tr>
<th scope="row">物件種別</th>
<td colspan="3" class="font1"><?php my_custom_bukkenshubetsu_print($post_id); ?></td>
</tr>
<tr>
<th scope="row">価格</th>
<td colspan="3" class="font1"><?php my_custom_kakaku_print($post_id);?></td>
</tr>
<tr>
<th scope="row">管理費</th>
<td><?php echo get_post_meta($post_id,"kakakukyouekihi",true);?>円</td>
<th scope="row">修繕積立金</th>
<td><?php echo get_post_meta($post_id,"kakakutsumitate",true);?>円</td>
</tr>
<tr>
<th scope="row">所在地</th>
<td colspan="3"><?php my_custom_shozaichi_print($post_id); ?><?php echo get_post_meta($post_id, 'shozaichimeisho', true); ?><?php echo get_post_meta($post_id, 'shozaichimeisho2', true); ?></td>
</tr>
<tr>
<th scope="row">専有面積</th>
<td><?php echo get_post_meta($post_id, 'tatemonomenseki', true);?> m&sup2;</td>
<th scope="row">管理形態</th>
<td class="right-Column-w02"><?php my_custom_kanrikeitai_print($post_id);?></td>
</tr>
<tr>
<th scope="row">建物構造</th>
<td><?php my_custom_tatemonokozo_print($post_id);?></td>
<th scope="row">所在階／階数</th>
<td><?php echo get_post_meta($post_id,"heyakaisu",true);?>階／<?php echo get_post_meta($post_id,"tatemonokaisu1",true);?>階建</td>
</tr>
<tr>
<th scope="row"><p>築年月</p></th>
<td><?php tikunen($post_id);?></td>
<th scope="row">総戸数</th>
<td><?php echo get_post_meta($post_id,"bukkensoukosu",true);?>戸</td>
</tr>
<tr>
<th scope="row">用途地域</th>
<td><?php my_custom_tochiyouto_print($post_id);?></td>
<th scope="row">管理人</th>
<td><?php my_custom_kanrininn_print($post_id);?></td>
</tr>
<tr>
<th scope="row">引渡し</th>
<td><?php my_custom_nyukyosyun_print($post_id);?></td>
<th scope="row">取引様態</th>
<td><?php my_custom_torihikitaiyo_print($post_id);?></td>
</tr>
<tr>
<th scope="row">駐車場</th>
<td><?php my_custom_chushajo_print($post_id);?></td>
<th scope="row">部屋建物向き</th>
<td><?php my_custom_heyamuki_print($post_id);?></td>
</tr>
<tr>
  <th scope="row">間取り内容</th>
  <td colspan="3"><?php my_custom_madorinaiyo_print($post_id);?></td>
</tr>
<tr>
<th scope="row">小学校</th>
<td><?php echo get_post_meta($post_id,"shuuhenshougaku",true);?></td>
<th scope="row">中学校</th>
<td><?php echo get_post_meta($post_id,"shuuhenchuugaku",true);?></td>
</tr>
<tr class="setubi">
<th scope="row">設備・備考</th>
<td colspan="3"><?php my_custom_setsubi_print($post_id);?>
</tr>
</table>



</div><!-- end of right-Column  -->
</div><!-- end of upper  -->

<?php pdf_footer();?>

</div><!-- end of container  -->