<body style="margin:0;padding:0;">
<div class="base" style="max-width:640px;min-width:300px;margin:0 auto;line-height:1.6;color:#111;background:#fff;font-size:12px;-webkit-text-size-adjust:none;">
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="mso"><tr><td>

<table cellpadding="0" cellspacing="0" border="0"><tr><td><a href="http://seibunet.jp/?p=matchingmail"><img src="http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/common/f_logo.png" alt="HOME'S" width="180" border="0" style="vertical-align:bottom"></a></td><td><img src="http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/mail/logo_service.png" alt="新着お知らせメール" width="171" height="20" border="0" style="vertical-align:bottom"></td></tr>
<tr>
<td colspan="2">
<p style="color:#666666;line-height:1.6em;">株式会社セイブのマッチングメールをご利用いただき、誠にありがとうございます。
<br /><br />
ご登録頂いております条件より該当する物件が見つかりましたので、メールにて配信させていただきます。
詳細な情報のご希望がございましたら、ホームページ又は物件番号を記載の上メールにてご返信 又はお電話でお問い合わせ下さい。
尚「会員」表示されている物件はホームページ閲覧にはログインが必要です。
</p>
</td>
</tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0" style="margin-top:-1em;border-bottom:4px solid #1467d0;"><tr><td align="right" style="padding:0 5px 5px 0;line-height:1;"><?php echo $date;?></td></tr></table>

<!-- 提案物件用ヘッダ -->

<!-- /提案物件用ヘッダ -->

<!-- 提案物件 -->
<div class="na">

</div>
<!-- / 提案物件 -->

<!-- 新着物件用ヘッダ -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="pd">
<tr><td class="btn" style="padding-top:15px;padding-bottom:15px;">
<table cellpadding="0" cellspacing="0" border="0" align="left">
<tr><td style="font-size:14px;">
<span style="font-size:14px;font-weight:bold;">本日の新着：<span style="color:#ed6103;font-size:20px;line-height:1;"><?php echo $count;?></span>件</span>ありました
</td></tr></table>
<table width="329" cellpadding="0" cellspacing="0" border="0" align="right">
<tr><td background="http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/mail/button.png" style="text-align:center;background-repeat:no-repeat;">
<a href="<?php echo $joken_url;?>" style="display:block;width:100%;height:32px;overflow:hidden;">
<span style="padding-left:12px;line-height:32px;font-weight:bold;">1週間以内に登録された希望条件の物件をすべて見る</span>
</a></td></tr></table>
</td></tr></table>
<!-- /新着物件用ヘッダ -->

<!-- 新着物件 -->
<div class="na">
<?php
				$i = 1;
				foreach ( $metas as $meta ):
					$post_id =  $meta['ID'];
					$post_title =  $meta['post_title'];
					$post_excerpt =  $meta['post_excerpt'];
					if($meta["post_status"]=="private"){
						$post_url =  home_url() . "/?page_id=" . $contact_url;
						$post_url .= "&subject=" . $post_title;
					}else{
						$post_url =  get_permalink($post_id);
						$post_url .=  "&ui=". $user_mail_ID;
					}
					
					$post_shubetu = mail_custom_bukkenshubetsu_print($post_id);
					$post_kakaku  = mail_custom_kakaku_print($post_id,false);
					$post_koutsu  = mail_custom_koutsu1_print($post_id,false) . " " . mail_custom_koutsu2_print($post_id,false) . " " . get_post_meta($post_id, 'koutsusonota', true);
					$post_add     = mail_custom_shozaichi_print($post_id,false) . get_post_meta($post_id, 'shozaichimeisho', true);
					if(get_post_meta($post_id,'bukkenshubetsu',true) > 1100 && get_post_meta($post_id,'bukkenshubetsu',true) < 1200){
					$post_menseki = get_post_meta($post_id, 'tochikukaku', true).'m&sup2;';}else{
					$post_menseki = get_post_meta($post_id, 'tatemonomenseki', true).'m&sup2; ';}
					//サムネイル画像
					$img_path = get_option('upload_path');
					if ($img_path == '')	$img_path = 'wp-content/uploads';
	
					$imagetag = array();
					for( $imgid=1; $imgid<=1; $imgid++ ){

						$fudoimg_data = get_post_meta($post_id, "fudoimg$imgid", true);
						$fudoimgcomment_data = get_post_meta($post_id, "fudoimgcomment$imgid", true);
						//$fudoimg_alt = $fudoimgcomment_data . mail_custom_fudoimgtype_print(get_post_meta($post_id, "fudoimgtype$imgid", true));
						$fudoimg_alt = $post_title;
						if($fudoimg_data !="" ){

							$sql  = "";
							$sql .=  "SELECT P.ID,P.guid";
							$sql .=  " FROM $wpdb->posts as P";
							$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
						//	$sql = $wpdb->prepare($sql,'');
							$imetas = $wpdb->get_row( $sql );
	
							$attachmentid = '';
							if ( $imetas != '' ){
								$attachmentid  =  $imetas->ID;
								$guid_url  =  $imetas->guid;
							}

							if($attachmentid !=''){
								//thumbnail、medium、large、full 
								$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');
								$fudoimg_url = $fudoimg_data1[0];
//								$imagetag[$imgid] .=  '<a href="' . $guid_url . '" rel="lightbox['.$post_id.'] lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';
								if($fudoimg_url !=''){
									$imagetag[$imgid] = '<img src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" class="s_size" style="max-width:140px;max-height:105px;border:1px solid #003af6;" />';
								}else{
									$imagetag[$imgid] = '<img src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'"  class="s_size" style="max-width:140px;max-height:105px;border:1px solid #003af6;" />';
								}
							}else{
								$imagetag[$imgid] = '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'.$fudoimg_data.'" class="s_size" style="max-width:140px;max-height:105px;border:1px solid #003af6;"  />';
							}
						}else{
							$imagetag[$imgid] = '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" class="s_size" style="max-width:140px;max-height:105px;border:1px solid #003af6;"  />';
						}
					}
?>
					<!-- 1件目 -->
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="bkn" style="padding-bottom:10px;"><tr>
					<td style="padding:3px 5px;border-top:1px solid #cad3d4;border-bottom:1px solid #cad3d4;line-height:1.4;background:#f7f3e8;font-size:14px;">
					<table cellpadding="0" cellspacing="0" border="0" align="left">
					<tr><td style="padding:4px 8px 3px;border:1px solid #2c70a4;line-height:1;color:#fff;background:#397db2;font-size:10px;"><?php echo $post_shubetu;?></td></tr></table>　
					<a href="<?php echo $post_url;?>" style="font-weight:bold;display:inline-block;"><?php echo $post_title;?></a></td>
					</tr><tr>
					<td class="wrap" style="padding:10px 0;"><div>
					<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr valign="top">
					<td class="pic" style="width:142px;text-align:center;"><a href="<?php echo $post_url;?>"><?php echo $imagetag[1];?></a></td>
					<td class="txt" style="padding-left:3px;"><a href="{$post_url}" style="text-decoration:none;color:#111;">
					<span style="color:#ed6103;"><span style="font-size:16px;font-weight:bold;"><?php echo $post_kakaku;?></span></span><br>
					<span style="font-weight:bold;"><?php echo $post_menseki;?></span><br>
					<?php echo $post_koutsu;?><br>
					<?php echo $post_add;?><br>
					</a>
					<div class="go" style="text-align:right;">
					<a href="<?php echo $post_url;?>" style="display:inline-block;font-size:16px;font-weight:bold;background:url(http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/mail/link_arrow_inline.png) no-repeat 100% 50%;padding-right:12px;">詳細を見る</a>
					</div></td>
					</tr></table>
					</div></td>
					</tr></table>
<?php
endforeach;
//新着物件
$myposts = get_posts(array("post_type"=>"headline","numberposts"=>3,"post_status"=>array("publish","private")));
$headline="";
foreach ( $myposts as $post ){
setup_postdata( $post );
	$headline .= '<tr>
<td style="margin-bottom:10px;font-size:13px;font-weight:bold;padding:0px 10px 0px 0px;">' . get_the_time("Y.m.d",$post->ID) . '</td><td><a href="'
 . get_permalink($post->ID) . '">'. $post->post_title .'</td></tr>';
}
?>
</div>
<!-- / 新着物件 -->

<table width="100%" cellpadding="0" cellspacing="0" border="0" class="pd"><tr>
<td class="btn" style="padding-top:15px;padding-bottom:15px;">
<table width="329" cellpadding="0" cellspacing="0" border="0" align="right">
<tr><td background="http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/mail/button.png" style="text-align:center;background-repeat:no-repeat;">
<a href="<?php echo $joken_url;?>" style="display:block;width:100%;height:32px;overflow:hidden;">
<span style="padding-left:12px;line-height:32px;font-weight:bold;">1週間以内に登録された希望条件の物件をすべて見る</span>
</a></td></tr></table>
</td></tr></table>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="pd"><tr><td style="padding-bottom:20px;"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
<td style="padding:10px;border:1px solid #cad3d4;">
<span style="font-weight:bold;">あなたが保存した、以下の条件にヒットした物件をお送りしています。</span><br>
<?php echo $joken_html;?>
</td></tr></table></td></tr></table>


<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
<td style="padding:0 8px;border-top:1px solid #cad3d4;border-bottom:1px solid #cad3d4;background:#f7f7f7;line-height:1.8;font-weight:bold;">セイブおすすめ新着情報</td>
</tr></table>

<div style="overflow:hidden;padding-bottom:10px;">
 
<div class="kks" style="margin-bottom:10px;">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td class="wrap" style="padding:10px 0;">
<div>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr valign="top">
<td class="txt" style="vertical-align:top;padding-left:13px;">  
<table cellpadding="0" cellspacing="0" border="0" align="left" style="margin-bottom:10px;">
<?php echo $headline;?>
</table><br style="clear:both;">
</td>
</tr>
</table>
</div>
</td>
</tr>
</table>
</div>
</div>
<!-- /お気に入り＆最近見た＆他動線 -->

<div style="overflow:hidden;padding-bottom:10px;">
<!-- コールセンター -->
<div class="call pd" style="float:left;width:100%;margin-bottom:10px;">
<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td class="in" style="padding:8px;border:1px solid #cad3d4;">
<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr>
<td style="width:60px;"><img src="http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/mail/kaeru.png" width="100" alt="" style="vertical-align:bottom;"></td>
<td style="line-height:1;font-weight:bold;padding-left:10px;">
<span style="line-height:1.6;font-size:14px;">お問合せはこちらまで　お気軽にどうぞ！<br></span>
<table cellpadding="0" cellspacing="0" border="0">
<tr>
<td background="http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/mail/tel.png" style="line-height:1;font-size:23px;color:#ed6103;padding-left:40px;background-repeat:no-repeat;background-position:2px 50%;">
<a href="tel:0263-27-9910">0263-27-9910</a></td></tr></table>
</td>
</tr></table>
</td></tr></table>
</div>

<div class="o" style="float:left;width:20px;height:1px;font-size:1px;"><br></div>

<!-- アプリ -->

</div>

<!-- 各種アナウンス -->
<table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding:8px 4px;border-top:1px solid #cad3d4;border-bottom:1px solid #cad3d4;line-height:1.4;">
※メール配信のタイミングによっては成約済みとなっている物件が掲載されている場合もございます。あらかじめご了承ください<br><br>
・<a href="http://seibunet.jp/wp/wp-content/plugins/fudoumail/fudou_user.php">保存した条件の確認、配信停止はこちら</a><br>
（ログインが必要です）<br><br>
・<a href="mailto:info@smileland.net?subject=会員メールアドレス変更&amp;body=会員メールアドレスを削除します">マッチングメールアカウント削除はこちら</a><br>
（お手数ですがメールをお送り下さい）<br><br>
・<a href="http://seibunet.jp/?page_id=65">ご意見・ご要望等はこちら</a><br><br>
発行：株式会社セイブ（長野県松本市双葉24-10） <span style="display:inline-block;"><a href="http://seibunet.jp/">http://seibunet.jp/</a></span><br>
株式会社セイブの<a href="http://seibunet.jp/?page_id=68">個人情報取扱方針についてはこちら</a>をご覧ください。<br>
</td></tr></table>
<!-- / 各種アナウンス -->

<!-- フッター -->
<table width="100%" cellpadding="10" cellspacing="0" border="0"><tr>
<td align="center"><a href="http://seibunet.jp"><img src="http://seibunet.jp/wp/wp-content/themes/seibunet_pc/images/common/f_logo.png" alt="あなたにあった住まいはきっとある。株式会社セイブ"></a><br>&copy;Seibu Co., Ltd.</td></tr></table>
<!-- フッター -->

</td></tr></table>
</div>
</body>