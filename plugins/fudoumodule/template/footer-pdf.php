<!-- footer  -->
<div id="footer">
<div id="footer_upper_wrap">
<table id="footer_upper_table">
<tr>
<td rowspan="5" class="footer-logo">
<p><img src="<?php echo IMGURL;?>/pdfimg/logo.png" alt="[会社名]"></p>
<p class="url">[URL]</p></td>
<td class="font1">[会社名]</td>
<td>お気軽にお問い合わせください。</td>
<td rowspan="5"><span class="f_right"><img src="<?php echo IMGURL;?>/pdfimg/smart_phone_logo.jpg" alt="スマートフォンで物件情報を確認する"></span></td>
<td rowspan="5"><?php pdf_qrcode($post_id);?></td>
</tr>
<tr>
<td>宅建業免許番号：[宅建業免許番号]</td>
<td rowspan="2" class="font2"><img class="telimg" src="<?php echo IMGURL;?>/pdfimg/tel.jpg" alt="tel"><span class="tel">[電話番号]</span></td>
</tr>
<tr>
<td>[住所]</td>
</tr>
<tr>
<td></td>
<td>定休日：[定休日]</td>
</tr>
<tr>
<td><img src="<?php echo IMGURL;?>/pdfimg/train_icon.jpg"><span>[XX駅から徒歩XX分]</span></td>
<td>営業時間：[営業時間]</td>
</tr>
</table>
</div>


<table border="0" class="footer_btm_table">
<tr>
<td>※掲載の不動産物件情報は、当該情報と当該不動産物件の現況が異なる場合には現況を優先するものとします。</td>
<td class="t_right">物件管理番号：<?php echo BUKKEN_NO;?></td>
</tr>
</table>



</div><!-- end of footer  -->