<?php
/*
 * 不動産マッチングメールプラグイン
 * @package WordPress3.5
 * @subpackage Fudousan Plugin
 * Fudousan mail Plugin
 * Version: 1.3.3
*/
global $contact_url;
$contact_url=5943;

//メール送信
function users_mail_send($user_mail_ID,$send_mode){
	global $wpdb;

	$mail_comment = users_mail_bukkenlist($user_mail_ID,$send_mode);

	//メール送信

		$mail_send_ok = '未送信';

		//ヘッダー
		$user_mail_fromname = get_option('user_mail_fromname');
		$user_mail_frommail = get_option('user_mail_frommail');
		if($user_mail_frommail == '')
			$user_mail_frommail = get_option('admin_email');

		if($user_mail_fromname == '')
			$user_mail_fromname = $user_mail_frommail;

//		$headers = 'From: '.$user_mail_fromname.' <'.$user_mail_frommail.'>' . "\r\n";
mb_language("Ja") ;
mb_internal_encoding("UTF-8") ;
		$mime_boundary_alt = substr(base_convert(md5(uniqid()), 16, 36), 0, 20);		 
		$headers = "From: " .mb_encode_mimeheader($user_mail_fromname,"ISO-2022-JP") .' <'.$user_mail_frommail.'>' . "\n" .
		           "MIME-Version: 1.0\n" .
		           "Content-Type: multipart/alternative;".
				   "boundary=" . $mime_boundary_alt . "\n";
		//メール内容
		$user_mail_subject = get_option('user_mail_subject');
		$user_mail_comment = get_option('user_mail_comment');

		$last_name = get_user_meta( $user_mail_ID, 'last_name', true);
		$first_name = get_user_meta( $user_mail_ID, 'first_name', true);

		$sql = "SELECT U.user_login ,  U.user_email ";
		$sql .=  " FROM $wpdb->users AS U";
		$sql .=  " WHERE U.ID = " . $user_mail_ID;
		$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		$user_login = $metas->user_login;
		$user_email = $metas->user_email;

		$user_mail_comment = str_replace("[mail]", $user_email , $user_mail_comment);
		$user_mail_comment = str_replace("[name]", $last_name . ' ' . $first_name , $user_mail_comment);
		$user_mail_comment = str_replace("[bukken]", $mail_comment, $user_mail_comment);


$html = users_mail_bukkenlist_html($user_mail_ID,$send_mode);
		$user_mail_comment =<<<EOF
--{$mime_boundary_alt}
Content-Type: text/plain; charset=ISO-2022-JP
Content-Transfer-Encoding: 7bit
$user_mail_comment
--{$mime_boundary_alt}
Content-Type: text/html; charset=ISO-2022-JP
Content-Transfer-Encoding: 7bit
{$html}
--{$mime_boundary_alt}--
EOF;

		//メール送信
		if($send_mode == 'view'){

			echo 'To: ' . $user_email . '<br />';
			echo $headers . '<br />';
			echo 'Subject: ' . $user_mail_subject . '<br />';
			echo '<hr />';
			echo str_replace("\r\n", "<br />", $user_mail_comment);
			echo "<br />";

		}else{
		
			if( isset($_GET['sendmailmaga']) && $_GET['sendmailmaga'] == '1'){

				if($user_email !=''){
					wp_mail($user_email, $user_mail_subject, $user_mail_comment,$headers);
					

					//メール送信カウント更新
					$mail_count = get_user_meta( $user_mail_ID, 'mail_count', true);
					if($mail_count !=''){
						update_user_meta( $user_mail_ID, 'mail_count', $mail_count + 1 );
					}else{
						update_user_meta( $user_mail_ID, 'mail_count', '1' );
					}
					$mail_send_ok = '送信しました';
				}

			}else{

				if( $mail_comment !=''){
					if($user_email !=''){
						mb_send_mail($user_email, $user_mail_subject, $user_mail_comment,$headers);
						//wp_mail($user_email, $user_mail_subject, $user_mail_comment,$headers);


				
						//メール日付更新
						date_default_timezone_set('Asia/Tokyo');
						$date = date("Y-m-d H:i:s") ; 
						update_user_meta( $user_mail_ID, 'mail_date', $date );


						//メール送信カウント更新
						$mail_count = get_user_meta( $user_mail_ID, 'mail_count', true);
						if($mail_count !=''){
							update_user_meta( $user_mail_ID, 'mail_count', $mail_count + 1 );
						}else{
							update_user_meta( $user_mail_ID, 'mail_count', '1' );
						}
						$mail_send_ok = '送信しました';
					}
				}
			}

			return  '* ' . $last_name . '　' . $first_name . '　' .$user_login . '　' . $user_email . '　' . $mail_send_ok .  '　'. $mail_comment ."\r\n" ;
		}
}




//ユーザー別物件リスト
function users_mail_bukkenlist($user_mail_ID,$send_mode=""){
	//$post_status = "P.post_status='publish'";
	$post_status = "P.post_status IN ('publish','private')";
	global $wpdb;
	global $contact_url;
	$next_sql = true;

	$mail_date_data = "";
	$mail_date = get_user_meta( $user_mail_ID, 'mail_date', true);
	if( $mail_date != '') 
		$mail_date_data = " AND P.post_modified >= CAST( '".$mail_date."' AS DATETIME) ";
	if(isset($_GET["test"])){
		$mail_date_data = "";
	}
	$user_mail_kaiin = get_option('user_mail_kaiin');
	if($user_mail_kaiin == '') $user_mail_kaiin = 0;

	$user_mail_bukkenlimit = get_option('user_mail_bukkenlimit');
	if($user_mail_bukkenlimit == '') $user_mail_bukkenlimit = 20;


	//条件種別
		$user_mail_shu = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_shu', true) );

		if (is_array($user_mail_shu)) {
			$i=0;
			$shu_data = ' IN ( 0 ';
			foreach($user_mail_shu as $meta_set){
				$shu_data .= ','. $user_mail_shu[$i] . '';
				$i++;
			}
			$shu_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='bukkenshubetsu' AND PM.meta_value ".$shu_data."";
			$sql .=   $mail_date_data;

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';

			}else{
				$next_sql = false;
			}
		}


	//echo '<br />条件種別 ';
	//echo $id_data;


	//成約 3:空無/売止 4:成約 9:削除 成約日

		if( $next_sql ){

			$not_id_data = ' AND P.ID NOT IN ( 0 ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id ";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='seiyakubi' AND PM.meta_value != '' ";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {
				foreach ( $metas as $meta ) {
						$not_id_data .= ','. $meta['ID'];
				}
			}

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM_1.meta_key='jyoutai' AND PM_1.meta_value >= 3 ";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {
				foreach ( $metas as $meta ) {
						$not_id_data .= ','. $meta['ID'];
				}
			}

			$not_id_data .= ') ';


			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=    $not_id_data;

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}


	//条件エリア
		$user_mail_sik = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_sik', true) );

		if(is_array( $user_mail_sik ) && $next_sql ){
			$i=0;
			$sik_data = ' IN ( 0 ';
			foreach($user_mail_sik as $meta_set){
				$sik_data .= ','. $user_mail_sik[$i] . '';
				$i++;
			}
			$sik_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='shozaichicode' AND PM.meta_value ".$sik_data."";
			$sql .=   $mail_date_data;
			$sql .=   $id_data;

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}

/////////////////////////追加
	//条件エリア
		$town_match = maybe_unserialize(get_user_meta( $user_mail_ID, 'user_town_match', true));
		if(is_array( $town_match ) && $next_sql ){
			$i=0;
			$twn_data = ' IN ( \'物件町名\' ';
			foreach($town_match as $meta_set){
				$twn_data .= ",'". $meta_set . "'";
			}
			$twn_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='shozaichimeisho' AND PM.meta_value ".$twn_data."";
			$sql .=   $mail_date_data;
			$sql .=   $id_data;
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}


///////////////////////////



	//echo '<br />エリア ';
	//echo $id_data;

	//条件路線駅
		$user_mail_eki = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_eki', true) );


		if(is_array( $user_mail_eki ) && $next_sql ){
			$i=0;
			$eki_data = ' IN ( 0 ';
			foreach($user_mail_eki as $meta_set){
				$eki_data .= ',' . intval(myLeft($user_mail_eki[$i],6)) . intval(myRight($user_mail_eki[$i],6));
				$i++;
			}
			$eki_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM.meta_key='koutsurosen1' AND PM_1.meta_key='koutsueki1' ";
			$sql .=  " AND PM.meta_value !='' ";
			$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $eki_data . "";


			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data2 = '';
			if(!empty($metas)) {
				$id_data2 = ' OR (P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data2 .= ','. $meta['ID'];
				}
				$id_data2 .= ')) ';
			}

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ( ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM.meta_key='koutsurosen2' AND PM_1.meta_key='koutsueki2' ";
			$sql .=  " AND PM.meta_value !='' ";
			$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $eki_data . ")";
			$sql .=  " " . $id_data2 . "";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}

		}

	//echo '<br />駅 ';
	//echo $id_data;


	//条件価格
		$kalb_data = get_user_meta( $user_mail_ID, 'user_mail_kalb', true);
		$kahb_data = get_user_meta( $user_mail_ID, 'user_mail_kahb', true);
		$kalc_data = get_user_meta( $user_mail_ID, 'user_mail_kalc', true);
		$kahc_data = get_user_meta( $user_mail_ID, 'user_mail_kahc', true);

		if($kalb_data+$kahb_data+$kalc_data+$kahc_data >0 && $next_sql){

			$kalb_data =$kalb_data*10000 ;

			if($kahb_data == '0' ){
				$kahb_data = 1000000000 ;
			}else{
				$kahb_data =$kahb_data*10000 ;
			}

			$kalc_data =$kalc_data*10000 ;

			if($kahc_data == '0' ){
				$kahc_data = 9990000 ;
			}else{
				$kahc_data =$kahc_data*10000 ;
			}


			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id )";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu' AND CAST(PM_1.meta_value AS SIGNED) < 3000";
			$sql .=  " AND PM.meta_key='kakaku'";
			$sql .=  " AND CAST(PM.meta_value AS SIGNED) >= $kalb_data AND CAST(PM.meta_value AS SIGNED) <= $kahb_data  ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data2 = '';
			if(!empty($metas)) {
				$id_data2 = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data2 .= ','. $meta['ID'];
				}
				$id_data2 .= ') ';
			}else{
				$next_sql = false;
			}

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id )";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu' AND CAST(PM_1.meta_value AS SIGNED) > 3000";
			$sql .=  " AND PM.meta_key='kakaku'";
			$sql .=  " AND CAST(PM.meta_value AS SIGNED) >= $kalc_data  AND CAST(PM.meta_value AS SIGNED) <= $kahc_data ";
			$sql .=  " OR ( P.ID " . $id_data2 . ")";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}

	//echo '<br />価格 ';
	//echo $id_data;



	//専有面積
		$tatemo_l_data = get_user_meta( $user_mail_ID, 'user_mail_tatemonomenseki_l', true);
		$tatemo_h_data = get_user_meta( $user_mail_ID, 'user_mail_tatemonomenseki_h', true);

		if( !empty($tatemo_l_data) || !empty($tatemo_h_data) ){

			if( $tatemo_h_data == '0' ) $tatemo_h_data = 9999 ;

			if(( $tatemo_l_data != 0 || $tatemo_h_data != 9999 ) && $id_data !='' ){
				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
				$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=    $mail_date_data;
				$sql .=    $id_data;
				$sql .=  " AND PM_2.meta_key='tatemonomenseki'";

				$sql .=  " AND PM_2.meta_value *100 >= $tatemo_l_data*100 ";
				$sql .=  " AND PM_2.meta_value *100 <= $tatemo_h_data*100 ";


				$sql = $wpdb->prepare($sql,'');
				$metas = $wpdb->get_results( $sql,  ARRAY_A );
				$id_data = '';
				if(!empty($metas)) {
					$id_data = ' AND P.ID IN ( 0 ';
					foreach ( $metas as $meta ) {
							$id_data .= ','. $meta['ID'];
					}
					$id_data .= ') ';
				}else{
					$next_sql = false;
				}
			}
		}

	//echo '<br />専有面積 ';
	//echo $id_data;


	//土地面積
		$tochim_l_data = get_user_meta( $user_mail_ID, 'user_mail_tochikukaku_l', true);
		$tochim_h_data = get_user_meta( $user_mail_ID, 'user_mail_tochikukaku_h', true);

		if( !empty($tochim_l_data) || !empty($tochim_h_data) ){

			if( $tochim_h_data  == '0' ) $tochim_h_data = 9999 ;

			if(( $tochim_l_data != 0 || $tochim_h_data != 9999 ) && $id_data !='' ){
				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
				$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=    $mail_date_data;
				$sql .=    $id_data;
				$sql .=  " AND PM_2.meta_key='tochikukaku'";
				$sql .=  " AND PM_2.meta_value *100 >= $tochim_l_data*100 ";
				$sql .=  " AND PM_2.meta_value *100 <= $tochim_h_data*100 ";

				$sql = $wpdb->prepare($sql,'');
				$metas = $wpdb->get_results( $sql,  ARRAY_A );
				$id_data = '';
				if(!empty($metas)) {
					$id_data = ' AND P.ID IN ( 0 ';
					foreach ( $metas as $meta ) {
							$id_data .= ','. $meta['ID'];
					}
					$id_data .= ') ';
				}else{
					$next_sql = false;
				}

			}
		}
	//echo '<br />土地面積 ';
	//echo $id_data;





	//条件間取り
		$user_mail_madori = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_madori', true) );

		if(is_array( $user_mail_madori ) && $next_sql ){
			$i=0;
			$madori_data = ' IN ( 0 ';
			foreach($user_mail_madori as $meta_set){
				$madori_data .= ','. $user_mail_madori[$i] . '';
				$i++;
			}
			$madori_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM.meta_key='madorisu' AND PM_1.meta_key='madorisyurui' ";
			$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $madori_data . "";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}

		}

	//echo '<br />間取り ';
	//echo $id_data;

	//駅歩分
		$hof_data = get_user_meta( $user_mail_ID, 'user_mail_hohun', true);


		if( $hof_data != 0  && $next_sql ){

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2   ON P.ID = PM_2.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND (PM_2.meta_key='koutsutoho1f' OR PM_2.meta_key='koutsutoho2f' )";
			$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) > 0 ";
			$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) <= $hof_data ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}

	//echo '<br />歩分 ';
	//echo $id_data;


	//条件設備
		$user_mail_setsubi = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_setsubi', true) );

		if(is_array( $user_mail_setsubi ) && $next_sql ){
			$i=0;
			$setsubi_data = " AND (";
			foreach($user_mail_setsubi as $meta_set){
			//	if($i!=0) $setsubi_data .= " OR ";
				if($i!=0) $setsubi_data .= " AND ";
				$setsubi_data .= " PM.meta_value LIKE '%/". $user_mail_setsubi[$i] . "%' ";
				$i++;
			}
			$setsubi_data .= ")";


			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=   $mail_date_data;
			$sql .=   $id_data;
			$sql .=  " AND PM.meta_key='setsubi' ".$setsubi_data."";

		//	$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}

		}

	//echo '<br />設備 ';
	//echo $id_data;


	//日付
	date_default_timezone_set('Asia/Tokyo');
	$date = date("Y/m/d") ; 

	$mail_comment = '';

	//物件データー
		if($id_data !='' && $user_mail_bukkenlimit > 0 ){

			$sql  = "SELECT P.ID,P.post_title,P.post_excerpt,P.post_status";
			$sql .= " FROM $wpdb->posts AS P";

			if($user_mail_kaiin == 1 )
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";

			$sql .= " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=   $id_data;

			if($user_mail_kaiin == 1 )
				$sql .=  " AND PM.meta_key='kaiin' AND PM.meta_value = '1' ";

			$sql .= " ORDER BY P.post_modified DESC";

			if($user_mail_kaiin == 0 )
				$sql .=  " LIMIT ".$user_mail_bukkenlimit."";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			$mail_comment = '';
			if(!empty($metas)) {

				$i = 1;
				foreach ( $metas as $meta ) {

					$post_id =  $meta['ID'];
					$post_title =  $meta['post_title'];
					$post_excerpt =  $meta['post_excerpt'];
					if($meta["post_status"]=="private"){
						$post_url =  home_url() . "/?page_id=" . $contact_url;
						$post_url .= "&subject=" . $post_title;
					}else{
						$post_url =  get_permalink($post_id);
						$post_url .=  "&ui=". $user_mail_ID;
					}

					//SEND LOG
					if( empty($send_mode) ){
						users_sendmail_log($post_id,$user_mail_ID,$date);
					}

					if($user_mail_kaiin == 2 && get_post_meta($post_id,'kaiin',true) == '1'){
					
					}else{

						$mail_comment .= "\r\n　";

						//物件番号
						$mail_comment .= '物件番号 ' . get_post_meta($post_id,'shikibesu',true);
						if(get_post_meta($post_id,'kaiin',true)=="1")
							$mail_comment .= '　会員限定物件';
						$mail_comment .= "\r\n　";


						//タイトル
							$mail_comment .= $post_title . '　';

						//価格

							if( get_post_meta($post_id, 'seiyakubi', true) != "" ){
								$mail_comment .= 'ご成約済　';
							}else{
								//非公開の場合
								if(get_post_meta($post_id,'kakakukoukai',true) == "0"){
									$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
									if($kakakujoutai_data_data=="1")	$mail_comment .= '相談　';
									if($kakakujoutai_data_data=="2")	$mail_comment .= '確定　';
									if($kakakujoutai_data_data=="3")	$mail_comment .= '入札　';

								}else{
									$kakaku_data = get_post_meta($post_id,'kakaku',true);
									if(is_numeric($kakaku_data)){
										$mail_comment .= floatval($kakaku_data)/10000;
										$mail_comment .= "万円　";
									}
								}					
							}


						//間取り

							$madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);
							$mail_comment .= get_post_meta($post_id,'madorisu',true);
							if($madorisyurui_data=="10")	$mail_comment .= 'R ';
							if($madorisyurui_data=="20")	$mail_comment .= 'K ';
							if($madorisyurui_data=="25")	$mail_comment .= 'SK ';
							if($madorisyurui_data=="30")	$mail_comment .= 'DK ';
							if($madorisyurui_data=="35")	$mail_comment .= 'SDK ';
							if($madorisyurui_data=="40")	$mail_comment .= 'LK ';
							if($madorisyurui_data=="45")	$mail_comment .= 'SLK ';
							if($madorisyurui_data=="50")	$mail_comment .= 'LDK ';
							if($madorisyurui_data=="55")	$mail_comment .= 'SLDK ';

							$mail_comment .= "\r\n　";

						//抜粋
							if( $post_excerpt != "" ){
							if( $post_excerpt != " " ){
								$mail_comment .= $post_excerpt;
								$mail_comment .= "\r\n　";
							}
							}

						//所在地
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);
							$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichicode_data = myLeft($shozaichicode_data,5);
							$shozaichicode_data = myRight($shozaichicode_data,3);

							if($shozaichiken_data !="" && $shozaichicode_data !=""){
								$sql = "SELECT narrow_area_name FROM ".$wpdb->prefix."area_narrow_area WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

								$sql = $wpdb->prepare($sql,'');
								$metas = $wpdb->get_row( $sql );
								$mail_comment .= "".$metas->narrow_area_name."";
							}
							$mail_comment .= get_post_meta($post_id, 'shozaichimeisho', true);

							$mail_comment .= "\r\n　";

						//交通路線

							$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
							$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);

							if($koutsurosen_data !=""){
								$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql,'');
								$metas = $wpdb->get_row( $sql );
								$mail_comment .= "".$metas->rosen_name;
							}

							//交通駅
							if($koutsurosen_data !="" && $koutsueki_data !=""){
								$sql = "SELECT DTS.station_name";
								$sql = $sql . " FROM ".$wpdb->prefix."train_rosen AS DTR";
								$sql = $sql . " INNER JOIN ".$wpdb->prefix."train_station as DTS ON DTR.rosen_id = DTS.rosen_id";
								$sql = $sql . " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql,'');
								$metas = $wpdb->get_row( $sql );
								if(!empty($metas)){
									$mail_comment .= $metas->station_name.'駅';
								}
							}


							if(get_post_meta($post_id, 'koutsubusstei', true) !="" || get_post_meta($post_id, 'koutsubussfun', true) !=""  )
								$mail_comment .= ' バス('.$koutsubusstei.' '.$koutsubussfun.'分)';

							if(get_post_meta($post_id, 'koutsutoho1', true) !="")
								$mail_comment .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho1', true).'m';

							if(get_post_meta($post_id, 'koutsutoho1f', true) !="")
								$mail_comment .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho1f', true).'分';


						$mail_comment .= "\r\n　";
						$mail_comment .= $post_url;
						$mail_comment .= "\r\n";

						$mail_comment .= "　----------------------------------------------------------\r\n";

						$i++;
					}

					if( $i > $user_mail_bukkenlimit ) break;
				}
			}
		}
		return  $mail_comment;
}


//ユーザー別物件リストHTML
function users_mail_bukkenlist_html($user_mail_ID,$send_mode=""){
global $work_bukkenshubetsu;
	//$post_status = "P.post_status='publish'";
	$post_status = "P.post_status IN ('publish','private')";
	global $wpdb;
	global $contact_url;	
	$next_sql = true;
	$joken_html = "";
	$joken_url  = home_url() . "/?bukken=jsearch&shub=1&b=1&sd=7";

	$mail_date_data = "";
	$mail_date = get_user_meta( $user_mail_ID, 'mail_date', true);
	if( $mail_date != '') 
		$mail_date_data = " AND P.post_modified >= CAST( '".$mail_date."' AS DATETIME) ";
	if(isset($_GET["test"])){
		$mail_date_data = "";
	}
	$user_mail_kaiin = get_option('user_mail_kaiin');
	if($user_mail_kaiin == '') $user_mail_kaiin = 0;

	$user_mail_bukkenlimit = get_option('user_mail_bukkenlimit');
	if($user_mail_bukkenlimit == '') $user_mail_bukkenlimit = 20;


	//条件種別
		$user_mail_shu = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_shu', true) );
		//echo $user_mail_shu;
		if (is_array($user_mail_shu)) {
			$joken_html .= "【物件種別】";
			$i=0;
			$shu_data = ' IN ( 0 ';
			$sep = "";
			foreach($user_mail_shu as $meta_set){
				$shu_data .= ','. $user_mail_shu[$i] . '';
				$shubetu_work = $work_bukkenshubetsu[$user_mail_shu[$i]];
				$joken_html .= $sep . preg_replace("/【(.+)】/","&nbsp;",$shubetu_work["name"]);
				$joken_url  .= "&shu[]=" . $user_mail_shu[$i];
				$sep = " , ";
				$i++;
			}
			$shu_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='bukkenshubetsu' AND PM.meta_value ".$shu_data."";
			$sql .=   $mail_date_data;

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';

			}else{
				$next_sql = false;
			}
		}


	//echo '<br />条件種別 ';
	//echo $id_data;


	//成約 3:空無/売止 4:成約 9:削除 成約日

		if( $next_sql ){

			$not_id_data = ' AND P.ID NOT IN ( 0 ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id ";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='seiyakubi' AND PM.meta_value != '' ";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {
				foreach ( $metas as $meta ) {
						$not_id_data .= ','. $meta['ID'];
				}
			}

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM_1.meta_key='jyoutai' AND PM_1.meta_value >= 3 ";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			if(!empty($metas)) {
				foreach ( $metas as $meta ) {
						$not_id_data .= ','. $meta['ID'];
				}
			}

			$not_id_data .= ') ';


			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=    $not_id_data;

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}

	$area = "";
	//条件エリア
		$user_mail_sik = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_sik', true) );
		print_r($user_mail_sik);
		if(is_array( $user_mail_sik ) && $next_sql ){
			$i=0;
			$sik_data = ' IN ( 0 ';
			foreach($user_mail_sik as $meta_set){
				$sik_data .= ','. $user_mail_sik[$i] . '';
				$i++;
			}
			$sik_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='shozaichicode' AND PM.meta_value ".$sik_data."";
			$sql .=   $mail_date_data;
			$sql .=   $id_data;

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}

/////////////////////////追加
	//条件エリア
	$table_name = $wpdb->prefix . 'user_town_meta';
	$sql = "SELECT middle_area_id,narrow_area_id,town_area_id FROM $table_name WHERE user_id =" . $user_mail_ID;
	$sql = $wpdb->prepare($sql,'');
	$town_match = $wpdb->get_results( $sql );
	
	
		//$town_match = maybe_unserialize(get_user_meta( $user_mail_ID, 'user_town_match', true));
		//echo "TOWN!";
	if(is_array($town_match)){
		$sql2  = "SELECT P.ID FROM (($wpdb->posts AS P ";
		$sql2 .= "INNER JOIN  $wpdb->postmeta AS M1 on P.ID = M1.post_id ) ";
		$sql2 .= "INNER JOIN  $wpdb->postmeta AS M2 on P.ID = M2.post_id ) ";
		$sql2 .= "INNER JOIN  $wpdb->postmeta AS M3 on P.ID = M3.post_id ";
		$sql2 .= "WHERE ";
		$townline = "【エリア】";
		$sep = "";
		foreach($town_match as $tt){
			$skc="";$tname="";
			if($tt->middle_area_id || $tt->narrow_area_id || $tt->town_area_id){
					$kname = $wpdb->get_var("SELECT middle_area_name FROM {$wpdb->prefix}area_middle_area WHERE middle_area_id = {$tt->middle_area_id}");
					$townline .= $sep . "&nbsp;" . $kname;
				$sql2 .= "(M1.meta_key = 'shozaichiken' AND M1.meta_value='{$tt->middle_area_id}' ";
				$sql2 .= "AND M2.meta_key = 'shozaichicode' ";
				if($tt->narrow_area_id){
					$skc = $tt->middle_area_id . $tt->narrow_area_id . "000000";
					$sname = $wpdb->get_var("SELECT narrow_area_name FROM {$wpdb->prefix}area_narrow_area WHERE middle_area_id = {$tt->middle_area_id} AND narrow_area_id = {$tt->narrow_area_id}");
					$townline .= ">" . $sname;
					$sql2 .= "AND M2.meta_value = '{$skc}' ";
					$joken_url .= "&ksik[]=" . $tt->middle_area_id . $tt->narrow_area_id;
				}
				$sql2 .= "AND M3.meta_key = 'shozaichimeisho' ";
				if($tt->town_area_id){
					$tname = $wpdb->get_var("SELECT town_name FROM {$wpdb->prefix}area_town_area WHERE town_id = {$tt->town_area_id}");
					$townline .= ">" . $tname;
					$sql2 .= "AND M3.meta_value LIKE '%{$tname}%' ";
				}
				$sql2 .= " ) OR ";
				$sep = " , ";
			}
		}
		$sql2 .= "(P.ID = 999999) AND ";
		$sql2 .= "P.post_type = 'fudo' AND ".$post_status." ";
		$town_ids = $wpdb->get_results( $sql2 );
		$joken_html .= $townline;
	}
		if(!empty( $town_ids ) && $next_sql ){
			$i=0;
			if(is_array( $town_ids )){
			$twn_data = ' IN ( \'22222222222\' ';
			foreach($town_ids as $meta_set){
				$town_ids .= ",'". $meta_set->ID . "'";
				$twn_data .= ",'". $meta_set->ID . "'";
			}
			$twn_data .= ') ';
			}
			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND P.ID ".$twn_data."";
			$sql .=   $mail_date_data;
			$sql .=   $id_data;
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}


///////////////////////////



	//echo '<br />エリア ';
	//echo $id_data;

	//条件路線駅
		$user_mail_eki = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_eki', true) );


		if(is_array( $user_mail_eki ) && $next_sql ){
			$i=0;
			$eki_data = ' IN ( 0 ';
			foreach($user_mail_eki as $meta_set){
				$eki_data .= ',' . intval(myLeft($user_mail_eki[$i],6)) . intval(myRight($user_mail_eki[$i],6));
				$i++;
			}
			$eki_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE  ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM.meta_key='koutsurosen1' AND PM_1.meta_key='koutsueki1' ";
			$sql .=  " AND PM.meta_value !='' ";
			$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $eki_data . "";


			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data2 = '';
			if(!empty($metas)) {
				$id_data2 = ' OR (P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data2 .= ','. $meta['ID'];
				}
				$id_data2 .= ')) ';
			}

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ( ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM.meta_key='koutsurosen2' AND PM_1.meta_key='koutsueki2' ";
			$sql .=  " AND PM.meta_value !='' ";
			$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $eki_data . ")";
			$sql .=  " " . $id_data2 . "";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}

		}

	//echo '<br />駅 ';
	//echo $id_data;


	//条件価格
		$kalb_data = get_user_meta( $user_mail_ID, 'user_mail_kalb', true);
		$kahb_data = get_user_meta( $user_mail_ID, 'user_mail_kahb', true);
		$kalc_data = get_user_meta( $user_mail_ID, 'user_mail_kalc', true);
		$kahc_data = get_user_meta( $user_mail_ID, 'user_mail_kahc', true);

		if($kalb_data + $kahb_data > 0){
			$joken_html .= "【価格】条件なし";
		}else{
			$joken_html .= "【価格】";
			$joken_html .= $kalb_data?$kalb_data . "万円":"下限なし";
			$joken_html .= " ～ ";
			$joken_html .= $kahb_data?$kahb_data . "万円":"上限なし";
		}
		if($kalb_data+$kahb_data+$kalc_data+$kahc_data >0 && $next_sql){
			$joken_url .= "&kalb={$kalb_data}&kahb={$kahb_data}";
			$kalb_data =$kalb_data*10000 ;

			if($kahb_data == '0' ){
				$kahb_data = 1000000000 ;
			}else{
				$kahb_data =$kahb_data*10000 ;
			}

			$kalc_data =$kalc_data*10000 ;

			if($kahc_data == '0' ){
				$kahc_data = 9990000 ;
			}else{
				$kahc_data =$kahc_data*10000 ;
			}


			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id )";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu' AND CAST(PM_1.meta_value AS SIGNED) < 3000";
			$sql .=  " AND PM.meta_key='kakaku'";
			$sql .=  " AND CAST(PM.meta_value AS SIGNED) >= $kalb_data AND CAST(PM.meta_value AS SIGNED) <= $kahb_data  ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data2 = '';
			if(!empty($metas)) {
				$id_data2 = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data2 .= ','. $meta['ID'];
				}
				$id_data2 .= ') ';
			}else{
				$next_sql = false;
			}

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id )";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu' AND CAST(PM_1.meta_value AS SIGNED) > 3000";
			$sql .=  " AND PM.meta_key='kakaku'";
			$sql .=  " AND CAST(PM.meta_value AS SIGNED) >= $kalc_data  AND CAST(PM.meta_value AS SIGNED) <= $kahc_data ";
			$sql .=  " OR ( P.ID " . $id_data2 . ")";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}

	//echo '<br />価格 ';
	//echo $id_data;



	//専有面積
		$tatemo_l_data = get_user_meta( $user_mail_ID, 'user_mail_tatemonomenseki_l', true);
		$tatemo_h_data = get_user_meta( $user_mail_ID, 'user_mail_tatemonomenseki_h', true);

		if( !empty($tatemo_l_data) || !empty($tatemo_h_data) ){

			if( $tatemo_h_data == '0' ) $tatemo_h_data = 9999 ;

			if(( $tatemo_l_data != 0 || $tatemo_h_data != 9999 ) && $id_data !='' ){
				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
				$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=    $mail_date_data;
				$sql .=    $id_data;
				$sql .=  " AND PM_2.meta_key='tatemonomenseki'";

				$sql .=  " AND PM_2.meta_value *100 >= $tatemo_l_data*100 ";
				$sql .=  " AND PM_2.meta_value *100 <= $tatemo_h_data*100 ";


				$sql = $wpdb->prepare($sql,'');
				$metas = $wpdb->get_results( $sql,  ARRAY_A );
				$id_data = '';
				if(!empty($metas)) {
					$id_data = ' AND P.ID IN ( 0 ';
					foreach ( $metas as $meta ) {
							$id_data .= ','. $meta['ID'];
					}
					$id_data .= ') ';
				}else{
					$next_sql = false;
				}
			}
		}

	//echo '<br />専有面積 ';
	//echo $id_data;


	//土地面積
		$tochim_l_data = get_user_meta( $user_mail_ID, 'user_mail_tochikukaku_l', true);
		$tochim_h_data = get_user_meta( $user_mail_ID, 'user_mail_tochikukaku_h', true);

		if( !empty($tochim_l_data) || !empty($tochim_h_data) ){

			if( $tochim_h_data  == '0' ) $tochim_h_data = 9999 ;

			if(( $tochim_l_data != 0 || $tochim_h_data != 9999 ) && $id_data !='' ){
				$sql = "SELECT DISTINCT( P.ID )";
				$sql .=  " FROM $wpdb->posts AS P";
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
				$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
				$sql .=    $mail_date_data;
				$sql .=    $id_data;
				$sql .=  " AND PM_2.meta_key='tochikukaku'";
				$sql .=  " AND PM_2.meta_value *100 >= $tochim_l_data*100 ";
				$sql .=  " AND PM_2.meta_value *100 <= $tochim_h_data*100 ";

				$sql = $wpdb->prepare($sql,'');
				$metas = $wpdb->get_results( $sql,  ARRAY_A );
				$id_data = '';
				if(!empty($metas)) {
					$id_data = ' AND P.ID IN ( 0 ';
					foreach ( $metas as $meta ) {
							$id_data .= ','. $meta['ID'];
					}
					$id_data .= ') ';
				}else{
					$next_sql = false;
				}

			}
		}
	//echo '<br />土地面積 ';
	//echo $id_data;





	//条件間取り
		$user_mail_madori = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_madori', true) );

		if(is_array( $user_mail_madori ) && $next_sql ){
			$i=0;
			$madori_data = ' IN ( 0 ';
			foreach($user_mail_madori as $meta_set){
				$madori_data .= ','. $user_mail_madori[$i] . '';
				$i++;
			}
			$madori_data .= ') ';

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM ($wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM   ON P.ID = PM.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND PM.meta_key='madorisu' AND PM_1.meta_key='madorisyurui' ";
			$sql .=  " AND concat( PM.meta_value,PM_1.meta_value) " . $madori_data . "";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}

		}

	//echo '<br />間取り ';
	//echo $id_data;

	//駅歩分
		$hof_data = get_user_meta( $user_mail_ID, 'user_mail_hohun', true);


		if( $hof_data != 0  && $next_sql ){

			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2   ON P.ID = PM_2.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=    $mail_date_data;
			$sql .=    $id_data;
			$sql .=  " AND (PM_2.meta_key='koutsutoho1f' OR PM_2.meta_key='koutsutoho2f' )";
			$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) > 0 ";
			$sql .=  " AND CAST(PM_2.meta_value AS SIGNED) <= $hof_data ";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}
		}

	//echo '<br />歩分 ';
	//echo $id_data;


	//条件設備
		$user_mail_setsubi = maybe_unserialize( get_user_meta( $user_mail_ID, 'user_mail_setsubi', true) );

		if(is_array( $user_mail_setsubi ) && $next_sql ){
			$i=0;
			$setsubi_data = " AND (";
			foreach($user_mail_setsubi as $meta_set){
			//	if($i!=0) $setsubi_data .= " OR ";
				if($i!=0) $setsubi_data .= " AND ";
				$setsubi_data .= " PM.meta_value LIKE '%/". $user_mail_setsubi[$i] . "%' ";
				$i++;
			}
			$setsubi_data .= ")";


			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=   $mail_date_data;
			$sql .=   $id_data;
			$sql .=  " AND PM.meta_key='setsubi' ".$setsubi_data."";

		//	$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$id_data = '';
			if(!empty($metas)) {
				$id_data = ' AND P.ID IN ( 0 ';
				foreach ( $metas as $meta ) {
						$id_data .= ','. $meta['ID'];
				}
				$id_data .= ') ';
			}else{
				$next_sql = false;
			}

		}

	//echo '<br />設備 ';
	//echo $id_data;


	//日付
	date_default_timezone_set('Asia/Tokyo');
	$date = date("Y/m/d") ; 

	$mail_comment = '';
	//物件データー
		if($id_data !='' && $user_mail_bukkenlimit > 0 ){

			$sql  = "SELECT P.ID,P.post_title,P.post_excerpt,P.post_status";
			$sql .= " FROM $wpdb->posts AS P";

			if($user_mail_kaiin == 1 )
				$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id ";

			$sql .= " WHERE ".$post_status." AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=   $id_data;

			if($user_mail_kaiin == 1 )
				$sql .=  " AND PM.meta_key='kaiin' AND PM.meta_value = '1' ";

			$sql .= " ORDER BY P.post_modified DESC";

			if($user_mail_kaiin == 0 )
				$sql .=  " LIMIT ".$user_mail_bukkenlimit."";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			$mail_comment='';
			if(!empty($metas)) {
				$count = $user_mail_bukkenlimit<count($metas)?$user_mail_bukkenlimit:count($metas);
				$date  = date("Y/m/d");
				  ob_start();

				  include dirname(__DIR__) . "/template/mail_html.php";
				  $mail_comment = ob_get_contents();
				  ob_end_clean();

				
			}
		}
	//	return  apply_filters("users_mail_bukkenlist_html",$mail_comment,$metas);
	return $mail_comment;
}

//ユーザー別送信物件LOG
function users_sendmail_log($post_id,$user_id,$date){


			//個別 LOG
			$user_sendmail_log = maybe_unserialize( get_user_meta( $user_id, 'user_sendmail_log', true) );
			$count_new = 1;
			if (is_array($user_sendmail_log)) {

				foreach ($user_sendmail_log as $key => $val) {
					if( $key == $date && isset( $val[$post_id])  ){
						$user_sendmail_log[$date][$post_id] = $val[$post_id] + 1;
						$count_new = 0;
					}
				}
			}

			if( $count_new == 1){
				$user_sendmail_log[$date][$post_id] = '1';
			}
/*
			//１ヶ月以前は削除
			$old_day = date("Y/m/d",strtotime("-1 month"));
			foreach ($user_sendmail_log as $key => $val) {
				if( $key < $old_day ){
					unset($user_sendmail_log[$key]);
				}
			}
*/
			update_user_meta( $user_id, 'user_sendmail_log', maybe_serialize($user_sendmail_log) );

}




















//////////////追加関数///////////////////////////////////////////////////////////////////////
/**
 * 物件画像タイプ
 *
 * @since Fudousan Plugin 1.0.0 *
 * @param int|string $imgtype.
 * @return text
 */
function mail_custom_fudoimgtype_print($imgtype) {

	switch ($imgtype) {
		case "1" :
			$imgtype = "(間取)"; break;
		case "2" :
			$imgtype = "(外観)"; break;
		case "3" :
			$imgtype = "(地図)"; break;
		case "4" :
			$imgtype = "(周辺)"; break;
		case "5" :
			$imgtype = "(内装)"; break;
		case "9" :
			$imgtype = ""; break;	//(その他画像)
		case "10" :
			$imgtype = "(玄関)"; break;
		case "11" :
			$imgtype = "(居間)"; break;
		case "12" :
			$imgtype = "(キッチン)"; break;
		case "13" :
			$imgtype = "(寝室)"; break;
		case "14" :
			$imgtype = "(子供部屋)"; break;
		case "15" :
			$imgtype = "(風呂)"; break;
	}

	return $imgtype;
}

function mail_custom_kakaku_print($post_id, $echo_flg = true) {
	$buf = '';
	//非公開の場合
	if(get_post_meta($post_id,'kakakukoukai',true) == "0"){

		$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
		if($kakakujoutai_data=="1")	$buf = '相談';
		if($kakakujoutai_data=="2")	$buf = '確定';
		if($kakakujoutai_data=="3")	$buf = '入札';

	}else{
		$kakaku_data = get_post_meta($post_id,'kakaku',true);
		if(is_numeric($kakaku_data)){
			$buf = floatval($kakaku_data)/10000 . "万円";
		}
	}
	if( $echo_flg )
		echo $buf;
	else
		return $buf;
}

/**
 * 路線 駅 バス 徒歩
 *
 * @since Fudousan Plugin 1.0.0 *
 * @param int $post_id Post ID.
 */
function mail_custom_koutsu1_print($post_id, $echo_flg = true) {
	$buf = '';
	global $wpdb;
	$rosen_name = '';

	$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
	$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = myLeft($shozaichiken_data,2);

	if($koutsurosen_data !=""){
		$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
	//	$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) $rosen_name = $metas->rosen_name;
		$buf .=  "".$rosen_name;
	}

	if($koutsurosen_data !="" && $koutsueki_data !=""){
		$sql = "SELECT DTS.station_name";
		$sql .=  " FROM ".$wpdb->prefix."train_rosen AS DTR";
		$sql .=  " INNER JOIN ".$wpdb->prefix."train_station as DTS ON DTR.rosen_id = DTS.rosen_id";
		$sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
	//	$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) {
			if($metas->station_name != '＊＊＊＊') 	$buf .=  $metas->station_name.'駅';
		}
	}

	$koutsubusstei=get_post_meta($post_id, 'koutsubusstei1', true);
	$koutsubussfun=get_post_meta($post_id, 'koutsubussfun1', true);
	$koutsutohob1f=get_post_meta($post_id, 'koutsutohob1f', true);

	if($koutsubusstei !="" || $koutsubussfun !=""){

		if($rosen_name == 'バス'){
			$buf .=  '(' . $koutsubusstei;
			if(!empty($koutsubussfun)) $buf .=  ' '.$koutsubussfun.'分';
		}else{
			$buf .=  ' バス(' . $koutsubusstei;
			if(!empty($koutsubussfun)) $buf .=  ' '.$koutsubussfun.'分';
		}

		if($koutsutohob1f !="" )
			$buf .=  ' 停歩'.$koutsutohob1f.'分';
		$buf .=  ')';
	}


	if(get_post_meta($post_id, 'koutsutoho1', true) !="")
		$buf .=  ' 徒歩'.get_post_meta($post_id, 'koutsutoho1', true).'m';

	if(get_post_meta($post_id, 'koutsutoho1f', true) !="")
		$buf .=  ' 徒歩'.get_post_meta($post_id, 'koutsutoho1f', true).'分';

	if( $echo_flg )
		echo $buf;
	else
		return $buf;
}
function mail_custom_koutsu2_print($post_id, $echo_flg = true) {
	$buf = '';
	global $wpdb;
	$rosen_name = '';

	$koutsurosen_data = get_post_meta($post_id, 'koutsurosen2', true);
	$koutsueki_data = get_post_meta($post_id, 'koutsueki2', true);

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = myLeft($shozaichiken_data,2);

	if($koutsurosen_data !=""){
		$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
	//	$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) $rosen_name = $metas->rosen_name;
		$buf .= "<br />".$rosen_name;
	}

	if($koutsurosen_data !="" && $koutsueki_data !=""){
		$sql = "SELECT DTS.station_name";
		$sql .=  " FROM ".$wpdb->prefix."train_rosen AS DTR";
		$sql .=  " INNER JOIN ".$wpdb->prefix."train_station AS DTS ON DTR.rosen_id = DTS.rosen_id";
		$sql .=  " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
	//	$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if(!empty($metas)) {
			if($metas->station_name != '＊＊＊＊') 	$buf .= $metas->station_name.'駅';
		}
	}

	$koutsubusstei=get_post_meta($post_id, 'koutsubusstei2', true);
	$koutsubussfun=get_post_meta($post_id, 'koutsubussfun2', true);
	$koutsutohob2f=get_post_meta($post_id, 'koutsutohob2f', true);

	if($koutsubusstei !="" || $koutsubussfun !=""){

		if($rosen_name == 'バス'){
			$buf .= '(' . $koutsubusstei;
			if(!empty($koutsubussfun)) $buf .= ' '.$koutsubussfun.'分';
		}else{
			$buf .= ' バス(' . $koutsubusstei;
			if(!empty($koutsubussfun)) $buf .= ' '.$koutsubussfun.'分';
		}

		if($koutsutohob2f !="" )
			$buf .= ' 停歩'.$koutsutohob2f.'分';
		$buf .= ')';
	}


	if(get_post_meta($post_id, 'koutsutoho2', true) !="")
		$buf .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho2', true).'m';

	if(get_post_meta($post_id, 'koutsutoho2f', true) !="")
		$buf .= ' 徒歩'.get_post_meta($post_id, 'koutsutoho2f', true).'分';

	$buf .= '';

	if( $echo_flg )
		echo $buf;
	else
		return $buf;
}

/**
 * 所在地
 *
 * @since Fudousan Plugin 1.0.0 *
 * @param int $post_id Post ID.
 */
function mail_custom_shozaichi_print($post_id, $echo_flg = true) {
	$buf = '';
	global $wpdb;

	$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichiken_data = myLeft($shozaichiken_data,2);

	if($shozaichiken_data=="")
		$shozaichiken_data = get_post_meta($post_id,'shozaichiken',true);

		$sql = "SELECT `middle_area_name` FROM `".$wpdb->prefix."area_middle_area` WHERE `middle_area_id`=".$shozaichiken_data."";
	//	$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if( !empty($metas) ) $buf .= $metas->middle_area_name;

	$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
	$shozaichicode_data = myLeft($shozaichicode_data,5);
	$shozaichicode_data = myRight($shozaichicode_data,3);

	if($shozaichiken_data !="" && $shozaichicode_data !=""){
		$sql = "SELECT narrow_area_name FROM ".$wpdb->prefix."area_narrow_area WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

	//	$sql = $wpdb->prepare($sql,'');
		$metas = $wpdb->get_row( $sql );
		if( !empty($metas) ) $buf .= $metas->narrow_area_name;
	}

	if( $echo_flg )
		echo $buf;
	else
		return $buf;
}

/**
 * 物件種別
 *
 * @since Fudousan Plugin 1.4.3 *
 * @param int $post_id Post ID.
 */
function mail_custom_bukkenshubetsu_print($post_id) {
	$bukkenshubetsu_txt= '';
	$bukkenshubetsu_data = get_post_meta($post_id,'bukkenshubetsu',true);

	global $work_bukkenshubetsu;
	foreach ($work_bukkenshubetsu as $meta_box ){

		if( $bukkenshubetsu_data == $meta_box['id'] ){
			$bukkenshubetsu_txt = $meta_box['name'];
			$bukkenshubetsu_txt = str_replace( "【売地】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売戸建】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売マン】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売建物全部】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売建物一部】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【賃貸居住】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【賃貸事業】" ,"" , $bukkenshubetsu_txt);
			return $bukkenshubetsu_txt;
			break;
		}
	}
}



?>