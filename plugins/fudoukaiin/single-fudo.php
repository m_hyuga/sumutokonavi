<?php
/*
Template Name: 物件詳細テンプレート
*/
require_once get_template_directory() . '/inc-single-fudo.php';
$fudo = new Fudo();
$post_id = $post->ID;
$shub = get_post_meta($post_id,'bukkenshubetsu',true)>3000?2:1;
$kakakuname = $shub == 2?"賃料":"価格";

$kakaku_data = get_post_meta($post_id,'kakaku',true);
if(is_numeric($kakaku_data)){
	$pc = "&pc=" . floatval($kakaku_data)/10000;
}else{
	$pc="";
}
?>
<?php get_header();?>
<!-- main  -->
<div id="main" class="bukken">


<div id="main-inner" class="cf">


<!-- article  -->
<article>






<!-- content  -->
<div id="content">


<section  id="post-<?php the_ID(); ?>" <?php post_class("bukken_dt_cnt");?>>


<h2><?php the_title();?></h2>


<div class="list_simple_box bukken_info cf">

<dl class="add">
<dt><span>所在地</span></dt>
<dd><?php $fudo->my_custom_shozaichi_print($post->ID);?>
<?php if ( get_post_meta( get_the_ID(), 'shozaichimeisho', true ) ) : ?>
<?php echo get_post_meta( get_the_ID(), 'shozaichimeisho', true ) ?>
<?php endif; ?>&nbsp;</dd>
</dl>

<dl class="madori">
<dt><span>間取り</span></dt>
<dd><?php $fudo->my_custom_madorisu_print($post->ID);?>&nbsp;</dd>
</dl>

<dl class="charge">
<dt><span><?php echo $kakakuname;?></span></dt>
<dd><?php $fudo->my_custom_kakaku_print($post->ID);?>&nbsp;</dd>
</dl>

</div>

<div id="slider" class="flexslider">
<ul class="slides">
<?php for($i=0;$i<10;$i++):?>
<?php if(get_post_meta($post->ID, "fudoimg$i", true)):?>
<?php $fudo->fudo_imageslide($post->ID,$i);?>
<?php endif;?>
<?php endfor;?>
</ul>
</div>
<div id="carousel" class="flexslider">
<ul class="slides">
<?php for($i=0;$i<10;$i++):?>
<?php if(get_post_meta($post->ID, "fudoimg$i", true)):?>
<?php $fudo->fudo_imageslide($post->ID,$i);?>
<?php endif;?>
<?php endfor;?>
</ul>
</div>


<?php /*
<ul class="sns_btn">
<li><img src="<?php echo get_template_directory_uri(); ?>/images/facebook_icon.jpg" width="30" height="30" alt="facebook"></li>
<li><img src="<?php echo get_template_directory_uri(); ?>/images/twitter_icon.jpg" width="30" height="30" alt="twitter"></li>
<li><img src="<?php echo get_template_directory_uri(); ?>/images/google_icon.jpg" width="30" height="30" alt="google plus"></li>
</ul>*/?>
<a href="<?php echo  home_url();?>/?page_id=1448&post_id=<?php echo $post->ID;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/btn-openpdf.png"></a>
<?php if(is_user_logged_in() && $shub==1):?>
<?php 
global $current_user;
get_currentuserinfo();
if ($current_user->user_level == 10):?>
<a href="<?php echo  home_url();?>/?page_id=1520<?php echo $pc;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/btn-Simulation.PNG"></a>
<?php endif;endif;?>
<div class="bnr">

<p>物件についてご質問などございましたらお気軽にお問い合わせ下さい</p>


<ul class="cf">
<li><img src="<?php echo get_template_directory_uri(); ?>/images/cnt_bnr_01.png" width="332" height="113" alt="お電話でお問い合わせ先株式会社 くびき開発025-543-0918受付時間： 9：00～18：00"></li>
<li class="fade"><a href="#mail_form"><img src="<?php echo get_template_directory_uri(); ?>/images/cnt_bnr_02.png" width="332" height="113" alt="メールでのお問い合わせ"></a></li>
</ul>

</div>


<section class="bukken_dt_cnt_02">
<h3>セールスコメント</h3>
<?php echo $post->post_content;?>
</section>

<div class="detail_table">
<h4 class="top">物件詳細情報</h4>
<dl>

<?php if($post->shozaichicode):?>
<dt>所在地</dt>
<dd class="wide"><?php $fudo->my_custom_shozaichi_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->bukkenmei):?>
<dt>物件名</dt>
<dd class="wide"><?php $fudo->fudometa($post->ID,"bukkenmei");?>　</dd>
<?php endif;?>

<?php if($post->bukkenshubetsu):?>
<dt>物件種別</dt>
<dd><?php $fudo->my_custom_bukkenshubetsu_print($post->ID);?></dd>
<?php endif;?>

<?php if($post->bukkennaiyo):?>
<dt>部屋番号</dt>
<dd><?php $fudo->fudometa($post->ID,"bukkennaiyo");?>　</dd>
<?php endif;?>

<?php if($post->tatemonochikunenn):?>
<dt>築年月</dt>
<dd><?php $fudo->my_custom_yearbuilt_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->nyukyogenkyo):?>
<dt>現況</dt>
<dd><?php $fudo->my_custom_nyukyogenkyo_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->madorisu):?>
<dt>間取り部屋数</dt>
<dd><?php $fudo->fudometa($post->ID,"madorisu","室");?>　</dd>
<?php endif;?>

<?php if($post->madorisyurui):?>
<dt>間取りタイプ</dt>
<dd><?php $fudo->my_custom_madorisyurui_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->nyukyojiki):?>
<dt>引渡し時期</dt>
<dd><?php $fudo->my_custom_nyukyojiki_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->torihikitaiyo):?>
<dt>取引形態</dt>
<dd><?php $fudo->my_custom_torihikitaiyo_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->tatemonomenseki):?>
<dt>専有面積</dt>
<dd><span class="a"><?php $fudo->fudometa($post->ID,"tatemonomenseki","㎡");?></span>（<span class="b"></span>坪）　</dd>
<?php endif;?>

<?php if($post->tatemonokozo):?>
<dt>建物構造</dt>
<dd><?php $fudo->my_custom_tatemonokozo_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->chushajokubun):?>
<dt>駐車場有無</dt>
<dd><?php $fudo->my_custom_chushajo_print_archive($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->tatemonokaisu1):?>
<dt>建物階建（地上）</dt>
<dd><?php $fudo->fudometa($post->ID,"tatemonokaisu1","階");?>　</dd>
<?php endif;?>

<?php if($post->chushajodai):?>
<dt>駐車台数</dt>
<dd ><?php $fudo->fudometa($post->ID,"chushajodai","台");?>　</dd>
<?php endif;?>

<?php if($post->setsubisonota):?>
<dt class="heightLine">その他</dt>
<dd class="heightLine wide"><?php $fudo->fudometa($post->ID,"setsubisonota");?>　</dd>
<?php endif;?>
<?php if($post->shuuhenshougaku):?>
<dt>小学校</dt>
<dd><?php $fudo->fudometa($post->ID,"shuuhenshougaku");?>　</dd>
<?php endif;?>

<?php if($post->shuuhenchuugaku):?>
<dt>中学校</dt>
<dd><?php $fudo->fudometa($post->ID,"shuuhenchuugaku");?>　</dd>
<?php endif;?>
</dl>

	
</div>

<div class="detail_table">
<h4 class="top">お支払条件</h4>
<dl>
<?php if($post->kakaku):?>
	<dt class="tb_01"><?php echo $kakakuname;?></dt>
	<dd class="tb_01"><?php $fudo->my_custom_kakaku_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->kakakuhoshoukin):?>
	<dt class="tb_01">保証金</dt>
    <dd class="tb_01"><?php $fudo->my_custom_kakakuhoshoukin_print($post->ID);?>　</dd>
 <?php endif;?>

<?php if($post->kakakureikin):?>
	<dt class="tb_01">礼金</dt>
	<dd class="tb_01"><?php $fudo->my_custom_kakakureikin_print($post->ID);?>　</dd>
<?php endif;?>

<?php if($post->kakakushikikin):?>
    <dt class="tb_01">敷金</dt>
<dd class="tb_01"><?php $fudo->my_custom_kakakushikikin_print($post->ID);?>　</dd>
 <?php endif;?>

<?php if($post->tyukaitesuuryo):?>
	<dt class="tb_01">仲介手数料</dt>
    <dd class="tb_01"><?php $fudo->my_custom_tyukaitesuuryo_print($post->ID);?>　</dd>
 <?php endif;?>

  <?php if($post->kakakuhoken):?>
<dt>住宅保険料</dt>
<dd><?php $fudo->my_custom_kakakuhoken_print($post->ID);?>　</dd>
 <?php endif;?>

<?php if($post->nyukyosum):?>
    <dt class="tb_01">入居時合計金額</dt>
    <dd class="tb_01"><?php $fudo->my_custom_nyukyosum_print($post->ID);?>　</dd>
     <?php endif;?>

 <?php if($post->chushajoryokin):?>
<dt class="tb_01">駐車場料金</dt>
<dd class="tb_01"><?php $fudo->my_custom_chushajo_ryokin_print($post->ID);?>　</dd>
 <?php endif;?>
</dl>
</div>

<?php
/*
<tr>
<th scope="row">自治会費</th>
<td>700円/月（別途徴収）</td>
<th colspan="2" scope="row" class="white">&nbsp;</th>
</tr>*/?>



<div class="detail_table">
<h4 class="top">交通</h4>
<dl class="tb_01">
 <?php if($post->koutsurosen1):?>
	<dt class="tb_01">路線名1</dt>
	<dd class="tb_01"><?php $fudo->my_custom_rosen1_print($post->ID);?>　</dd>
 <?php endif;?>

 <?php if($post->koutsueki1):?>
	<dt class="tb_01">駅名1</dt>
	<dd class="tb_01"><?php $fudo->my_custom_eki1_print($post->ID);?>　</dd>
 <?php endif;?>
</dl>
</div>

 <?php if($post->setsubi):?>
<table class="tb_04">
<caption>
設備
</caption>
<tr>
<th scope="row">設備</th>
<td><?php $fudo->my_custom_setsubi_print($post->ID);?>
</td>
</tr>
</table>
<?php endif;?>
<section  class="bukken_dt_cnt_01">
						<!-- GoogleMaps v3 -->

						<?php 
						
							//map
							$lat = get_post_meta($post_id,'bukkenido',true);
							$lng = get_post_meta($post_id,'bukkenkeido',true);
							
							$bukkenshubetsu_data = get_post_meta($post_id,'bukkenshubetsu',true);
							$icon_url = 'gmapmark_'.$bukkenshubetsu_data.'.png';
							if($bukkenshubetsu_data == '')
								$icon_url = 'gmapmark_1399.png';


							if ($lat != "" && $lng != ""){
						?>
								<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
								<script type="text/javascript"> 
									var map;
									var gmapmark = '<?php echo WP_PLUGIN_URL;?>/fudou/img/<?php echo $icon_url;?>';

									function initialize() {

										var myLatLng = new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lng;?>);
										var myOptions = {
											zoom: 16,
											center: myLatLng,
											scrollwheel: false,
											mapTypeId: google.maps.MapTypeId.ROADMAP,
											streetViewControl: true
										};
										map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

										//ElevationService
										elev = new google.maps.ElevationService();
										var latlng = new Array();
										latlng[0] = new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lng;?>);
										var req = {locations: latlng};
										elev.getElevationForLocations(req, elevResultCallback);

									<?php if( get_option('fudo_map_directions') == 'true' ){ ?>
										//DirectionsService
										var From = "<?php echo my_custom_shozaichi_eki_map($post_id); ?>";
										var To = new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lng;?>); 
										if(From != '' ){
											new google.maps.DirectionsService().route({
												origin: From, 
												destination: To,
												travelMode: google.maps.DirectionsTravelMode.WALKING 
											}, function(result, status) {
												if (status == google.maps.DirectionsStatus.OK) {
												//	new google.maps.DirectionsRenderer({map: map}).setDirections(result);
													new google.maps.DirectionsRenderer({map: map,suppressMarkers: true }).setDirections(result);
												}
											});
										}
									<?php } ?>


									}  //End function initialize()



									function elevResultCallback(result, status) {
										if (status != google.maps.ElevationStatus.OK) {
											var image = new google.maps.MarkerImage( gmapmark , new google.maps.Size(44,41));
											var marker = new google.maps.Marker({
												position:result[0].location,
												map:map,
												icon: image
											});
											var content = '<div class="msg"><?php if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){echo "　会員物件";}else{echo $title;} ?>' + 
													'<br /><?php if ( get_post_meta($post_id,'bukkenshubetsu',true) <3000 ) { echo '価格';}else{echo '賃料';} ?> ' + 
													'<?php  if( get_post_meta($post_id, 'seiyakubi', true) != "" ){ echo 'ご成約済'; }else{  my_custom_kakaku_print($post_id); } ?>' + 
													'　<?php my_custom_bukkenshubetsu_print($post_id); ?></div>';

										}else{
											var image = new google.maps.MarkerImage( gmapmark , new google.maps.Size(44,41));
											var marker = new google.maps.Marker({
												position:result[0].location,
												title: '標高 ' + result[0].elevation.toFixed(0) + 'm' ,
												map:map,
												icon: image
											});

											var content = '<div class="msg"><?php if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){echo "　会員物件";}else{echo $title;} ?>' + 
													'<br /><?php if ( get_post_meta($post_id,'bukkenshubetsu',true) <3000 ) { echo '価格';}else{echo '賃料';} ?> ' + 
													'<?php  if( get_post_meta($post_id, 'seiyakubi', true) != "" ){ echo 'ご成約済'; }else{  my_custom_kakaku_print($post_id); } ?>' + 
													'　<?php my_custom_bukkenshubetsu_print($post_id); ?>' + 
												<?php if( get_option('fudo_map_elevation') == 'true' ){ ?>
													'<br />標高 ' + result[0].elevation.toFixed(0) + 'm' +
												<?php } ?>
													'</div>';
										}

											var infowindow = new google.maps.InfoWindow({
												content: content ,
												maxWidth: 300
											//	,size: new google.maps.Size(50, 50)
											});
											google.maps.event.addListener(marker, 'click', function() {
												infowindow.open(map, marker);
											});
									}


								</script>

								<script type="text/javascript">
									addOnload_single(function() { initialize(); });
									function addOnload_single(func){
										try {
											window.addEventListener("load", func, false);
										} catch (e) {   
											window.attachEvent("onload", func);   	// IE用
										}
									}
								</script>

								<style type="text/css">
								<!--
								#content #map_canvas img {margin: 0;height: auto;max-width: none;width: auto;}
								#content #map_canvas .msg {margin: 5px;font-size:12px;}
								-->
								</style>
								
								<div class="map_canvas" id="map_canvas" style="border:1px solid #979797; background-color:#e5e3df; width:99%; height:340px; z-index:1">
									<div style="padding:1em; color:gray;">Loading...</div>
								</div>
																<?php echo get_option('fudo_map_comment'); ?>

							<?php } ?>

						<!-- end GoogleMaps v3 -->
</section>
	
	
	
	
<section class="bukken_dt_cnt_02">

<h3>新着物件情報</h3>


<div class="box-outer cf">
<?php
		$args["post_type"] = "fudo";
		$args["posts_per_page"]=4;
		$the_query = new WP_Query($args);
		if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) : $the_query->the_post();
?>
<section class="box">

<a href="<?php the_permalink();?>">

<h4><?php the_title();?></h4>
<?php $fudo->fudo_image($post_id);?>
<ul>
<li><?php $fudo->my_custom_bukkenshubetsu_print($post->ID);?></li>
<li><?php $fudo->my_custom_shozaichi_print($post->ID);?></li>
<li><?php $fudo->my_custom_kakaku_print($post->ID);?></li>
</ul>

<p><?php echo mb_substr(strip_tags($post->post_content,'<br><br/>'),0,80);?></p>

</a>

</section>

<?php
endwhile;
endif;
// 投稿データをリセット
wp_reset_postdata(); 
?>



</div>



</section>
    
    
    
    
    
    
<div class="bnr">

<p>物件についてご質問などございましたらお気軽にお問い合わせ下さい</p>


<ul class="cf">
<li><img src="<?php echo get_template_directory_uri(); ?>/images/bukken_bnr_01.png" width="290" height="113" alt="お電話でお問い合わせ先株式会社 くびき開発025-543-0918受付時間： 9：00～18：00"></li>
<li class="fade"><a href="#mail_form"><img src="<?php echo get_template_directory_uri(); ?>/images/bukken_bnr_02.png" width="291" height="113" alt="メールでのお問い合わせ"></a></li>
</ul>

</div>   
	<section class="bukken_dt_cnt_03" id="mail_form">
	   <?php 
	    //物件問合せ先
	    echo get_option('fudo_annnai');
	    echo "<br />";
	/*
	    if( $kaiin == 1 ) {
	     }else{
	
	      if ( $kaiin2 ){
	*/
	      //SSL
	      $fudou_ssl_site_url = get_option('fudou_ssl_site_url');
	       if( $fudou_ssl_site_url !=''){
	
	        //SSl問合せフォーム
	        echo '<div align="center">';
	        echo '<a href="'.$fudou_ssl_site_url.'/wp-content/plugins/fudou/themes/contact.php?p='.$post_id.'&action=register&KeepThis=true&TB_iframe=true&height=500&width=620" class="thickbox">';
	        echo '<img src="'.get_option('siteurl').'/wp-content/plugins/fudou/img/ask_botton.jpg" alt="物件お問合せ" title="物件お問合せ" /></a>';
	        echo '</div>';
	       }else{
	
	        //問合せフォーム
	        $content = get_option('fudo_form');
	        $content = apply_filters('the_content', $content);
	        $content = str_replace(']]>', ']]&gt;', $content);
	        echo $content;
	
	       }
		   /*
	      }
	    }*/
	    ?>
	</section>
</section>
      
</div><!-- end of content  -->

</article><!-- end of article  -->


<?php get_sidebar(); ?>

</div><!-- end of main-inner  -->


</div><!-- end of main  -->



<?php get_footer(); ?>