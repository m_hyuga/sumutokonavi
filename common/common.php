<?php
define( 'COMMONPATH', __DIR__ );

function post_r(){
	if(is_user_logged_in()){
		global $post;
		print_r($post);
	}
}

function common_theme($template){
	$theme = __DIR__ . "/theme";
	$common_template = str_replace(STYLESHEETPATH, $theme, $template);
	$common_template = str_replace(TEMPLATEPATH, $theme, $common_template);
	if(file_exists($common_template)){
		return $common_template;
	}else{
		return $template;
	}

}

add_filter('template_include', 'common_theme',100,1);

/**
get_template_directoryのパスをcommonに変更
**/
function get_common_directory($template_dir, $template, $theme_root){
	$theme = __DIR__ . "/theme";
	return $theme;
}
//add_filter('template_directory', 'get_common_directory',10,3);

function get_common_directory_uri($template_dir_uri, $template, $theme_root_uri){
	return (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . "/common/theme/";
}
//add_filter('template_directory_uri', 'get_common_directory_uri',10,3);

//敷金、礼金なしの検索条件を実装
add_filter("fudou_org_meta_dat_archive","fudou_sikirei_sql",10,2);
function fudou_sikirei_sql($meta_dat, $next_sql){
global $wpdb;
if(isset($_GET["shikikin"]) && $next_sql){
			$sql = "SELECT DISTINCT( P.ID )";
			$sql .=  " FROM $wpdb->posts AS P";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM ON P.ID = PM.post_id";
			$sql .=  " WHERE  P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo'";
			$sql .=  " AND PM.meta_key='kakakushikikin' ";
			$sql .=  " AND PM.meta_value ='0' ";
			if(!empty($meta_dat))$sql .=  " AND P.ID IN ({$meta_dat})";

			$metas = $wpdb->get_results( $sql, ARRAY_A );
			if(!empty($metas)) {
				$i=0;
				foreach ( $metas as $meta ) {
					if($i!=0) $meta_dat .= ",";
					$meta_dat .= $meta['ID'];
					$i++;
				}
			}else{
				global $next_sql;
				$next_sql = false;
			}
//			echo "meta_dat:".$sql;
}

return $meta_dat;	
}
?>