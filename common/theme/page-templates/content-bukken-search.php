<?php
//物件を探す

//query_posts('post_type=whatsnew&posts_per_page=3&post_status=publish');
?>
<section class="bukken-search">
<h3>物件を探す</h3>

<ul class="tab">
  <li class="select">路線･駅から探す</li>
  <li>地図から探す</li>
  <li>地名から探す</li>
</ul>
<ul class="content_area">
  <li class="content">
		<?php
  query_posts('post_type=page&name=tochi_ensen_search&post_status=publish');
		if( have_posts()): while ( have_posts() ) : the_post();
		the_content();
		endwhile; endif;
		?>
  </li>
  <li class="content hide"><iframe src="<?php echo home_url(); ?>/fmap.php?jyo2=0" height="400" width="670" frameborder="0"></iframe></li>
  <li class="content hide">
		<?php
  query_posts('post_type=page&name=tochi_addr_search&post_status=publish');
		if( have_posts()): while ( have_posts() ) : the_post();
		the_content();
		endwhile; endif;
		?>
  </li>
</ul>

</section>

<script>
jQuery(function(){
jQuery('.tab li').click(function() {
var index = jQuery('.tab li').index(this);
jQuery('.content_area li.content').css('display','none');
jQuery('.content_area li.content').eq(index).css('display','block');
jQuery('.tab li').removeClass('select');
jQuery(this).addClass('select')
});
});
</script>

