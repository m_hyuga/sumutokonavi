<?php
/**
 * The Template for displaying fudou single posts.
 *
 * Template Name: single-fudo.php
 * 
 * @package WordPress3.7
 * @subpackage Fudousan Plugin
 * Version: 1.4.0
 */

//include_once(WP_PLUGIN_DIR.'/fudou/inc/inc-single-fudo.php');

include_once(COMMONPATH . '/inc-single-fudo.php');

	global $is_fudouktai,$is_fudoumap,$is_fudoukaiin;
	//global $post_id;

	$post_id = myIsNum_f($_GET['p']);

// 1.5.3
	$post_id = isset( $_GET['p'] ) ? myIsNum_f( $_GET['p'] ) : '';
	if( empty($post_id) ){
		$post_id = $post->ID;
	}

	/** for Contact Form7 hack
	global $wpcf7;
	$wpcf7->processing_within = 'p' . $post_id;
	$wpcf7->unit_count = 0;
	**/


	//会員
	$kaiin = 0;
	if( !is_user_logged_in() && get_post_meta($_GET['p'], 'kaiin', true) == 1 ) $kaiin = 1;
	//ユーザー別会員物件リスト
	$kaiin_users_rains_register = get_option('kaiin_users_rains_register');
	$kaiin2 = users_kaiin_bukkenlist($post_id,$kaiin_users_rains_register,get_post_meta($post_id, 'kaiin', true));

	//title変更
	if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){
		add_action('wp_title', 'add_post_type_wp_title_ka');
	}
	function add_post_type_wp_title_ka($title = '') {
		$title =  '会員物件　';
		return $title;
	}

	$post_id_array = get_post( $post_id ); 
	$title = $post_id_array->post_title;
	$excerpt = $post_id_array->post_excerpt;
	$content = $post_id_array->post_content;
	$modified = $post_id_array->post_modified;

	//newup_mark
	$newup_mark = get_option('newup_mark');
	if($newup_mark == '') $newup_mark=14;

	$post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($modified, "%d-%d-%d"));
	$post_date =  vsprintf("%d-%02d-%02d", sscanf($post_id_array->post_date, "%d-%d-%d"));

	$newup_mark_img =  '';
	if( $newup_mark != 0 && is_numeric($newup_mark) ){

		if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){
			if($post_modified_date == $post_date ){
				$newup_mark_img = '<div class="new_mark">new</div>';
			}else{
				$newup_mark_img =  '<div class="new_mark">up</div>';
			}
		}
	}

	//SSL
	$fudou_ssl_site_url = get_option('fudou_ssl_site_url');
	if( $fudou_ssl_site_url !=''){
		$site_url = $fudou_ssl_site_url;
	}else{
		$site_url = get_option('siteurl');
	}


	status_header( 200 );
	get_header(); 
	the_post();


?>
<div id="container" class="site-content">

	<div id="content" role="main">

<?php
//パスワード保護
if ( post_password_required() ){
	?>
		<div id="list_simplepage2">
			<?php if(is_user_logged_in()){ ?>
			<div class="mylist"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=11138"><img src="<?php echo bloginfo('template_directory');?>/images/btn_mylistichi.png" class="" alt="" /></a></div>
			<?php }; ?>
			<!-- favbox -->
			<!-- #nav-above -->
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="entry-title">

					<?php if( get_post_meta($post_id, 'kaiin', true) == 1 ) { ?>
						<span style="float:right;margin:3px"><img src="<?php echo get_option('siteurl'); ?>/wp-content/plugins/fudou/img/kaiin_s.jpg" alt="" /></span>
					<?php } ?>
					<?php 
					//会員項目表示判定
					if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){
						echo "　会員物件";
					}else{
						echo $title;
					} 
					
					echo  $newup_mark_img;
					?>
				</h2>

				<div class="list_simple_box<?php if(!is_user_logged_in()){echo ' no_mylist';}; ?>">
					<?php the_content();?>
				</div>
			</div>
		</div>

	<?php

}else{
?>

		<div id="list_simplepage2">
			<?php if(is_user_logged_in()){ ?>
			<div class="mylist"><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=11138"><img src="<?php echo bloginfo('template_directory');?>/images/btn_mylistichi.png" class="" alt="" /></a></div>
			<?php }; ?>
			<!-- favbox -->
			<!-- #nav-above -->
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="title_area">
				<h2 class="entry-title">

					<?php if( get_post_meta($post_id, 'kaiin', true) == 1 ) { ?>
						<!--<span style="float:right;margin:3px"><img src="<?php echo get_option('siteurl'); ?>/wp-content/plugins/fudou/img/kaiin_s.jpg" alt="" /></span>-->
					<?php } ?>
					<?php 
					//会員項目表示判定
					if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){
						//echo "　会員物件";
					}else{
						?>
						<span class="shubetsu_icon"><?php my_custom_bukkenshubetsu_print($post_id); ?></span>
						<span class="title_text">
						<?php 
						echo $title;
					} 
					
					echo  $newup_mark_img;
					?>
					</span>
				</h2>
				<ul class="cf">
					<li class="price"><span><?php if ( get_post_meta($post_id,'bukkenshubetsu',true) <3000 ) { echo '価格';}else{echo '賃料';} ?></span><b><?php  if( get_post_meta($post_id, 'seiyakubi', true) != "" ){ echo 'ご成約済'; }else{  my_custom_kakaku_print($post_id); } ?></b></li>
					<li class="address"><span>所在地</span><?php my_custom_shozaichi_print($post_id); ?><?php echo get_post_meta($post_id, 'shozaichimeisho', true); ?>
										<?php if ( get_post_meta($post_id,'bukkenmeikoukai',true) != '0' ) echo '<br />'. get_post_meta($post_id,'bukkenmei',true);?></li>
					<li class="traffic"><span>交通</span><?php my_custom_koutsu1_print($post_id); ?>
										<?php my_custom_koutsu2_print($post_id); ?>
										<?php if( get_post_meta($post_id, 'koutsusonota', true) !="") 	echo '<br />'.get_post_meta($post_id, 'koutsusonota', true);?></li>
				</ul>
				<p class="btn_contact"><a href="#toiawasesaki" class="phone over spnone"><a href="tel:0000000000" class="phone over pcnone"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_phone_sp.png" class="pcnone"></a><a href="#role_form" class="contact over"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_contact_sp.png" class="pcnone"></a></p>
				</div>
				
				<?php do_action( 'single-fudo1' ); ?>

				<!-- .entry-content -->

				<div class="list_simple_box<?php if(!is_user_logged_in()){echo ' no_mylist';}; ?>">
					<div class="entry-excerpt">
					<?php
						// if ( my_custom_kaiin_view('kaiin_excerpt',$kaiin,$kaiin2) ){
						// 		echo $excerpt; 
						// }

					?>
					</div>

					<?php if( $kaiin == 1 ) { ?>

						<?php if( $is_fudoukaiin && get_option('kaiin_users_can_register') == 1 ){ ?>

							<br />
							この物件は、「会員様にのみ限定公開」している物件です。<br />
							非公開物件につき、詳細情報の閲覧には会員ログインが必要です。<br />
							非公開物件を閲覧・資料請求するには会員登録が必要です。<br />

							<?php if( get_option('kaiin_moushikomi') != 1 ){ ?>
								まだ会員登録をしていない方は、簡単に会員登録ができますので是非ご登録ください。<br />
								<br />
								<div align="center">
								<a href="<?php echo $site_url; ?>/wp-content/plugins/fudoukaiin/wp-login.php?action=register&KeepThis=true&TB_iframe=true&height=500&width=400" class="thickbox">
								<img src="<?php echo get_option('siteurl'); ?>/wp-content/plugins/fudou/img/kaiin_botton.jpg" alt="会員登録" /></a>
								</div>
							<?php } ?>
							<br />

						<?php }else{ ?>
							<br />
							この物件は、閲覧できません。<br />
							<br />
						<?php }

					}else{

						//ユーザー別会員物件リスト
						if ($kaiin2 === false ) {

							echo '<br />';
							echo 'この物件は、「閲覧条件に合った物件のみ公開」している物件です。<br />';
							echo '条件変更をする事で閲覧ができますので、閲覧条件の登録・変更をしてください。<br />';
							echo '<br />';
							echo '<div align="center">';
							echo '<div id="maching_mail"><a href="'.WP_PLUGIN_URL.'/fudoumail/fudou_user.php?KeepThis=true&TB_iframe=true&height=500&width=680" class="thickbox">';
							echo '閲覧条件・メール設定</a></div>';
							echo '</div>';
							echo '<br />';

						}else{

				?>
						        <!-- ここから左ブロック --> 
						        <div class="list_picsam_x">
										<h3>物件写真</h3>
							<?php

								//画像
								if (!defined('FUDOU_IMG_MAX')){
									$fudou_img_max = 30;
								}else{
									$fudou_img_max = FUDOU_IMG_MAX;
								}

	//スライド画像
								$img_path = get_option('upload_path');
								if ($img_path == '')	$img_path = 'wp-content/uploads';
								echo '<div id="slider" class="flexslider"><ul class="slides">';
								for( $imgid=1; $imgid<=30; $imgid++ ){

									$fudoimg_data = get_post_meta($post_id, "fudoimg$imgid", true);
									$fudoimgcomment_data = get_post_meta($post_id, "fudoimgcomment$imgid", true);
									$fudoimg_alt = $fudoimgcomment_data . my_custom_fudoimgtype_print(get_post_meta($post_id, "fudoimgtype$imgid", true));

									if($fudoimg_data !="" ){

											$sql  = "";
											$sql .=  "SELECT P.ID,P.guid";
											$sql .=  " FROM $wpdb->posts as P";
											$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
										//	$sql = $wpdb->prepare($sql,'');
											$metas = $wpdb->get_row( $sql );
											$attachmentid = '';
											if( !empty($metas) ){
												$attachmentid  =  $metas->ID;
												$guid_url  =  $metas->guid;
											}

											if($attachmentid !=''){
												//thumbnail、medium、large、full 
												$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'large');
												$fudoimg_url = $fudoimg_data1[0];
												echo '<li>';
												//echo '<a href="' . $guid_url . '" rel="lightbox lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';
												echo '<a href="' . $guid_url . '" rel="" title="'.$fudoimg_alt.'">';//lightboxが起動しないように改変
												if($fudoimg_url !=''){
													echo '<img src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" /></a>';
												}else{
													echo '<img src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'"  />';
												}
											}else{
												echo '<li>';
												echo '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'. $fudoimg_data .'" />';
											}
											if($fudoimgcomment_data)echo '<p class="flex-caption"><span>'.$fudoimgcomment_data.'</span></p>';
											echo '</li>';

									}else{
										if( $imgid==1 )
										echo '<li><img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" /></li>';
									}
									echo "\n";
								}
								echo '</ul></div><!-- /slider -->';

						if(!is_smartphone()){

								//サムネイル画像
								$img_path = get_option('upload_path');
								if ($img_path == '')	$img_path = 'wp-content/uploads';
								echo '<div id="carousel" class="flexslider"><ul class="slides">';
								for( $imgid=1; $imgid<=10; $imgid++ ){

									$fudoimg_data = get_post_meta($post_id, "fudoimg$imgid", true);
									$fudoimgcomment_data = get_post_meta($post_id, "fudoimgcomment$imgid", true);
									$fudoimg_alt = $fudoimgcomment_data . my_custom_fudoimgtype_print(get_post_meta($post_id, "fudoimgtype$imgid", true));

									if($fudoimg_data !="" ){

											$sql  = "";
											$sql .=  "SELECT P.ID,P.guid";
											$sql .=  " FROM $wpdb->posts as P";
											$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
										//	$sql = $wpdb->prepare($sql,'');
											$metas = $wpdb->get_row( $sql );
											$attachmentid = '';
											if( !empty($metas) ){
												$attachmentid  =  $metas->ID;
												$guid_url  =  $metas->guid;
											}

											if($attachmentid !=''){
												//thumbnail、medium、large、full 
												$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');
												$fudoimg_url = $fudoimg_data1[0];
												echo '<li>';
												//echo '<a href="' . $guid_url . '" rel="lightbox lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';
												//thickbox無効
												echo '<a href="' . $guid_url . '" rel="" title="'.$fudoimg_alt.'">';
												if($fudoimg_url !=''){
													echo '<img src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" /></a>';
												}else{
													echo '<img src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'"  />';
												}
											}else{
												echo '<li><img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'. $fudoimg_data .'" /></li>';
											}
										echo '</li>';
									}else{
										if( $imgid==1 )
										echo '<li><img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" /></li>';
									}
									echo "\n";
								}
								echo '</ul></div><!-- /carousel -->';

						}//end sm
						?>
						<p class="btn_area_sub">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank" class="panorama hover"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_panorama.png"></a>
						<a href="<?php echo  home_url();?>/?page_id=11292&post_id=<?php echo $post->ID;?>" class="print hover"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_print.png"></a>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=11159?pc=<?php if(get_post_meta($post_id,'kakakukoukai',true) == "0"){}else{
									$kakaku_data = get_post_meta($post_id,'kakaku',true);
									if(is_numeric($kakaku_data)){
										echo floatval($kakaku_data)/10000;
									}
								} ?>" target="_blank" class="loan hover"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_loan.png"></a>
						</p>
						<!---QRコード-->
						<dl class="qr_area spnone cf">
						<dt>
						<?php
								//携帯QR
									$yoursubject = '%e7%89%a9%e4%bb%b6%e3%82%b5%e3%82%a4%e3%83%88%e3%81%aeURL'; //物件サイトのURL
									echo "\n";
									echo '<a href="mailto:?subject='.$yoursubject.'&body='. urlencode( get_permalink($post_id) ) .'">';
									$options = '';
									$culum3 = false;
									if (function_exists('unpc_get_theme_options')) 
										$options = unpc_get_theme_options();

									if ( is_array( $options ) ){
										$current_layout = $options['theme_layout'];
										if ( in_array( $current_layout, array( 'sidebar-content-sidebar' ) ) )
											$culum3 = true;
									}

									if ( $culum3 ){
										echo '<img src="http://chart.apis.google.com/chart?chs=100x100&amp;cht=qr&amp;chl=' . urlencode( get_permalink($post_id) ) . '" alt="クリックでURLをメール送信" title="クリックでURLをメール送信" /></a>';
									}else{
										echo '<img src="http://chart.apis.google.com/chart?chs=130x130&amp;cht=qr&amp;chl=' . urlencode( get_permalink($post_id) ) . '" alt="クリックでURLをメール送信" title="クリックでURLをメール送信" /></a>';
									}
							?>
							</dt>
							<dd>QRコードをクリックしてこの物件のURLをメールで送信する</dd>
							</dl>
							</div>


							
							<?php
							$bukken_sub = get_post_meta($post->ID,'bukken_sub',true);
							$bukken_detail = get_post_meta($post->ID,'bukken_detail',true);
							if($bukken_sub or $bukken_detail){
								echo '<div class="konobukken">';
								echo '<h4>この物件について</h4>';
								echo '<dl>';
								echo '<dt>'.$bukken_sub.'</dt>';
								echo '<dd>'.$bukken_detail.'</dd>';
								echo '</dl>';
								echo '</div>';
							}
							?>
							

							<!-- ここから右ブロック -->
							<!--
<a href="<?php echo pdf_url($post_id);?>">PDFを表示</a>
								<?php do_action( 'single-fudo2' ); ?>
											<?php if(is_smartphone()){ ?>
													<div class="dpoint6"><?php 	if ( my_custom_kaiin_view('kaiin_excerpt',$kaiin,$kaiin2) ){
								echo $excerpt; } ?></div>
											<?php } ?>-->
								<div class="list_detail">
								<h3>物件詳細情報</h3>
								<table width="100%" id="list_other">

								<!-- 土地以外 -->
								<?php if ( get_post_meta($post_id,'bukkenshubetsu',true) >1200 && get_post_meta($post_id,'bukkenshubetsu',true) != 3212 ) { ?>
									<tr>
										<th>所在地</th>
										<td><?php my_custom_shozaichi_print($post_id); ?><?php echo get_post_meta($post_id, 'shozaichimeisho', true); ?>
										<?php if ( get_post_meta($post_id,'bukkenmeikoukai',true) != '0' ) echo '<br />'. get_post_meta($post_id,'bukkenmei',true);?></td>
									</tr>
									<tr>
										<th>交通</th>
										<td><?php my_custom_koutsu1_print($post_id); ?>
										<?php my_custom_koutsu2_print($post_id); ?>
										<?php if( get_post_meta($post_id, 'koutsusonota', true) !="") 	echo '<br />'.get_post_meta($post_id, 'koutsusonota', true);?></td>
									</tr>
									<?php if( get_post_meta($post_id, 'tatemonochikunenn', true) !=""){ ?>
									<tr>
										<th>築年月</th>
										<td><?php echo get_post_meta($post_id, 'tatemonochikunenn', true);?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'tatemonoshinchiku', true) !=""){ ?>
									<tr>
										<th class="th2">新築/中古</th>
										<td><?php my_custom_tatemonoshinchiku_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'tatemonomenseki', true) !=""){ ?>
									<tr>
										<th>面積</th>
										<td><?php echo get_post_meta($post_id, 'tatemonomenseki', true);?>m&sup2;</td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'tatemonohosiki', true) !=""){ ?>
									<tr>
										<th class="th2">計測方式</th>
										<td><?php my_custom_tatemonohosiki_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'heyabarukoni', true) !=""){ ?>
									<tr>
										<th>バルコニー</th>
										<td><?php echo get_post_meta($post_id, 'heyabarukoni', true);?>m&sup2;</td>  
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'heyamuki', true) !=""){ ?>
									<tr>
										<th class="th2">向き</th>
										<td><?php my_custom_heyamuki_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<tr>
										<th>建物階数</th>
										<td><?php if(get_post_meta($post_id, 'tatemonokaisu1', true)!="") echo '地上'.get_post_meta($post_id, 'tatemonokaisu1', true).'階　' ;?>
										    <?php if(get_post_meta($post_id, 'tatemonokaisu2', true)!="") echo '地下'.get_post_meta($post_id, 'tatemonokaisu2', true).'階' ;?></td>
									</tr>
									<?php if( get_post_meta($post_id, 'heyakaisu', true) !=""){ ?>
									<tr>
										<th class="th2">部屋階数</th>
										<td><?php echo get_post_meta($post_id, 'heyakaisu', true);?>階</td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'bukkennaiyo', true) !=""){ ?>
									<tr>
										<th>部屋/区画番号</th>
										<td><?php echo get_post_meta($post_id, 'bukkennaiyo', true);?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'bukkensoukosu', true) !=""){ ?>
									<tr>
										<th class="th2">総戸/区画数</th>
										<td><?php echo get_post_meta($post_id, 'bukkensoukosu', true);?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'tatemonokozo', true) !=""){ ?>
									<tr>
										<th>建物構造</th>
										<td><?php my_custom_tatemonokozo_print($post_id) ?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id,'tatemonozentaimenseki',true)!='' || get_post_meta($post_id,'tatemononobeyukamenseki',true)!='' ){ ?>
									<tr>
										<th>敷地全体面積</th>
										<td><?php echo get_post_meta($post_id, 'tatemonozentaimenseki', true);?>m&sup2;</td>
									</tr>
									<tr>
										<th class="th2">延べ床面積</th>
										<td><?php echo get_post_meta($post_id, 'tatemononobeyukamenseki', true);?>m&sup2;</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tatemonokentikumenseki',true)!=''){ ?>
									<tr>
										<th>建築面積</th>
										<td><?php echo get_post_meta($post_id, 'tatemonokentikumenseki', true);?>m&sup2;</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kanrininn', true)!='' || get_post_meta($post_id, 'kanrikeitai', true)!='' || get_post_meta($post_id, 'kanrikumiai', true)!='' ){ ?>
									<tr>
										<th>管理形態</th>
										<td>
										<?php my_custom_kanrikeitai_print($post_id); ?>
										<?php my_custom_kanrininn_print($post_id);?>
										<?php my_custom_kanrikumiai_print($post_id); ?>
										</td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id,'madoribiko',true)!=''){ ?>
									<tr>
										<th>間取内容</th>
										<td><?php my_custom_madorinaiyo_print($post_id); ?><br />
										<?php echo get_post_meta($post_id, 'madoribiko', true);?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'kakakuhoken', true)!='' || get_post_meta($post_id, 'kakakuhokenkikan', true)!='' || get_post_meta($post_id, 'kakakutsumitate', true)!='' ){ ?>
									<tr>
										<th>住宅保険料</th>
										<td><?php my_custom_kakakuhoken_print($post_id);?><?php echo get_post_meta($post_id, 'kakakuhokenkikan', true);?>年</td>
									</tr>
									<tr>
										<th class="th2">修繕積立金</th>
										<td><?php echo get_post_meta($post_id, 'kakakutsumitate', true); ?>円</td>
									</tr>
									<?php } ?>
									  
								<?php };
								if(  get_post_meta($post_id, 'chushajoryokin', true)){ ?>
									<tr>
										<th>駐車場</th>
										<td><?php my_custom_chushajo_print($post_id); ?></td>
									</tr>
								<?php };
								if(  get_post_meta($post_id, 'torihikitaiyo', true)){ ?>
									<tr>
										<th class="th2">取引態様</th>
										<td><?php my_custom_torihikitaiyo_print($post_id); ?></td>
									</tr>
								<?php };
								if(  get_post_meta($post_id, 'nyukyonengetsu', true) or get_post_meta($post_id, 'nyukyojiki', true) or get_post_meta($post_id, 'nyukyosyun', true)){ ?>
									<tr>
										<th class="th2">引渡/入居時期</th>
										<td><?php my_custom_nyukyojiki_print($post_id); ?>
										<?php echo get_post_meta($post_id, 'nyukyonengetsu', true);?>
										<?php my_custom_nyukyosyun_print($post_id);?></td>
									</tr>
								<?php };
								if(  get_post_meta($post_id, 'nyukyogenkyo', true) ){ ?>
									<tr>
										<th class="th2">現況</th>
										<td><?php my_custom_nyukyogenkyo_print($post_id); ?></td>
									</tr>
								<?php }; ?>


								<!-- 土地 -->
									<?php
									if(  get_post_meta($post_id, 'tochichimoku', true) ){
									?>
									<tr>
										<th class="th2">地目</th>
										<td><?php my_custom_tochichimoku_print($post_id); ?></td>
									</tr>
									<?php } ?>
									
									<?php
									if(  get_post_meta($post_id, 'tochiyouto', true) ){
									?>
									<tr>
										<th class="th2">用途地域</th>
										<td><?php my_custom_tochiyouto_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php
									if(  get_post_meta($post_id, 'tochikeikaku', true) ){
									?>
									<tr>
										<th class="th2">都市計画</th>
										<td><?php my_custom_tochikeikaku_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php
									if(  get_post_meta($post_id, 'tochichisei', true) ){
									?>
									<tr>
										<th class="th2">地勢</th>
										<td><?php my_custom_tochichisei_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php
									if(  get_post_meta($post_id, 'tochikukaku', true) ){
									?>
									<tr>
										<th class="th2">土地面積</th>
										<td><?php echo get_post_meta($post_id, 'tochikukaku', true);?>m&sup2;</td>
									</tr>
									<?php } ?>
									<?php
									if(  get_post_meta($post_id, 'tochisokutei', true) ){
									?>
									<tr>
										<th class="th2">土地面積計測方式</th>
										<td><?php my_custom_tochisokutei_print($post_id); ?></td>
									</tr>

									<?php } ?>

									<?php if( get_post_meta($post_id,'tochishido',true)!='' ){ ?>
									<tr>
										<th>私道負担面積</th>
										<td><?php echo get_post_meta($post_id, 'tochishido', true);?>m&sup2;</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochisetback',true)!='' || get_post_meta($post_id,'tochisetback2',true)!='' ){ ?>
									<tr>
										<th>セットバック</th>
										<td><?php my_custom_tochisetback_print($post_id); ?></td>
									</tr>
									<tr>
										<th class="th2">セットバック量</th>
										<td><?php echo get_post_meta($post_id, 'tochisetback2', true);?>m&sup2;</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochikenpei',true)!='' || get_post_meta($post_id,'tochiyoseki',true)!='' ){ ?>
									<tr>
										<th>建ぺい率</th>
										<td><?php echo get_post_meta($post_id, 'tochikenpei', true);?>%</td>
									</tr>
									<tr>
										<th class="th2">容積率</th>
										<td><?php echo get_post_meta($post_id, 'tochiyoseki', true);?>%</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochikenri',true)!='' ){ ?>
									<tr>
										<th>土地権利</th>
										<td><?php my_custom_tochikenri_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id,'tochisetsudo',true)!='' ){ ?>
									<tr>
										<th class="th2">接道状況</th>
										<td><?php my_custom_tochisetsudo_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochisetsudohouko1',true)!='' ){ ?>
									<tr>
										<th>接道方向1</th>
										<td><?php my_custom_tochisetsudohouko1_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id,'tochisetsudomaguchi1',true)!='' ){ ?>
									<tr>
										<th class="th2">接道間口1</th>
										<td><?php echo get_post_meta($post_id, 'tochisetsudomaguchi1', true);?>m</td>
									</tr>
									<?php } ?>


									<?php if( get_post_meta($post_id,'tochisetsudoshurui1',true)!=''  ){ ?>
									<tr>
										<th>接道種別1</th>
										<td><?php my_custom_tochisetsudoshurui1_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php if(  get_post_meta($post_id,'tochisetsudofukuin1',true)!='' ){ ?>
									<tr>
										<th class="th2">接道幅員1</th>
										<td><?php echo get_post_meta($post_id, 'tochisetsudofukuin1', true);?>m</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochisetsudoichishitei1',true)!='' ){ ?>
									<tr>
										<th>位置指定道路1</th>
										<td><?php my_custom_tochisetsudoichishitei1_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochisetsudohouko2',true)!=''  ){ ?>
									<tr>
										<th>接道方向2</th>
										<td><?php my_custom_tochisetsudohouko2_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id,'tochisetsudomaguchi2',true)!='' ){ ?>
									<tr>
										<th class="th2">接道間口2</th>
										<td><?php echo get_post_meta($post_id, 'tochisetsudomaguchi2', true);?>m</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochisetsudoshurui2',true)!='' ){ ?>
									<tr>
										<th>接道種別2</th>
										<td><?php my_custom_tochisetsudoshurui2_print($post_id); ?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id,'tochisetsudofukuin2',true)!='' ){ ?>
									<tr>
										<th class="th2">接道幅員2</th>
										<td><?php echo get_post_meta($post_id, 'tochisetsudofukuin2', true);?>m</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochisetsudoichishitei2',true)!='' ){ ?>
									<tr>
										<th>位置指定道路2</th>
										<td><?php my_custom_tochisetsudoichishitei2_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id,'tochikokudohou',true)!='' ){ ?>
									<tr>
										<th>国土法届出</th>
										<td><?php my_custom_tochikokudohou_print($post_id); ?></td>
									</tr>
									<?php } ?>

								<!-- .土地 -->

								<?php if( get_post_meta($post_id, 'shuuhenshougaku', true) !='' or get_post_meta($post_id, 'shuuhenchuugaku', true) !='' or get_post_meta($post_id, 'shuuhensonota', true) !=''){ ?>
									<tr>
										<th>周辺環境</th>
										<td>
										<?php if( get_post_meta($post_id, 'shuuhenshougaku', true) !='' ){ ?>
											<?php echo get_post_meta($post_id, 'shuuhenshougaku', true);?>　
										<?php } ?>
										<?php if( get_post_meta($post_id, 'shuuhenchuugaku', true) !='' ){ ?>
											<?php echo get_post_meta($post_id, 'shuuhenchuugaku', true);?>
										<?php } ?>
										<?php if( get_post_meta($post_id,'shuuhensonota',true)!='' && ( get_post_meta($post_id, 'shuuhenshougaku', true) !='' || get_post_meta($post_id, 'shuuhenchuugaku', true) !='')){ ?>
											<br />
										<?php } ?>
											<?php echo get_post_meta($post_id, 'shuuhensonota', true);?>
										</td>
									</tr>
									<?php }; ?>
									<?php if(get_post_meta($post_id, 'setsubisonota',true)  !=''){ ?>
									<tr>
										<th>設備・条件</th>
										<td><?php ez_my_custom_setsubi_print($post_id); ?></td>
									</tr>
									<?php }; ?>

									<?php if( get_post_meta($post_id,'targeturl',true)!='' ){ ?>
									<tr>
										<th>URL</th>
										<td><?php my_custom_targeturl_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<tr class="bukkenbango">
										<th>物件番号</th>
										<td<?php if( get_post_meta($post_id,'keisaikigenbi',true)=='' ) echo ''; ?>>
										<?php echo get_post_meta($post_id, 'shikibesu', true);?></td>
									</tr>
									<tr>
										<?php if( get_post_meta($post_id,'keisaikigenbi',true)!='' ){ ?>
										<th class="th2">掲載期限日</th>
										<td><?php echo get_post_meta($post_id, 'keisaikigenbi', true);?></td>
										<?php } ?>
									</tr>

									<?php if( get_post_meta($post_id,'koukaijisha',true)!='' || get_post_meta($post_id,'jyoutai',true)!='' ){ ?>
									<tr>
										<th>自社物</th>
										<td><?php my_custom_koukaijisha_print($post_id);?></td>
									</tr>
									<tr>
										<th class="th2">状態</th>
										<td><?php my_custom_jyoutai_print($post_id);?></td>
									</tr>
									<?php } ?>
									<?php if( get_post_meta($post_id, 'kakakutsubo', true) !=""){ ;?>
									<tr>
									<th>坪単価</th>
									<td><?php my_custom_kakakutsubo_print($post_id) ;?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakukyouekihi', true) !=""){ ;?>
									<tr>
									<th>共益費・管理費</th>
									<td><?php echo get_post_meta($post_id, 'kakakukyouekihi', true);?>円</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakuhyorimawari', true) !="" ||  get_post_meta($post_id, 'kakakurimawari', true) !=""){ ;?>
									<br /><tr>
									<th>満室時表面利回り</th>
									<td><?php echo get_post_meta($post_id, 'kakakuhyorimawari', true);?>%</td>
									</tr>
									<tr>
									<th>現行利回り</th>
									<td><?php echo get_post_meta($post_id, 'kakakurimawari', true);?>%</td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'shakuchiryo', true) !="" || get_post_meta($post_id, 'shakuchikikan', true) !=""){ ;?>
									<tr>
									<th></th>
									<td><?php echo my_custom_shakuchi_print($post_id);?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakushikikin', true) !=""){ ;?>
									<tr>
									<th>敷金</th>
									<td><?php my_custom_kakakushikikin_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakureikin', true) !=""){ ;?>
									<tr>
									<th>礼金</th>
									<td><?php my_custom_kakakureikin_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakuhoshoukin', true) !=""){ ;?>
									<tr>
									<th>保証金</th>
									<td><?php my_custom_kakakuhoshoukin_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakukenrikin', true) !=""){ ;?>
									<tr>
									<th>権利金</th>
									<td><?php my_custom_kakakukenrikin_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakushikibiki', true) !=""){ ;?>
									<tr>
									<th>償却・敷引金</th>
									<td><?php my_custom_kakakushikibiki_print($post_id); ?></td>
									</tr>
									<?php } ?>

									<?php if( get_post_meta($post_id, 'kakakukoushin', true) !=""){ ;?>
									<tr>
									<th>更新料</th>
									<td><?php my_custom_kakakukoushin_print($post_id); ?></td>
									</tr>
									<?php } ?>
								<?php do_action( 'single-fudo3' ); ?>

								</table>
								<p class="btn_contact"><a href="#toiawasesaki" class="phone over"><a href="tel:0000000000" class="phone over pcnone"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_phone_sp.png" class="pcnone"></a><a href="#role_form" class="contact over"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_contact_sp.png" class="pcnone"></a></p>

								<?php
								//
								// 周辺地図表示
								if (get_post_meta($post->ID,'bukkenido',true) or get_post_meta($post->ID,'bukkenkeido',true)){
								//
								?>
								<h3>周辺地図</h3>
								<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
								<script type="text/javascript">
								var lati = <?php echo sprintf('%.6f',get_post_meta($post->ID,'bukkenido',true)); ?>;
								var long = <?php echo sprintf('%.6f',get_post_meta($post->ID,'bukkenkeido',true)); ?>;
								</script>
								<script src="<?php echo get_template_directory_uri(); ?>/js/template.min.js"></script>
								<script src="<?php echo get_template_directory_uri(); ?>/js/template_user.js"></script>
										<!-- 地図 -->
										<section>
										<ul class="clearfix">
											<li class="span6" style="float:left;width:50%;">
												<div id="map-canvas" style="margin-right:20px;float:left;width: 100%; height: 300px"></div>
											</li>
											<li class="span6" style="float:left;width:50%;">
												<div id="pano" style="float:left;width: 100%; height: 300px;"></div>
											</li>
										</ul>
										</section>
										<!-- 地図 -->
										<!-- $content  -->
										<div class="entry-content"><?php
	
									//Tweet, Like, Google +1 and Share
									if ( function_exists('disp_social') ) 
										remove_filter('the_content', 'disp_social');
									//WP Social Bookmarking Light
									if ( function_exists('wp_social_bookmarking_light_the_content') ) 
										remove_filter('the_content', 'wp_social_bookmarking_light_the_content');

									//echo do_shortcode($content);
									$content = apply_filters('the_content', $content);
									$content = str_replace(']]>', ']]&gt;', $content);
									echo $content;

									?>

							<!-- 地図 -->
								<?php 
								/**
								 * 地図表示 GoogleMaps Places
								 *
								 * @since Fudousan Plugin ver1.6.0
								 * For single-fudo.php apply_filters( 'fudou_single_googlemaps', $post_id , $kaiin , $kaiin2 , $title );
								 *
								 * @param int $post_id Post ID.
								 * @param int $kaiin.
								 * @param int $kaiin2.
								 * @param str $title.
								 * @return text
								 */
								apply_filters( 'fudou_single_googlemaps', $post_id , $kaiin , $kaiin2 , $title ); 
								?>
							<!-- // 地図 -->

									</div>



						<?php
							//画像 11～20
							// if( $fudou_img_max > 10 ){

							// 	echo '<div id="second_img">';
							// 	for( $imgid=11; $imgid<=$fudou_img_max; $imgid++ ){

							// 		$fudoimg_data = get_post_meta($post_id, "fudoimg$imgid", true);
							// 		$fudoimgcomment_data = get_post_meta($post_id, "fudoimgcomment$imgid", true);
							// 		$fudoimg_alt = $fudoimgcomment_data . my_custom_fudoimgtype_print(get_post_meta($post_id, "fudoimgtype$imgid", true));

							// 		if($fudoimg_data !="" ){
							// 				$attachmentid = '';
							// 				$sql  = "";
							// 				$sql .=  "SELECT P.ID,P.guid";
							// 				$sql .=  " FROM $wpdb->posts as P";
							// 				$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
							// 			//	$sql = $wpdb->prepare($sql,'');
							// 				$metas = $wpdb->get_row( $sql );
							// 				if( !empty($metas) ){
							// 					$attachmentid  =  $metas->ID;
							// 					$guid_url  =  $metas->guid;
							// 				}

							// 				if($attachmentid !=''){
							// 					//thumbnail、medium、large、full 
							// 					$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');
							// 					$fudoimg_url = $fudoimg_data1[0];

							// 					echo '<a href="' . $guid_url . '" rel="lightbox lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';
							// 					if($fudoimg_url !=''){
							// 						echo '<img src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" width="100" /></a>';
							// 					}else{
							// 						echo '<img src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" width="100"  />';
							// 					}
							// 				}
							// 		}
							// 	}
							// 	echo '</div>';
							// }
						?>
							<p class="btn_contact"><a href="#toiawasesaki" class="phone over"><a href="tel:0000000000" class="phone over pcnone"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_phone_sp.png" class="pcnone"></a><a href="#role_form" class="contact over"><img src="<?php echo get_template_directory_uri(); ?>/images/btn_single_contact_sp.png" class="pcnone"></a></p>
							<?php }; ?>
							<h3>関連物件</h3>
							<!-- 物件詳細ウィジェット -->
							<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('syousai_widgets') ) : ?>
							<?php endif; ?>


							<?php do_action( 'single-fudo4' ); ?>


						</div><!-- .list_detail -->

						<div class="list_detail_bottom_info">※物件掲載内容と現況に相違がある場合は現況を優先と致します。</div>

					<?php } ?>

					<?php } ?><!-- //ユーザー別会員物件リスト -->


				</div><!-- .list_simple_box -->



    <?
    //
		// YOUTUBE　再生
		//
		$YoutubeCode = get_post_meta($post->ID,'youtube',true);
		if($YoutubeCode):
		?>
    <!-- YOUTUBE　再生 -->
    <section id="youtubeArea">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $YoutubeCode; ?>" frameborder="0" allowfullscreen></iframe>
    </section>
    <!-- YOUTUBE　再生 -->
    <?php endif; ?>

				<?php edit_post_link( '編集', '<span class="edit-link">', '</span>' ); ?>

			</div><!-- .#nav-above#post-## -->
<?php 
			//SSL
			$fudou_ssl_site_url = get_option('fudou_ssl_site_url');

			//物件問合せ先
			echo '<div id="toiawasesaki">';
			if( $fudou_ssl_site_url != ''){

				//Tweet, Like, Google +1 and Share
				if ( function_exists('disp_social') ) 
					add_filter('the_content', 'disp_social',1);
				//WP Social Bookmarking Light
				if ( function_exists('wp_social_bookmarking_light_the_content') ) 
					add_filter('the_content', 'wp_social_bookmarking_light_the_content');

				$fudo_annnai = get_option('fudo_annnai');
				$fudo_annnai = apply_filters('the_content', $fudo_annnai);
				$fudo_annnai = str_replace(']]>', ']]&gt;', $fudo_annnai);
				echo $fudo_annnai;

			}else{
				echo get_option('fudo_annnai');
			}

			echo '</div>';

			do_action( 'single-fudo5' );

?>

<script>
	var toiawasesaki_contact = jQuery('#toiawasesaki .contact').clone(true);
	jQuery('#toiawasesaki #charge').after(toiawasesaki_contact);
</script>

<?php
			if( $kaiin == 1 ) {
			}else{

				if ( $kaiin2 ){

					//SSL
					if( $fudou_ssl_site_url !=''){
						//SSL問合せフォーム
						echo '<div id="ssl_botton" align="center">';
						echo '<a href="'.$fudou_ssl_site_url.'/wp-content/plugins/fudou/themes/contact.php?p='.$post_id.'&action=register&KeepThis=true&TB_iframe=true&height=500&width=620" class="thickbox">';
						echo '<img src="'.get_option('siteurl').'/wp-content/plugins/fudou/img/ask_botton.jpg" alt="物件お問合せ" title="物件お問合せ" /></a>';
						echo '</div>';
					}else{

						//問合せフォーム
						echo '<h3 id="role_form">お問い合わせフォーム</h3>';
						echo '<div id="contact_form">';

						//Tweet, Like, Google +1 and Share
						if ( function_exists('disp_social') ) 
							add_filter('the_content', 'disp_social',1);
						//WP Social Bookmarking Light
						if ( function_exists('wp_social_bookmarking_light_the_content') ) 
							add_filter('the_content', 'wp_social_bookmarking_light_the_content');

						$fudo_form = get_option('fudo_form');
						$fudo_form = apply_filters('the_content', $fudo_form);
						$fudo_form = str_replace(']]>', ']]&gt;', $fudo_form);
						echo $fudo_form;
						echo '</div>';
					}
				}
			}

			//コメント
			if( FUDOU_TRA_COMMENT )	 comments_template( '', true ); 



	//WPコメント
	if((get_option('expression_option_comment')=='yes') && (!FUDOU_TRA_COMMENT)){
	comments_template( '', true ); 
	// comments_template( '/short-comments.php' ); 
	}

if(get_option('expression_option_fbcomment')=='yes'){?>

<div class="fbcomment">

	<script src="http://connect.facebook.net/ja_JP/all.js#xfbml=1"></script>
	<fb:comments href="<?php echo urlencode(the_permalink());?>"></fb:comments>

</div>

<?php } 



			do_action( 'single-fudo6' );

?>
		</div><!-- .list_simplepage2 -->

<?php } //パスワード保護 ?>

	</div><!-- .#content -->

</div><!-- .#container -->


<?php 
	get_sidebar();
	get_footer(); 
?>




<?php

//設備・条件（カスタマイズ）
function ez_my_custom_setsubi_print($post_id) {
	global $work_setsubi;

echo '<ul class="setsubi_list">';
	$setsubi_data = get_post_meta($post_id, 'setsubi',true);
	foreach($work_setsubi as $meta_box){
		if( strpos($setsubi_data, $meta_box['code']) ){	echo '<li>'.$meta_box['name'].'</li>'; }
		//$setsubi_data_arr = explode('/' , $setsubi_data);
		//if( in_array($meta_box['code'] , $setsubi_data_arr)){	echo '<li>'.$meta_box['name'].'</li>'; }

	}
echo '</ul>';
	echo '<br style="clear:both;" />';
	echo get_post_meta($post_id,'setsubisonota',true);
}


