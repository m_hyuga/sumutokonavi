<?php
/**
 * The Template for displaying fudou archive posts.
 *
 * Template Name: archive-fudo-loop.php
 * 
 * @package WordPress3.7
 * @subpackage Fudousan Plugin
 * Version: 1.4.0
 */

	//会員
	$kaiin = 0;
	if( !is_user_logged_in() && get_post_meta($meta_id, 'kaiin', true) == 1 ) $kaiin = 1;

	//ユーザー別会員物件リスト
	$kaiin2 = users_kaiin_bukkenlist($meta_id,$kaiin_users_rains_register, get_post_meta($meta_id, 'kaiin', true) );

	//$_post = & get_post( intval($meta_id) ); 
	$_post = get_post( intval($meta_id) ); 

	//newup_mark
	$post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($_post->post_modified, "%d-%d-%d"));
	$post_date =  vsprintf("%d-%02d-%02d", sscanf($_post->post_date, "%d-%d-%d"));
	$newup_mark_img=  '';
	if( $newup_mark != 0 && is_numeric($newup_mark) ){
		/*
		if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){
			if($post_modified_date == $post_date ){
				$newup_mark_img = '<div class="new_mark">new</div>';
			}else{
				$newup_mark_img =  '<div class="new_mark">up</div>';
			}
		}
		*/
	}

?>


<div class="list_simple_boxtitle<?php if(!is_user_logged_in()){echo ' no_mylist';}; ?>">
	<h2 class="entry-title"><input type="checkbox">

	<?php if( get_post_meta($meta_id, 'kaiin', true) == 1 ) { ?>
		<!--<span style="float:right;margin:7px"><img src="<?php echo get_option('siteurl'); ?>/wp-content/plugins/fudou/img/kaiin_s.jpg" alt="" /></span>-->
	<?php } ?>

	<?php if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){ ?>
			<a href="<?php echo get_permalink($meta_id).$add_url; ?>" title="">会員物件<?php echo $newup_mark_img; ?></a>
	<?php }else{ ?>
		<a href="<?php echo get_permalink($meta_id).$add_url; ?>" title="<?php echo $meta_title; ?>" rel="bookmark"><!--<span class="shubetsu_icon"><?php my_custom_bukkenshubetsu_print($meta_id); ?></span>--><span class="bukken_title"><?php echo $meta_title; ?></span><?php echo $newup_mark_img; ?></a>
	<?php } ?>
	</h2>
</div>


<div class="list_simple_box">
	<div class="entry-excerpt">
	<?php
		if ( my_custom_kaiin_view('kaiin_excerpt',$kaiin,$kaiin2) ){
			echo $_post->post_excerpt; 
		}
	?>
	</div>

	<!-- 左ブロック --> 
	<div class="list_picsam">

		<!-- 画像 -->
		<div class="list_picsam_img">
		<?php
			if ( !my_custom_kaiin_view('kaiin_gazo',$kaiin,$kaiin2) ){
				echo '<img src="'.WP_PLUGIN_URL.'/fudou/img/kaiin.jpg" alt="" />';
			}else{
				//サムネイル画像
				$img_path = get_option('upload_path');
				if ($img_path == '')	$img_path = 'wp-content/uploads';

				for( $imgid=1; $imgid<=1; $imgid++ ){

					$fudoimg_data = get_post_meta($meta_id, "fudoimg$imgid", true);
					$fudoimgcomment_data = get_post_meta($meta_id, "fudoimgcomment$imgid", true);
					$fudoimg_alt = $fudoimgcomment_data . my_custom_fudoimgtype_print(get_post_meta($meta_id, "fudoimgtype$imgid", true));

					if($fudoimg_data !="" ){

						$sql  = "";
						$sql .=  "SELECT P.ID,P.guid";
						$sql .=  " FROM $wpdb->posts as P";
						$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
					//	$sql = $wpdb->prepare($sql,'');
						$metas = $wpdb->get_row( $sql );

						$attachmentid = '';
						if ( $metas != '' ){
							$attachmentid  =  $metas->ID;
							$guid_url  =  $metas->guid;
						}

						if($attachmentid !=''){
							//thumbnail、medium、large、full 
							$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');
							$fudoimg_url = $fudoimg_data1[0];
							echo '<a href="' . $guid_url . '" rel="lightbox['.$meta_id.'] lytebox['.$meta_id.']" title="'.$fudoimg_alt.'">';
							if($fudoimg_url !=''){
								echo '<img src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" /></a>';
							}else{
								echo '<img src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'"  />';
							}
						}else{
							echo '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'.$fudoimg_data.'" />';
						}


					}else{
						echo '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" />';
					}
				}
			}
		?>
		</div> <!-- #list_picsam_img -->

	</div>

	<!-- 右ブロック -->   
	<div class="list_detail list_price<?php if( get_post_meta($meta_id,'bukkenshubetsu',true) > 3000 ) echo ' rent'; ?>">
		<dl class="price">
			<dt><?php if( get_post_meta($meta_id,'bukkenshubetsu',true) < 3000 ) { echo '価格';}else{echo '賃料';} ?></dt>
			<dd class="cf">
				<p class="fll"><?php 
												//価格
												if ( !my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){
													echo "会員物件";
												}else{
													if( get_post_meta($meta_id, 'seiyakubi', true) != "" ){ echo 'ご成約済'; }else{  my_custom_kakaku_print($meta_id); }
												}
												?></p>
				<p class="flr">
				<?php if( get_post_meta($meta_id, 'kakakushikikin', true) !=""
					|| get_post_meta($meta_id, 'kakakureikin', true) !=""
					|| get_post_meta($meta_id, 'kakakuhoshoukin', true) !=""
					|| get_post_meta($meta_id, 'kakakukenrikin', true) !=""
					|| get_post_meta($meta_id, 'kakakushikibiki', true) !=""
					|| get_post_meta($meta_id, 'kakakukoushin', true) !="" ){ ;?>

				<?php if ( my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){ ?>
						<?php if(get_post_meta($meta_id, 'kakakushikikin', true) !=""){?>
						<span>敷金</span><?php my_custom_kakakushikikin_print($meta_id); ?>
						<?php } ?>

						<?php if(get_post_meta($meta_id, 'kakakureikin', true) !=""){?>
						<span>礼金</span><?php my_custom_kakakureikin_print($meta_id); ?>
						<?php } ?>
				<?php } ?>
				<?php } ?>

				</p>
			</dd>
		</dl>
		<?php if( $kaiin == 0 ) { ?>
		<dl class="madori">
			<dt>間取り/面積</dt>
			<dd>
			<?php
				//間取り
				if ( my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2) ){
					my_custom_madorisu_print($meta_id);
					echo '<br>';
				}
				?><?php
	
				//面積
				if ( my_custom_kaiin_view('kaiin_menseki',$kaiin,$kaiin2) ){
					if(get_post_meta($meta_id, 'bukkenshubetsu', true) < 1200 ){
						if( get_post_meta($meta_id, 'tochikukaku', true) !="" ) echo '&nbsp;'.get_post_meta($meta_id, 'tochikukaku', true).'m&sup2; ';
					}else{
						if( get_post_meta($meta_id, 'tatemonomenseki', true) !="" ) echo '&nbsp;'.get_post_meta($meta_id, 'tatemonomenseki', true).'m&sup2; ';
					}
				}
			?>
			</dd>
		</dl>
		<dl class="year">
			<dt>築年数</dt>
			<dd><?php echo get_post_meta($meta_id, 'tatemonochikunenn', true);?></dd>
		</dl>
		<dl class="address">
			<dt>住所</dt>
			<dd><?php my_custom_shozaichi_print($meta_id); ?><?php echo get_post_meta($meta_id, 'shozaichimeisho', true);?></dd>
		</dl>
		<dl class="area">
			<dt>沿線・駅</dt>
			<dd><?php my_custom_koutsu1_print($meta_id); ?>
					<?php my_custom_koutsu2_print($meta_id); ?>
					<?php if(get_post_meta($meta_id, 'koutsusonota', true) !='') echo '<br />'.get_post_meta($meta_id, 'koutsusonota', true).'';?></dd>
		</dl>
			<!-- 更新日
			<div align="right"><?php echo $post_modified_date; ?> UpDate</div>
			-->
			<?php } ?>

			<?php if( $kaiin == 1 ) { ?>
				この物件は、「会員様にのみ限定公開」している物件です。
				非公開物件につき、詳細情報の閲覧には会員ログインが必要です。
				非公開物件を閲覧・資料請求するには会員登録(無料)が必要です。
			<?php } else { ?>

			<?php
				//ユーザー別会員物件リスト
				if (!$kaiin2 && get_option('kaiin_users_rains_register') == 1 && get_post_meta($meta_id, 'kaiin', true) == 1 ) {
					echo 'この物件は、「閲覧条件」に 該当していない物件です。<br />';
					echo '閲覧条件を変更をする事で閲覧が可能になります。<br />';
					echo '<div align="center">';
					echo '<div id="maching_mail"><a href="'.WP_PLUGIN_URL.'/fudoumail/fudou_user.php?KeepThis=true&TB_iframe=true&height=500&width=680" class="thickbox">';
					echo '閲覧条件・メール設定</a></div>';
					echo '</div>';
				}
			}
			?>
		</dl>
	</div>
		<!-- 詳細リンクボタン -->
		<p class="btn_area cf"><a href="<?php echo get_permalink($meta_id).$add_url; ?>" title="<?php echo $meta_title; ?>" rel="bookmark" target="_blank" class="shosai">詳細を表示する</a><a href="<?php echo get_permalink($meta_id).$add_url; ?>#role_form" title="<?php echo $meta_title; ?>" target="_blank" class="shiryo">資料請求する</a></p>
</div><!-- end list_simple_box -->



