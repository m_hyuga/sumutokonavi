<?php
/*------[ カスタム投稿タイプ ]---------*/

function new_post_type() {
  register_post_type(
    'blog',//投稿タイプ名（識別子）
    array(
		 'label' => 'ブログ',
      'labels' => array(
        'add_new_item' => 'ブログを追加',
        'edit_item' => 'ブログを編集',
        'view_item' => 'ブログを表示',
        'search_items' => 'ブログを検索',
      ),
      'public' => true,// 管理画面に表示しサイト上にも表示する
      'hierarchicla' => false,//コンテンツを階層構造にするかどうか(投稿記事と同様に時系列に)
      'has_archive' => true,//trueにすると投稿した記事のアーカイブページを生成
      'supports' => array(//記事編集画面に表示する項目を配列で指定することができる
        'title',//タイトル
        'editor',//本文（の編集機能）
        'thumbnail',//アイキャッチ画像
      ),
      'menu_position' => 5//「投稿」の下に追加
    )
  );
/*  register_post_type(
    'whatsnew',//投稿タイプ名（識別子）
    array(
		 'label' => '新着情報',
      'labels' => array(
        'add_new_item' => '新着情報を追加',
        'edit_item' => '新着情報を編集',
        'view_item' => '新着情報を表示',
        'search_items' => '新着情報を検索',
      ),
      'public' => true,// 管理画面に表示しサイト上にも表示する
      'hierarchicla' => false,//コンテンツを階層構造にするかどうか(投稿記事と同様に時系列に)
      'has_archive' => true,//trueにすると投稿した記事のアーカイブページを生成
      'supports' => array(//記事編集画面に表示する項目を配列で指定することができる
        'title',//タイトル
        'editor',//本文（の編集機能）
        'thumbnail',//アイキャッチ画像
      ),
      'menu_position' => 5//「投稿」の下に追加
    )
  );
*/
}
add_action('init', 'new_post_type');





/*------[ Custom Menu Support ]---------*/
add_theme_support( 'menus' );
if ( function_exists( 'register_nav_menus' ) ) {
register_nav_menus(array(
    'primary' => 'グローバルメニュー',
    'headernavi' => 'ヘッダーメニュー',
    'footermenu-1' => 'フッターメニュー（１）',
    'footermenu-2' => 'フッターメニュー（２）',
    'footermenu-3' => 'フッターメニュー（３）',
    'footermenu-4' => 'フッターメニュー（４）',
    'footermenu-5' => 'フッターメニュー（５）',
    'footermenu-6' => 'フッターメニュー（６）',
    'spmenu' => 'スマホ下部メニュー',
    'primary-sp' => 'グローバルメニュー(スマホ)'
));
}

/*-----------------------------------------------------------------------------------*/
/*	Register Footer widgets
/*-----------------------------------------------------------------------------------*/
if (function_exists('register_sidebar')) {
	$sidebars = array(1, 2, 3, 4, 5, 6);
	foreach($sidebars as $number) {
	register_sidebar(array(
		'name' => 'Footer ' . $number,
		'id' => 'footer-' . $number,
		'before_widget' => '<div class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	));
	}
}
function widgetized_footer() {
?>
	<div class="f-widget f-widget-1">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 1') ) : ?>
		<?php endif; ?>
	</div>
	<div class="f-widget f-widget-2">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 2') ) : ?>
		<?php endif; ?>
	</div>
	<div class="f-widget f-widget-3">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 3') ) : ?>
		<?php endif; ?>
	</div>
	<div class="f-widget f-widget-4">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 4') ) : ?>
		<?php endif; ?>
	</div>
	<div class="f-widget f-widget-5">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 5') ) : ?>
		<?php endif; ?>
	</div>
	<div class="f-widget f-widget-6 last">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 6') ) : ?>
		<?php endif; ?>
	</div>
<?php
}




//
//
// ログインをメールアドレスでも出来るようにする
//
//
function login_with_email_address($username) {
        $user = get_user_by('email',$username);
        if(!empty($user->user_login))
                $username = $user->user_login;
        return $username;
}
add_action('wp_authenticate','login_with_email_address');

function change_username_text($text){
       if(in_array($GLOBALS['pagenow'], array('wp-login.php'))){
         if ($text == 'ユーザー名'){$text = 'ユーザー名 又は　メールアドレス';}
            }
                return $text;
         }
add_filter( 'gettext', 'change_username_text' );







?>