/*main script for frontend*/

wp_url = '[rep_url]';
expr = [rep_expr];

//delete cookie
function deleteCookie(){
	 jQuery.cookie('fudo-mylist-list' , '');
	 document.location.href='';
}

//get GET
function getRequest(){
  if(location.search.length > 1) {
    var get = new Object();
    var ret = location.search.substr(1).split("&");
    for(var i = 0; i < ret.length; i++) {
      var r = ret[i].split("=");
      get[r[0]] = r[1];
    }
    return get;
  } else {
    return false;
  }
}


jQuery(document).ready(function($){

var items = $.cookie('fudo-mylist-list');
if(items){var items_ar = items.split(',');}else{items_ar = new Array();}

//insert favorite link
function insert_fav(r , type , obj){
	c1 = "<div class=\"fudo-fav-box fudo-fav-box-"+r+"\">";
	c2 = "<!--<span class=\"fudo-fav-box-link \"><a href=\"[rep_url]/?page_id=[rep_mylist]\" title=\"マイ・リスト一覧\"></a></span>-->";
	c3 = "<button class=\"fudo-fav-box-check fudo-fav-box-check-off\"></button></div>";
	if(type == 'single'){
		$('#post-'+r).find('.list_simple_box:not(.no_mylist)').parent().before(c1 + c2 + c3);
	}else if(type == 'archive'){
		$(obj).parent().parent().before(c1 + c2 + c3);
	}else if(type == 'mylist'){
		$(obj).parent().parent().before(c1 + c3);
	}
}

function callAjax(type,r,reload){
	$.get('[rep_thispluginurl]mylist-ajax.php', { mylist: type, mypageid: r },
	  function(data){
	  	if(reload)location.reload();
		//alert('Data Loaded: ' + data);
	  });

}

function addMylist(r){
	ck = $.cookie('fudo-mylist-list');
	if(ck){
		myList = ck.split(',');
		//check for exist
		chk = 0;
		for(i=0 ; i < myList.length ; i++){
			if(myList[i] == r )chk = 1;
		}
		if(chk == 0){
			myList.push(r);
		}
	}else{//first time
		myList = new Array();
		myList[0] = r;
	}
	nck = myList.join(',');
	callAjax('add' , r);
	$.cookie('fudo-mylist-list' , nck , {'expires':expr});
}

function removeMylist(r){
		ck = $.cookie('fudo-mylist-list');
	if(ck){
		myList = ck.split(',');
		//check for exist
		chk = 0;
		for(i=0 ; i < myList.length ; i++){

			if(myList[i] == r ){no = i;}
		}
		if(no >= 0){
			myList.splice(no , 1);
		}
	}else{//first time
		return
	}
	nck = myList.join(',');
	$.cookie('fudo-mylist-list' , nck);
	callAjax('remove' , r , false);



	//location.reload();
}

function removeMylistReload(r){
		ck = $.cookie('fudo-mylist-list');
	if(ck){
		myList = ck.split(',');
		//check for exist
		chk = 0;
		for(i=0 ; i < myList.length ; i++){

			if(myList[i] == r ){no = i;}
		}
		if(no >= 0){
			myList.splice(no , 1);
		}
	}else{//first time
		return
	}
	nck = myList.join(',');
	$.cookie('fudo-mylist-list' , nck);
	callAjax('remove' , r , true);



	//location.reload();
}

/* for single post */
	jQuery('.fudo').each(function(){
	match = jQuery(this).attr('id').match(/post-.+/);
	if(match){
 		r = match[0].slice(5);
		//rが投稿ID
		insert_fav(r , 'single' , false);
	}
	});

g = getRequest();

/* for bukken archive */

if ( typeof( g['bukken'] )  != 'undefined'){ 

	jQuery('.entry-title a').each(function(){
		match = jQuery(this).attr('href').match(/post_type=fudo&p=\d+/);
		if(match){
	 		r = match[0].slice(17).replace('&' , '');
			//rが投稿ID
			insert_fav(r , 'archive' , this);
		}
	});

}else if(g['page_id'] == [rep_mylist]){

	jQuery('.entry-title a').each(function(){
		match = jQuery(this).attr('href').match(/post_type=fudo&p=\d+/);
		if(match){
	 		r = match[0].slice(17).replace('&' , '');
			//rが投稿ID
			insert_fav(r , 'mylist' , this);
		}
	});
	
}

/* touch checkbox */


$(".fudo-fav-box-check-off").live("click", function(){
		$(this).removeClass('fudo-fav-box-check-off');
		$(this).addClass('fudo-fav-box-check-on');
		match = $(this).parent().attr('class').match(/fudo-fav-box-.+/);
		if(match){
			r = match[0].slice(13);
			//rが投稿ID
			addMylist(r);
		}
});

$(".fudo-fav-box-check-on").live("click", function(){
		$(this).removeClass('fudo-fav-box-check-on');
		$(this).addClass('fudo-fav-box-check-off');
		match = $(this).parent().attr('class').match(/fudo-fav-box-.+/);
		if(match[0]){
			r = match[0].slice(13);
			//rが投稿ID
			if(g['page_id'] == [rep_mylist]){
				removeMylistReload(r);
			}else{
				removeMylist(r);
		}
		}
});

/* check listed items */
$(".fudo-fav-box").each(function(){
		match = $(this).attr('class').match(/fudo-fav-box-.+/);
		if(match[0]){
			r = match[0].slice(13);
			rs = String(r);
			if($.inArray(rs, items_ar)>=0){
				$(this).find(".fudo-fav-box-check").removeClass('fudo-fav-box-check-off');
				$(this).find(".fudo-fav-box-check").addClass('fudo-fav-box-check-on');
			}

		}
});




});