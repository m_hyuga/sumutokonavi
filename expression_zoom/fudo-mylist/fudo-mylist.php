<?php
/*
fudo-mylist-lite
*/

/*Release notes
12.11.22 1.0 リリース
12.12.27 バグfix
13.12.03 1.11 マークアップ見直し
15.01.21 permalink注意書き
*/

/*
使用オプション(wp_option)
fudo-mylist-mylist 	マイリスト一覧ページpageID
fudo-mylist-theme  テーマ(simple||vivid||minimum)
fudo-mylist-expires  保存期間（日）
fudo-mylist-css  追加CSS

使用クッキー
fudo-mylist-list  ユーザーページリスト(ID,ID,ID...)

使用meta
postmeta 'mylist-count'
*/

if(!function_exists('get_current_screen')){
	require_once( ABSPATH . 'wp-admin/includes/screen.php' );
}
if(!function_exists('wp_redirect')){
	require_once( ABSPATH . 'wp-includes/pluggable.php' );
}


/* init */

/* lite fudo-mylist-themeをminimalに固定 */
if(!get_option('fudo-mylist-theme') || (get_option('fudo-mylist-theme') != 'minimum')){
	update_option('fudo-mylist-theme' , 'minimum' );
}


/*ex-plus内に設置するのでパス変更*/
require_once('inc/func.php');
$purl = plugins_url();
$purl_root =  '/wp-content/plugins/ex-plus/fudo-mylist/';
$purl_js = '/wp-content/plugins/ex-plus/fudo-mylist/js/';
$purl_css = '/wp-content/plugins/ex-plus/fudo-mylist/css/';

$my_css = '.fudo-fav-box {

}

.fudo-fav-box .fudo-fav-box-link {

}

.fudo-fav-box .fudo-fav-box-link a{

}

.fudo-fav-box .fudo-fav-box-check{

}

.fudo-fav-box .fudo-fav-box-check-on {

}

.fudo-fav-box .fudo-fav-box-check-off {

}




.fudo-fav-box .fudo-fav-box-check .fudo-fav-box-checkbox{
	
}';

if (get_option('fudo-mylist-theme')==''){
	update_option('fudo-mylist-theme' , 'simple');
}

if (get_option('fudo-mylist-expires')==''){
	update_option('fudo-mylist-expires' , '30');
}

if (get_option('fudo-mylist-css')==''){
	update_option('fudo-mylist-css' , $my_css);
}
/* scripts */

function my_scripts() {

	global $purl_js;
/*
	wp_enqueue_script( 
		 'fudo-mylist-front'
		,$purl_js.'front.js.php'
	);
*/
	wp_enqueue_script( 
		 'jquery.cookie.js'
		,$purl_js.'jquery.cookie.js'
		,array('jquery')
	);

}

add_action( 'wp_enqueue_scripts', 'my_scripts', 0 );

function mylist_script(){

	global $purl_js , $purl_root;

	ob_start();
		include("js/front.js.php");
		$con=ob_get_contents();
	ob_end_clean();
	
	$surl = get_bloginfo('url') ;
	$mylist = get_option('fudo-mylist-mylist');
	$expr = get_option('fudo-mylist-expires');
	$con = str_replace('[rep_url]' , $surl , $con);
	$con = str_replace('[rep_mylist]' , $mylist , $con);
	$con = str_replace('[rep_expr]' , $expr , $con);
	$con = str_replace('[rep_thispluginurl]' , site_url().$purl_root , $con);
	
	echo '<script type="text/javascript">';
	echo $con;
	echo '</script>';

	$sc = get_option('fudo-mylist-css');

	if($sc){
		echo '<style type="text/css">';
		echo $sc;
		echo '</script>';
	}
}

add_action( 'wp_head', 'mylist_script' );

/* styles */

function my_styles(){

	global $purl_css;

	wp_enqueue_style( 'fudo_history_front',$purl_css.'fudo-mylist-style.css');


}

add_action( 'wp_enqueue_scripts', 'my_styles' , 0);

/* custom styles */

function custom_styles(){

	global $purl_root;

	if(get_option('fudo-mylist-css')){
		echo '<style type="text/css">';
		echo get_option('fudo-mylist-css');
		echo '</script>';
	}



	if(get_option('fudo-mylist-theme')){
		echo '<style type="text/css">';
		$theme = get_option('fudo-mylist-theme');
		$pre = '-'.$theme;
echo "
.fudo-fav-box {
	
}
.fudo-fav-box .fudo-fav-box-link,
.fudo-fav-box-widget-link
 {
	background:url('".site_url('').$purl_root."img/mylist_view".$pre.".png') no-repeat;
}
.fudo-fav-box .fudo-fav-box-check-on {
	background:url('".site_url('').$purl_root."img/mylist_check_on".$pre.".png') no-repeat;
}

.fudo-fav-box .fudo-fav-box-check-off {
	background:url('".site_url('').$purl_root."img/mylist_check_off".$pre.".png') no-repeat;
}
";
		echo '</style>';
	}


}

add_action( 'wp_head', 'custom_styles' );

/* add mylist page */

function createMylistPage(){

	if( get_option('fudo-mylist-mylist') == '') {
	
	  $my_post = array();
	  $my_post['post_type'] = 'page';
	  $my_post['post_name'] = 'mylist'; 
	  $my_post['post_title'] = 'マイリスト一覧';
	  $my_post['post_content'] = '[mylist]';
	  $my_post['post_status'] = 'publish';
	  $my_post['comment_status'] = 'closed';
	  $my_post['post_author'] = 1;
	
	$page_id = wp_insert_post($my_post);

	update_option('fudo-mylist-mylist' , $page_id);

	}

}

add_action('init' , 'createMylistPage');

/* shortcode mylist*/

function mylist_func($atts) {
	include_once('inc/func.php');

	if($_COOKIE['fudo-mylist-list'] != ''){
		$itemlist = explode(',' , $_COOKIE['fudo-mylist-list']);
	}else{
		$itemlist = '';
	}

		$p = get_post($item);

?>
			<div id="list_simplepage">
	<?php	
				//loop SQL
				if(!empty($itemlist)){
					//$sql2 = $wpdb->prepare($sql2);
					//$metas = $wpdb->get_results( $sql2, ARRAY_A );
				$title_arr = array();

						foreach ( $itemlist as $meta_id ) {


							if($meta_id != ''){
								//$meta_id = $meta['object_id'];	//post_id
								$meta_data = get_post( $meta_id ); 
								$number = get_post_meta($meta_id, 'shikibesu', true);
								$meta_title =  $meta_data->post_title;
							
							//$title_arr[] ='<a href="'.get_permalink($meta_id).'">'.$meta_title.'</a>'."\n";
$title_arr[] ='['.get_permalink($meta_id).']\n'.'(物件番号'.$number.')'.$meta_title;


									include('view/mylist.php');
							}

						}
						echo '<div class="fudo-mylist-delete"><button href="" onclick="deleteCookie()">マイ・リストをクリア</button></div>';

						//メールフォーム充填処理
							$mailcontent = implode("xxx" , $title_arr); 
							echo '<script type="text/javascript">';
							echo 'jQuery(document).ready(function($){';
							echo 'var mc = "'.$mailcontent.'";';
							echo 'res = mc.split("xxx").join("\n\r");';
						
						echo '
						$(".wpcf7-form textarea").text(res);
						';
							echo '});';
							echo '</script>';

				}else{
						echo "マイリストにはまだ登録されていません";
				}
				//loop SQL END


		?>
			</div><!-- list_simplepage -->


<?php



}

add_shortcode('mylist', 'mylist_func');

/* ショートコード mailform_mylist*/

function mailform_mylist_func($atts) {


	if($_COOKIE['fudo-mylist-list'] != ''){
		$itemlist = explode(',' , $_COOKIE['fudo-mylist-list']);
	}else{
		$itemlist = '';
	}

		$p = get_post($item);



	//物件問合せ先
	echo '<div class="wpcf7">';
	echo get_option('fudo_annnai');
	echo '</div>';

	//会員
	$kaiin = 0;
	if( !is_user_logged_in() && get_post_meta($meta_id, 'kaiin', true) == 1 ) $kaiin = 1;
	
	//ユーザー別会員物件リスト
	
	//$kaiin2 = users_kaiin_bukkenlist($meta_id,$kaiin_users_rains_register, get_post_meta($meta_id, 'kaiin', true) );

	/*

		if ( $kaiin2 ){

			//SSL
			$fudou_ssl_site_url = get_option('fudou_ssl_site_url');
			if( $fudou_ssl_site_url !=''){

				//SSL問合せフォーム
				echo '<div id="ssl_botton" align="center">';
				echo '<a href="'.$fudou_ssl_site_url.'/wp-content/plugins/fudou/themes/contact.php?p='.$post_id.'&action=register&KeepThis=true&TB_iframe=true&height=500&width=620" class="thickbox">';
				echo '<img src="'.get_option('siteurl').'/wp-content/plugins/fudou/img/ask_botton.jpg" alt="物件お問合せ" title="物件お問合せ" /></a>';
				echo '</div>';
			}else{

				//問合せフォーム
				$content = get_option('fudo_form');
				$content = apply_filters('the_content', $content);
				$content = str_replace(']]>', ']]&gt;', $content);
				echo $content;
			}
		}
	
*/

}

//add_shortcode('mailform_mylist', 'mailform_mylist_func');



add_filter('plugin_action_links', 'myplugin_plugin_action_links', 10, 2);
 
function myplugin_plugin_action_links($links, $file) {
    static $this_plugin;
 
    if (!$this_plugin) {
        $this_plugin = plugin_basename(__FILE__);
    }
 
    if ($file == $this_plugin) {
        // The "page" query string value must be equal to the slug
        // of the Settings admin page we defined earlier, which in
        // this case equals "myplugin-settings".
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=fudo-mylist/fudo-mylist.php">設定</a>';
        array_unshift($links, $settings_link);
    }
 
    return $links;
}

//select生成
//$arr = optionの連想配列($key => $val)
//$exval = 既定値
function fd_make_sel($name , $arr , $exval){

	echo '<select name="'.$name.'">';	
		foreach ($arr as $k => $v){
			($exval == $v)? $sel = 'selected' : $sel = '';
			echo '<option '.$sel.' value="'.$v.'">'.$k.'</option>';
		}
	echo '</select>';

} 

//radio生成
//$arr = optionの連想配列($key => $val)
//$exval = 既定値
function fd_make_radio($name ,$arr , $exval ){

	if($exval == ''){$exval = $arr[1][1];}


	foreach ($arr as $k => $v){
		if($exval != ''){
				if($exval == $v){
					$sel = 'checked';
				}else{
					$sel = '';
				}
			}else{

		}


		echo '<input type="radio" '.$sel.' name="'.$name.'" value="'.$v.'">'.$k.'&nbsp;';


	}


} 

//radio生成
//$arr = optionの連想配列($key => $val)
//$exval = 既定値
function fd_make_radio2($name ,$arr , $exval ){

	if($exval == ''){$exval = $arr[1][1];}

	echo '<table><tr>';

	foreach ($arr as $k => $v){
		if($exval != ''){
				if($exval == $v){
					$sel = 'checked';
				}else{
					$sel = '';
				}
			}else{

		}

		echo '<td style="width:120px;">';
		echo '<input type="radio" '.$sel.' name="'.$name.'" value="'.$v.'">'.$k.'&nbsp;';
		echo '<br>';

		if($v=='simple'){
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_check_on-simple.png">';
			//echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_check_off-simple.png">';
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_view-simple.png">';	
		}
		else if($v == 'rich'){
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_check_on-rich.png">';
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_check_off-rich.png">';
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_view-rich.png">';	

		}
		else if($v == 'minimum'){
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_check_on-minimum.png">';
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_check_off-minimum.png">';
			echo '<img src="'.WP_PLUGIN_URL.'/fudo-mylist/img/mylist_view-minimum.png">';	
		}
		echo '</td>';
	}

echo '</table></tr>';
} 

//カウントされた上位物件を得る
//$count = 表示数
function getTopFav($count){
	global $wpdb;
	$sql ="
	SELECT *
	FROM $wpdb->postmeta 
	JOIN $wpdb->posts ON post_id = ID
	WHERE `meta_key` = 'mylist-count'
	AND `post_status` = 'publish'  
	LIMIT 0 , $count";

$res = $wpdb->get_results($sql ,ARRAY_A);
return $res;
}

/*
function get_current_screen() {
	global $current_screen;
	
	if ( ! isset( $current_screen ) )
	return null;
	
	return $current_screen;
}
*/

//管理画面のテーブル

if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Links_List_Table extends WP_List_Table {

	/**
	 * Constructor, we override the parent to pass our own arguments
	 * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
	 */
	 function __construct() {
		 parent::__construct( array(
		'singular'=> 'wp_list_text_link', //Singular label
		'plural' => 'wp_list_test_links', //plural label, also this well be one of the table css class
		'ajax'	=> false //We won't support Ajax for this table
		) );
	 }

	/**
	 * Define the columns that are going to be used in the table
	 * @return array $columns, the array of columns to use with the table
	 */
	function get_columns() {
		return $columns= array(
			'post_id'=>__('ID'),
			'post_title'=>'タイトル',
			'meta_value'=>'カウント'

		);
	}

	/**
	 * Decide which columns to activate the sorting functionality on
	 * @return array $sortable, the array of columns that can be sorted by the user
	 */
	public function get_sortable_columns() {
		return $sortable = array(
			'post_id'=>'post_id',
			'meta_value'=>'meta_value'
		);
	}

	public function column_default($item, $column_name){
		return $item -> $column_name;
	}
	/**
	 * Prepare the table with different parameters, pagination, columns and table elements
	 */
	function prepare_items() {
		global $wpdb, $_wp_column_headers;
		//$screen = get_current_screen();
		global $current_screen;
		$screen = $current_screen;
		/* -- Preparing your query -- */
			$query = "
		SELECT *
		FROM $wpdb->postmeta 
		JOIN $wpdb->posts ON post_id = ID
		WHERE `meta_key` = 'mylist-count' 
		AND `meta_value` > 0 
		AND `post_status` = 'publish'
		";
	
		/* -- Ordering parameters -- */
			//Parameters that are going to be used to order the result
			$orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'ASC';
			$order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : '';
			if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }
	
		/* -- Pagination parameters -- */
			//Number of elements in your table?
			$totalitems = $wpdb->query($query); //return the total number of affected rows
			//How many to display per page?
			$perpage = 20;
			//Which page is this?
			$paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
			//Page Number
			if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }
			//How many pages do we have in total?
			$totalpages = ceil($totalitems/$perpage);
			//adjust the query to take pagination into account
			if(!empty($paged) && !empty($perpage)){
				$offset=($paged-1)*$perpage;
				$query.=' LIMIT '.(int)$offset.','.(int)$perpage;
			}
	
		/* -- Register the pagination -- */
			$this->set_pagination_args( array(
				"total_items" => $totalitems,
				"total_pages" => $totalpages,
				"per_page" => $perpage,
			) );
			//The pagination links are automatically built according to those parameters
	
		/* -- Register the Columns -- */
$columns = $this->get_columns();
$hidden = array();
$sortable = $this->get_sortable_columns();
$this->_column_headers = array($columns, $hidden, $sortable);
	
		/* -- Fetch the items -- */
			$this->items = $wpdb->get_results($query);
//var_dump($this->items);
	}


	/**
	 * Print column headers, accounting for hidden and sortable columns.
	 *
	 * @since 3.1.0
	 * @access protected
	 *
	 * @param bool $with_id Whether to set the id attribute or not
	 */
	function print_column_headers( $with_id = true ) {
		$screen = get_current_screen();

		list( $columns, $hidden, $sortable ) = $this->get_column_info();

	

		$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		$current_url = remove_query_arg( 'paged', $current_url );

		if ( isset( $_GET['orderby'] ) )
			$current_orderby = $_GET['orderby'];
		else
			$current_orderby = '';

		if ( isset( $_GET['order'] ) && 'desc' == $_GET['order'] )
			$current_order = 'desc';
		else
			$current_order = 'asc';

		foreach ( $columns as $column_key => $column_display_name ) {
			$class = array( 'manage-column', "column-$column_key" );

			$style = '';
			if ( in_array( $column_key, $hidden ) )
				$style = 'display:none;';

			$style = ' style="' . $style . '"';

			if ( 'cb' == $column_key )
				$class[] = 'check-column';
			elseif ( in_array( $column_key, array( 'posts', 'comments', 'links' ) ) )
				$class[] = 'num';

			if ( isset( $sortable[$column_key] ) ) {
				//list( $orderby, $desc_first ) = $sortable[$column_key];//コアのバグ?
				$orderby	=	$column_key;
				$desc_first	=	$column_key;
				if ( $current_orderby == $orderby ) {
					$order = 'asc' == $current_order ? 'desc' : 'asc';
					$class[] = 'sorted';
					$class[] = $current_order;
				} else {
					$order = $desc_first ? 'desc' : 'asc';
					$class[] = 'sortable';
					$class[] = $desc_first ? 'asc' : 'desc';
				}

				$column_display_name = '<a href="' . esc_url( add_query_arg( compact( 'orderby', 'order' ), $current_url ) ) . '"><span>' . $column_display_name . '</span><span class="sorting-indicator"></span></a>';
			}

			$id = $with_id ? "id='$column_key'" : '';

			if ( !empty( $class ) )
				$class = "class='" . join( ' ', $class ) . "'";

			echo "<th scope='col' $id $class $style>$column_display_name</th>";
		}
	}



/**
 * Display the rows of records in the table
 * @return string, echo the markup of the rows
 */

function display_rows() {

	//Get the records registered in the prepare_items method
	$records = $this->items;

	//Get the columns registered in the get_columns and get_sortable_columns methods

	list( $columns, $hidden ) = $this->get_column_info();


	//Loop for each record
	if(!empty($records)){foreach($records as $rec){


		//Open the line
        echo '<tr id="record_'.$rec->post_id.'">';
		foreach ( $columns as $column_name => $column_display_name ) {

			//Style attributes for each col
			$class = "class='$column_name column-$column_name'";
			$style = "";
			if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
			$attributes = $class . $style;

			//edit link
			$editlink  = '/wp-admin/link.php?action=edit&link_id='.(int)$rec->post_id;

			//Display the cell
			switch ( $column_name ) {
				case "post_id":	echo '<td '.$attributes.'>'.stripslashes($rec->post_id).'</td>';	break;
				case "post_title": echo '<td '.$attributes.'><strong><a href="'.get_permalink($rec->post_id).'" title="">'.stripslashes($rec->post_title).'</a></strong></td>'; break;
				case "meta_value": echo '<td '.$attributes.'>'.stripslashes($rec->meta_value).'</td>'; break;
			}
		}

		//Close the line
		echo'</tr>';
	}}
}


}


//人気物件表示ウイジェット
add_action('widgets_init', 'fudo_widgetInit_top_fv');
function fudo_widgetInit_top_fv() {
	register_widget('fudo_widget_top_fv');
}

class fudo_widget_top_fv extends WP_Widget {

	/** constructor */
	function fudo_widget_top_fv() {
		parent::WP_Widget(false, $name = '人気物件表示' , array('description'=>'お気に入りに追加された回数順に物件表示'));
	}

	/** @see WP_Widget::form */	
	function form($instance) {

		global $is_fudoukaiin;

		$title      = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$item       = isset($instance['item']) ? esc_attr($instance['item']) : '';
		$shubetsu   = isset($instance['shubetsu']) ? esc_attr($instance['shubetsu']) : '';
		$bukken_cat = isset($instance['bukken_cat']) ? esc_attr($instance['bukken_cat']) : '';
		$sort       = isset($instance['sort']) ? esc_attr($instance['sort']) : '';

		$view1 = isset($instance['view1']) ? esc_attr($instance['view1']) : '';
		$view2 = isset($instance['view2']) ? esc_attr($instance['view2']) : '';
		$view3 = isset($instance['view3']) ? esc_attr($instance['view3']) : '';
		$view4 = isset($instance['view4']) ? esc_attr($instance['view4']) : '';
		$view5 = isset($instance['view5']) ? esc_attr($instance['view5']) : '';
		$kaiinview = isset($instance['kaiinview']) ? esc_attr($instance['kaiinview']) : '';

		if($item=='') $item = 4;

		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">
		タイトル <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>


		<p><label for="<?php echo $this->get_field_id('item'); ?>">
		表示数 <input class="widefat" id="<?php echo $this->get_field_id('item'); ?>" name="<?php echo $this->get_field_name('item'); ?>" type="text" value="<?php echo $item; ?>" /></label></p>

<!--
		<p><label for="<?php echo $this->get_field_id('shubetsu'); ?>">
		種別 <select class="widefat" id="<?php echo $this->get_field_id('shubetsu'); ?>" name="<?php echo $this->get_field_name('shubetsu'); ?>">
			<option value="1"<?php if($shubetsu == 1){echo ' selected="selected"';} ?>>物件すべて</option>
			<option value="2"<?php if($shubetsu == 2){echo ' selected="selected"';} ?>>売買すべて</option>
			<option value="3"<?php if($shubetsu == 3){echo ' selected="selected"';} ?>>売買土地</option>
			<option value="4"<?php if($shubetsu == 4){echo ' selected="selected"';} ?>>売買戸建</option>
			<option value="5"<?php if($shubetsu == 5){echo ' selected="selected"';} ?>>売買マンション</option>
			<option value="6"<?php if($shubetsu == 6){echo ' selected="selected"';} ?>>売買住宅以外の建物全部</option>
			<option value="7"<?php if($shubetsu == 7){echo ' selected="selected"';} ?>>売買住宅以外の建物一部</option>
			<option value="10"<?php if($shubetsu == 10){echo ' selected="selected"';} ?>>賃貸すべて</option>
			<option value="11"<?php if($shubetsu == 11){echo ' selected="selected"';} ?>>賃貸居住用</option>
			<option value="12"<?php if($shubetsu == 12){echo ' selected="selected"';} ?>>賃貸事業用</option>
		</select></label></p>
-->

<!--
		<p><label for="<?php echo $this->get_field_id('bukken_cat'); ?>">
		物件カテゴリ <select class="widefat" id="<?php echo $this->get_field_id('bukken_cat'); ?>" name="<?php echo $this->get_field_name('bukken_cat'); ?>">
			<option value="0"<?php if($bukken_cat == 0){echo ' selected="selected"';} ?>>すべて</option>
<?php

		//物件カテゴリ
		$terms = get_terms('bukken', 'hide_empty=0');

		if ( !empty( $terms ) ){
			foreach ( $terms as $term ) {
				echo  '<option value="'.$term->term_id.'"';
				if( $bukken_cat == $term->term_id )	echo ' selected="selected"';
				echo '>'.$term->name.'</option>';
			}
		}
?>
		</select></label></p>
-->


<!--
		<p><label for="<?php echo $this->get_field_id('sort'); ?>">
		並び順 <select class="widefat" id="<?php echo $this->get_field_id('sort'); ?>" name="<?php echo $this->get_field_name('sort'); ?>">
			<option value="2"<?php if($sort == 2){echo ' selected="selected"';} ?>>新しい順</option>
			<option value="3"<?php if($sort == 3){echo ' selected="selected"';} ?>>古い順</option>
			<option value="4"<?php if($sort == 4){echo ' selected="selected"';} ?>>高い順</option>
			<option value="5"<?php if($sort == 5){echo ' selected="selected"';} ?>>安い順</option>
			<option value="6"<?php if($sort == 6){echo ' selected="selected"';} ?>>広い順(面積)</option>
			<option value="7"<?php if($sort == 7){echo ' selected="selected"';} ?>>狭い順(面積)</option>
			<option value="1"<?php if($sort == 1){echo ' selected="selected"';} ?>>ランダム</option>
		</select></label></p>

-->
		表示項目<br />
		<p><label for="<?php echo $this->get_field_id('view1'); ?>">
		タイトル <select class="widefat" id="<?php echo $this->get_field_id('view1'); ?>" name="<?php echo $this->get_field_name('view1'); ?>">
			<option value="1"<?php if($view1 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view1 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>




		<p><label for="<?php echo $this->get_field_id('view2'); ?>">
		価格 <select class="widefat" id="<?php echo $this->get_field_id('view2'); ?>" name="<?php echo $this->get_field_name('view2'); ?>">
			<option value="1"<?php if($view2 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view2 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

		<p><label for="<?php echo $this->get_field_id('view3'); ?>">
		間取り・土地面積 <select class="widefat" id="<?php echo $this->get_field_id('view3'); ?>" name="<?php echo $this->get_field_name('view3'); ?>">
			<option value="1"<?php if($view3 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view3 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

		<p><label for="<?php echo $this->get_field_id('view4'); ?>">
		地域 <select class="widefat" id="<?php echo $this->get_field_id('view4'); ?>" name="<?php echo $this->get_field_name('view4'); ?>">
			<option value="1"<?php if($view4 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view4 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

		<p><label for="<?php echo $this->get_field_id('view5'); ?>">
		路線・駅 <select class="widefat" id="<?php echo $this->get_field_id('view5'); ?>" name="<?php echo $this->get_field_name('view5'); ?>">
			<option value="1"<?php if($view5 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view5 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

	<?php if($is_fudoukaiin && get_option('kaiin_users_can_register') == '1' ){ ?>
		<p><label for="<?php echo $this->get_field_id('kaiinview'); ?>">
		会員物件 <select class="widefat" id="<?php echo $this->get_field_id('kaiinview'); ?>" name="<?php echo $this->get_field_name('kaiinview'); ?>">
			<option value="1"<?php if($kaiinview == 1){echo ' selected="selected"';} ?>>会員・一般物件を表示する</option>
			<option value="2"<?php if($kaiinview == 2){echo ' selected="selected"';} ?>>会員物件を表示しない</option>
			<option value="3"<?php if($kaiinview == 3){echo ' selected="selected"';} ?>>会員物件だけ表示</option>
		</select></label></p>

	<?php } ?>

		<?php 
	}


	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {

		global $is_fudoukaiin;
		global $wpdb;

		// outputs the content of the widget

	        extract( $args );
		$title = @$instance['title'];
		$item =  @$instance['item'];
		$shubetsu = @$instance['shubetsu'];
		$bukken_cat = @$instance['bukken_cat'];

		$sort = @$instance['sort'];
		$view1 = @$instance['view1'];
		$view2 = @$instance['view2'];
		$view3 = @$instance['view3'];
		$view4 = @$instance['view4'];
		$view5 = @$instance['view5'];

		$kaiinview = @$instance['kaiinview'];
		if($kaiinview == '') $kaiinview = 1;

		if( !$is_fudoukaiin || get_option('kaiin_users_can_register') != '1' ){
			$kaiinview = 1;
		}


		if($item =="") 	$item=4;

		$newup_mark = get_option('newup_mark');
		if($newup_mark == '') $newup_mark=14;


		//
		switch ($shubetsu) {
			case 1 :	//物件すべて
				$where_data = "";
				break;
			case 2 :	//売買すべて
				$where_data = " AND CAST( PM_1.meta_value AS SIGNED )<3000";
				break;
			case 3 :	//売買土地
				$where_data = " AND Left(PM_1.meta_value,2) ='11'";
				break;
			case 4 :	//売買戸建
				$where_data = " AND Left(PM_1.meta_value,2) ='12'";
				break;
			case 5 :	//売買マンション
				$where_data = " AND Left(PM_1.meta_value,2) ='13'";
				break;
			case 6 :	//売住宅以外の建物全部
				$where_data = " AND Left(PM_1.meta_value,2) ='14'";
				break;
			case 7 :	//売住宅以外の建物一部
				$where_data = " AND Left(PM_1.meta_value,2) ='15'";
				break;

			case 10 :	//賃貸すべて
				$where_data = " AND  CAST( PM_1.meta_value AS SIGNED )>3000";
				break;
			case 11 :	//賃貸居住用
				$where_data = " AND Left(PM_1.meta_value,2) ='31'";
				break;
			case 12 :	//賃貸事業用
				$where_data = " AND Left(PM_1.meta_value,2) ='32'";
				break;

			default:
				$order_data = "";
				break;
		}


		//sort
		switch ($sort) {
			case 1 :	//ランダム
				$order_data = " rand()";
				break;
			case 2 :	//新しい順
				$order_data = " P.post_date DESC";
				break;
			case 3 :	//古い順
				$order_data = " P.post_date ASC";
				break;
			case 4 :	//高い順
				$order_data = " CAST( PM_2.meta_value AS SIGNED ) DESC";
				break;
			case 5 :	//安い順
				$order_data = " CAST( PM_2.meta_value AS SIGNED ) ASC";
				break;
			case 6 :	//広い順
				$order_data = " CAST( PM_3.meta_value AS SIGNED ) DESC";
				break;
			case 7 :	//狭い順
				$order_data = " CAST( PM_3.meta_value AS SIGNED ) ASC";
				break;
			default:
				$order_data = " rand()";
				break;
		}



		//SQL
		$sql = "";


			$sql = "
				SELECT * 
				FROM $wpdb->postmeta 
				JOIN $wpdb->posts ON post_id = ID 
				WHERE `meta_key` = 'mylist-count' 
				AND `post_status` = 'publish'
				ORDER BY `$wpdb->postmeta`.`meta_value` DESC 
				LIMIT 0 , $item";
		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_results( $sql, ARRAY_A );

		//ユーザー別会員物件リスト
		$kaiin_users_rains_register = get_option('kaiin_users_rains_register');


		if(!empty($metas)) {

			echo $before_widget;

			if ( $title ){
				echo $before_title . $title . $after_title; 
			}

			$img_path = get_option('upload_path');
			if ($img_path == '')
				$img_path = 'wp-content/uploads';

			echo '<div id="box'.$args['widget_id'].'">';
			echo '<ul id="'.$args['widget_id'].'_1" class="grid-content">';

			foreach ( $metas as $meta ) {

				$post_id =  $meta['ID'];
				$post_title =  $meta['post_title'];
				$post_url =  str_replace('&p=','&amp;p=',get_permalink($post_id));

				//newup_mark
				$post_modified =  $meta['post_modified'];
				$post_date =   $meta['post_date'];
				$post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($post_modified, "%d-%d-%d"));
				$post_date =  vsprintf("%d-%02d-%02d", sscanf($post_date, "%d-%d-%d"));

				$newup_mark_img=  '';
				if( $newup_mark != 0 && is_numeric($newup_mark) ){
					if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){
						if($post_modified_date == $post_date ){
							$newup_mark_img = '<div class="new_mark">new</div>';
						}else{
							$newup_mark_img =  '<div class="new_mark">up</div>';
						}
					}
				}


				//会員
				$kaiin = 0;
				if( !is_user_logged_in() && get_post_meta($post_id, 'kaiin', true) == 1 ) $kaiin = 1;

				//ユーザー別会員物件リスト
				$kaiin2 = users_kaiin_bukkenlist($post_id,$kaiin_users_rains_register,get_post_meta($post_id, 'kaiin', true));


				echo '<li class="'.$args['widget_id'].' box1">';

				echo $newup_mark_img;

				//会員項目表示判定
				if ( !my_custom_kaiin_view('kaiin_gazo',$kaiin,$kaiin2) ){
					echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/kaiin.jpg" alt="" />';
				}else{

						//サムネイル画像
						$fudoimg1_data = get_post_meta($post_id, 'fudoimg1', true);
						if($fudoimg1_data != '')	$fudoimg_data=$fudoimg1_data;
						$fudoimg2_data = get_post_meta($post_id, 'fudoimg2', true);
						if($fudoimg2_data != '')	$fudoimg_data=$fudoimg2_data;
						$fudoimg_alt = str_replace("　"," ",$post_title);

						echo '<a href="' . $post_url . '">';

						if($fudoimg_data !="" ){

							$sql  = "";
							$sql .=  "SELECT P.ID,P.guid";
							$sql .=  " FROM $wpdb->posts as P";
							$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
						//	$sql = $wpdb->prepare($sql);
							$metas = $wpdb->get_row( $sql );

							$attachmentid = '';
							if ( $metas != '' ){
								$attachmentid  =  $metas->ID;
								$guid_url  =  $metas->guid;
							}

							if($attachmentid !=''){
								//thumbnail、medium、large、full 
								$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');
								$fudoimg_url = $fudoimg_data1[0];

								if($fudoimg_url !=''){
									echo '<img class="box1image" src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" />';
								}else{
									echo '<img class="box1image" src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" />';
								}
							}else{
								echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'.$fudoimg_data.'" />';
							}

						}else{
						//	echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" style="width:150px; height:150px;"  alt="" />';
							echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" />';
						}
						echo "</a>\n";
				}


				//抜粋
				//$_post = & get_post( intval($post_id) ); 
				//echo $_post->post_excerpt; 


				//タイトル
				if ( my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){
					if($view1=="1" && $post_title !=''){
							echo '<span class="top_title">';
							echo str_replace("　"," ",$post_title).'';
							echo '</span><br />';
					}
				}


				//価格
				if ( my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){

						if($view2=="1"){
							echo '<span class="top_price">';
							if( get_post_meta($post_id, 'seiyakubi', true) != "" ){
								echo 'ご成約済　';
							}else{
								//非公開の場合
								if(get_post_meta($post_id,'kakakukoukai',true) == "0"){
									$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
									if($kakakujoutai_data_data=="1")	echo '相談　';
									if($kakakujoutai_data_data=="2")	echo '確定　';
									if($kakakujoutai_data_data=="3")	echo '入札　';

								}else{
									$kakaku_data = get_post_meta($post_id,'kakaku',true);
									if(is_numeric($kakaku_data)){
										echo floatval($kakaku_data)/10000;
										echo "万円 ";
									}
								}
							}
							echo '</span>';
						}
				}


				//間取り・土地面積
				if($view3=="1"){

					//間取り
					if ( my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2) ){
							echo '<span class="top_madori">';
							$madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);
							echo get_post_meta($post_id,'madorisu',true);
							if($madorisyurui_data=="10")	echo 'R ';
							if($madorisyurui_data=="20")	echo 'K ';
							if($madorisyurui_data=="25")	echo 'SK ';
							if($madorisyurui_data=="30")	echo 'DK ';
							if($madorisyurui_data=="35")	echo 'SDK ';
							if($madorisyurui_data=="40")	echo 'LK ';
							if($madorisyurui_data=="45")	echo 'SLK ';
							if($madorisyurui_data=="50")	echo 'LDK ';
							if($madorisyurui_data=="55")	echo 'SLDK ';
							echo '</span>';
					}


					//土地面積
					if ( my_custom_kaiin_view('kaiin_menseki',$kaiin,$kaiin2) ){
							echo '<span class="top_menseki">';
							if ( get_post_meta($post_id,'bukkenshubetsu',true) < 1200 ) {
								if( get_post_meta($post_id, 'tochikukaku', true) !="" ) 
									echo ' '.get_post_meta($post_id, 'tochikukaku', true).'m&sup2;';
							}
							echo '</span>';
					}
				}


				//所在地
				if ( my_custom_kaiin_view('kaiin_shozaichi',$kaiin,$kaiin2) ){

						if($view4=="1"){
							echo '<span class="top_shozaichi">';
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);
							$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichicode_data = myLeft($shozaichicode_data,5);
							$shozaichicode_data = myRight($shozaichicode_data,3);

							if($shozaichiken_data !="" && $shozaichicode_data !=""){
								$sql = "SELECT narrow_area_name FROM ".$wpdb->prefix."area_narrow_area WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

								$sql = $wpdb->prepare($sql , '');
								$metas = $wpdb->get_row( $sql );
								echo "<br />".$metas->narrow_area_name."";
							}
							echo get_post_meta($post_id, 'shozaichimeisho', true);
							echo '</span>';
						}
				}

				//交通路線
				if ( my_custom_kaiin_view('kaiin_kotsu',$kaiin,$kaiin2) ){

						if($view5=="1"){
							echo '<span class="top_kotsu">';
							$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
							$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);

							if($koutsurosen_data !=""){
								$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql , '');
								$metas = $wpdb->get_row( $sql );
								echo "<br />".$metas->rosen_name;
							}

							//交通駅
							if($koutsurosen_data !="" && $koutsueki_data !=""){
								$sql = "SELECT DTS.station_name";
								$sql = $sql . " FROM ".$wpdb->prefix."train_rosen AS DTR";
								$sql = $sql . " INNER JOIN ".$wpdb->prefix."train_station as DTS ON DTR.rosen_id = DTS.rosen_id";
								$sql = $sql . " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql , '');
								$metas = $wpdb->get_row( $sql );
								if($metas->station_name != '＊＊＊＊') 	echo $metas->station_name.'駅';
							}
							echo '</span>';
						}
				}

				/*
				echo '<br />更新日:';
				echo $post_modified_date;
				*/

				echo '<div>';
				if( get_post_meta($post_id, 'kaiin', true) == 1 )
				echo '<span style="float:left;"><img src="'. WP_PLUGIN_URL . '/fudou/img/kaiin_s.jpg" alt="" /></span>';

				echo '<span style="float:right;" class="box1low"><a href="' . $post_url . '">→物件詳細</a></span>';
				echo '</div>';
				echo '</li>';
			}
			echo '</ul>';

			echo '</div>';
			echo $after_widget;


			$top_widget_id = str_replace( '-' , '_' ,$args['widget_id']);

?>

<script type="text/javascript">
//<![CDATA[
	setTimeout('topbukken<?php echo $top_widget_id; ?>()', 1000); 
	function topbukken<?php echo $top_widget_id; ?>() { 
		jQuery.noConflict();
		var jtop$ = jQuery;
		jtop$(function(){
			var sets = [], temp = [];
			jtop$('#<?php echo $args['widget_id']; ?>_1 > li').each(function(i) {
				temp.push(this);
				if (i % 4 == 3) {
					sets.push(temp);
					temp = [];
				};
			});
			if (temp.length) sets.push(temp);
			jtop$.each(sets, function() {
				jtop$(this).flatHeights();
			});
		});
	}

//]]>
</script>

<?php

		}


	}


}


//マイリスト物件表示ウイジェット
add_action('widgets_init', 'fudo_widgetInit_top_ml');
function fudo_widgetInit_top_ml() {
	register_widget('fudo_widget_top_ml');
}

class fudo_widget_top_ml extends WP_Widget {

	/** constructor */
	function fudo_widget_top_ml() {
		parent::WP_Widget(false, $name = 'マイリスト物件表示' , array('description'=>'マイリスト物件表示'));
	}


	/** @see WP_Widget::form */	
	function form($instance) {

		global $is_fudoukaiin;

		$title      = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$item       = isset($instance['item']) ? esc_attr($instance['item']) : '';
		$shubetsu   = isset($instance['shubetsu']) ? esc_attr($instance['shubetsu']) : '';
		$bukken_cat = isset($instance['bukken_cat']) ? esc_attr($instance['bukken_cat']) : '';
		$sort       = isset($instance['sort']) ? esc_attr($instance['sort']) : '';

		$view1 = isset($instance['view1']) ? esc_attr($instance['view1']) : '';
		$view2 = isset($instance['view2']) ? esc_attr($instance['view2']) : '';
		$view3 = isset($instance['view3']) ? esc_attr($instance['view3']) : '';
		$view4 = isset($instance['view4']) ? esc_attr($instance['view4']) : '';
		$view5 = isset($instance['view5']) ? esc_attr($instance['view5']) : '';
		$kaiinview = isset($instance['kaiinview']) ? esc_attr($instance['kaiinview']) : '';

		if($item=='') $item = 4;

		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">
		タイトル <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></label></p>


		<p><label for="<?php echo $this->get_field_id('item'); ?>">
		表示数 <input class="widefat" id="<?php echo $this->get_field_id('item'); ?>" name="<?php echo $this->get_field_name('item'); ?>" type="text" value="<?php echo $item; ?>" /></label></p>

<!--
		<p><label for="<?php echo $this->get_field_id('shubetsu'); ?>">
		種別 <select class="widefat" id="<?php echo $this->get_field_id('shubetsu'); ?>" name="<?php echo $this->get_field_name('shubetsu'); ?>">
			<option value="1"<?php if($shubetsu == 1){echo ' selected="selected"';} ?>>物件すべて</option>
			<option value="2"<?php if($shubetsu == 2){echo ' selected="selected"';} ?>>売買すべて</option>
			<option value="3"<?php if($shubetsu == 3){echo ' selected="selected"';} ?>>売買土地</option>
			<option value="4"<?php if($shubetsu == 4){echo ' selected="selected"';} ?>>売買戸建</option>
			<option value="5"<?php if($shubetsu == 5){echo ' selected="selected"';} ?>>売買マンション</option>
			<option value="6"<?php if($shubetsu == 6){echo ' selected="selected"';} ?>>売買住宅以外の建物全部</option>
			<option value="7"<?php if($shubetsu == 7){echo ' selected="selected"';} ?>>売買住宅以外の建物一部</option>
			<option value="10"<?php if($shubetsu == 10){echo ' selected="selected"';} ?>>賃貸すべて</option>
			<option value="11"<?php if($shubetsu == 11){echo ' selected="selected"';} ?>>賃貸居住用</option>
			<option value="12"<?php if($shubetsu == 12){echo ' selected="selected"';} ?>>賃貸事業用</option>
		</select></label></p>
-->

<!--
		<p><label for="<?php echo $this->get_field_id('bukken_cat'); ?>">
		物件カテゴリ <select class="widefat" id="<?php echo $this->get_field_id('bukken_cat'); ?>" name="<?php echo $this->get_field_name('bukken_cat'); ?>">
			<option value="0"<?php if($bukken_cat == 0){echo ' selected="selected"';} ?>>すべて</option>
<?php

		//物件カテゴリ
		$terms = get_terms('bukken', 'hide_empty=0');

		if ( !empty( $terms ) ){
			foreach ( $terms as $term ) {
				echo  '<option value="'.$term->term_id.'"';
				if( $bukken_cat == $term->term_id )	echo ' selected="selected"';
				echo '>'.$term->name.'</option>';
			}
		}
?>
		</select></label></p>
-->


<!--
		<p><label for="<?php echo $this->get_field_id('sort'); ?>">
		並び順 <select class="widefat" id="<?php echo $this->get_field_id('sort'); ?>" name="<?php echo $this->get_field_name('sort'); ?>">
			<option value="2"<?php if($sort == 2){echo ' selected="selected"';} ?>>新しい順</option>
			<option value="3"<?php if($sort == 3){echo ' selected="selected"';} ?>>古い順</option>
			<option value="4"<?php if($sort == 4){echo ' selected="selected"';} ?>>高い順</option>
			<option value="5"<?php if($sort == 5){echo ' selected="selected"';} ?>>安い順</option>
			<option value="6"<?php if($sort == 6){echo ' selected="selected"';} ?>>広い順(面積)</option>
			<option value="7"<?php if($sort == 7){echo ' selected="selected"';} ?>>狭い順(面積)</option>
			<option value="1"<?php if($sort == 1){echo ' selected="selected"';} ?>>ランダム</option>
		</select></label></p>

-->
		表示項目<br />
		<p><label for="<?php echo $this->get_field_id('view1'); ?>">
		タイトル <select class="widefat" id="<?php echo $this->get_field_id('view1'); ?>" name="<?php echo $this->get_field_name('view1'); ?>">
			<option value="1"<?php if($view1 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view1 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>




		<p><label for="<?php echo $this->get_field_id('view2'); ?>">
		価格 <select class="widefat" id="<?php echo $this->get_field_id('view2'); ?>" name="<?php echo $this->get_field_name('view2'); ?>">
			<option value="1"<?php if($view2 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view2 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

		<p><label for="<?php echo $this->get_field_id('view3'); ?>">
		間取り・土地面積 <select class="widefat" id="<?php echo $this->get_field_id('view3'); ?>" name="<?php echo $this->get_field_name('view3'); ?>">
			<option value="1"<?php if($view3 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view3 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

		<p><label for="<?php echo $this->get_field_id('view4'); ?>">
		地域 <select class="widefat" id="<?php echo $this->get_field_id('view4'); ?>" name="<?php echo $this->get_field_name('view4'); ?>">
			<option value="1"<?php if($view4 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view4 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

		<p><label for="<?php echo $this->get_field_id('view5'); ?>">
		路線・駅 <select class="widefat" id="<?php echo $this->get_field_id('view5'); ?>" name="<?php echo $this->get_field_name('view5'); ?>">
			<option value="1"<?php if($view5 == 1){echo ' selected="selected"';} ?>>表示する</option>
			<option value="2"<?php if($view5 == 2){echo ' selected="selected"';} ?>>表示しない</option>
		</select></label></p>

	<?php if($is_fudoukaiin && get_option('kaiin_users_can_register') == '1' ){ ?>
		<p><label for="<?php echo $this->get_field_id('kaiinview'); ?>">
		会員物件 <select class="widefat" id="<?php echo $this->get_field_id('kaiinview'); ?>" name="<?php echo $this->get_field_name('kaiinview'); ?>">
			<option value="1"<?php if($kaiinview == 1){echo ' selected="selected"';} ?>>会員・一般物件を表示する</option>
			<option value="2"<?php if($kaiinview == 2){echo ' selected="selected"';} ?>>会員物件を表示しない</option>
			<option value="3"<?php if($kaiinview == 3){echo ' selected="selected"';} ?>>会員物件だけ表示</option>
		</select></label></p>

	<?php } ?>

		<?php 
	}


	/** @see WP_Widget::update */
	function update($new_instance, $old_instance) {
		return $new_instance;
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance) {

		global $is_fudoukaiin;
		global $wpdb;

		// outputs the content of the widget

	        extract( $args );
		$title = @$instance['title'];
		$item =  @$instance['item'];
		$shubetsu = @$instance['shubetsu'];
		$bukken_cat = @$instance['bukken_cat'];

		$sort = @$instance['sort'];
		$view1 = @$instance['view1'];
		$view2 = @$instance['view2'];
		$view3 = @$instance['view3'];
		$view4 = @$instance['view4'];
		$view5 = @$instance['view5'];

		$kaiinview = $instance['kaiinview'];
		if($kaiinview == '') $kaiinview = 1;

		if( !$is_fudoukaiin || get_option('kaiin_users_can_register') != '1' ){
			$kaiinview = 1;
		}


		if($item =="") 	$item=4;

		$newup_mark = get_option('newup_mark');
		if($newup_mark == '') $newup_mark=14;


		//
		switch ($shubetsu) {
			case 1 :	//物件すべて
				$where_data = "";
				break;
			case 2 :	//売買すべて
				$where_data = " AND CAST( PM_1.meta_value AS SIGNED )<3000";
				break;
			case 3 :	//売買土地
				$where_data = " AND Left(PM_1.meta_value,2) ='11'";
				break;
			case 4 :	//売買戸建
				$where_data = " AND Left(PM_1.meta_value,2) ='12'";
				break;
			case 5 :	//売買マンション
				$where_data = " AND Left(PM_1.meta_value,2) ='13'";
				break;
			case 6 :	//売住宅以外の建物全部
				$where_data = " AND Left(PM_1.meta_value,2) ='14'";
				break;
			case 7 :	//売住宅以外の建物一部
				$where_data = " AND Left(PM_1.meta_value,2) ='15'";
				break;

			case 10 :	//賃貸すべて
				$where_data = " AND  CAST( PM_1.meta_value AS SIGNED )>3000";
				break;
			case 11 :	//賃貸居住用
				$where_data = " AND Left(PM_1.meta_value,2) ='31'";
				break;
			case 12 :	//賃貸事業用
				$where_data = " AND Left(PM_1.meta_value,2) ='32'";
				break;

			default:
				$order_data = "";
				break;
		}


		//sort
		switch ($sort) {
			case 1 :	//ランダム
				$order_data = " rand()";
				break;
			case 2 :	//新しい順
				$order_data = " P.post_date DESC";
				break;
			case 3 :	//古い順
				$order_data = " P.post_date ASC";
				break;
			case 4 :	//高い順
				$order_data = " CAST( PM_2.meta_value AS SIGNED ) DESC";
				break;
			case 5 :	//安い順
				$order_data = " CAST( PM_2.meta_value AS SIGNED ) ASC";
				break;
			case 6 :	//広い順
				$order_data = " CAST( PM_3.meta_value AS SIGNED ) DESC";
				break;
			case 7 :	//狭い順
				$order_data = " CAST( PM_3.meta_value AS SIGNED ) ASC";
				break;
			default:
				$order_data = " rand()";
				break;
		}



		//SQL
		
$sql = '';

$mylist = $_COOKIE['fudo-mylist-list'];
$mylist_a = explode (',' , $mylist);
$w = array();
foreach ($mylist_a as $i){
	$w[] = "'".$i."'";
}

$mylist_where = "WHERE `ID` IN(".implode (' , ' , $w).")";

			$sql = "
				SELECT * 
				FROM $wpdb->posts
				";

		$mylimit = " LIMIT 0 , $item";

		$sql = $sql .' '.$mylist_where.$mylimit;

		$sql = $wpdb->prepare($sql , '');
		$metas = $wpdb->get_results( $sql, ARRAY_A );


		//ユーザー別会員物件リスト
		$kaiin_users_rains_register = get_option('kaiin_users_rains_register');

//$after_title='<div class="fudo-fav-box"><span class="fudo-fav-box-link"><a href="'.get_bloginfo('url').'/?page_id='.get_option('fudo-mylist-mylist').'" title="マイ・リスト一覧"></a></span></div>';


		

			echo $before_widget;

			//$after_title = '<div class="fudo-fav-box" style="float:right;"><span class="fudo-fav-box-link"><a href="'.get_bloginfo('url').'/?page_id='.get_option('fudo-mylist-mylist').'" title="マイ・リスト一覧"></a></span></div><span style="clear:both;"></span></h3>';

			$btn = '<a class="fudo-fav-box-widget-link" style="" href="'.get_bloginfo('url').'/?page_id='.get_option('fudo-mylist-mylist').'" title="マイ・リスト一覧"></a>';

			//$after_title = "</h3>";

			$before_title = '<h3  style="position:relative;">';
			if ( $title ){
				echo $before_title .'<span>' .$title .$btn.'</span>'. $after_title; 
			}


			$img_path = get_option('upload_path');
			if ($img_path == '')
				$img_path = 'wp-content/uploads';

			echo '<div id="box'.$args['widget_id'].'">';
			echo '<ul id="'.$args['widget_id'].'_1" class="grid-content">';
if(!empty($metas)) {
			foreach ( $metas as $meta ) {

				$post_id =  $meta['ID'];
				$post_title =  $meta['post_title'];
				$post_url =  str_replace('&p=','&amp;p=',get_permalink($post_id));

				//newup_mark
				$post_modified =  $meta['post_modified'];
				$post_date =   $meta['post_date'];
				$post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($post_modified, "%d-%d-%d"));
				$post_date =  vsprintf("%d-%02d-%02d", sscanf($post_date, "%d-%d-%d"));

				$newup_mark_img=  '';
				if( $newup_mark != 0 && is_numeric($newup_mark) ){
					if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){
						if($post_modified_date == $post_date ){
							$newup_mark_img = '<div class="new_mark">new</div>';
						}else{
							$newup_mark_img =  '<div class="new_mark">up</div>';
						}
					}
				}


				//会員
				$kaiin = 0;
				if( !is_user_logged_in() && get_post_meta($post_id, 'kaiin', true) == 1 ) $kaiin = 1;

				//ユーザー別会員物件リスト
				$kaiin2 = users_kaiin_bukkenlist($post_id,$kaiin_users_rains_register,get_post_meta($post_id, 'kaiin', true));


				echo '<li class="'.$args['widget_id'].' box1">';

				echo $newup_mark_img;

				//会員項目表示判定
				if ( !my_custom_kaiin_view('kaiin_gazo',$kaiin,$kaiin2) ){
					echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/kaiin.jpg" alt="" />';
				}else{

						//サムネイル画像
						$fudoimg1_data = get_post_meta($post_id, 'fudoimg1', true);
						if($fudoimg1_data != '')	$fudoimg_data=$fudoimg1_data;
						$fudoimg2_data = get_post_meta($post_id, 'fudoimg2', true);
						if($fudoimg2_data != '')	$fudoimg_data=$fudoimg2_data;
						$fudoimg_alt = str_replace("　"," ",$post_title);

						echo '<a href="' . $post_url . '">';

						if($fudoimg_data !="" ){

							$sql  = "";
							$sql .=  "SELECT P.ID,P.guid";
							$sql .=  " FROM $wpdb->posts as P";
							$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
						//	$sql = $wpdb->prepare($sql);
							$metas = $wpdb->get_row( $sql );

							$attachmentid = '';
							if ( $metas != '' ){
								$attachmentid  =  $metas->ID;
								$guid_url  =  $metas->guid;
							}

							if($attachmentid !=''){
								//thumbnail、medium、large、full 
								$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');
								$fudoimg_url = $fudoimg_data1[0];

								if($fudoimg_url !=''){
									echo '<img class="box1image" src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" />';
								}else{
									echo '<img class="box1image" src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" />';
								}
							}else{
								echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'.$fudoimg_data.'" />';
							}

						}else{
						//	echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" style="width:150px; height:150px;"  alt="" />';
							echo '<img class="box1image" src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" />';
						}
						echo "</a>\n";
				}


				//抜粋
				//$_post = & get_post( intval($post_id) ); 
				//echo $_post->post_excerpt; 


				//タイトル
				if ( my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){
					if($view1=="1" && $post_title !=''){
							echo '<span class="top_title">';
							echo str_replace("　"," ",$post_title).'';
							echo '</span><br />';
					}
				}


				//価格
				if ( my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){

						if($view2=="1"){
							echo '<span class="top_price">';
							if( get_post_meta($post_id, 'seiyakubi', true) != "" ){
								echo 'ご成約済　';
							}else{
								//非公開の場合
								if(get_post_meta($post_id,'kakakukoukai',true) == "0"){
									$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
									if($kakakujoutai_data_data=="1")	echo '相談　';
									if($kakakujoutai_data_data=="2")	echo '確定　';
									if($kakakujoutai_data_data=="3")	echo '入札　';

								}else{
									$kakaku_data = get_post_meta($post_id,'kakaku',true);
									if(is_numeric($kakaku_data)){
										echo floatval($kakaku_data)/10000;
										echo "万円 ";
									}
								}
							}
							echo '</span>';
						}
				}


				//間取り・土地面積
				if($view3=="1"){

					//間取り
					if ( my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2) ){
							echo '<span class="top_madori">';
							$madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);
							echo get_post_meta($post_id,'madorisu',true);
							if($madorisyurui_data=="10")	echo 'R ';
							if($madorisyurui_data=="20")	echo 'K ';
							if($madorisyurui_data=="25")	echo 'SK ';
							if($madorisyurui_data=="30")	echo 'DK ';
							if($madorisyurui_data=="35")	echo 'SDK ';
							if($madorisyurui_data=="40")	echo 'LK ';
							if($madorisyurui_data=="45")	echo 'SLK ';
							if($madorisyurui_data=="50")	echo 'LDK ';
							if($madorisyurui_data=="55")	echo 'SLDK ';
							echo '</span>';
					}


					//土地面積
					if ( my_custom_kaiin_view('kaiin_menseki',$kaiin,$kaiin2) ){
							echo '<span class="top_menseki">';
							if ( get_post_meta($post_id,'bukkenshubetsu',true) < 1200 ) {
								if( get_post_meta($post_id, 'tochikukaku', true) !="" ) 
									echo ' '.get_post_meta($post_id, 'tochikukaku', true).'m&sup2;';
							}
							echo '</span>';
					}
				}


				//所在地
				if ( my_custom_kaiin_view('kaiin_shozaichi',$kaiin,$kaiin2) ){

						if($view4=="1"){
							echo '<span class="top_shozaichi">';
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);
							$shozaichicode_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichicode_data = myLeft($shozaichicode_data,5);
							$shozaichicode_data = myRight($shozaichicode_data,3);

							if($shozaichiken_data !="" && $shozaichicode_data !=""){
								$sql = "SELECT narrow_area_name FROM ".$wpdb->prefix."area_narrow_area WHERE middle_area_id=".$shozaichiken_data." and narrow_area_id =".$shozaichicode_data."";

								$sql = $wpdb->prepare($sql , '');
								$metas = $wpdb->get_row( $sql );
								echo "<br />".$metas->narrow_area_name."";
							}
							echo get_post_meta($post_id, 'shozaichimeisho', true);
							echo '</span>';
						}
				}

				//交通路線
				if ( my_custom_kaiin_view('kaiin_kotsu',$kaiin,$kaiin2) ){

						if($view5=="1"){
							echo '<span class="top_kotsu">';
							$koutsurosen_data = get_post_meta($post_id, 'koutsurosen1', true);
							$koutsueki_data = get_post_meta($post_id, 'koutsueki1', true);
							$shozaichiken_data = get_post_meta($post_id,'shozaichicode',true);
							$shozaichiken_data = myLeft($shozaichiken_data,2);

							if($koutsurosen_data !=""){
								$sql = "SELECT `rosen_name` FROM `".$wpdb->prefix."train_rosen` WHERE `rosen_id` =".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql , '');
								$metas = $wpdb->get_row( $sql );
								echo "<br />".$metas->rosen_name;
							}

							//交通駅
							if($koutsurosen_data !="" && $koutsueki_data !=""){
								$sql = "SELECT DTS.station_name";
								$sql = $sql . " FROM ".$wpdb->prefix."train_rosen AS DTR";
								$sql = $sql . " INNER JOIN ".$wpdb->prefix."train_station as DTS ON DTR.rosen_id = DTS.rosen_id";
								$sql = $sql . " WHERE DTS.station_id=".$koutsueki_data." AND DTS.rosen_id=".$koutsurosen_data."";
								$sql = $wpdb->prepare($sql , '');
								$metas = $wpdb->get_row( $sql );
								if($metas->station_name != '＊＊＊＊') 	echo $metas->station_name.'駅';
							}
							echo '</span>';
						}
				}

				/*
				echo '<br />更新日:';
				echo $post_modified_date;
				*/

				echo '<div>';
				if( get_post_meta($post_id, 'kaiin', true) == 1 )
				echo '<span style="float:left;"><img src="'. WP_PLUGIN_URL . '/fudou/img/kaiin_s.jpg" alt="" /></span>';

				echo '<span style="float:right;" class="box1low"><a href="' . $post_url . '">→物件詳細</a></span>';
				echo '</div>';
				echo '</li>';
			}
		}else{
			echo '<p>マイリストにはまだ登録されていません</p>';
		}
			echo '</ul>';

			echo '</div>';
			echo $after_widget;


			$top_widget_id = str_replace( '-' , '_' ,$args['widget_id']);

?>



<script type="text/javascript">
//<![CDATA[
	setTimeout('topbukken<?php echo $top_widget_id; ?>()', 1000); 
	function topbukken<?php echo $top_widget_id; ?>() { 
		jQuery.noConflict();
		var jtop$ = jQuery;
		jtop$(function(){
			var sets = [], temp = [];
			jtop$('#<?php echo $args['widget_id']; ?>_1 > li').each(function(i) {
				temp.push(this);
				if (i % 4 == 3) {
					sets.push(temp);
					temp = [];
				};
			});
			if (temp.length) sets.push(temp);
			jtop$.each(sets, function() {
				jtop$(this).flatHeights();
			});
		});
	}

//]]>
</script>

<?php

		


	}


}


//actions

//統計情報クリア

	if(isset($_POST['action'])&&($_POST['action']=='mylist-clrflg'))
	{
		global $wpdb;
		$sql = "DELETE FROM $wpdb->postmeta WHERE `meta_key` = 'mylist-count'";
		$wpdb->query($sql);
		wp_redirect(get_bloginfo('url').'/wp-admin/tools.php?page=fudo-mylist/fudo-mylist.php');
	}

