<?php
//require_once( dirname(__FILE__) . '/wp-blog-header.php' );
require_once( '../../../wp-blog-header.php' );
require_once (ABSPATH . WPINC . '/pluggable.php');

/* ajax */
if(isset($_GET['mylist']) && ($_GET['mylist']!='')){

	$mode = $_GET['mylist'];//add remove
	$pid = $_GET['mypageid'];//post id

	if($mode == 'add'){
		addMylistCount($pid);
	}else if($mode == 'remove'){
		removeMylistCount($pid);
	}

}

/* funcs */

function addMylistCount($pid){

//retrieve count
	$cnt = get_post_meta($pid , 'mylist-count' , true);

//plus

	$newcnt = $cnt + 1;

//save
	$res = update_post_meta($pid , 'mylist-count' , $newcnt);

}

function removeMylistCount($pid){

//retrieve count
	$cnt = get_post_meta($pid , 'mylist-count' , true);
//minus
	if($cnt[0]>0){
		$newcnt = $cnt[0] - 1;
	}
//save
	update_post_meta($pid , 'mylist-count' , $newcnt);

}

