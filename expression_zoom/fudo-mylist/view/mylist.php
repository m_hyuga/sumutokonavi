<?php
global $wpdb;

//会員
$kaiin = 0;
if( !is_user_logged_in() && get_post_meta($meta_id, 'kaiin', true) == 1 ) $kaiin = 1;
//ログインしていればtrue

//ユーザー別会員物件リスト
$kaiin_users_rains_register = get_option('kaiin_users_rains_register');
//1||0

//$kaiin2 = '';
$kaiin2 = users_kaiin_bukkenlist($meta_id,$kaiin_users_rains_register, get_post_meta($meta_id, 'kaiin', true) );


//my_custom_kaiin_view($post_id,$kaiin,$kaiin2) 
//trueなら公開 falseなら会員のみ公開


	$_post = & get_post( intval($meta_id) ); 

	//newup_mark
	$post_modified_date =  vsprintf("%d-%02d-%02d", sscanf($_post->post_modified, "%d-%d-%d"));
	$post_date =  vsprintf("%d-%02d-%02d", sscanf($_post->post_date, "%d-%d-%d"));

	$newup_mark = get_option('newup_mark');
	if($newup_mark == '') $newup_mark=14;

	$newup_mark_img=  '';
	if( $newup_mark != 0 && is_numeric($newup_mark) ){

		if( ( abs(strtotime($post_modified_date) - strtotime(date("Y/m/d"))) / (60 * 60 * 24) ) < $newup_mark ){
			if($post_modified_date == $post_date ){
				$newup_mark_img = '<span class="new_mark">new</span>';
			}else{
				$newup_mark_img =  '<span class="new_mark">up</span>';
			}
		}
	}



?>


<div class="list_simple_boxtitle">
	<h2 class="entry-title">

	<?php if( get_post_meta($meta_id, 'kaiin', true) == 1 ) { ?>
		<span style="float:right;margin:0px"><img src="<?php echo get_option('siteurl'); ?>/wp-content/plugins/fudou/img/kaiin_s.jpg" alt="" /></span>
	<?php } ?>


	<?php if ( !my_custom_kaiin_view('kaiin_title',$kaiin,$kaiin2) ){ ?>
			<a href="<?php echo get_permalink($meta_id); ?>" title="">会員物件<?php echo $newup_mark_img; ?></a>
	<?php }else{ ?>
		<a href="<?php echo get_permalink($meta_id); ?>" title="<?php echo $meta_title; ?>" rel="bookmark"><?php echo $meta_title; ?>
			<?php if ( my_custom_kaiin_view('kaiin_shikibesu',$kaiin,$kaiin2) ){ ?>
		<span class="shikibesu">(<?php echo get_post_meta($meta_id, 'shikibesu', true);?>)</span>
			<?php } ?>

		
		<?php echo $newup_mark_img; ?></a>
	<?php } ?>
	</h2>
</div>


<div class="list_simple_box">
	<div class="entry-excerpt">
<?php
	if ( my_custom_kaiin_view('kaiin_excerpt',$kaiin,$kaiin2) ){
		echo $_post->post_excerpt; 
	}
?>
	</div>

	<!-- ここから左ブロック --> 
	<div class="list_picsam">

	<!-- 価格 -->
		<div class="dpoint1">
<?php 
			//価格
			if ( !my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){
				echo "会員物件";
			}else{
				if( get_post_meta($meta_id, 'seiyakubi', true) != "" ){ echo 'ご成約済'; }else{  mylist_custom_kakaku_print($meta_id); } 
			} 
?>
		</div>
		<div class="dpoint2">
			<div class="d_madori">
<?php
			//間取り
			if ( my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2) ){
				mylist_custom_madorisu_print($meta_id);
			}
?>
			</div>
			<div class="d_menseki">
<?php
			//面積
			if ( my_custom_kaiin_view('kaiin_menseki',$kaiin,$kaiin2) ){
				if(get_post_meta($meta_id, 'bukkenshubetsu', true) < 1200 ){
					if( get_post_meta($meta_id, 'tochikukaku', true) !="" ) echo '&nbsp;'.get_post_meta($meta_id, 'tochikukaku', true).'m&sup2; ';
				}else{
					if( get_post_meta($meta_id, 'tatemonomenseki', true) !="" ) echo '&nbsp;'.get_post_meta($meta_id, 'tatemonomenseki', true).'m&sup2; ';
				}
			}
?>
			</div>
		</div><br style="clear:both" />


	<!-- 画像 -->


<?php
		

		if ( !my_custom_kaiin_view('kaiin_gazo',$kaiin,$kaiin2) ){
			echo '<div class="picbox"><img src="'.WP_PLUGIN_URL.'/fudou/img/kaiin.jpg" alt="" /></div>';
			echo '<div class="picbox"><img src="'.WP_PLUGIN_URL.'/fudou/img/kaiin.jpg" alt="" /></div>';
		}else{
				//サムネイル画像
				$img_path = get_option('upload_path');
				if ($img_path == '')	$img_path = 'wp-content/uploads';
				/*1枚のみ表示*/
				for( $imgid=1; $imgid<=1; $imgid++ ){

					$fudoimg_data = get_post_meta($meta_id, "fudoimg$imgid", true);
					$fudoimgcomment_data = get_post_meta($meta_id, "fudoimgcomment$imgid", true);
					$fudoimg_alt = $fudoimgcomment_data . mylist_custom_fudoimgtype_print(get_post_meta($meta_id, "fudoimgtype$imgid", true));
					$add_url = '';

					if($fudoimg_data !="" ){

						$sql  = "";
						$sql .=  "SELECT P.ID,P.guid";
						$sql .=  " FROM $wpdb->posts as P";
						$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
					//	$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%$fudoimg_data' ";
					//	$sql = $wpdb->prepare($sql);
						$metas = $wpdb->get_row( $sql );
						$attachmentid  =  $metas->ID;
						$guid_url  =  $metas->guid;
						echo '<div class="picbox">';
						if($attachmentid !=''){
							//thumbnail、medium、large、full 
							$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'thumbnail');
							$fudoimg_url = $fudoimg_data1[0];

							echo '<a href="' . get_permalink($meta_id).$add_url . '" rel="lightbox['.$meta_id.'] lytebox['.$meta_id.']" title="'.$fudoimg_alt.'">';
							if($fudoimg_url !=''){
								echo '<img src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" /></a>';
							}else{
								echo '<img src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'"  />';
							}
						}else{
							echo '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'.$fudoimg_data.'" />';
						}
							echo '</div>';


					}else{
						echo '<div class="picbox">';
						echo '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="" />';
						echo '</div>';
					}
				}
		}

	?>
	<!-- 詳細ボタンエリア -->
		<a href="<?php echo get_permalink($meta_id).$add_url; ?>" title="<?php echo $meta_title; ?>" rel="bookmark"><div class="list_details_button">→物件の詳細を見る</div></a>
	</div>

	<!-- ここから右ブロック -->   
	<div class="list_detail">
		<dl class="list_price<?php if( get_post_meta($meta_id,'bukkenshubetsu',true) > 3000 ) echo ' rent'; ?>">
			<table width="100%">
				<tr>
					<td>
						<dt><?php if( get_post_meta($meta_id,'bukkenshubetsu',true) < 3000 ) { echo '価格';}else{echo '賃料';} ?></dt>
						<dd><div class="dpoint3">
							<span class="price">
<?php 
							if ( !my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){
								echo "--";
							}else{
								if( get_post_meta($meta_id, 'seiyakubi', true) != "" ){ echo '--'; }else{
								mylist_custom_kakaku_print($meta_id); }
							}
?>
							</span>

			<?php if ( my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){ ?>


								<?php if(get_post_meta($meta_id, 'kakakukyouekihi', true) !=""){?>
								<?php echo '(共・管'.get_post_meta($meta_id, 'kakakukyouekihi', true).'円)'; ?>
								<?php } ?>

								<?php if(get_post_meta($meta_id, 'kakakushikikin', true) !=""){?>
								敷:<?php mylist_custom_kakakushikikin_print($meta_id); ?>
								<?php } ?>


								<?php if(get_post_meta($meta_id, 'kakakureikin', true) !=""){?>
								&nbsp;礼:<?php mylist_custom_kakakureikin_print($meta_id); ?>
								<?php } ?>
<!--
								<?php if(get_post_meta($meta_id, 'kakakuhoshoukin', true) !=""){?>
								<dt>保証金</dt><dd><?php mylist_custom_kakakuhoshoukin_print($meta_id); ?></dd>
								<?php } ?>

								<?php if(get_post_meta($meta_id, 'kakakukenrikin', true) !=""){?>
								<dt>権利金</dt><dd><?php mylist_custom_kakakukenrikin_print($meta_id); ?></dd>
								<?php } ?>

								<?php if(get_post_meta($meta_id, 'kakakushikibiki', true) !=""){?>
								<dt>償却・敷引金</dt><dd><?php mylist_custom_kakakushikibiki_print($meta_id); ?></dd>
								<?php } ?> 
-->
				<?php } ?>


						</div></dd>

						<dd><?php mylist_custom_bukkenshubetsu_print($meta_id); ?></dd>
<?php 
							if (my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2) ){
								if( get_post_meta($meta_id, 'madorisu', true) !=""){ 
?>
										<dt>間取</dt><dd><div class="dpoint3"><?php mylist_custom_madorisu_print($meta_id); ?></div></dd>
<?php
								} 
							} 
?>

	
					</td>
				</tr>

	
			</table>
		</dl>


		<dl class="list_address">
			<table width="100%">
				<?php if ( my_custom_kaiin_view('kaiin_shozaichi',$kaiin,$kaiin2) ){ ?>
						<tr>
							<td><dt>所在地</dt></td>
							<td width="90%"><dd><?php mylist_custom_shozaichi_print($meta_id); ?>
							<?php echo get_post_meta($meta_id, 'shozaichimeisho', true);?></dd></td>
						</tr>
				<?php } ?>

				<?php if ( my_custom_kaiin_view('kaiin_kotsu',$kaiin,$kaiin2) ){ ?>
						<tr>
							<td><dt>交通</dt></td>
							<td width="90%"><dd><?php mylist_custom_koutsu1_print($meta_id); ?>
							<?php mylist_custom_koutsu2_print($meta_id); ?>
							<?php if(get_post_meta($meta_id, 'koutsusonota', true) !='') echo '<br />'.get_post_meta($meta_id, 'koutsusonota', true).'';?></dd></td>
						</tr>
				<?php } ?>
			</table>
		</dl>


		<dl class="list_price_others">

			<table width="100%">

				<?php if ( my_custom_kaiin_view('kaiin_tikunen',$kaiin,$kaiin2) ){ ?>
						<?php if(get_post_meta($meta_id, 'tatemonochikunenn', true) !="" || get_post_meta($meta_id, 'tatemonokozo', true) !="" ){ ?>
						<tr>
							<td colspan="2"><dt>築年月</dt><dd><?php echo get_post_meta($meta_id, 'tatemonochikunenn', true);?></dd></td>
							<td><dd class="oldnew"><?php mylist_custom_tatemonoshinchiku_print($meta_id); ?></dd></td>

						</tr>
						<tr>
							<td colspan="3">
							<dt>構造</dt><dd><?php mylist_custom_tatemonokozo_print($meta_id) ?></dd></td>

						</tr>

						<?php } ?>
				<?php } ?>


				<tr>
					<td colspan="3"><dt>面積</dt><dd>
					<?php if ( my_custom_kaiin_view('kaiin_menseki',$kaiin,$kaiin2) ){ ?>
							<?php if( get_post_meta($meta_id, 'tatemonomenseki', true) !="" ) echo '建物:'.get_post_meta($meta_id, 'tatemonomenseki', true).'m&sup2;';?>
							<?php if( get_post_meta($meta_id, 'tochikukaku', true) !="" ) echo '区画:'.get_post_meta($meta_id, 'tochikukaku', true).'m&sup2; ';?></dd>
					<?php } ?>


					<dt>階数</dt><dd>
					<?php if ( my_custom_kaiin_view('kaiin_kaisu',$kaiin,$kaiin2) ){ ?>
							<?php if(get_post_meta($meta_id, 'tatemonokaisu1', true)!="") echo '地上 '.get_post_meta($meta_id, 'tatemonokaisu1', true).'階 ' ;?>
							<?php if(get_post_meta($meta_id, 'tatemonokaisu2', true)!="") echo '地下 '.get_post_meta($meta_id, 'tatemonokaisu2', true).'階' ;?></dd>
					<?php } ?>
					</td>
				</tr>

			</table>

		<!-- 更新日
		<div align="right"><?php echo $post_modified_date; ?> UpDate</div>
		-->
			<?php if( $kaiin == 1 ) { ?>
				<br />
				この物件は、「会員様にのみ限定公開」している物件です。<br />
				非公開物件につき、詳細情報の閲覧には会員ログインが必要です。<br />
				非公開物件を閲覧・資料請求するには会員登録(無料)が必要です。
			<?php } else { ?>


<?php
				//ユーザー別会員物件リスト
				if (!$kaiin2 && get_option('kaiin_users_rains_register') == 1 && get_post_meta($meta_id, 'kaiin', true) == 1 ) {
					echo 'この物件は、「閲覧条件」に 該当していない物件です。<br />';
					echo '閲覧条件を変更をする事で閲覧が可能になります。<br />';
					echo '<div align="center">';
					echo '<div id="maching_mail"><a href="'.WP_PLUGIN_URL.'/fudoumail/fudou_user.php?KeepThis=true&TB_iframe=true&height=500&width=680" class="thickbox">';
					echo '閲覧条件・メール設定</a></div>';
					echo '</div>';
				}
			}

?>
		</dl>
	</div>

</div><!-- end list_simple_box -->
