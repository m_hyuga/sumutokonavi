<?php

//共通テンプレート読み込み
define( 'COMMONPATH', ABSPATH . '../common/' );
if(file_exists(COMMONPATH . "common.php")){

	require(COMMONPATH . "common.php");
}

/**
 * Twenty Twelve functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

/**
 * Sets up the content width value based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 625;

/**
 * Sets up theme defaults and registers the various WordPress features that
 * Twenty Twelve supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_setup() {
	/*
	 * Makes Twenty Twelve available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Twelve, use a find and replace
	 * to change 'twentytwelve' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentytwelve', get_stylesheet_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme supports a variety of post formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'twentytwelve' ) );

	/*
	 * This theme supports custom background color and image, and here
	 * we also set up the default background color.
	 */
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 183, 183 ); // Unlimited height, soft crop
}

add_action( 'after_setup_theme', 'twentytwelve_setup' );

/**
 * Adds support for a custom header image.
 */
require( get_template_directory() . '/inc/custom-header.php' );

/**
 * Enqueues scripts and styles for front-end.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_scripts_styles() {
	global $wp_styles;

	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/*
	 * Adds JavaScript for handling the navigation menu hide-and-show behavior.
	 */
	wp_enqueue_script( 'twentytwelve-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );

	/*
	 * Loads our special font CSS file.
	 *
	 * The use of Open Sans by default is localized. For languages that use
	 * characters not supported by the font, the font can be disabled.
	 *
	 * To disable in a child theme, use wp_dequeue_style()
	 * function mytheme_dequeue_fonts() {
	 *     wp_dequeue_style( 'twentytwelve-fonts' );
	 * }
	 * add_action( 'wp_enqueue_scripts', 'mytheme_dequeue_fonts', 11 );
	 */

	/* translators: If there are characters in your language that are not supported
	   by Open Sans, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'twentytwelve' ) ) {
		$subsets = 'latin,latin-ext';

		/* translators: To add an additional Open Sans character subset specific to your language, translate
		   this to 'greek', 'cyrillic' or 'vietnamese'. Do not translate into your own language. */
		$subset = _x( 'no-subset', 'Open Sans font: add new subset (greek, cyrillic, vietnamese)', 'twentytwelve' );

		if ( 'cyrillic' == $subset )
			$subsets .= ',cyrillic,cyrillic-ext';
		elseif ( 'greek' == $subset )
			$subsets .= ',greek,greek-ext';
		elseif ( 'vietnamese' == $subset )
			$subsets .= ',vietnamese';

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => 'Open+Sans:400italic,700italic,400,700',
			'subset' => $subsets,
		);
		wp_enqueue_style( 'twentytwelve-fonts', add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" ), array(), null );
	}

	/*
	 * Loads our main stylesheet.
	 */
	//wp_enqueue_style( 'twentytwelve-style', get_stylesheet_uri() );

	/*
	 * Loads the Internet Explorer specific stylesheet.
	 */
	wp_enqueue_style( 'twentytwelve-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentytwelve-style' ), '20121010' );
	$wp_styles->add_data( 'twentytwelve-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'twentytwelve_scripts_styles' );

/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @since Twenty Twelve 1.0
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string Filtered title.
 */
function twentytwelve_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'twentytwelve_wp_title', 10, 2 );

/**
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'twentytwelve_page_menu_args' );

/**
 * Registers our main widget area and the front page widget areas.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'twentytwelve' ),
		'id' => 'sidebar-1',
		'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'twentytwelve' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'First Front Page Widget Area', 'twentytwelve' ),
		'id' => 'sidebar-2',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'twentytwelve' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Second Front Page Widget Area', 'twentytwelve' ),
		'id' => 'sidebar-3',
		'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'twentytwelve' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'twentytwelve_widgets_init' );

if ( ! function_exists( 'twentytwelve_content_nav' ) ) :
/**
 * Displays navigation to next/previous pages when applicable.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_content_nav( $html_id ) {
	global $wp_query;

	$html_id = esc_attr( $html_id );
/*
	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="<?php echo $html_id; ?>" class="navigation" role="navigation">
			<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentytwelve' ); ?></h3>
			<div class="nav-previous alignleft"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentytwelve' ) ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?></div>
		</nav><!-- #<?php echo $html_id; ?> .navigation -->
	<?php endif;
*/
}
endif;

if ( ! function_exists( 'twentytwelve_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentytwelve_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'twentytwelve' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<header class="comment-meta comment-author vcard">
				<?php
					echo get_avatar( $comment, 44 );
					printf( '<cite class="fn">%1$s %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span> ' . __( 'Post author', 'twentytwelve' ) . '</span>' : ''
					);
					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'twentytwelve' ), get_comment_date(), get_comment_time() )
					);
				?>
			</header><!-- .comment-meta -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentytwelve' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( __( 'Edit', 'twentytwelve' ), '<p class="edit-link">', '</p>' ); ?>
			</section><!-- .comment-content -->

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'twentytwelve' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

if ( ! function_exists( 'twentytwelve_entry_meta' ) ) :
/**
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own twentytwelve_entry_meta() to override in a child theme.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_entry_meta() {
	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'twentytwelve' ) );

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'twentytwelve' ) );

	$date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$author = sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'twentytwelve' ), get_the_author() ) ),
		get_the_author()
	);

	// Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.
	if ( $tag_list ) {
		$utility_text = __( 'This entry was posted in %1$s and tagged %2$s on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );
	} elseif ( $categories_list ) {
		$utility_text = __( 'This entry was posted in %1$s on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );
	} else {
		$utility_text = __( 'This entry was posted on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );
	}

	printf(
		$utility_text,
		$categories_list,
		$tag_list,
		$date,
		$author
	);
}
endif;

/**
 * Extends the default WordPress body class to denote:
 * 1. Using a full-width layout, when no active widgets in the sidebar
 *    or full-width template.
 * 2. Front Page template: thumbnail in use and number of sidebars for
 *    widget areas.
 * 3. White or empty background color to change the layout and spacing.
 * 4. Custom fonts enabled.
 * 5. Single or multiple authors.
 *
 * @since Twenty Twelve 1.0
 *
 * @param array Existing class values.
 * @return array Filtered class values.
 */
function twentytwelve_body_class( $classes ) {
	$background_color = get_background_color();

	if ( ! is_active_sidebar( 'sidebar-1' ) || is_page_template( 'page-templates/full-width.php' ) )
		$classes[] = 'full-width';

	if ( is_page_template( 'page-templates/front-page.php' ) ) {
		$classes[] = 'template-front-page';
		if ( has_post_thumbnail() )
			$classes[] = 'has-post-thumbnail';
		if ( is_active_sidebar( 'sidebar-2' ) && is_active_sidebar( 'sidebar-3' ) )
			$classes[] = 'two-sidebars';
	}

	if ( empty( $background_color ) )
		$classes[] = 'custom-background-empty';
	elseif ( in_array( $background_color, array( 'fff', 'ffffff' ) ) )
		$classes[] = 'custom-background-white';

	// Enable custom font class only if the font CSS is queued to load.
	if ( wp_style_is( 'twentytwelve-fonts', 'queue' ) )
		$classes[] = 'custom-font-enabled';

	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	return $classes;
}
add_filter( 'body_class', 'twentytwelve_body_class' );

/**
 * Adjusts content_width value for full-width and single image attachment
 * templates, and when there are no active widgets in the sidebar.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_content_width() {
	if ( is_page_template( 'page-templates/full-width.php' ) || is_attachment() || ! is_active_sidebar( 'sidebar-1' ) ) {
		global $content_width;
		$content_width = 960;
	}
}
add_action( 'template_redirect', 'twentytwelve_content_width' );

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @since Twenty Twelve 1.0
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 * @return void
 */
function twentytwelve_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';
}
//add_action( 'customize_register', 'twentytwelve_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_customize_preview_js() {
	wp_enqueue_script( 'twentytwelve-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20120827', true );
}
//add_action( 'customize_preview_init', 'twentytwelve_customize_preview_js' );



add_filter('walker_nav_menu_start_el', 'description_in_nav_menu', 10, 4);

function description_in_nav_menu($item_output, $item){
	return preg_replace('/(<a.*?>[^<]*?)</', '$1' . "<span>{$item->attr_title}</span><", $item_output);
}

/*
================================================================================
ここからExpression用カスタマイズ
================================================================================
*/
define('EX_FUNCTIONS', false);

set_post_thumbnail_size( 160, 160 );
update_option('thumbnail_size_w',182);
update_option('thumbnail_size_h',182);
update_option('medium_size_w',300);
update_option('medium_size_h',300);
update_option('large_size_w',780);
update_option('large_size_h',520);


//トップテンプレート切替キャンセル
//この記述で、オリジナルテーマ内home.phpを読みます。
remove_filter( 'template_include', 'get_post_type_template3' );
remove_filter( 'template_include', 'get_post_type_top_template_fudou' ); //不動産プラグイン v1.4.0


//物件詳細テンプレート切替キャンセル
//この記述で、オリジナルテーマ内single-fudo.phpを読みます。
remove_filter( 'template_include', 'get_post_type_single_template' );
remove_filter( 'template_include', 'get_post_type_single_template_fudou' ); //不動産プラグイン v1.4.0

//物件リストテンプレート切替
//オリジナルテーマ内 archive-fudo.php、archive-fudo-loop.php を読みます。
	//標準切替キャンセル (不動産プラグイン v1.3.5 迄)
	global $wp_version;
	if ( version_compare($wp_version, '3.7', '<') ) {
		remove_filter('template_include', 'get_post_type_archive_template', 10);
	}
	//標準切替キャンセル (不動産プラグイン v1.4.0 〜)
	remove_filter( 'template_include', 'get_post_type_archive_template_fudou' ); 	

	//標準切替キャンセル (不動産プラグイン v1.5.3 〜)
	remove_filter( 'template_include', 'get_post_type_archive_template_fudou' , 11);


	//オリジナルテーマ内 archive-fudo.php、archive-fudo-loop.php を読みます。
	function fudo_body_org_class( $class ) {
		$class[0] = 'archive archive-fudo';
		return $class;
	}
	function get_post_type_archive_org_template( $template = '' ) {
		global $wp_query;
		$cat = $wp_query->get_queried_object();
		$cat_name = isset( $cat->taxonomy ) ? $cat->taxonomy : '';

		if ( isset( $_GET['bukken'] ) || isset( $_GET['bukken_tag'] ) 
			|| $cat_name == 'bukken' || $cat_name =='bukken_tag' ) {
			status_header( 200 );
			$template = locate_template( array('archive-fudo.php', 'archive.php') );
			add_filter( 'body_class', 'fudo_body_org_class' );
		 }
		return $template;
	}
	add_filter( 'template_include', 'get_post_type_archive_org_template' , 11 );



/*
 * js CSS追加キャンセル
 * 不動産プラグイン内の 各デフォルトテーマ用CSS の読み込みをキャンセルします。
 * 必要なCSS記述はオリジナルテーマ内 style.css へコピーしてください。
 * バージョンに合わせ切替キャンセルはてどちらかを利用してください。
*/

global $wp_version;
if ( version_compare($wp_version, '4.0', '>=') ) {
	//標準切替キャンセル (不動産プラグイン v1.5.0 ～)
	remove_action( 'wp_enqueue_scripts', 'add_header_css_js_fudou', 12 );

	//不動産閲覧履歴プラグイン の CSS を読み込まなくする (v1.5.0 ～)
	remove_action( 'wp_enqueue_scripts', 'add_header_history_css_fudou', 12 );

	//不動産トップスライダープラグインの CSS を読み込まなくする (v1.5.0 ～)
	remove_action( 'wp_enqueue_scripts', 'add_header_topslider_css_fudou', 12 );
}else{
	//標準切替キャンセル (不動産プラグイン v1.3.5 迄)
	remove_action( 'wp_head', 'add_post_type_template3' ); 

	//標準切替キャンセル (不動産プラグイン v1.4.0 ～ v1.4.6)
	remove_action( 'wp_head', 'add_header_css_js_fudou' );
}

/*
//標準マップ切替キャンセル
remove_filter('template_include', 'get_post_type_template_map' );


//マップページテンプレート切替
function get_post_type_template_map_org( $template = '' ) {
		global $wp_query;
		$pagename = isset($wp_query->query_vars['name']) ? $wp_query->query_vars['name'] : '';	//rewrite
		$page = isset($_GET['page']) ? esc_attr( stripslashes( $_GET['page'])) : '' ;

		if( ( $page == 'map' &&  is_front_page() ) || $pagename == 'map' ){
			//オリジナルで作ったファイルを テーマフォルダ 内に設置した場合。( page-map_org.php として )
			$template = locate_template( array('page-map_org.php', 'index.php') );
		}
		return $template;
	}
add_filter( 'template_include', 'get_post_type_template_map_org',12 );
*/

//フッター埋め込みタグ・代替(flatheightsキャンセルのため)
// function my_footer_post_ex() {
// 	if ( is_front_page() ){
// 	echo '<div id="nendebcopy"><a href="http://nendeb.jp" target="_blank" rel="nofollow" title="WordPress 不動産プラグイン">Fudousan Plugin Ver.'.FUDOU_VERSION.'</a></div>';
// 	}else{
// 	echo '<div id="nendebcopy">Fudousan Plugin Ver.'.FUDOU_VERSION.'</div>';
// 	}
// 	//echo '<script type="text/javascript" src="'.WP_PLUGIN_URL.'/fudou/js/jquery.flatheights.js"></script>';
// 	//フッター埋め込みタグ
// 	if( get_option('fudo_footer_tag') != '' ) echo "\n" . get_option('fudo_footer_tag') . "\n";
// 	echo '<!-- Fudousan Plugin Ver.'.FUDOU_VERSION.' -->';
// }
// add_filter( 'wp_footer', 'my_footer_post_ex' );

//js読込み(PC/スマホ切り替えが必要なものはheader.phpにで記述)

function ex_add_scripts(){

wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'));//全体を通したメイン
wp_enqueue_script( 'flatheights', get_template_directory_uri() . '/js/jquery.flatheights.js', array('jquery'));//flatheights改造版
wp_enqueue_script( 'slider', get_template_directory_uri() . '/js/jquery.flexslider/jquery.flexslider.js', array('jquery'));//flexslider
wp_enqueue_script( 'css_browser_selector', get_template_directory_uri() . '/js/css_browser_selector.js', array('jquery'));//全体を通したメイン

//wp_enqueue_script( 'css3-mediaqueries', get_template_directory_uri() . '/js/css3-mediaqueries.js');//IE用メディアクエリ

}

add_action('init' , 'ex_add_scripts');



/*スマートフォン振り分け判断*/

function is_smartphone(){

  $ua=$_SERVER['HTTP_USER_AGENT'];
  $browser = (
	(strpos($ua,'iPhone')!==false)||
	(strpos($ua,'iPod')!==false)||
	(strpos($ua,'iPad')!==false)||
	(strpos($ua,'Android')!==false)
	);
    if ($browser == true){
    //スマートフォンディレクトリへ
    	return true;
	}else{
		return false;
		}

}

//////////////////////////////////////////////////////////////////////////////
//管理画面

//radio生成
//$name = formのname
//$arr = array(ラベル=>array(value,default_flag) , ・・・)
//$exval = 既存の値



function ez_make_radio($name ,$arr , $exval ){
	
	foreach ($arr as $k => $v){
		if($exval != ''){
			if($exval == $v[0]){
				$sel = 'checked';
			}else{
				$sel = '';
			}
		}else{
			if($v[1] == true){
				$sel = 'checked';
			}else{
				$sel = '';
			}
		}
		
		echo '<input type="radio" '.$sel.' name="'.$name.'" value="'.$v[0].'">'.$k.'&nbsp;';
	}
} 

add_action('admin_menu', 'my_plugin_menu');

function my_plugin_menu() {
	add_theme_page('Expression', 'Expression', 'read', '', 'my_plugin_function');
}

//////////////////////////////////////////////////////////////////////////////
add_action('admin_print_scripts', 'my_admin_scripts');
add_action('admin_print_styles' , 'my_admin_styles');

function my_admin_styles() {
    wp_enqueue_style('thickbox');
}

function my_admin_scripts() {
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('jquery');
}
//////////////////////////////////////////////////////////////////////////////


function my_plugin_function(){




?>
<script>
jQuery('document').ready(function(){
    jQuery('.media-upload').each(function(){//aタグの画像編集ボタンのそれぞれについて
        var rel = jQuery(this).attr("rel");//rel属性（すなわちid,nameと同じ）
        jQuery(this).click(function(){//クリックイベントをバインド
            window.send_to_editor = function(html) {//send_to_editor(html)定義
            	html = '<div>' + html + '</div>';//aタグにくるまれない場合の処置
              imgurl = jQuery('img', html).attr('src');//引数html内のimgタグのsrcを得る
					jQuery('#'+rel).val('');
					jQuery('#'+rel).val(imgurl);//ここでinputの内容を書き込んでいる
                tb_remove();//自分自身を消す
            }
            formfield = jQuery('#'+rel).attr('name');//inputフィールド
            tb_show(null, 'media-upload.php?post_id=0&type=image&TB_iframe=true');//iframeを開く
            return false;
        });
    });
});
</script>

<style>
.form-table ul li{
	padding:10px;
	margin:10px;
	border:1px solid #ccc;
	overflow:hidden;
}

.form-table ul li img{
	float:right;
}

.my-notice{
	font-size:11px;
	color:#4a99f2;
}

</style>

	<div class="wrap">
    <h2>Expression テーマ設定</h2>
<span class="my-notice">※カラーのカスタマイズについては「外観＞カスタマイズ」をご利用下さい</span>

 

<form method="post" action="options.php">
 

    <?php wp_nonce_field('update-options'); ?>
 
    <table class="form-table">
 
        <tr valign="top">
            <th scope="row">電話番号<br><!-- <span class="my-notice">※スマートフォンでは表示されません</span> --></th>
            <td><input type="text" name="expression_option_tel" value="<?php echo get_option('expression_option_tel'); ?>" />
            	<span class="my-notice"></span></td>
        </tr>
 
        <tr valign="top">
            <th scope="row">営業時間<br><!-- <span class="my-notice">※スマートフォンでは表示されません</span> --></th>
            <td><input type="text" name="expression_option_open" value="<?php echo get_option('expression_option_open'); ?>" />
            <span class="my-notice">※サイト上部フリーテキストとして利用可</span>
          </td>
        </tr>
 

<!--
         <tr valign="top">
            <th scope="row">お問い合わせリンク<br><span class="my-notice">※スマートフォンでは表示されません</span></th>
            <td>
<?php
	$arr = array(
			'なし' => array('none',''),
			'管理者メールリンク' => array('admin',true),
			'このURLへリンク' => array('page','')
			);
	$name = 'expression_option_mail';
	$exval = get_option('expression_option_mail');

ez_make_radio($name ,$arr , $exval );

?>
				<input type="text" name="expression_option_mail_page" value="<?php echo get_option('expression_option_mail_page'); ?>">

			</td>
        </tr>
-->
        <tr valign="top">
            <th scope="row">キャッチフレーズを表示</th>
            <td>
<?php
	$arr = array(
			'表示する' => array('yes',true),
			'表示しない' => array('no',''),
			);
	$name = 'expression_option_description';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );

?>

			</td>
        </tr>
<!--
        <tr valign="top">
            <th scope="row">サイトドメインを表示</th>
            <td>
<?php
	$arr = array(
			'表示する' => array('yes',true),
			'表示しない' => array('no',''),
			);
	$name = 'expression_option_domain';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );

?>

			</td>
        </tr>
   -->


        <tr valign="top">
            <th scope="row">ロゴ画像</th>
            <td>
<h4>ロゴ画像登録の手順</h4>
<p class="my-notice">
推奨サイズ:横300px内外<br>
1.下記「画像を選択」リンクをクリック<br>
2.ウインドウ内でファイルを選択後、「アップロード」をクリック<br>
3.「投稿へ挿入」をクリック</p>

<img width="160" src="<?php echo get_option('expression_option_logo'); ?>" /><br />

<input type="text" id="expression_option_logo" name="expression_option_logo" value="<?php echo get_option('expression_option_logo'); ?>" />
<a class="media-upload" href="JavaScript:void(0);" rel="expression_option_logo">画像を選択</a>



	</td>
        </tr>
         <tr valign="top">
            <th scope="row">シェアボタンを表示<br />(サイトトップ)</th>
            <td>
<?php
	$arr = array(
			'表示する' => array('yes',true),
			'表示しない' => array('no',''),
			);
	$name = 'expression_option_share_btn_top';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );

?>

			</td>
        </tr>

<!--          <tr valign="top">
            <th scope="row">シェアボタンを表示<br />(物件詳細ページ)</th>
            <td>
<?php
	$arr = array(
			'表示する' => array('yes',true),
			'表示しない' => array('no',''),
			);
	$name = 'expression_option_share_btn';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );

?>

			</td>
        </tr> -->

    <tr valign="top">
            <th scope="row">Wordpressコメント欄を表示</th>
            <td>

<?php
	$arr = array(
			'表示する' => array('yes',true),
			'表示しない' => array('no',''),
			);
	$name = 'expression_option_comment';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );
?>

<h4>コメント機能について</h4>
<p class="my-notice">
※ご利用にはwp-content/plugins/fudou/fudou.phpの編集が必要になります<br>
テキストエディタ上でfudou.phpを開き、<br>
49行目付近<br>
define('FUDOU_TRA_COMMENT', 0);<br>
を次の様に変更します<br>
define('FUDOU_TRA_COMMENT', 1);<br>
（「設定」「ディスカッション」の設定が優先されます）
</p>
        </tr>

<tr>
 

         <tr valign="top">
            <th scope="row">Facebookコメント欄を表示</th>
            <td>

<?php
	$arr = array(
			'表示する' => array('yes',true),
			'表示しない' => array('no',''),
			);
	$name = 'expression_option_fbcomment';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );

?>
        </tr>

<tr>
<th>
<h4>スライドショー表示</h4>
</th>
<td>
<?php
	$arr = array(
			'表示する' => array('yes',true),
			'表示しない' => array('no',''),
			);
	$name = 'expression_slideshow_display';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );
?>
</td>
</tr>
<tr>
<th>
<h4>スライドショー切り替えタイプ</h4>
</th>
<td>
<?php
	$arr = array(
			'横スライド' => array('slide',true),
			'フェイド' => array('fade',''),
			);
	$name = 'expression_slideshow_type';
	$exval = get_option($name);

	ez_make_radio($name ,$arr , $exval );
?>
</td>
</tr>
<tr>
<th>
<h4>スライドショー切り替えタイミング</h4>
</th>
<td>

<input size="6" type="text" name="expression_slideshow_time" value="<?php echo (get_option('expression_slideshow_time'))?get_option('expression_slideshow_time'):"2000"; ?>" />ミリ秒(ms) ※2000以上として下さい

</td>
</tr>
<tr>
	<td>
	</td>
	<td>
<ul>
<li>
<p><b>スライドショー画像1</b>&nbsp;(フリーサイズ：1440px x 400px推奨)</p>
		<img width="315" src="<?php echo get_option('expression_slideshow_1'); ?>" /><br />

		<input type="text" id="expression_slideshow_1" name="expression_slideshow_1" value="<?php echo get_option('expression_slideshow_1'); ?>" />
		<a class="media-upload" href="JavaScript:void(0);" rel="expression_slideshow_1">画像を選択</a><br />
		リンク先URL<input size="50" type="text" id="expression_slideshow_link_1" name="expression_slideshow_link_1" value="<?php echo get_option('expression_slideshow_link_1'); ?>" />
		<br />
		説明文
		<input type="text" style="width:90%" id="expression_slideshow_cap_1" name="expression_slideshow_cap_1" value="<?php echo get_option('expression_slideshow_cap_1'); ?>" />
</li>
<li>
<p><b>スライドショー画像2</b>&nbsp;(フリーサイズ：1440px x 400px推奨)</p>
		<img width="315" src="<?php echo get_option('expression_slideshow_2'); ?>" /><br />

		<input type="text" id="expression_slideshow_2" name="expression_slideshow_2" value="<?php echo get_option('expression_slideshow_2'); ?>" />
		<a class="media-upload" href="JavaScript:void(0);" rel="expression_slideshow_2">画像を選択</a><br />
		リンク先URL<input size="50" type="text" id="expression_slideshow_link_2" name="expression_slideshow_link_2" value="<?php echo get_option('expression_slideshow_link_2'); ?>" />
				<br />
		説明文
		<input type="text" style="width:90%" id="expression_slideshow_cap_2" name="expression_slideshow_cap_2" value="<?php echo get_option('expression_slideshow_cap_2'); ?>" />
</li>
<li>
<p><b>スライドショー画像3</b>&nbsp;(フリーサイズ：1440px x 400px推奨)</p>
		<img width="315" src="<?php echo get_option('expression_slideshow_3'); ?>" /><br />

		<input type="text" id="expression_slideshow_3" name="expression_slideshow_3" value="<?php echo get_option('expression_slideshow_3'); ?>" />
		<a class="media-upload" href="JavaScript:void(0);" rel="expression_slideshow_3">画像を選択</a><br />
		リンク先URL<input size="50" type="text" id="expression_slideshow_link_3" name="expression_slideshow_link_3" value="<?php echo get_option('expression_slideshow_link_3'); ?>" />
				<br />
		説明文
		<input type="text" style="width:90%" id="expression_slideshow_cap_3" name="expression_slideshow_cap_3" value="<?php echo get_option('expression_slideshow_cap_3'); ?>" />
</li>
<li>
<p><b>スライドショー画像4&nbsp;</b>(フリーサイズ：1440px x 400px推奨)</p>
		<img width="315" src="<?php echo get_option('expression_slideshow_4'); ?>" /><br />

		<input type="text" id="expression_slideshow_4" name="expression_slideshow_4" value="<?php echo get_option('expression_slideshow_4'); ?>" />
		<a class="media-upload" href="JavaScript:void(0);" rel="expression_slideshow_4">画像を選択</a><br />
		リンク先URL<input size="50" type="text" id="expression_slideshow_link_4" name="expression_slideshow_link_4" value="<?php echo get_option('expression_slideshow_link_4'); ?>" />
				<br />
		説明文
		<input type="text" style="width:90%" id="expression_slideshow_cap_4" name="expression_slideshow_cap_4" value="<?php echo get_option('expression_slideshow_cap_4'); ?>" />
</li>
<li>
<p><b>スライドショー画像5&nbsp;</b>(フリーサイズ：1440px x 400px推奨)</p>
		<img width="315" src="<?php echo get_option('expression_slideshow_5'); ?>" /><br />

		<input type="text" id="expression_slideshow_5" name="expression_slideshow_5" value="<?php echo get_option('expression_slideshow_5'); ?>" />
		<a class="media-upload" href="JavaScript:void(0);" rel="expression_slideshow_5">画像を選択</a><br />
		リンク先URL<input size="50" type="text" id="expression_slideshow_link_5" name="expression_slideshow_link_5" value="<?php echo get_option('expression_slideshow_link_5'); ?>" />
				<br />
		説明文
		<input type="text" style="width:90%" id="expression_slideshow_cap_5" name="expression_slideshow_cap_5" value="<?php echo get_option('expression_slideshow_cap_5'); ?>" />
</li>
<li>
<p><b>スライドショー画像6&nbsp;</b>(フリーサイズ：1440px x 400px推奨)</p>
		<img width="315" src="<?php echo get_option('expression_slideshow_6'); ?>" /><br />

		<input type="text" id="expression_slideshow_6" name="expression_slideshow_6" value="<?php echo get_option('expression_slideshow_6'); ?>" />
		<a class="media-upload" href="JavaScript:void(0);" rel="expression_slideshow_6">画像を選択</a><br />
		リンク先URL<input size="50" type="text" id="expression_slideshow_link_6" name="expression_slideshow_link_6" value="<?php echo get_option('expression_slideshow_link_6'); ?>" />
				<br />
		説明文
		<input type="text" style="width:90%" id="expression_slideshow_cap_6" name="expression_slideshow_cap_6" value="<?php echo get_option('expression_slideshow_cap_6'); ?>" />
</li>
</ul>
	</td>
</tr>



    </table>
 
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="expression_option_tel,expression_option_open,expression_option_description,expression_option_domain,expression_option_logo,expression_option_mail,expression_option_comment,expression_option_mail_page,expression_option_share_btn,expression_option_share_btn_top,expression_option_fbcomment,expression_slideshow_display,expression_slideshow_type,expression_slideshow_time,expression_slideshow_1,expression_slideshow_2,expression_slideshow_3,expression_slideshow_4,expression_slideshow_5,expression_slideshow_6,expression_slideshow_link_1,expression_slideshow_link_2,expression_slideshow_link_3,expression_slideshow_link_4,expression_slideshow_link_5,expression_slideshow_link_6,expression_slideshow_cap_1,expression_slideshow_cap_2,expression_slideshow_cap_3,expression_slideshow_cap_4,expression_slideshow_cap_5,expression_slideshow_cap_6" />
 
    <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>
 
</form>
</div>
<?php
}

/*
-------------------------- WPテーマカスタマイザー ---------------------------
*/


$c_items = array(

'sitename_color' => array(
							'section' => 'colors',
							'default' => '#999999',
							'transport' => 'postMessage',
							'label' => 'タイトル文字色',
							'css' => array(
								'selector' => '.site-header .ex_site-title a',
								'property' => 'color'
								)
					),

'sitedescription_color' => array(
							'section' => 'colors',
							'default' => '#416cc1',
							'transport' => 'postMessage',
							'label' => 'サブタイトル文字色',
							'css' => array(
								'selector' => '.site-header .ex_site-description',
								'property' => 'color'
								)
					),

'siteaccent_color' => array(
							'section' => 'colors',
							'default' => '#19489C',
							'transport' => 'postMessage',
							'label' => 'トップアクセントボーダー色',
							'css' => array(
								'selector' => '#page',
								'property' => 'border-top-color'
								)
					),

'h3_color' => array(
							'section' => 'colors',
							'default' => '#272c91',
							'transport' => 'postMessage',
							'label' => 'h3色',
							'css' => array(
								'selector' => '#main #container #content h3',
								'property' => 'color'
								)
					),

'h3_bordercolor' => array(
							'section' => 'colors',
							'default' => '#E0E0E0',
							'transport' => 'postMessage',
							'label' => 'h3下線色',
							'css' => array(
								'selector' => '#main #container #content h3',
								'property' => 'border-bottom-color'
								)
					),

'site_bgcolor' => array(
							'section' => 'colors',
							'default' => '#FFFFFF',
							'transport' => 'postMessage',
							'label' => 'コンテナ背景色',
							'css' => array(
								'selector' => '.site',
								'property' => 'background-color'
								)
					),


'top_price_bgcolor' => array(
							'section' => 'colors',
							'default' => '#00acbf',
							'transport' => 'postMessage',
							'label' => '価格背景色',
							'css' => array(
								'selector' => '.top_price',
								'property' => 'background-color'
								)
					),

'top_madori_color' => array(
							'section' => 'colors',
							'default' => '#cc8b2a',
							'transport' => 'postMessage',
							'label' => '間取文字色',
							'css' => array(
								'selector' => '.top_madori',
								'property' => 'color'
								)
					),

'goto_bgcolor' => array(
							'section' => 'colors',
							'default' => '#E0E0E0',
							'transport' => 'postMessage',
							'label' => '詳細リンク背景色',
							'css' => array(
								'selector' => '.box1low a , #list_simplepage .list_simple_box .list_details_button',
								'property' => 'background-color'
								)
					),


'goto_color' => array(
							'section' => 'colors',
							'default' => '#EE0000',
							'transport' => 'postMessage',
							'label' => '詳細リンク文字色',
							'css' => array(
								'selector' => '.box1low a , #list_simplepage .list_simple_box .list_details_button',
								'property' => 'color'
								)
					),

'archive_bordercolor' => array(
							'section' => 'colors',
							'default' => '#E0E0E0',
							'transport' => 'postMessage',
							'label' => '検索結果上下線色',
							'css' => array(
								'selector' => '#list_simplepage .list_simple_box , #list_simplepage .list_simple_boxtitle',
								'property' => 'border-bottom-color'
								)
					),

'widget_bordercolor' => array(
							'section' => 'colors',
							'default' => '#eaeaea',
							'transport' => 'postMessage',
							'label' => 'ウイジェット枠色',
							'css' => array(
								'selector' => '#secondary .widget',
								'property' => 'border-color'
								)
					),

'widget_h3bgcolor' => array(
							'section' => 'colors',
							'default' => '#140859',
							'transport' => 'postMessage',
							'label' => 'ウイジェットh3背景色',
							'css' => array(
								'selector' => '#page #main #secondary.widget-area h3.widget-title',
								'property' => 'background-color'
								)
					),

'widget_h3color' => array(
							'section' => 'colors',
							'default' => '#FFFFFF',
							'transport' => 'postMessage',
							'label' => 'ウイジェットh3色',
							'css' => array(
								'selector' => '#secondary .widget .widget-title',
								'property' => 'color'
								)
					),

'widget_bgcolor' => array(
							'section' => 'colors',
							'default' => '#FFFFFF',
							'transport' => 'postMessage',
							'label' => 'ウイジェット背景色',
							'css' => array(
								'selector' => '#secondary .widget',
								'property' => 'background-color'
								)
					),

'jyouken_barcolor' => array(
							'section' => 'colors',
							'default' => '#FFFFFF',
							'transport' => 'postMessage',
							'label' => '条件検索項目色',
							'css' => array(
								'selector' => '.jsearch_roseneki, .jsearch_chiiki, .jsearch_hofun, .jsearch_chikunen, .jsearch_memseki',
								'property' => 'color'
								)
					),

'jyouken_barbgcolor' => array(
							'section' => 'colors',
							'default' => '#8ba6ba',
							'transport' => 'postMessage',
							'label' => '条件検索項目背景色',
							'css' => array(
								'selector' => '.jsearch_roseneki, .jsearch_chiiki, .jsearch_hofun, .jsearch_chikunen, .jsearch_memseki',
								'property' => 'background-color'
								)
					),

'jyouken_btnbgcolor' => array(
							'section' => 'colors',
							'default' => '#127c1f',
							'transport' => 'postMessage',
							'label' => '条件検索ボタン色',
							'css' => array(
								'selector' => '#secondary .widget #searchitem_m input[type=submit],#secondary .widget #searchitem input[type=submit]',
								'property' => 'background-color'
								)
					),

'detail_bgcolor' => array(
							'section' => 'colors',
							'default' => '#EFEFEF',
							'transport' => 'postMessage',
							'label' => '詳細画面-価格/所在地 背景色',
							'css' => array(
								'selector' => '#list_simplepage2 .list_detail table#list_add',
								'property' => 'background-color'
								)
					),

'tel_color' => array(
							'section' => 'colors',
							'default' => '#ce0022',
							'transport' => 'postMessage',
							'label' => '電話番号文字色',
							'css' => array(
								'selector' => '#site-tel .no,#site-tel .prefix,#site-tel .no a',
								'property' => 'color'
								)
					),

'open_color' => array(
							'section' => 'colors',
							'default' => '#B1B1B1',
							'transport' => 'postMessage',
							'label' => '営業時間文字色',
							'css' => array(
								'selector' => '#site-open',
								'property' => 'color'
								)
					),

// 'detail_bordercolor' => array(
// 							'section' => 'colors',
// 							'default' => '#DDDDDD',
// 							'transport' => 'postMessage',
// 							'label' => '詳細画面-罫線色',
// 							'css' => array(
// 								'selector' => '#list_simplepage2 .list_simple_box .list_detail #list_add th , #list_simplepage2 .list_simple_box .list_detail #list_other th , #list_simplepage2 .list_simple_box .list_detail #list_other td , #list_simplepage2 .list_simple_box .list_detail #list_add td',
// 								'property' => 'border-bottom-color'
// 								)
// 					),

	);



function mytheme_customize_register( $wp_customize )
   {

global $c_items;



//ボディ背景色
	$wp_customize->get_setting( 'background_color' )->default = '#FFF';
	$wp_customize->get_control( 'background_color' )->priority = 0;
	$wp_customize->remove_control('header_textcolor');

//pageシャドウ

	// $wp_customize->add_setting( 'page_shadow_color' , array(
 //   	   'section'    => 'colors',
 //       'default'     => '#FFF',
 //       'transport'   => 'postMessage',
 //   ) );

 // 	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize,   'page_shadow_color', array(
 //       'label'        => 'ページ・シャドウ色',
 //       'section'    => 'colors',
 //       'settings'   => 'page_shadow_color',
 //       'priority' => 2
 //   ) ) );




$i = 10;

foreach ($c_items as $id => $data){

	$wp_customize->add_setting( $id , array(
   	   'section'    => 'colors',
       'default'     => $data['default'],
       'transport'   => 'postMessage',
   ) );

 	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize,   $id, array(
       'label'        => $data['label'],
       'section'    => 'colors',
       'settings'   => $id,
       'priority' => $i
   ) ) );

$i++;
}



   }


function mytheme_customize_css()//各cssを上書きするためのstyle出力・header.php内で呼ばれる
   {
   	global $c_items;
       ?>
           <style type="text/css">

							<?php foreach ($c_items as $id => $data) {
								echo $data['css']['selector'].'{'.$data['css']['property'].':'.get_theme_mod($id).';}'."\n";
							}
?>
.dpoint4{
	color:<?php echo get_theme_mod('top_price_bgcolor'); ?>;
}
#list_simplepage2 .list_simple_box .list_detail .list_price .dpoint4_td dt{
	color:<?php echo get_theme_mod('top_price_bgcolor'); ?>;	
}
.dpoint4_td {
  border-left-color: <?php echo get_theme_mod('top_price_bgcolor'); ?>;
}

#list_simplepage div.dpoint1{
	background-color: <?php echo get_theme_mod('top_price_bgcolor'); ?>;
}

.dpoint5{
	color:<?php echo get_theme_mod('top_madori_color'); ?>;
}
#list_simplepage2 .list_simple_box .list_detail .list_price .dpoint5_td dt{
	color:<?php echo get_theme_mod('top_madori_color'); ?>;	
}
#page #content .list_detail .widget ul li .top_price{
	color:<?php echo get_theme_mod('top_price_bgcolor'); ?>;
}
#page #content .list_detail .widget ul li .top_madori{
	color:<?php echo get_theme_mod('top_madori_color'); ?>;
}
.dpoint5_td {
  border-left-color: <?php echo get_theme_mod('top_madori_color'); ?>;
}
/*#page {
	box-shadow:0 0 20px <?php echo get_theme_mod('page_shadow_color'); ?>;
}*/
           </style>
       <?php
   }

   add_action( 'customize_register', 'mytheme_customize_register' );


   /**
    * 実行されるフック: 'customize_preview_init'
    * 
    * @see add_action('customize_preview_init',$func)
    */
   function mytheme_customizer_live_preview()
   {
       wp_enqueue_script( 
           'mytheme-themecustomizer',  //このスクリプトの ID 名
           get_template_directory_uri().'/js/theme-customizer.js', // このファイルの URL
           array( 'jquery','customize-preview' ),  //　必須の依存ファイル
           '',  // このスクリプトのバージョン（任意）
           true // フッターに出力する場合 true にします
       );
   }
   add_action( 'customize_preview_init', 'mytheme_customizer_live_preview' );


   function mytheme_customizer_script(){//未使用
   	   	global $c_items;
	   	echo '<script type="text/javascript">';
	   	echo 'jQuery(document).ready(function(){';
	   	echo '(function($){';
	   	echo 'if(typeof(wp)!="undefined"){';
echo "
// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).html( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).html( to );
		} );
	} );

	//Hook into background color change and adjust body class value as needed.
	wp.customize( 'background_color', function( value ) {
		value.bind( function( to ) {
			if ( '#ffffff' == to || '#fff' == to )
				$( 'body' ).addClass( 'custom-background-white' );
			else if ( '' == to )
				$( 'body' ).addClass( 'custom-background-empty' );
			else
				$( 'body' ).removeClass( 'custom-background-empty custom-background-white' );
		} );
	} );


   wp.customize( 'top_price_bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('#list_simplepage2 .list_simple_box .list_detail .list_price .dpoint4_td dt').css('color', newval );
           $('.dpoint4_td').css('border-left-color', newval );
           $('.dpoint4').css('color', newval );
       } );
   } );

   wp.customize( 'top_madori_color', function( value ) {
       value.bind( function( newval ) {
           $('#list_simplepage2 .list_simple_box .list_detail .list_price .dpoint5_td dt').css('color', newval );
           $('.dpoint5_td').css('border-left-color', newval );
           $('.dpoint5').css('color', newval );
       } );
   } );

	 // wp.customize( 'page_shadow_color', function( value ) {
  //      value.bind( function( newval ) {
  //          $('#page').css('box-shadow', '0 0 20px '+newval );
  //      } );
  //  } );

";

foreach ($c_items as $id => $data) {
echo "
   wp.customize( '".$id."', function( value ) {
       value.bind( function( newval ) {
           $('".$data['css']['selector']."').css('".$data['css']['property']."', newval );
       } );
   } );
";

}

			echo '}';
	   	echo '})(jQuery);';
			echo '});';
	   	echo '</script>';
   }

//add_action( 'customize_preview_init', 'mytheme_customizer_script' );


//画像ファイル名から添付IDを得る
function get_id_from_filename($filename){
	global $wpdb;
	$result = $wpdb->get_row("SELECT * FROM $wpdb->posts WHERE `post_type` = 'attachment' AND `guid` = '".$filename."'" , ARRAY_A);
	return $result['ID'];
}

//disable thickbox for frontend
function wpse71503_init() {
    if (is_single()) {
        wp_deregister_style('thickbox');
        wp_deregister_script('thickbox');
    }
}
add_action('init', 'wpse71503_init');




/////////////////////////////////////////////////////////////
/*Expression PRO functions*/

if(EX_FUNCTIONS){

class ExSpecial extends WP_Widget {

		//public $p_id;

		function ExSpecial() {
    	parent::WP_Widget( 'ex_special', // Base ID
            'EXトップ特集リンク', // Name
            array( 'description' => 'トップページに特集リンクを表示', ) // Args
        );
    	//include_once(WP_PLUGIN_DIR.'/fudou/inc/inc-archive-fudo.php');
    }

    function widget($args, $instance) {
        extract( $args );

        for ($i=1; $i <= 4 ; $i++) {
        	$title = esc_attr($instance['title']);
	        $text{$i} = esc_attr($instance['text'.$i]);
	        $tag{$i} = esc_attr($instance['tag'.$i]);
	        $spimage{$i} = $instance['spimage'.$i];
      	}

?>
		    	<div id="ex_special" class="widget">
		          <?php if ( $title ) ?>
		        	<?php echo $before_title . $title . $after_title; ?>

						<ul class="ex_special_list">
						<?php for ($i=1; $i <= 4 ; $i++) {
							if(strpos($tag{$i}, 'http')===false){
								$link = home_url().'?bukken_tag='.$tag{$i};
							}else{
								$link = $tag{$i};
							}
						 ?>
						<li>
							<a href="<?php echo $link; ?>">
								<div class="li_inner">
									<img src="<?php echo $spimage{$i}; ?>" />
									<p><?php echo $text{$i}; ?></p>
								</div>
							</a>
						</li>
						<?php } ?>
						</ul>
		      </div>
        <?php
      }
  

    function update($new_instance, $old_instance) {

				$instance = $old_instance;
				for ($i=1; $i <= 4 ; $i++) {
					$instance['title'] = strip_tags($new_instance['title']);
					$instance['text'.$i] = strip_tags($new_instance['text'.$i]);
					$instance['tag'.$i] = strip_tags($new_instance['tag'.$i]);
					$instance['spimage'.$i] = strip_tags($new_instance['spimage'.$i]);
				}
        return $instance;
    }

    function form($instance) {

    	for ($i=1; $i <= 4 ; $i++) {
    		$title = esc_attr($instance['title']);
        $text{$i} = esc_attr($instance['text'.$i]);
        $tag{$i} = esc_attr($instance['tag'.$i]);
        $spimage{$i} = $instance['spimage'.$i];
      }
 
        ?>
				<script>
				jQuery('document').ready(function(){
				    jQuery('.media-upload').each(function(){//aタグの画像編集ボタンのそれぞれについて
				        var rel = jQuery(this).attr("rel");//rel属性（すなわちid,nameと同じ）
				        jQuery(this).click(function(){//クリックイベントをバインド
				            window.send_to_editor = function(html) {//send_to_editor(html)定義
				            	html = '<div>' + html + '</div>';//aタグにくるまれない場合の処置
				              imgurl = jQuery('img', html).attr('src');

				              //引数html内のimgタグのsrcを得る
									jQuery('#'+rel).val('');
									jQuery('input#'+rel).val(imgurl);//ここでinputの内容を書き込んでいる
				                tb_remove();//自分自身を消す
				            }
				            formfield = jQuery('#'+rel).attr('name');//inputフィールド
				            tb_show(null, 'media-upload.php?post_id=0&type=image&TB_iframe=true');//iframeを開く
				            return false;
				        });
				    });
				});
				</script>


			<p>
        <label for="<?php echo $this->get_field_id('title'); ?>">
        タイトル
        </label>
        <input class="" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
      </p>

      <hr>

				<?php for ($i=1; $i <= 4 ; $i++) { ?>
			<h3>特集<?php echo $i;?></h3>

        <p>
           <label for="<?php echo $this->get_field_id('text'.$i); ?>">
           説明文<?php echo $i;?>
           </label>
           <textarea  class="widefat" rows="5" colls="20" id="<?php echo $this->get_field_id('text'.$i); ?>" name="<?php echo $this->get_field_name('text'.$i); ?>"><?php echo $text{$i}; ?></textarea>
        </p>

        <p>
          <label for="<?php echo $this->get_field_id('tag'.$i); ?>">
          リンクする物件タグまたはURL
          </label>
          <input class="" id="<?php echo $this->get_field_id('tag'.$i); ?>" name="<?php echo $this->get_field_name('tag'.$i); ?>" type="text" value="<?php echo $tag{$i}; ?>" />
        </p>

				<label for="<?php echo $this->get_field_id('spimage'.$i); ?>">
					画像<?php echo $i;?>（300x300px以上・中サイズを選択して下さい）
				</label>
        <input type="text" id="<?php echo $this->get_field_id('spimage'.$i); ?>" name="<?php echo $this->get_field_name('spimage'.$i); ?>" value="<?php echo $spimage{$i}; ?>" />
				<a class="media-upload" href="JavaScript:void(0);" rel="<?php echo $this->get_field_id('spimage'.$i); ?>">画像を選択</a><br />
	<hr>

				<?php } ?>

        <?php
    }
}



add_action('widgets_init', create_function('', 'return register_widget("ExSpecial");'));



class ExPickUp extends WP_Widget {

		//public $p_id;

		function ExPickUp() {
    	parent::WP_Widget( 'ex_pickup', // Base ID
            'EXピックアップ物件', // Name
            array( 'description' => '特定の物件をトップページでアピール', ) // Args
        );
    	//include_once(WP_PLUGIN_DIR.'/fudou/inc/inc-archive-fudo.php');
    }

    function widget($args, $instance) {
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
        $body = apply_filters( 'widget_body', $instance['body'] );
        $catch = $instance['catch'];
        $display_caption = $instance['display_caption'];

        //会員
				$kaiin = 0;
				if( !is_user_logged_in() && get_post_meta($meta_id, 'kaiin', true) == 1 ) $kaiin = 1;

				//ユーザー別会員物件リスト
				$kaiin2 = users_kaiin_bukkenlist($meta_id,$kaiin_users_rains_register, get_post_meta($meta_id, 'kaiin', true) );

        if(!empty($instance['my_post_id']))
        	{
        		$my_post_id = $instance['my_post_id'];
        		$post = get_post($my_post_id);
        		if(!$catch)$catch = $post->post_title;//titleがない場合はpost_title
        
    		?>
		    	<div id="ex_pickup" class="widget">
		          <?php if ( $title ) ?>
		        	<?php echo $before_title . $title . $after_title; ?>
		        	
					
					<dl class="bukken-data">
						<dt><?php if( get_post_meta($my_post_id,'bukkenshubetsu',true) < 3000 ) { echo '価格';}else{echo '賃料';} ?></dt>
						<dd>
							<?php 
							if ( !my_custom_kaiin_view('kaiin_kakaku',$kaiin,$kaiin2) ){
								echo "--";
							}else{
								if( get_post_meta($my_post_id, 'seiyakubi', true) != "" ){ echo '--'; }else{  ex_my_custom_kakaku_print($my_post_id); }
							}
							?>
						</dd>

						<dd><?php ex_my_custom_bukkenshubetsu_print($my_post_id); ?></dd>
							<?php 
							if ( my_custom_kaiin_view('kaiin_madori',$kaiin,$kaiin2 ) ){
								if( get_post_meta($my_post_id, 'madorisu', true) !=""){ 
							?>
						<dt>間取</dt>
						<dd><?php ex_my_custom_madorisu_print($my_post_id); ?>
						</dd>
							<?php
								} 
							} 
							?>
					</dl>

<?php ex_print_img_all($my_post_id , '' ,true, 'thumbnail' ,150, 150 , $display_caption) ?>
					
						<a href="<?php echo get_permalink($my_post_id); ?>">
		        	<?php if($catch)echo '<h4>'.$catch.'</h4>'; ?>
		        	<?php if($body)echo '<p class="body">' . nl2br($body) . '</p>'; ?>
		        </a>
		      </div>
        <?php
      }
    }

    function update($new_instance, $old_instance) {
				$instance = $old_instance;
				$instance['title'] = strip_tags($new_instance['title']);
				$instance['catch'] = strip_tags($new_instance['catch']);
				$instance['body'] = trim($new_instance['body']);
				$instance['display_caption'] = $new_instance['display_caption'];
				$instance['my_post_id'] = $new_instance['my_post_id'];
        return $instance;
    }

    function form($instance) {
        $title = esc_attr($instance['title']);
        $body = $instance['body'];
        $catch = esc_attr($instance['catch']);
        $display_caption = $instance['display_caption'];
        $my_post_id = esc_attr($instance['my_post_id']);
        ?>

				<p>
          <label for="<?php echo $this->get_field_id('title'); ?>">
          
          </label>
          <input class="" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>

        <p>
          <label for="<?php echo $this->get_field_id('catch'); ?>">
          キャッチフレーズ（未記入の場合は投稿名が入ります）
          </label>
          <input class="" id="<?php echo $this->get_field_id('catch'); ?>" name="<?php echo $this->get_field_name('catch'); ?>" type="text" value="<?php echo $catch; ?>" />
        </p>

        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>">
          表示したい物件のID
          </label>
          <input class="" id="<?php echo $this->get_field_id('my_post_id'); ?>" name="<?php echo $this->get_field_name('my_post_id'); ?>" type="text" size="5" value="<?php echo $my_post_id; ?>" />
        </p>

        <p>
           <label for="<?php echo $this->get_field_id('body'); ?>">
           アピール文
           </label>
           <textarea  class="widefat" rows="16" colls="20" id="<?php echo $this->get_field_id('body'); ?>" name="<?php echo $this->get_field_name('body'); ?>"><?php echo $body; ?></textarea>
        </p>

        <p>
          <label for="<?php echo $this->get_field_id('display_caption'); ?>">
          画像のキャプションの表示
          </label><br />
          <?php  ?>
          <input type="radio" <?php echo ($display_caption==1)?'checked':''; ?> class="" id="<?php echo $this->get_field_id('display_caption'); ?>" name="<?php echo $this->get_field_name('display_caption'); ?>" type="text" size="5" value="1" />表示する
          <input type="radio" <?php echo (!$display_caption)?'checked':''; ?> class="" id="<?php echo $this->get_field_id('display_caption'); ?>" name="<?php echo $this->get_field_name('display_caption'); ?>" type="text" size="5" value="0" />表示しない
        </p>
        <?php
    }
}

add_action('widgets_init', create_function('', 'return register_widget("ExPickUp");'));

}

/*
ex 連続画像出力
*/
function ex_print_img_all($post_id ,$imgid_array=array() ,$all=false, $size='thumbnail' ,$img_w = '' , $img_h = '' , $caption){

		global $post , $wpdb;
		$img_path = get_option('upload_path');

								if ($img_path == '')	$img_path = 'wp-content/uploads';

	if($all){//全取得
		if (!defined('FUDOU_IMG_MAX')){
											$fudou_img_max = 30;
										}else{
											$fudou_img_max = FUDOU_IMG_MAX;
										}
		for ($i=1; $i <=  $fudou_img_max; $i++) { 
			$imgid_array[] = $i;
		}
	}

	echo '<div id="img_all_box-'.$post_id.'" class="img_all_box flexslider">';

	echo '<ul class="slides">';

	foreach ($imgid_array as $imgid) {//loop
							
									$fudoimg_data = get_post_meta($post_id, "fudoimg".$imgid, true);
									$fudoimgcomment_data = get_post_meta($post_id, "fudoimgcomment".$imgid, true);
									$fudoimg_alt = $fudoimgcomment_data/* . my_custom_fudoimgtype_print(get_post_meta($post_id, "fudoimgtype".$imgid, true))*/;


									if($fudoimg_data !="" ){

											echo '<li>';

											$sql  = "";
											$sql .=  "SELECT P.ID,P.guid";
											$sql .=  " FROM $wpdb->posts as P";
											$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
										//	$sql = $wpdb->prepare($sql,'');
											$metas = $wpdb->get_row( $sql );
											$attachmentid = '';
											if( !empty($metas) ){
												$attachmentid  =  $metas->ID;
												$guid_url  =  $metas->guid;
											}

											if($attachmentid !=''){
												//thumbnail、medium、large、full 
												$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, $size);
												$fudoimg_url = $fudoimg_data1[0];
																		
												if($fudoimg_url !=''){
													echo '<div class="ex_img_box box-'.$imgid.'">';
													$width = $height = '';
													if($img_w)$width = 'width="'.$img_w.'"';
													if($img_h)$height = 'height="'.$img_h.'"';
													echo '<a href="'.get_permalink($post_id).'"><img '.$width.' '.$height.' src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" /></a>';

													//キャプション
													if($fudoimgcomment_data && $caption)echo '<p class="caption"><span>'.$fudoimgcomment_data.'</span></p>';

													echo '</div>';

												}
											}else{
												echo '<img src="'.WP_PLUGIN_URL.'/fudou/img/nowprinting.jpg" alt="'. $fudoimg_data .'" />';
											}
											
										echo '</li>';											

									}
								
		}//end loop

		echo '</slides>';

		echo '</div>';
								
}

/**
 * 賃料・価格
 *
 * @since Fudousan Plugin 1.0.0 *
 * @param int $post_id Post ID.
 */
function ex_my_custom_kakaku_print($post_id) {
	//非公開の場合
	if(get_post_meta($post_id,'kakakukoukai',true) == "0"){

		$kakakujoutai_data = get_post_meta($post_id,'kakakujoutai',true);
		if($kakakujoutai_data=="1")	echo '相談';
		if($kakakujoutai_data=="2")	echo '確定';
		if($kakakujoutai_data=="3")	echo '入札';

	}else{
		$kakaku_data = get_post_meta($post_id,'kakaku',true);
		if(is_numeric($kakaku_data)){
			echo floatval($kakaku_data)/10000;
			echo "万円";
		}
	}
}

/**
 * 物件種別
 *
 * @since Fudousan Plugin 1.4.3 *
 * @param int $post_id Post ID.
 */
function ex_my_custom_bukkenshubetsu_print($post_id) {
	$bukkenshubetsu_txt= '';
	$bukkenshubetsu_data = get_post_meta($post_id,'bukkenshubetsu',true);

	global $work_bukkenshubetsu;
	foreach ($work_bukkenshubetsu as $meta_box ){

		if( $bukkenshubetsu_data == $meta_box['id'] ){
			$bukkenshubetsu_txt = $meta_box['name'];
			$bukkenshubetsu_txt = str_replace( "【売地】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売戸建】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売マン】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売建物全部】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【売建物一部】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【賃貸居住】" ,"" , $bukkenshubetsu_txt);
			$bukkenshubetsu_txt = str_replace( "【賃貸事業】" ,"" , $bukkenshubetsu_txt);
			echo $bukkenshubetsu_txt;
			break;
		}
	}
}

/**
 * 間取り 部屋種類
 *
 * @since Fudousan Plugin 1.0.0 *
 * @param int $post_id Post ID.
 */
function ex_my_custom_madorisu_print($post_id) {
	$madorisyurui_data = get_post_meta($post_id,'madorisyurui',true);
	echo get_post_meta($post_id,'madorisu',true);
	if($madorisyurui_data=="10")	echo 'R';
	if($madorisyurui_data=="20")	echo 'K';
	if($madorisyurui_data=="25")	echo 'SK';
	if($madorisyurui_data=="30")	echo 'DK';
	if($madorisyurui_data=="35")	echo 'SDK';
	if($madorisyurui_data=="40")	echo 'LK';
	if($madorisyurui_data=="45")	echo 'SLK';
	if($madorisyurui_data=="50")	echo 'LDK';
	if($madorisyurui_data=="55")	echo 'SLDK';
}

function ex_log($text){
	if(WP_DEBUG_LOG){
	$file =WP_CONTENT_DIR . '/debug.log';
	// ファイルをオープンして既存のコンテンツを取得します
	$current = file_get_contents($file);
	// 新しい人物をファイルに追加します
	$current .= $text."\n";
	// 結果をファイルに書き出します
	file_put_contents($file, $current);
	}
}

/////////////////////////////////////////////////////////////
/* Mylist functions */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if(EX_FUNCTIONS){

if(!is_plugin_active('fudo-mylist/fudo-mylist.php')){//マイリストプラグインとの同時起動を防ぐ

//マイリストの読み込み
include_once('fudo-mylist/fudo-mylist.php');

add_action('admin_menu', 'ex_mylist_menu');

function ex_mylist_menu() {
  add_submenu_page('tools.php' ,'不動産マイリストlite', '不動産マイリストlite', 'activate_plugins', __FILE__, 'ex_mylist_options');
}


function ex_mylist_options() {
  echo '<div class="wrap">';

?>
<style type="text/css">
.form-table td img{
	margin:4px;
}
.form-table td input[type=text]{
	width:70%;
}
.form-table td input[type=text].small{
	width:20%;
}
.form-table td input[type=number]{
	width:50px;
}
.my_notice{
 	color:#21759B;
}
img.checked{
	border:3px solid #f99;
}
div.theme_box{
	float:left;
}
</style>

<script>
jQuery('document').ready(function(){
    jQuery('.media-upload').each(function(){//aタグの画像編集ボタンのそれぞれについて
        var rel = jQuery(this).attr("rel");//rel属性（すなわちid,nameと同じ）
        jQuery(this).click(function(){//クリックイベントをバインド
            window.send_to_editor = function(html) {//send_to_editor(html)定義
            	html = '<div>' + html + '</div>';//aタグにくるまれない場合の処置
              imgurl = jQuery('img', html).attr('src');//引数html内のimgタグのsrcを得る
					jQuery('#'+rel).val('');
					jQuery('#'+rel).val(imgurl);//ここでinputの内容を書き込んでいる
                tb_remove();//自分自身を消す
            }
            formfield = jQuery('#'+rel).attr('name');//inputフィールド
            tb_show(null, 'media-upload.php?post_id=0&type=image&TB_iframe=true');//iframeを開く
            return false;
        });
    });
});
</script>



<?php


//不動産プラグインバージョンチェック・1.0.9以上OK

if(version_compare(FUDOU_VERSION, '1.0.9') < 0){
	echo '<div class="update-nag">このプラグインに適合する不動産プラグインのバージョンは1.0.9以上です。現在のバージョンは'.FUDOU_VERSION.'です。出来る限りアップデートして下さい。</div>';
}

?>


<script type="text/javascript">

function disp(){
	if(window.confirm('本当にクリアしますか？（元には戻せません）')){
		return true;
	}else{
		window.alert('キャンセルされました');
		return false;
	}
}

</script>

<h2>不動産マイリスト設定</h2>
<!–　投稿先はoptions.php！–>
<form method="post" action="options.php">

<!–　お約束のnonce！–>
<?php wp_nonce_field('update-options'); ?>

<p class="my_notice">※マイリストを正しく動作させるには、「設定」「パーマリンク設定」を「デフォルト」にする必要があります</p>

<table class="form-table">

<tr valign="top">
<th scope="row">マイリスト保存期間</th>
<td>
<?php
$arr = array(
'1日'=>'1',
'7日'=>'7',
'30日'=>'30',
'90日'=>'90'
);
$exval = get_option('fudo-mylist-expires');
fd_make_radio('fudo-mylist-expires' ,$arr , $exval );
?>

</td>
</tr>
<!-- 
<tr valign="top">
<th scope="row">テーマ</th>
<td>
<?php
/*lite
$arr = array(
'シンプル(simple)'=>'simple',
'リッチ(rich)'=>'rich',
'ミニマム(minimum)'=>'minimum'
);
$exval = get_option('fudo-mylist-theme');
fd_make_radio2('fudo-mylist-theme' ,$arr , $exval );
*/
?>

</td>
</tr>

<tr valign="top">
<th scope="row"></th>
<td>
<div style="color:#21759B;">※画像をカスタマイズする場合は、プラグインフォルダ内の
<ul>
<li>img/mylist_check_on-{テーマ名}.png</li>
<li>img/mylist_check_off-{テーマ名}.png</li>
<li>img/mylist_view-{テーマ名}.png</li>
</ul>
を編集して下さい。</div>
</td> 
</tr>-->

<tr valign="top">
<th scope="row">追加css</th>
<td><textarea name="fudo-mylist-css" cols="80" rows="3"><?php echo get_option('fudo-mylist-css'); ?></textarea></td>
</tr>



</table>

<!–この2行が肝心！–>
<input type="hidden" name="action" value="update" />
<input type="hidden" name="page_options" value="fudo-mylist-theme,fudo-mylist-expires,fudo-mylist-css" />

<p class="submit">
<input type="submit" class="button-primary" value="<?php _e("Save Changes"); ?>" />
</p>

</form>
<!-- <h2>統計情報</h2>
<div class="countList">
<?php

//$wp_list_table = new Links_List_Table();
//$wp_list_table->prepare_items();


//$wp_list_table->display();

?>
<form id="clr" method="POST" onSubmit="return disp()">
<input type="hidden" name="action" value="mylist-clrflg">
<input type="submit" class="button-secondary" value="統計情報をクリアする">
</form>
</div> -->



<?php
  echo '</div>';
}

}else{
	//不動産マイリストプラグインが既に入っている場合	
}

}//EX_FUNCTIONS

//-----------[ custom_function ]---------------
include_once('function/custom_function.php');


