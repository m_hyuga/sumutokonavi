<?php
// おすすめ物件

query_posts('post_type=page&name=content-osusume&post_status=publish');
?>
<section class="osusume">
<h3>おすすめ物件</h3>

<?php if( have_posts()): while ( have_posts() ) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
</section>
