<?php
//新着情報

query_posts('post_type=whatsnew&posts_per_page=3&post_status=publish');
?>
<section class="whatsnew">
<h3>新着情報</h3>
<ul>
<?php if( have_posts() ): while( have_posts() ): the_post();?>
<li><span class="date"><?php the_time('Y-m-d'); ?></span><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?><span class="more">▶more</span></a></li>
<?php endwhile; endif; ?>
</ul>
<p class="ichiran"><a href="<?php echo home_url(); ?>?post_type=whatsnew">▶一覧を見る</a></p>
</section>

