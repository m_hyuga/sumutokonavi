/* script for PC */

jQuery(window).load(function() {

//flexslider
  jQuery('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 130,
    itemMargin: 1,
    asNavFor: '#slider'
  });

  jQuery('#slider').flexslider({
    animation: "slide",
    controlNav: true,
    animationLoop: false,
    slideshow: false,
    smoothHeight: true,
    prevText: "&lt;&lt;",
    nextText: "&gt;&gt;",
    sync: "#carousel"
  });

});




jQuery(document).ready(function(){
 
    jQuery(window).on("scroll", function() {
 
        if (jQuery(this).scrollTop() > 100) {
            // ↑ スクロール位置が100よりも小さい場合に以下の処理をする
            jQuery('.page-top').slideDown("fast");
            // ↑ (100より小さい時は)ページトップボタンをスライドダウン
        } else {
            jQuery('.page-top').slideUp("fast");
            // ↑ それ以外の場合の場合はスライドアップする。
        }
         
    // フッター固定する
 
        scrollHeight = jQuery(document).height(); 
        // ドキュメントの高さ
        scrollPosition = jQuery(window).height() + jQuery(window).scrollTop(); 
        //　ウィンドウの高さ+スクロールした高さ→　現在のトップからの位置
        footHeight = jQuery("#colophon").innerHeight();
        // フッターの高さ
                 
        if ( scrollHeight - scrollPosition  <= footHeight ) {
        // 現在の下から位置が、フッターの高さの位置にはいったら
        //  ".page-top"のpositionをabsoluteに変更し、フッターの高さの位置にする        
            jQuery(".page-top").css({
                "position":"absolute",
                "bottom": footHeight
            });
        } else {
        // それ以外の場合は元のcssスタイルを指定
            jQuery(".page-top").css({
                "position":"fixed",
                "bottom": "10px"
            });
        }
    });
 

 
});