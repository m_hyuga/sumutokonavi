function smartRollover() {
	if(document.getElementsByTagName) {
		var images = document.getElementsByTagName("img");
		for(var i=0; i < images.length; i++) {
			if(images[i].getAttribute("src").match("_off."))
			{
				images[i].onmouseover = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
				}
				images[i].onmouseout = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
				}
			}
		}
	}
}
if(window.addEventListener) {
	window.addEventListener("load", smartRollover, false);
}
else if(window.attachEvent) {
	window.attachEvent("onload", smartRollover);
}

/* Scroll
****************************************************************/



jQuery(document).ready(function($) {

//先頭にスクロール

var headH = 0;
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
/*	// PageTop
	$('a[href^=#], area[href^=#]').not('a[href=#], area[href=#]').each(function () {
		// jquery.easing
		jQuery.easing.quart = function (x, t, b, c, d) {
			return -c * ((t = t / d - 1) * t * t * t - 1) + b;
		};
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname && this.hash.replace(/#/, '')) {
			var $targetId = $(this.hash),
				$targetAnchor = $('[name=' + this.hash.slice(1) + ']');
			var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
			if ($target) {
				var targetOffset = $target.offset().top - headH;
				$(this).click(function () {
					$('html, body').animate({
						scrollTop: targetOffset
					}, 500, 'quart');
					return false;
				});
			}
		}
	});
		if (location.hash) {
			var hash = location.hash;
			window.scroll(0, headH)
			$('a[href=' + hash + ']').click();
		}
*/

//ドロップダウン
$('.access ul>li').mouseenter(function(){
    $(this).find('ul:not(:animated)').fadeIn(200);
});
$('.access ul>li').mouseleave(function(){
    $(this).find('ul:not(:animated)').fadeOut(200);
});

//ボックス内htmlの整形
//$('#top_fbox #content .grid-content li div:not(.new_mark)').addClass('goto');//クラス付加

$('#top_fbox #content .grid-content li br').remove();//余計なbr除去

$('.widget_fudo_b_k br').remove();//余計なbr除去

$('.widget_fudo_kaiin br').remove();//余計なbr除去




//価格の整形
$('.top_price').each(function(){
	rp = $(this).text().replace('万円 ','');
	$(this).text(rp);
	x = $('<span>万円</span>');
	x.appendTo(this);
});
$('.dpoint1').each(function(){
	rp = $(this).text().replace('万円','');
	$(this).text(rp);
	x = $('<span>万円</span>');
	x.appendTo(this);
});

//画像ポップアップ
// $('.list_picsam_img').magnificPopup({
//   delegate: 'a', // child items selector, by clicking on it popup will open
//   type: 'image'
//   // other options
// });




		jQuery(".page-top").hide();
     // ↑ページトップボタンを非表示にする
 

});


