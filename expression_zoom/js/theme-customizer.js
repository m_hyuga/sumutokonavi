/**
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 * Things like site title, description, and background color changes.
 */

( function( $ ) {
  //Site title and description.
  // wp.customize( 'sitename', function( value ) {
  //  value.bind( function( newval ) {
  //    $( '.site-header .ex_site-title a' ).css( 'color' , newval);
  //  } );
  // } );
  // wp.customize( 'sitedescription', function( value ) {
  //  value.bind( function( newval ) {
  //    $( '.site-header .ex_site-description' ).css( 'color' , newval);
  //  } );
  // } );

  //Hook into background color change and adjust body class value as needed.
  wp.customize( 'background_color', function( value ) {
    value.bind( function( to ) {
      if ( '#ffffff' == to || '#fff' == to )
        $( 'body' ).addClass( 'custom-background-white' );
      else if ( '' == to )
        $( 'body' ).addClass( 'custom-background-empty' );
      else
        $( 'body' ).removeClass( 'custom-background-empty custom-background-white' );
    } );
  } );


/*-------------------------------------以下はコード生成した後貼付ける-----------------------------------------------*/

   wp.customize( 'sitename_color', function( value ) {
       value.bind( function( newval ) {
           $('.site-header .ex_site-title a').css('color', newval );
       } );
   } );

    wp.customize( 'sitedescription_color', function( value ) {
       value.bind( function( newval ) {
           $('.site-header .ex_site-description').css('color', newval );
       } );
   } );

   wp.customize( 'top_price_bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('#list_simplepage2 .list_simple_box .list_detail .list_price .dpoint4_td dt').css('color', newval );
           $('#list_simplepage div.dpoint1').css('background-color', newval );
           $('.dpoint4_td').css('border-left-color', newval );
           $('.dpoint4').css('color', newval );
           $('#page #content .list_detail .widget ul li .top_price').css('color', newval );
       } );
   } );

   wp.customize( 'top_madori_color', function( value ) {
       value.bind( function( newval ) {
           $('#list_simplepage2 .list_simple_box .list_detail .list_price .dpoint5_td dt').css('color', newval );
           $('.dpoint5_td').css('border-left-color', newval );
           $('.dpoint5').css('color', newval );
           $('#page #content .list_detail .widget ul li .top_madori').css('color', newval );
       } );
   } );

   // wp.customize( 'page_shadow_color', function( value ) {
  //      value.bind( function( newval ) {
  //          $('#page').css('box-shadow', '0 0 20px '+newval );
  //      } );
  //  } );

   wp.customize( 'siteaccent_color', function( value ) {
       value.bind( function( newval ) {
           $('#page').css('border-top-color', newval );
       } );
   } );

   wp.customize( 'h3_color', function( value ) {
       value.bind( function( newval ) {
           $('#main #container #content h3').css('color', newval );
       } );
   } );

   wp.customize( 'h3_bordercolor', function( value ) {
       value.bind( function( newval ) {
           $('#main #container #content h3').css('border-bottom-color', newval );
       } );
   } );

   wp.customize( 'site_bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('.site').css('background-color', newval );
       } );
   } );

   wp.customize( 'top_price_bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('#top_fbox .top_price').css('background-color', newval );
       } );
   } );

   wp.customize( 'top_madori_color', function( value ) {
       value.bind( function( newval ) {
           $('#top_fbox .top_madori').css('color', newval );
       } );
   } );

   wp.customize( 'goto_bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('.box1low a , #list_simplepage .list_simple_box .list_details_button').css('background-color', newval );
       } );
   } );

   wp.customize( 'goto_color', function( value ) {
       value.bind( function( newval ) {
           $('.box1low a , #list_simplepage .list_simple_box .list_details_button').css('color', newval );
       } );
   } );

   wp.customize( 'archive_bordercolor', function( value ) {
       value.bind( function( newval ) {
           $('#list_simplepage .list_simple_box , #list_simplepage .list_simple_boxtitle').css('border-bottom-color', newval );
       } );
   } );

   wp.customize( 'widget_bordercolor', function( value ) {
       value.bind( function( newval ) {
           $('#secondary .widget').css('border-color', newval );
       } );
   } );

   wp.customize( 'widget_h3bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('#secondary.widget-area h3.widget-title').css('background-color', newval );
       } );
   } );

   wp.customize( 'widget_h3color', function( value ) {
       value.bind( function( newval ) {
           $('#secondary .widget .widget-title').css('color', newval );
       } );
   } );

   wp.customize( 'widget_bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('#secondary .widget').css('background-color', newval );
       } );
   } );

   wp.customize( 'jyouken_barcolor', function( value ) {
       value.bind( function( newval ) {
           $('.jsearch_roseneki, .jsearch_chiiki, .jsearch_hofun, .jsearch_chikunen, .jsearch_memseki').css('color', newval );
       } );
   } );

   wp.customize( 'jyouken_barbgcolor', function( value ) {
       value.bind( function( newval ) {
           $('.jsearch_roseneki, .jsearch_chiiki, .jsearch_hofun, .jsearch_chikunen, .jsearch_memseki').css('background-color', newval );
       } );
   } );



   wp.customize( 'detail_bgcolor', function( value ) {
       value.bind( function( newval ) {
           $('#list_simplepage2 .list_detail table#list_add').css('background-color', newval );
       } );
   } );

   wp.customize( 'tel_color', function( value ) {
       value.bind( function( newval ) {
           $('#site-tel .no,#site-tel .prefix,#site-tel .no a').css('color', newval );
       } );
   } );

   wp.customize( 'open_color', function( value ) {
       value.bind( function( newval ) {
           $('#site-open').css('color', newval );
       } );
   } );

   wp.customize( 'jyouken_btnbgcolor', function( value ) {
       value.bind( function( newval ) {
           $('#secondary .widget #searchitem_m input[type=submit],#secondary .widget #searchitem input[type=submit]').css('background-color', newval );
       } );
   } );   

} )( jQuery );