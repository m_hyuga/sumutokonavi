<?php
/**
 * The Template for displaying fudou single posts.
 *
 * Template Name: 賃貸物件を探す
 * 
 * @package WordPress3.5
 * @subpackage Fudousan Plugin
 * Version: 1.2.0
 */
$shu_data = '> 3000' ;
//売買 賃貸
$shub = 2;
$shub_txt = ' (賃貸)';

$site = site_url( '/' ); 
global $wpdb;
get_header(); ?>



	<div id="primary" class="site-content">
		<div id="content" role="main">

<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<header class="entry-header">

			<h1 class="entry-title">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h1>

		</header><!-- .entry-header -->


		<div class="entry-content">
	<script type="text/javascript">// <![CDATA[
	setTimeout('topbukkenwidget_0_0_0()', 1000);
	function topbukkenwidget_0_0_0() {
		jQuery.noConflict();
		var jtop$ = jQuery;
		jtop$('.tab li').click(function() {
			var index = jtop$('.tab li').index(this);
			jtop$('.tab_content li').css('display','none');
			jtop$('.tab_content li').eq(index).css('display','block');
			jtop$('.tab li').removeClass('select');
			jtop$(this).addClass('select')
			});
			jtop$("article.page").addClass("tcintaibukken search_page");
			
			// 開閉
			jtop$(".tab_content dt").on("click", function() {
            jtop$(this).next().slideToggle();
            jtop$(this).toggleClass("close");
        });
			};	
// ]]></script>
<ul class="tab">
	<li class="select">条件から探す</li>
	<li>住所から探す</li>
	<li>沿線駅から探す</li>
	<li>地図から探す</li>
</ul>
<ul class="tab_content">
	<li class="jyoken">
	<form method="get" id="searchpage" name="searchpage" action="<?php echo site_url("/");?>" >
	<input type="hidden" name="bukken" value="jsearch" >
<input type="hidden" name="shub" value="2" >
	<h4>物件種別(賃貸)</h4>
	<div class="bukkenshu">
<?php
	$sql  =  " SELECT DISTINCT PM.meta_value AS bukkenshubetsu";
			$sql .=  " FROM $wpdb->posts as P ";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM.meta_key='bukkenshubetsu' ";
			$sql .=  " AND CAST( PM.meta_value AS SIGNED ) ". $shu_data ;
			$sql .=  " ORDER BY PM.meta_value";
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			if(!empty($metas)) {

				foreach ( $metas as $meta ) {
					$bukkenshubetsu_id = $meta['bukkenshubetsu'];

					foreach($work_bukkenshubetsu as $meta_box){
						if( $bukkenshubetsu_id ==  $meta_box['id'] ){
							echo '<label for="'.$meta_box['id'].'">';
							echo '<input type="checkbox" name="shu[]"  value="'.$meta_box['id'].'" id="'.$meta_box['id'].'">';
							echo $meta_box['name'];
							echo '</label>';
						}
					}
				}

			}
		?>
	</div>
	<h4>条件を設定する</h4>
	<div class="sentaku">
	<span>賃料</span>
<select name="kalc" id="kalc">
<option value="0">下限なし</option>
<option value="3">3万円</option>
<option value="4">4万円</option>
<option value="5">5万円</option>
<option value="6">6万円</option>
<option value="7">7万円</option>
<option value="8">8万円</option>
<option value="9">9万円</option>
<option value="10">10万円</option>
<option value="11">11万円</option>
<option value="12">12万円</option>
<option value="13">13万円</option>
<option value="14">14万円</option>
<option value="15">15万円</option>
<option value="16">16万円</option>
<option value="17">17万円</option>
<option value="18">18万円</option>
<option value="19">19万円</option>
<option value="20">20万円</option>
<option value="30">30万円</option>
<option value="50">50万円</option>
<option value="100">100万円</option>
</select>　から　
<select name="kahc" id="kahc">
<option value="3">3万円</option>
<option value="4">4万円</option>
<option value="5">5万円</option>
<option value="6">6万円</option>
<option value="7">7万円</option>
<option value="8">8万円</option>
<option value="9">9万円</option>
<option value="10">10万円</option>
<option value="11">11万円</option>
<option value="12">12万円</option>
<option value="13">13万円</option>
<option value="14">14万円</option>
<option value="15">15万円</option>
<option value="16">16万円</option>
<option value="17">17万円</option>
<option value="18">18万円</option>
<option value="19">19万円</option>
<option value="20">20万円</option>
<option value="30">30万円</option>
<option value="50">50万円</option>
<option value="100">100万円</option>
<option value="0" selected="selected">上限なし</option>
</select>
	<label><input type="checkbox" name="shikikin" value="敷金なし">敷金なし</label><label><input type="checkbox" name="reikin" value="礼金なし">礼金なし</label><br>
	<span>土地面積</span><select name="mel" id="mel">
	<option value="0">下限なし</option>
	<option value="10">10m²</option>
	<option value="15">15m²</option>
	<option value="20">20m²</option>
	<option value="25">25m²</option>
	<option value="30">30m²</option>
	<option value="35">35m²</option>
	<option value="40">40m²</option>
	<option value="50">50m²</option>
	<option value="60">60m²</option>
	<option value="70">70m²</option>
	<option value="80">80m²</option>
	<option value="90">90m²</option>
	<option value="100">100m²</option>
	<option value="200">200m²</option>
	<option value="300">300m²</option>
	<option value="400">400m²</option>
	<option value="500">500m²</option>
	<option value="600">600m²</option>
	<option value="700">700m²</option>
	<option value="800">800m²</option>
	<option value="900">900m²</option>
	<option value="1000">1000m²</option>
	</select>　から　<select name="meh" id="meh">
	<option value="10">10m²</option>
	<option value="15">15m²</option>
	<option value="20">20m²</option>
	<option value="25">25m²</option>
	<option value="30">30m²</option>
	<option value="35">35m²</option>
	<option value="40">40m²</option>
	<option value="50">50m²</option>
	<option value="60">60m²</option>
	<option value="70">70m²</option>
	<option value="80">80m²</option>
	<option value="90">90m²</option>
	<option value="100">100m²</option>
	<option value="200">200m²</option>
	<option value="300">300m²</option>
	<option value="400">400m²</option>
	<option value="500">500m²</option>
	<option value="600">600m²</option>
	<option value="700">700m²</option>
	<option value="800">800m²</option>
	<option value="900">900m²</option>
	<option value="1000">1000m²</option>
	<option value="0" selected="selected">上限なし</option>
	</select>	
	<br>
<span>築年数</span>
<select name="tik" id="tik">
<option value="0">指定なし</option>
<option value="1">1年以内(新築)</option>
<option value="3">3年以内</option>
<option value="5">5年以内</option>
<option value="10">10年以内</option>
<option value="15">15年以内</option>
<option value="20">20年以内</option>
</select>
	</div>
	<h4>間取り</h4>
	<div class="madori">
<?php 

		if( $shu_data !='' ){

			$sql  =  "SELECT DISTINCT PM.meta_value AS madorisu,PM_2.meta_value AS madorisyurui";
			$sql .=  " FROM ((($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id)) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
			$sql .=  " AND PM.meta_key='madorisu'";
			$sql .=  " AND PM_2.meta_key='madorisyurui'";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$madori_dat = '';
			if(!empty($metas)) {

				//ソート
				foreach($metas as $key => $row1){
					$foo1[$key] = $row1["madorisu"];
					$bar1[$key] = $row1["madorisyurui"];
				}
				array_multisort($foo1,SORT_ASC,$bar1,SORT_ASC,$metas);

				$madori_dat .= '<ul>';
				foreach ( $metas as $meta ) {

					$madorisu_data = $meta['madorisu'];
					$madorisyurui_data = $meta['madorisyurui'];

					if( $madorisu_data == 11 ) break;

					$madori_code = $madorisu_data;
					$madori_code .= $madorisyurui_data;

					foreach( $work_madori as $meta_box ){
						if( $madorisyurui_data == $meta_box['code'] ){
							$madori_dat .= '<li><input name="mad[]" value="'.$madori_code.'" id="mad2'.$madori_code.'" type="checkbox"';
							$madori_dat .= ' /><label for="mad2'.$madori_code.'">'.$madorisu_data.$meta_box['name'].'</label></li>';
						}
					}
				}
				$madori_dat .= '</ul>';
			}
		}
?>
	</div>
	<h4>こだわり条件</h4>
	<div class="kodawari"><label><input type="checkbox" name="kodawari" value="">即入居可</label><label><input type="checkbox" name="kodawari" value="">ペット相談可</label><label><input type="checkbox" name="kodawari" value="">楽器相談可</label><label><input type="checkbox" name="kodawari" value="">単身者限定</label>
	</div>
	<p class="btn_area"><input type="submit" value="この条件で検索"></p>
	</form>
	</li>
	<li class="hide jyusyo">
	<form method="get" id="searchpage" name="searchpage" action="<?php echo site_url("/");?>" >
	<input type="hidden" name="bukken" value="jsearch" >
<input type="hidden" name="shub" value="2" >
	<h4>エリアの選択</h4>
	<div class="area">
	<?php

	//営業県
			$ken_id = '';
			for( $i=1; $i<48 ; $i++ ){
				if( get_option('ken'.$i) != ''){

					$ken_id = get_option('ken'.$i);

					$sql  =  "SELECT DISTINCT NA.narrow_area_name, LEFT(PM.meta_value,5) as middle_narrow_area_id";
					$sql .=  " FROM (($wpdb->posts as P";
					$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id) ";
					$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
					$sql .=  " INNER JOIN ".$wpdb->prefix."area_narrow_area as NA ON CAST( RIGHT(LEFT(PM.meta_value,5),3) AS SIGNED ) = NA.narrow_area_id";
					$sql .=  " WHERE PM.meta_key='shozaichicode' ";
					$sql .=  " AND P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
					$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
					$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
					$sql .=  " AND CAST( LEFT(PM.meta_value,2) AS SIGNED ) =  ". $ken_id . "";
					$sql .=  " AND NA.middle_area_id = ". $ken_id . "";
					$sql .=  " ORDER BY CAST( PM.meta_value AS SIGNED )";

					$sql = $wpdb->prepare($sql,'');
					$metas = $wpdb->get_results( $sql,  ARRAY_A );
					if(!empty($metas)) {
						echo "<dl>";
						echo '<dt>'.fudo_ken_name($i).'</dt>';
						echo '<dd>';
						foreach ( $metas as $meta ) {
							$middle_narrow_area_id = $meta['middle_narrow_area_id'];
							$narrow_area_name = $meta['narrow_area_name'];
							echo '<label>';
							echo '<input type="checkbox" name="ksik[]"  value="'.$middle_narrow_area_id.'" id="'.$middle_narrow_area_id.'">';
							echo $narrow_area_name;
							echo '</label>';
						}
						echo '</dd>';
						echo "</dl>";
					}

				}
			}

?>		
	</div>
	<h4>物件種別(賃貸)</h4>
	<div class="bukkenshu">
<?php
	$sql  =  " SELECT DISTINCT PM.meta_value AS bukkenshubetsu";
			$sql .=  " FROM $wpdb->posts as P ";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM.meta_key='bukkenshubetsu' ";
			$sql .=  " AND CAST( PM.meta_value AS SIGNED ) ". $shu_data ;
			$sql .=  " ORDER BY PM.meta_value";
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			if(!empty($metas)) {

				foreach ( $metas as $meta ) {
					$bukkenshubetsu_id = $meta['bukkenshubetsu'];

					foreach($work_bukkenshubetsu as $meta_box){
						if( $bukkenshubetsu_id ==  $meta_box['id'] ){
							echo '<label for="'.$meta_box['id'].'">';
							echo '<input type="checkbox" name="shu[]"  value="'.$meta_box['id'].'" id="'.$meta_box['id'].'">';
							echo $meta_box['name'];
							echo '</label>';
						}
					}
				}

			}
		?>
	</div>
	<h4>条件を設定する</h4>
	<div class="sentaku">
	<span>賃料</span>
<select name="kalc" id="kalc">
<option value="0">下限なし</option>
<option value="3">3万円</option>
<option value="4">4万円</option>
<option value="5">5万円</option>
<option value="6">6万円</option>
<option value="7">7万円</option>
<option value="8">8万円</option>
<option value="9">9万円</option>
<option value="10">10万円</option>
<option value="11">11万円</option>
<option value="12">12万円</option>
<option value="13">13万円</option>
<option value="14">14万円</option>
<option value="15">15万円</option>
<option value="16">16万円</option>
<option value="17">17万円</option>
<option value="18">18万円</option>
<option value="19">19万円</option>
<option value="20">20万円</option>
<option value="30">30万円</option>
<option value="50">50万円</option>
<option value="100">100万円</option>
</select>　から　
<select name="kahc" id="kahc">
<option value="3">3万円</option>
<option value="4">4万円</option>
<option value="5">5万円</option>
<option value="6">6万円</option>
<option value="7">7万円</option>
<option value="8">8万円</option>
<option value="9">9万円</option>
<option value="10">10万円</option>
<option value="11">11万円</option>
<option value="12">12万円</option>
<option value="13">13万円</option>
<option value="14">14万円</option>
<option value="15">15万円</option>
<option value="16">16万円</option>
<option value="17">17万円</option>
<option value="18">18万円</option>
<option value="19">19万円</option>
<option value="20">20万円</option>
<option value="30">30万円</option>
<option value="50">50万円</option>
<option value="100">100万円</option>
<option value="0" selected="selected">上限なし</option>
</select>
	<label><input type="checkbox" name="shikikin" value="敷金なし">敷金なし</label><label><input type="checkbox" name="reikin" value="礼金なし">礼金なし</label><br>
	<span>土地面積</span><select name="mel" id="mel">
	<option value="0">下限なし</option>
	<option value="10">10m²</option>
	<option value="15">15m²</option>
	<option value="20">20m²</option>
	<option value="25">25m²</option>
	<option value="30">30m²</option>
	<option value="35">35m²</option>
	<option value="40">40m²</option>
	<option value="50">50m²</option>
	<option value="60">60m²</option>
	<option value="70">70m²</option>
	<option value="80">80m²</option>
	<option value="90">90m²</option>
	<option value="100">100m²</option>
	<option value="200">200m²</option>
	<option value="300">300m²</option>
	<option value="400">400m²</option>
	<option value="500">500m²</option>
	<option value="600">600m²</option>
	<option value="700">700m²</option>
	<option value="800">800m²</option>
	<option value="900">900m²</option>
	<option value="1000">1000m²</option>
	</select>　から　<select name="meh" id="meh">
	<option value="10">10m²</option>
	<option value="15">15m²</option>
	<option value="20">20m²</option>
	<option value="25">25m²</option>
	<option value="30">30m²</option>
	<option value="35">35m²</option>
	<option value="40">40m²</option>
	<option value="50">50m²</option>
	<option value="60">60m²</option>
	<option value="70">70m²</option>
	<option value="80">80m²</option>
	<option value="90">90m²</option>
	<option value="100">100m²</option>
	<option value="200">200m²</option>
	<option value="300">300m²</option>
	<option value="400">400m²</option>
	<option value="500">500m²</option>
	<option value="600">600m²</option>
	<option value="700">700m²</option>
	<option value="800">800m²</option>
	<option value="900">900m²</option>
	<option value="1000">1000m²</option>
	<option value="0" selected="selected">上限なし</option>
	</select>
	<br>
<span>築年数</span>
<select name="tik" id="tik">
<option value="0">指定なし</option>
<option value="1">1年以内(新築)</option>
<option value="3">3年以内</option>
<option value="5">5年以内</option>
<option value="10">10年以内</option>
<option value="15">15年以内</option>
<option value="20">20年以内</option>
</select>
	</div>
	<h4>間取り</h4>
	<div class="madori">
<?php 

		if( $shu_data !='' ){

			$sql  =  "SELECT DISTINCT PM.meta_value AS madorisu,PM_2.meta_value AS madorisyurui";
			$sql .=  " FROM ((($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id)) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
			$sql .=  " AND PM.meta_key='madorisu'";
			$sql .=  " AND PM_2.meta_key='madorisyurui'";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$madori_dat = '';
			if(!empty($metas)) {

				//ソート
				foreach($metas as $key => $row1){
					$foo1[$key] = $row1["madorisu"];
					$bar1[$key] = $row1["madorisyurui"];
				}
				array_multisort($foo1,SORT_ASC,$bar1,SORT_ASC,$metas);

				$madori_dat .= '<ul>';
				foreach ( $metas as $meta ) {

					$madorisu_data = $meta['madorisu'];
					$madorisyurui_data = $meta['madorisyurui'];

					if( $madorisu_data == 11 ) break;

					$madori_code = $madorisu_data;
					$madori_code .= $madorisyurui_data;

					foreach( $work_madori as $meta_box ){
						if( $madorisyurui_data == $meta_box['code'] ){
							$madori_dat .= '<li><input name="mad[]" value="'.$madori_code.'" id="mad2'.$madori_code.'" type="checkbox"';
							$madori_dat .= ' /><label for="mad2'.$madori_code.'">'.$madorisu_data.$meta_box['name'].'</label></li>';
						}
					}
				}
				$madori_dat .= '</ul>';
			}
		}
?>
	</div>
	<h4>こだわり条件</h4>
	<div class="kodawari"><label><input type="checkbox" name="kodawari" value="">即入居可</label><label><input type="checkbox" name="kodawari" value="">ペット相談可</label><label><input type="checkbox" name="kodawari" value="">楽器相談可</label><label><input type="checkbox" name="kodawari" value="">単身者限定</label>
	</div>
	<p class="btn_area"><input type="submit" value="この条件で検索"></p>
	</form>
	</li>
	<li class="hide ensen">
	<form method="get" id="searchpage" name="searchpage" action="<?php echo site_url("/");?>" >
	<input type="hidden" name="bukken" value="jsearch" >
<input type="hidden" name="shub" value="2" >
	<h4>沿線駅を選択する</h4>
	<div class="ensen">
	<?php
		$sql  = "SELECT DISTINCT DTR.rosen_name,DTR.rosen_id,DTS.station_name, DTS.station_id ,DTS.station_ranking ";
			$sql .= " FROM ((((( $wpdb->posts as P ) ";
			$sql .= " INNER JOIN $wpdb->postmeta as PM ON P.ID = PM.post_id ) ";
			$sql .= " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id ) ";
			$sql .= " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ) ";
			$sql .= " INNER JOIN ".$wpdb->prefix."train_rosen as DTR ON CAST( PM_1.meta_value AS SIGNED ) = DTR.rosen_id) ";
			$sql .= " INNER JOIN ".$wpdb->prefix."train_station as DTS ON DTS.rosen_id = DTR.rosen_id AND  CAST( PM.meta_value AS SIGNED ) = DTS.station_id";
			$sql .= " WHERE";
			$sql .= "  ( P.post_status='publish' ";
			$sql .= " AND P.post_password = '' ";
			$sql .= " AND P.post_type ='fudo' ";
			$sql .= " AND PM.meta_key='koutsueki1' ";
			$sql .= " AND PM_1.meta_key='koutsurosen1' ";
			$sql .= " AND PM_2.meta_key='bukkenshubetsu' ";
			$sql .= " AND PM_2.meta_value $shu_data ) ";
			$sql .= " OR ";
			$sql .= " ( P.post_status='publish' ";
			$sql .= " AND P.post_password = '' ";
			$sql .= " AND P.post_type ='fudo' ";
			$sql .= " AND PM.meta_key='koutsueki2' ";
			$sql .= " AND PM_1.meta_key='koutsurosen2' ";
			$sql .= " AND PM_2.meta_key='bukkenshubetsu' ";
			$sql .= " AND PM_2.meta_value $shu_data )";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql, ARRAY_A );

			if(!empty($metas)) {

				//ソート
				foreach($metas as $key => $row){
					$foo[$key] = $row["rosen_name"];
					$bar[$key] = $row["station_ranking"];
				}
				array_multisort($foo,SORT_DESC,$bar,SORT_ASC,$metas);

				$tmp_rosen_id= '';

				foreach ( $metas as $meta ) {

					$rosen_name =  $meta['rosen_name'];
					$rosen_id   =  $meta['rosen_id'];
					$station_name =  $meta['station_name'];
					$station_id   =  $meta['station_id'];

					$ros_id = sprintf('%06d', $rosen_id );

					//路線表示
					if( $tmp_rosen_id != $rosen_id){
						if( $tmp_rosen_id != '') echo "</dd></dl>\n";
						echo "<dl>";
						echo '<dt>'.$rosen_name.'</dt>';
						echo '<dd>';
					}
					//駅表示
						$station_id = $ros_id . ''. sprintf('%06d', $station_id);
						echo '<label><input type="checkbox" name="re[]" value="'.$station_id.'" id="eki'.$station_id.'" />';
						echo $station_name.'</label>';
					$tmp_rosen_id   = $rosen_id;
				}
				echo "</dl>\n";
			}
			?>
	</div>
	<h4>物件種別(賃貸)</h4>
	<div class="bukkenshu">
<?php
	$sql  =  " SELECT DISTINCT PM.meta_value AS bukkenshubetsu";
			$sql .=  " FROM $wpdb->posts as P ";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM ON P.ID = PM.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM.meta_key='bukkenshubetsu' ";
			$sql .=  " AND CAST( PM.meta_value AS SIGNED ) ". $shu_data ;
			$sql .=  " ORDER BY PM.meta_value";
			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );

			if(!empty($metas)) {

				foreach ( $metas as $meta ) {
					$bukkenshubetsu_id = $meta['bukkenshubetsu'];

					foreach($work_bukkenshubetsu as $meta_box){
						if( $bukkenshubetsu_id ==  $meta_box['id'] ){
							echo '<label for="'.$meta_box['id'].'">';
							echo '<input type="checkbox" name="shu[]"  value="'.$meta_box['id'].'" id="'.$meta_box['id'].'">';
							echo $meta_box['name'];
							echo '</label>';
						}
					}
				}

			}
		?>
	</div>
	<h4>条件を設定する</h4>
	<div class="sentaku">
	<span>賃料</span>
<select name="kalc" id="kalc">
<option value="0">下限なし</option>
<option value="3">3万円</option>
<option value="4">4万円</option>
<option value="5">5万円</option>
<option value="6">6万円</option>
<option value="7">7万円</option>
<option value="8">8万円</option>
<option value="9">9万円</option>
<option value="10">10万円</option>
<option value="11">11万円</option>
<option value="12">12万円</option>
<option value="13">13万円</option>
<option value="14">14万円</option>
<option value="15">15万円</option>
<option value="16">16万円</option>
<option value="17">17万円</option>
<option value="18">18万円</option>
<option value="19">19万円</option>
<option value="20">20万円</option>
<option value="30">30万円</option>
<option value="50">50万円</option>
<option value="100">100万円</option>
</select>　から　
<select name="kahc" id="kahc">
<option value="3">3万円</option>
<option value="4">4万円</option>
<option value="5">5万円</option>
<option value="6">6万円</option>
<option value="7">7万円</option>
<option value="8">8万円</option>
<option value="9">9万円</option>
<option value="10">10万円</option>
<option value="11">11万円</option>
<option value="12">12万円</option>
<option value="13">13万円</option>
<option value="14">14万円</option>
<option value="15">15万円</option>
<option value="16">16万円</option>
<option value="17">17万円</option>
<option value="18">18万円</option>
<option value="19">19万円</option>
<option value="20">20万円</option>
<option value="30">30万円</option>
<option value="50">50万円</option>
<option value="100">100万円</option>
<option value="0" selected="selected">上限なし</option>
</select>
	<label><input type="checkbox" name="shikikin" value="敷金なし">敷金なし</label><label><input type="checkbox" name="reikin" value="礼金なし">礼金なし</label><br>
	<span>土地面積</span><select name="mel" id="mel">
	<option value="0">下限なし</option>
	<option value="10">10m²</option>
	<option value="15">15m²</option>
	<option value="20">20m²</option>
	<option value="25">25m²</option>
	<option value="30">30m²</option>
	<option value="35">35m²</option>
	<option value="40">40m²</option>
	<option value="50">50m²</option>
	<option value="60">60m²</option>
	<option value="70">70m²</option>
	<option value="80">80m²</option>
	<option value="90">90m²</option>
	<option value="100">100m²</option>
	<option value="200">200m²</option>
	<option value="300">300m²</option>
	<option value="400">400m²</option>
	<option value="500">500m²</option>
	<option value="600">600m²</option>
	<option value="700">700m²</option>
	<option value="800">800m²</option>
	<option value="900">900m²</option>
	<option value="1000">1000m²</option>
	</select>　から　<select name="meh" id="meh">
	<option value="10">10m²</option>
	<option value="15">15m²</option>
	<option value="20">20m²</option>
	<option value="25">25m²</option>
	<option value="30">30m²</option>
	<option value="35">35m²</option>
	<option value="40">40m²</option>
	<option value="50">50m²</option>
	<option value="60">60m²</option>
	<option value="70">70m²</option>
	<option value="80">80m²</option>
	<option value="90">90m²</option>
	<option value="100">100m²</option>
	<option value="200">200m²</option>
	<option value="300">300m²</option>
	<option value="400">400m²</option>
	<option value="500">500m²</option>
	<option value="600">600m²</option>
	<option value="700">700m²</option>
	<option value="800">800m²</option>
	<option value="900">900m²</option>
	<option value="1000">1000m²</option>
	<option value="0" selected="selected">上限なし</option>
	</select>	
	<br>
<span>築年数</span>
<select name="tik" id="tik">
<option value="0">指定なし</option>
<option value="1">1年以内(新築)</option>
<option value="3">3年以内</option>
<option value="5">5年以内</option>
<option value="10">10年以内</option>
<option value="15">15年以内</option>
<option value="20">20年以内</option>
</select>
	</div>
	<h4>間取り</h4>
	<div class="madori">
<?php 

		if( $shu_data !='' ){

			$sql  =  "SELECT DISTINCT PM.meta_value AS madorisu,PM_2.meta_value AS madorisyurui";
			$sql .=  " FROM ((($wpdb->posts as P";
			$sql .=  " INNER JOIN $wpdb->postmeta as PM   ON P.ID = PM.post_id)) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_1 ON P.ID = PM_1.post_id) ";
			$sql .=  " INNER JOIN $wpdb->postmeta AS PM_2 ON P.ID = PM_2.post_id ";
			$sql .=  " WHERE P.post_status='publish' AND P.post_password = '' AND P.post_type ='fudo' ";
			$sql .=  " AND PM_1.meta_key='bukkenshubetsu'";
			$sql .=  " AND CAST( PM_1.meta_value AS SIGNED ) ".$shu_data."";
			$sql .=  " AND PM.meta_key='madorisu'";
			$sql .=  " AND PM_2.meta_key='madorisyurui'";

			$sql = $wpdb->prepare($sql,'');
			$metas = $wpdb->get_results( $sql,  ARRAY_A );
			$madori_dat = '';
			if(!empty($metas)) {

				//ソート
				foreach($metas as $key => $row1){
					$foo1[$key] = $row1["madorisu"];
					$bar1[$key] = $row1["madorisyurui"];
				}
				array_multisort($foo1,SORT_ASC,$bar1,SORT_ASC,$metas);

				$madori_dat .= '<ul>';
				foreach ( $metas as $meta ) {

					$madorisu_data = $meta['madorisu'];
					$madorisyurui_data = $meta['madorisyurui'];

					if( $madorisu_data == 11 ) break;

					$madori_code = $madorisu_data;
					$madori_code .= $madorisyurui_data;

					foreach( $work_madori as $meta_box ){
						if( $madorisyurui_data == $meta_box['code'] ){
							$madori_dat .= '<li><input name="mad[]" value="'.$madori_code.'" id="mad2'.$madori_code.'" type="checkbox"';
							$madori_dat .= ' /><label for="mad2'.$madori_code.'">'.$madorisu_data.$meta_box['name'].'</label></li>';
						}
					}
				}
				$madori_dat .= '</ul>';
			}
		}
?>
	</div>
	<h4>こだわり条件</h4>
	<div class="kodawari"><label><input type="checkbox" name="kodawari" value="">即入居可</label><label><input type="checkbox" name="kodawari" value="">ペット相談可</label><label><input type="checkbox" name="kodawari" value="">楽器相談可</label><label><input type="checkbox" name="kodawari" value="">単身者限定</label>
	</div>
	<p class="btn_area"><input type="submit" value="この条件で検索"></p>
	</form>
	</li>
	<li class="hide tizu">
	<div><iframe src="http://tochi-navi.net/wp/fmap.php?jyo2=0" height="700" width="670" frameborder="0"></iframe></div>

	</li>
</ul>



		</div><!-- .entry-content -->


	</article><!-- #post -->

	
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>