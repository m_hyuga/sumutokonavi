<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<div class="spnone">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div>
			<?php wp_nav_menu( array( 'theme_location' => 'spmenu', 'menu_class' => 'pcnone side_menu' ) ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>
