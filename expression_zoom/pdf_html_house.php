<!-- container  -->
<div id="container">
    <?php
	
    global $wpdb;
    //サムネイル画像
    $img_path = get_option('upload_path');
    if ($img_path == '')	$img_path = 'wp-content/uploads';
    //$imgid=1;
	$img_out = array();
    for( $imgid=1; $imgid<=2; $imgid++ ){
    	if($imgid == 1){$class='class="img-1st"';}else{$class="";}
    	$fudoimg_data = get_post_meta($post_id, "fudoimg$imgid", true);
    	$fudoimgcomment_data = get_post_meta($post_id, "fudoimgcomment$imgid", true);
    	$fudoimg_alt = $fudoimgcomment_data . my_custom_fudoimgtype_print(get_post_meta($post_id, "fudoimgtype$imgid", true));

    	if($fudoimg_data !="" ){
    			$sql  = "";
    			$sql .=  "SELECT P.ID,P.guid";
    			$sql .=  " FROM $wpdb->posts as P";
    			$sql .=  " WHERE P.post_type ='attachment' AND P.guid LIKE '%/$fudoimg_data' ";
    		//	$sql = $wpdb->prepare($sql,'');
    			$metas = $wpdb->get_row( $sql );
    			$attachmentid = '';
    			if( !empty($metas) ){
    				$attachmentid  =  $metas->ID;
    				$guid_url  =  $metas->guid;
					$guid_url  =  preg_replace("/http(.+)wp-content/","wp-content",$guid_url);
    			}

    			if($attachmentid !=''){
    				//thumbnail、medium、large、full
    				$fudoimg_data1 = wp_get_attachment_image_src( $attachmentid, 'middle');
    				$fudoimg_url = $fudoimg_data1[0];
					$fudoimg_url  =  preg_replace("/http(.+)wp-content/","wp-content",$fudoimg_url);
					
					if($imgid==1){$style='style="width:250px;max-height:180px;"';}else{$style='style="max-height:180px;width:250px;"';}
    				//echo '<li '.$class.'><a href="' . $guid_url . '" rel="lightbox lytebox['.$post_id.']" title="'.$fudoimg_alt.'">';
    				if($fudoimg_url !=''){
    					$img_out[$imgid] = '<img src="' . $fudoimg_url.'" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" '.$style.'/>';
    				}else{
    					$img_out[$imgid] = '<img src="' . $guid_url . '" alt="'.$fudoimg_alt.'" title="'.$fudoimg_alt.'" '.$style.'/>';
    				}
    			}else{
    				$img_out[$imgid] = '<img src="'.IMGPATH.'/fudogs/img/nowprinting.jpg" alt="'. $fudoimg_data .'" '.$style.' />';
    			}
    	}else{
    		if( $imgid==1 )
    		$img_out[$imgid] = '<img src="'.IMGPATH.'/fudogs/img/nowprinting.jpg" alt="" '.$style.' />';
    	}
    	$img_out[$imgid] .= "\n";
    }
    ?>
      <?php
        $gmapUrl = 'http://maps.google.com/maps/api/staticmap?';
        $lat = get_post_meta($post_id,'bukkenido',true);
        $lng = get_post_meta($post_id,'bukkenkeido',true);
        $zoom = 16; //ズーム レベル
        $size = '253x300'; //画像のサイズ
        $sensor='false'; //センサーの使用状況の表示
        $format = 'png'; //画像形式
        $color = 'red'; //マーカー色
        $label = 'S'; //マーカーラベル
        $saveFile = 'tmp.'. $format;
		$language = 'ja';
        $gmap  = $gmapUrl. 'center='. $lat. ','. $lng. '&zoom='. $zoom. '&size='. $size. '&sensor='. $sensor. '&language='. $language . '&format='. $format;
        $gmap .= '&markers=color:'. $color. '|label:'. $label. '|'. $lat. ','. $lng;
      ?>
<!-- upper  -->
<div id="upper">

<!-- left-Column  -->
<div id="left-Column">


<!-- header  -->
<div id="header">
<h1 class="header_title"><?php echo $title;?></h1>
<h2 class="header_access" style="font-family: meiryo;"><?php my_custom_koutsu1_print($post_id); ?>
     <?php my_custom_koutsu2_print($post_id); ?></h2>
</div><!-- end of header  -->
    
    
    
    
    
<!-- content  -->
<div id="content">


<table border="0" class="special">
<tr>
<td><div class="special01">間取り<span class="specialspan"><?php if( get_post_meta($post_id, 'madorisu', true) !=""){ ;?><?php my_custom_madorisu_print($post_id); ?><?php } ?> <?php echo get_post_meta($post_id, 'madoribiko', true);?></span></div></td>
<td><div class="special02">延床面積<span class="specialspan"><?php echo get_post_meta($post_id, 'tatemonomenseki', true);?> m&sup2;</span></div></td>
<td><div class="special03">土地面積<span class="specialspan"><?php echo get_post_meta($post_id, 'tochikukaku', true);?> m&sup2;</span></div></td>
</tr>
</table>



<div id="main-img">
<div class="img-left">
<p class="mb5"><?php if(!empty($img_out[2])){echo $img_out[2];}?></p>
<p class="bd"><?php if(!empty($img_out[1])){echo $img_out[1];}?></p>
</div>
<div class="img-right"><p><img src="<?php echo $gmap;?>" alt="*"></p></div>
</div>


    
<div class="comment_bd">
<table id="comment">
<tr>
<?php /*<th rowspan="2" scope="row"><img src="<?php echo IMGPATH;?>/pdfimg/kaeru.png" alt="かえる"></th>*/?>
<td class="sales_comment">セールスコメント</td>
</tr>
<tr>
<td>
<p class="comment_space">
<?php fudo_sales_comment($pdf);?>
</p>
</td>
</tr>
</table>
</div>
 
</div><!-- end of content  -->

</div><!-- end of left-Column  -->
    
    
    
    
<!-- right-Column  -->
<div id="right-Column">

<table id="right_table">
<tr>
<th scope="row">物件種別</th>
<td colspan="3" class="font1"><?php my_custom_bukkenshubetsu_print($post_id); ?></td>
</tr>
<tr>
<th scope="row">価格</th>
<td colspan="3" class="font1"><?php my_custom_kakaku_print($post_id);?></td>
</tr>
<tr>
<th scope="row">所在地</th>
<td colspan="3"><?php my_custom_shozaichi_print($post_id); ?><?php echo get_post_meta($post_id, 'shozaichimeisho', true); ?><?php echo get_post_meta($post_id, 'shozaichimeisho2', true); ?></td>
</tr>
<tr>
<th scope="row">延床面積</th>
<td><?php echo get_post_meta($post_id, 'tatemonomenseki', true);?> m&sup2;</td>
<th scope="row">土地面積</th>
<td class="right-Column-w02"><?php echo get_post_meta($post_id, 'tochikukaku', true);?> m&sup2;</td>
</tr>
<tr>
<th scope="row">築年月<br>（築年数）</th>
<td><?php if(get_post_meta($post_id, 'tatemonochikunenn', true)){echo date("Y年m月",strtotime(get_post_meta($post_id, 'tatemonochikunenn', true) . "/01"));}else{echo "-";}?></td>
<th scope="row">建ぺい率</th>
<td><?php echo get_post_meta($post_id, 'tochikenpei', true);?>％</td>
</tr>
<tr>
<th scope="row">建物構造</th>
<td><?php my_custom_tatemonokozo_print($post_id) ?></td>
<th scope="row">容積率</th>
<td><?php echo get_post_meta($post_id, 'tochiyoseki', true);?>％</td>
</tr>
<tr>
<th scope="row">用途地域</th>
<td><?php my_custom_tochiyouto_print($post_id);?></td>
<th scope="row">都市計画</th>
<td><?php  my_custom_tochikeikaku_print($post_id) ;?></td>
</tr>
<tr>
<th scope="row">駐車場</th>
<td><?php my_custom_chushajo_print($post_id);?></td>
<th scope="row">土地権利</th>
<td><?php my_custom_tochikenri_print($post_id);?></td>
</tr>
<tr>
<th scope="row">引渡し</th>
<td><?php my_custom_nyukyojiki_print($post_id); ?>
<?php echo get_post_meta($post_id, 'nyukyonengetsu', true);?>
<?php my_custom_nyukyosyun_print($post_id);?></td>
<th scope="row">取引態様</th>
<td><?php my_custom_torihikitaiyo_print($post_id);?></td>
</tr>
<tr>
<th scope="row">接道状況</th>
<td colspan="3"><?php my_custom_tochisetsudo_print($post_id);?><?php  my_custom_tochisetsudohouko1_print($post_id);?></td>
</tr>

<tr>
<th scope="row">小学校</th>
<td><?php echo get_post_meta($post_id,"shuuhenshougaku",true);?></td>
<th scope="row">中学校</th>
<td><?php echo get_post_meta($post_id,"shuuhenchuugaku",true);?></td>
</tr>
<tr>
<th scope="row">設備・備考</th>
<td colspan="3"><?php my_custom_setsubi_print($post_id);?>
</tr>
</table>



</div><!-- end of right-Column  -->
</div><!-- end of upper  -->




<?php get_footer("pdf");?>

</div><!-- end of container  -->