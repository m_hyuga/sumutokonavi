<?php
/**
 * The Template for displaying fudou single posts.
 *
 * Template Name: PDF出力
 *
 * @package WordPress3.5
 * @subpackage Fudousan Plugin
 * Version: 1.2.0
 */
if(empty($_GET["post_id"]))wp_redirect( home_url(), 302 ); 
$post_id = $_GET["post_id"];
define("BUKKEN_NO",get_post_meta($post_id,"shikibesu",true));
$view = isset($_GET["view"])?true:false;
$pdf = get_post($post_id);
$kaiin = 0;
if( !is_user_logged_in() && get_post_meta($post_id, 'kaiin', true) == 1 ) $kaiin = 1;
//ユーザー別会員物件リスト
$kaiin_users_rains_register = get_option('kaiin_users_rains_register');
$kaiin2 = users_kaiin_bukkenlist($post_id,$kaiin_users_rains_register,get_post_meta($post_id, 'kaiin', true));
$title = $pdf->post_title;
require get_stylesheet_directory() . "/mpdf/mpdf.php";
$mpdf=new mPDF('ja', 'A4-L',0,'',5,5,5,5,1,1);
$mpdf->SetFont("meiryo");
if(!$view){
$media_url  = WP_CONTENT_URL . "/uploads/pdf/" . $post_id . ".pdf";
$media_path = WP_CONTENT_DIR . "/uploads/pdf/" . $post_id . ".pdf";
/*if(file_exists($media_path)){
	wp_redirect($media_url);
	die();
}*/
ob_start();
}

$bukkenshubetsu = get_post_meta($post_id,'shu_cat',true);
if(in_array($bukkenshubetsu,array(21))){
		$type="_man";
	}elseif(in_array($bukkenshubetsu,array(22,12,23,13,15))){
		$type="_house";
	}else{
		$type="_land";
	}


require get_stylesheet_directory() . "/pdf_func.php";
require get_stylesheet_directory() . "/pdf.css";
require get_stylesheet_directory() . "/pdf_html{$type}.php";

if(!$view){
$html = ob_get_contents();
ob_end_clean();
$mpdf->WriteHTML($html);
$mpdf->Output();
//$mpdf->Output($post_id . ".pdf","D");
}


?>