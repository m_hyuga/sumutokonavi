<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->
	<div class="page-top"><a href="#page" id="link_to_top"></a></div>
	<footer id="colophon" role="contentinfo">
		<div class="footer_inner">
    <div class="FooterMenuArea spnone">
    <?php widgetized_footer(); ?>
    </div>
		</div><!-- .footer_inner -->
		<div class="site-info">Copyright &copy; 2014-2015 <?php bloginfo('name'); ?> All Rights Reserved.</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

 <?php //mytheme_customizer_script();
 //フロント画面にスクリプトが吐き出されるのを防ぐため
 //この部分はカスタマイズ後footerに生成されたスクリプトをtheme-customizer.jsに移動しコメントアウト
 //その後function.php L451 add_action( 'customize_preview_init', 'twentytwelve_customize_preview_js' );のコメントを外す
 ?>
<?php wp_footer();
//echo $_SERVER['HTTP_USER_AGENT'];
//if(is_smartphone()){echo '>>>>SMH';}else{echo '>>>>>PC';}
?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.rwdImageMaps.js" width="100%"></script>
<script type="text/livescript">
jQuery(function() {
		jQuery('img[usemap]').rwdImageMaps();
    object = jQuery('.SidebarMembersArea');

    //スクロールが100に達したらボタン表示
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 150) {
            object.fadeIn();
						jQuery(".SidebarMembersArea").css({
								"top":"50px"
						});
        } else {
						jQuery(".SidebarMembersArea").css({
								"top":"340px"
						});
        }
    });
		jQuery(object).hover(
			function (){
				jQuery(this).animate({ right:0 }, 100 );
			},
			function (){
				jQuery(this).animate({ right:-160 }, 100 );
			}
		);
	jQuery('footer ul li.thickbox a').addClass('thickbox');
});

</script>
</body>
</html>