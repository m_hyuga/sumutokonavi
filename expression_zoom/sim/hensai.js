// ローンシミュレーション（毎月返済金額算出） ver1.0
// 
// E-Mail: info@sitetool.biz
// Web   : http://www.sitetool.biz
// (C) 不動産WEBサイトツール
// 
// 上記著作権表示部は改変不可です。
// その他スクリプト部分は改変可です。


var hensai = Class.create({
    initialize: function(elm) {
        
        // エラー格納用変数
        this.errors = [];
        this.error_elm = '';
        
        // 出力エリアのID
        this.elm = elm;
        
        // 計算結果格納用
        this.results = {};
        
        // 入力フォームを出力
        this.putForm();
        // イベントリスナ登録
        this.setEventHandlers();
    },
    // イベントリスナ
    setEventHandlers: function() {
        // ボタン
        this.clearHandler = this.clearForm.bind(this);
        $('fwt_clear').observe('click', this.clearHandler);
        this.calcHandler = this.calcForm.bind(this);
        $('fwt_calc').observe('click', this.calcHandler);
        this.showDetailHandler = this.showDetail.bind(this);
        $('fwt_show_detail').observe('click', this.showDetailHandler);
        this.goTopHandler = this.goTop.bind(this);
        $('fwt_gotop').observe('click', this.goTopHandler);
        // 返済期間
        this.changeDebtRateYearHandler = this.changeDebtRateYear.bind(this);
        $('fwt_borrow_year').observe('change', this.changeDebtRateYearHandler);
        // 予定利率（期間）
        this.changeDebtRateYear2Handler = this.changeDebtRateYear2.bind(this);
        $('fwt_debt_rate_year1').observe('change', this.changeDebtRateYear2Handler);
        // 予定利率2（期間）
        this.changeDebtRateYear3Handler = this.changeDebtRateYear3.bind(this);
        $('fwt_debt_rate_year2').observe('change', this.changeDebtRateYear3Handler);
    },
    changeDebtRateYear3: function() {
        if($('fwt_borrow_year') && $('fwt_debt_rate_year1') && $('fwt_debt_rate_year2') && $('fwt_debt_rate_year3')) {
            Element.update($('fwt_debt_rate_year3'),this.makeDebtRateYearOption3());
        }
    },
    changeDebtRateYear2: function() {
        if($('fwt_borrow_year') && $('fwt_debt_rate_year1') && $('fwt_debt_rate_year2')) {
            Element.update($('fwt_debt_rate_year2'),this.makeDebtRateYearOption2());
            Element.update($('fwt_debt_rate_year3'),this.makeDebtRateYearOption3());
        }
    },
    changeDebtRateYear: function() {
        if($('fwt_borrow_year') && $('fwt_debt_rate_year1')) {
            Element.update($('fwt_debt_rate_year1'),this.makeDebtRateYearOption());
            Element.update($('fwt_debt_rate_year2'),this.makeDebtRateYearOption2());
            Element.update($('fwt_debt_rate_year3'),this.makeDebtRateYearOption3());
        }
    },
    showDetail: function() {
        $('fwt_detail_results').style.display = 'block';
        window.scroll(0,400);
    },
    hideDetail: function() {
        $('fwt_detail_results').style.display = 'none';
        window.scroll(0,400);
    },
    goTop: function() {
        window.scroll(0,0);
    },
    // 各種計算と結果出力
    calcForm: function() {
        this.hideDetail();
        this.clearResult();
        this.calcBegin();
        this.checkForm();
        if(this.errors.length > 0) {
            this.alerts();
        }
        else {
            // 各値をキャスト
            var values = {};
            values['fwt_borrow_money'] = parseInt($('fwt_borrow_money').value*10000);
            values['fwt_borrow_bonus_rate'] = parseFloat($('fwt_borrow_bonus_rate').value/100);
            values['fwt_borrow_bonus'] = parseInt($('fwt_borrow_bonus').value*10000);
            values['fwt_borrow_year'] = parseInt($('fwt_borrow_year').value);
            values['fwt_debt_rate1'] = parseFloat($('fwt_debt_rate1').value/100);
            values['fwt_debt_rate2'] = parseFloat($('fwt_debt_rate2').value/100);
            values['fwt_debt_rate3'] = parseFloat($('fwt_debt_rate3').value/100);
            values['fwt_debt_rate_year1'] = parseInt($('fwt_debt_rate_year1').value);
            values['fwt_debt_rate_year2'] = parseInt($('fwt_debt_rate_year2').value);
            values['fwt_debt_rate_year3'] = parseInt($('fwt_debt_rate_year3').value);
            values['fwt_income'] = 0;
            if($('fwt_income').value) {
                values['fwt_income'] = parseFloat($('fwt_income').value);
            }
            
            // 出力用変数（共通分）
            values['fwt_pay_money1'] = 0;
            values['fwt_pay_money2'] = 0;
            values['fwt_pay_money3'] = 0;
            values['fwt_pay_bonus1'] = 0;
            values['fwt_pay_bonus2'] = 0;
            values['fwt_pay_bonus3'] = 0;
            values['fwt_pay_total']  = 0;
            values['fwt_pay_money_total']  = 0;
            values['fwt_pay_bonus_total']  = 0;
            values['fwt_pay_total_year']  = 0;
            values['fwt_rate_income'] = 0;
            values['fwt_pay_type'] = 0;
            
            // 表示用
            values['fwt_debt_rate_year1_show'] = '（全期間）';
            values['fwt_debt_rate_year2_show'] = '';
            values['fwt_debt_rate_year3_show'] = '';
            if(values['fwt_debt_rate_year1'] <= 40) {
                values['fwt_debt_rate_year1_show'] = '（'+values['fwt_debt_rate_year1']+'年間）';
                values['fwt_debt_rate_year2_show'] = '（残期間）';
                if(values['fwt_debt_rate_year2'] <= 40) {
                    values['fwt_debt_rate_year2_show'] = '（'+values['fwt_debt_rate_year2']+'年間）';
                    values['fwt_debt_rate_year3_show'] = '（残期間）';
                }
            }
            
            if($('fwt_pay_type2').checked == true) {
                this.results = this.calcGankinKintou(values);
                // 結果出力
                this.putResults();
            }
            else {
                this.results = this.calcGanriKintou(values);
                // 結果出力
                this.putResults();
            }
            
        }
    },
    // 元金均等
    calcGankinKintou: function(values) {
        // 定額分：元金 / 支払回数
        // 利息　：残額 * 利率
        
        values['fwt_pay_type'] = 1;
        
        // 月払分とボーナス払分に分ける
        if(values['fwt_borrow_bonus'] > 0) {
            values['fwt_borrow_money'] -= values['fwt_borrow_bonus'];
        }
        else if(values['fwt_borrow_bonus_rate']) {
            values['fwt_borrow_bonus'] = Math.round(values['fwt_borrow_money'] * values['fwt_borrow_bonus_rate']);
            values['fwt_borrow_money'] -= values['fwt_borrow_bonus'];
        }
        
        // 利率切替年をあらかじめ設定しておく
        var pay_debt_switch1 = 0;
        var pay_debt_switch2 = 0;
        if(values['fwt_debt_rate_year1'] <= 40) {
            pay_debt_switch1 = values['fwt_debt_rate_year1'];
            if(values['fwt_debt_rate_year2'] <= 40) {
                pay_debt_switch2 = values['fwt_debt_rate_year1'] + values['fwt_debt_rate_year2'];
            }
        }
        
        // 定額支払（月払分）
        values['pay_money_flat'] = Math.round(values['fwt_borrow_money'] / (values['fwt_borrow_year'] * 12));
        
        // 月ごと支払（月払分）
        values['pay_money_months'] = new Array();
        values['fwt_borrow_money_balance'] = values['fwt_borrow_money'];
        var fwt_debt_rate = values['fwt_debt_rate1'];
        for(var i=1; i <= values['fwt_borrow_year']; i++) {
            values['pay_money_months'][i] = new Array();
            for(var j=1; j<=12; j++) {
                values['pay_money_months'][i][j] = Math.round(values['fwt_borrow_money_balance'] * (fwt_debt_rate / 12) + values['pay_money_flat']);
                values['fwt_pay_money_total'] += values['pay_money_months'][i][j];
                values['fwt_borrow_money_balance'] -= values['pay_money_flat'];
            }
            
            // 利率切替
            if(pay_debt_switch2 != 0 && pay_debt_switch2 == i) {
                fwt_debt_rate = values['fwt_debt_rate3'];
            }
            else if(pay_debt_switch1 != 0 && pay_debt_switch1 == i) {
                fwt_debt_rate = values['fwt_debt_rate2'];
            }
            
            // 各利率初回分支払金額を覚えておく
            if(pay_debt_switch2 != 0 && pay_debt_switch2+1 == i) {
                values['fwt_pay_money3'] = values['pay_money_months'][i][1];
            }
            else if(pay_debt_switch1 != 0 && pay_debt_switch1+1 == i) {
                values['fwt_pay_money2'] = values['pay_money_months'][i][1];
            }
        }
        values['fwt_pay_money1'] = values['pay_money_months'][1][1];
        
        // 定額支払（ボーナス払分）
        values['pay_bonus_flat'] = Math.round(values['fwt_borrow_bonus'] / (values['fwt_borrow_year'] * 2));
        
        // 月ごと支払（ボーナス分）
        values['pay_bonus_months'] = new Array();
        values['fwt_borrow_bonus_balance'] = values['fwt_borrow_bonus'];
        fwt_debt_rate = values['fwt_debt_rate1'];
        for(var i=1; i <= values['fwt_borrow_year']; i++) {
            values['pay_bonus_months'][i] = new Array();
            for(var j=1; j<=2; j++) {
                values['pay_bonus_months'][i][j] = Math.round(values['fwt_borrow_bonus_balance'] * (fwt_debt_rate / 2) + values['pay_bonus_flat']);
                values['fwt_pay_bonus_total'] += values['pay_bonus_months'][i][j];
                values['fwt_borrow_bonus_balance'] -= values['pay_bonus_flat'];
            }
            
            // 利率切替
            if(pay_debt_switch2 != 0 && pay_debt_switch2 == i) {
                fwt_debt_rate = values['fwt_debt_rate3'];
            }
            else if(pay_debt_switch1 != 0 && pay_debt_switch1 == i) {
                fwt_debt_rate = values['fwt_debt_rate2'];
            }
            
            // 各利率初回分支払金額を覚えておく
            if(pay_debt_switch2 != 0 && pay_debt_switch2+1 == i) {
                values['fwt_pay_bonus3'] = values['pay_bonus_months'][i][1];
            }
            else if(pay_debt_switch1 != 0 && pay_debt_switch1+1 == i) {
                values['fwt_pay_bonus2'] = values['pay_bonus_months'][i][1];
            }
        }
        values['fwt_pay_bonus1'] = values['pay_bonus_months'][1][1];
        
        // 総額
        values['fwt_pay_total']  = values['fwt_pay_money_total'] + values['fwt_pay_bonus_total'];
        
        // 年間総額（平均）
        values['fwt_pay_total_year'] = Math.round(values['fwt_pay_total'] / values['fwt_borrow_year']);
        
        // 年収に対しての支払負担率
        if(values['fwt_income'] > 0) {
            values['fwt_rate_income'] = values['fwt_pay_total'] / (values['fwt_income'] * values['fwt_borrow_year'] * 10000) * 100;
        }
        
        return values;
    },
    // 元利均等
    calcGanriKintou: function(values) {
        
        // 計算例（返済月額）-- 借入金1000万円，利率年3.0%，返済期間20年の場合の各月返済額 
        // 10000000*3/100/12*(1+3/12/100)^(20*12)/((1+3/100/12)^(20*12)-1) = 55459 
        // values['example'] = 10000000 * 0.03/12 * Math.pow(1+0.03/12,20*12)/(Math.pow((1+0.03/12),(20*12))-1);
        
        // 計算例（残元金）-- 元金20万円，利率年5.0%，各回（月）返済限度額1万円の場合の返済１０回後の残元金
        // (10000-(1+0.05/12)^10*(10000-200000*0.05/12))/(0.05/12) = 106,595円
        // values['example'] = (10000 - Math.pow((1+0.05/12),10)*(10000-200000*0.05/12))/(0.05/12);
        
        // 月払分とボーナス払分に分ける
        if(values['fwt_borrow_bonus'] > 0) {
            values['fwt_borrow_money'] -= values['fwt_borrow_bonus'];
        }
        else if(values['fwt_borrow_bonus_rate']) {
            values['fwt_borrow_bonus'] = Math.round(values['fwt_borrow_money'] * values['fwt_borrow_bonus_rate']);
            values['fwt_borrow_money'] -= values['fwt_borrow_bonus'];
        }
        
        // 残額用変数
        values['fwt_borrow_money_balance1'] = 0;
        values['fwt_borrow_money_balance2'] = 0;
        values['fwt_borrow_bonus_balance1'] = 0;
        values['fwt_borrow_bonus_balance2'] = 0;
        
        // 予定利率(1)の返済額を求める（月額＆ボーナス）
        values['fwt_pay_money1'] = Math.round(values['fwt_borrow_money'] * values['fwt_debt_rate1']/12 * Math.pow(1+values['fwt_debt_rate1']/12,values['fwt_borrow_year']*12) / (Math.pow((1+values['fwt_debt_rate1']/12),(values['fwt_borrow_year']*12))-1));
        values['fwt_pay_bonus1'] = Math.round(values['fwt_borrow_bonus'] * values['fwt_debt_rate1']/2 * Math.pow(1+values['fwt_debt_rate1']/2,values['fwt_borrow_year']*2) / (Math.pow((1+values['fwt_debt_rate1']/2),(values['fwt_borrow_year']*2))-1));
        
        // 予定利率(1)の返済期間が指定されている場合
        if(values['fwt_debt_rate_year1'] <= 40) {
            // 予定利率(1)終了時の残元金を求めて返済額を求める（月額＆ボーナス）
            values['fwt_borrow_money_balance1'] = (values['fwt_pay_money1'] - Math.pow(1+values['fwt_debt_rate1']/12,values['fwt_debt_rate_year1']*12) * (values['fwt_pay_money1'] - values['fwt_borrow_money'] * values['fwt_debt_rate1']/12)) / (values['fwt_debt_rate1']/12);
            if(values['fwt_borrow_money_balance1'] > 0) {
                values['fwt_pay_money2'] = Math.round(values['fwt_borrow_money_balance1'] * values['fwt_debt_rate2']/12 * Math.pow(1+values['fwt_debt_rate2']/12,(values['fwt_borrow_year']-values['fwt_debt_rate_year1'])*12) / (Math.pow((1+values['fwt_debt_rate2']/12),((values['fwt_borrow_year']-values['fwt_debt_rate_year1'])*12))-1));
            }
            
            values['fwt_borrow_bonus_balance1'] = (values['fwt_pay_bonus1'] - Math.pow(1+values['fwt_debt_rate1']/2 ,values['fwt_debt_rate_year1']*2)  * (values['fwt_pay_bonus1'] - values['fwt_borrow_bonus'] * values['fwt_debt_rate1']/2)) / (values['fwt_debt_rate1']/2);
            if(values['fwt_borrow_bonus_balance1'] > 0) {
                values['fwt_pay_bonus2'] = Math.round(values['fwt_borrow_bonus_balance1'] * values['fwt_debt_rate2']/2  * Math.pow(1+values['fwt_debt_rate2']/2 ,(values['fwt_borrow_year']-values['fwt_debt_rate_year1'])*2)  / (Math.pow((1+values['fwt_debt_rate2']/2) ,((values['fwt_borrow_year']-values['fwt_debt_rate_year1'])*2)) -1));
            }
            
            // 予定利率(2)の返済期間が指定されている場合
            if(values['fwt_debt_rate_year2'] <= 40) {
                // 予定利率(2)終了時の残元金を求めて返済額を求める（月額＆ボーナス）
                values['fwt_borrow_money_balance2'] = (values['fwt_pay_money2'] - Math.pow(1+values['fwt_debt_rate2']/12,values['fwt_debt_rate_year2']*12) * (values['fwt_pay_money2'] - values['fwt_borrow_money_balance1'] * values['fwt_debt_rate2']/12)) / (values['fwt_debt_rate2']/12);
                if(values['fwt_borrow_money_balance2'] > 0) {
                    values['fwt_pay_money3'] = Math.round(values['fwt_borrow_money_balance2'] * values['fwt_debt_rate3']/12 * Math.pow(1+values['fwt_debt_rate3']/12,(values['fwt_borrow_year']-values['fwt_debt_rate_year1']-values['fwt_debt_rate_year2'])*12) / (Math.pow((1+values['fwt_debt_rate3']/12),((values['fwt_borrow_year']-values['fwt_debt_rate_year1']-values['fwt_debt_rate_year2'])*12))-1));
                }
                
                values['fwt_borrow_bonus_balance2'] = (values['fwt_pay_bonus2'] - Math.pow(1+values['fwt_debt_rate2']/2 ,values['fwt_debt_rate_year2']*2)  * (values['fwt_pay_bonus2'] - values['fwt_borrow_bonus_balance1'] * values['fwt_debt_rate2']/2)) / (values['fwt_debt_rate2']/2);
                if(values['fwt_borrow_bonus_balance2'] > 0) {
                    values['fwt_pay_bonus3'] = Math.round(values['fwt_borrow_bonus_balance2'] * values['fwt_debt_rate3']/2  * Math.pow(1+values['fwt_debt_rate3']/2 ,(values['fwt_borrow_year']-values['fwt_debt_rate_year1']-values['fwt_debt_rate_year2'])*2)  / (Math.pow((1+values['fwt_debt_rate3']/2) ,((values['fwt_borrow_year']-values['fwt_debt_rate_year1']-values['fwt_debt_rate_year2'])*2)) -1));
                }
            }
        }
        
        // 利率切替年をあらかじめ設定しておく
        var pay_debt_switch1 = 0;
        var pay_debt_switch2 = 0;
        if(values['fwt_debt_rate_year1'] <= 40) {
            pay_debt_switch1 = values['fwt_debt_rate_year1'];
            if(values['fwt_debt_rate_year2'] <= 40) {
                pay_debt_switch2 = values['fwt_debt_rate_year1'] + values['fwt_debt_rate_year2'];
            }
        }
        
        // 月ごと支払
        values['pay_money_months'] = new Array();
        values['pay_bonus_months'] = new Array();
        var fwt_pay_money = values['fwt_pay_money1'];
        var fwt_pay_bonus = values['fwt_pay_bonus1'];
        for(var i=1; i <= values['fwt_borrow_year']; i++) {
            values['pay_money_months'][i] = new Array();
            values['pay_bonus_months'][i] = new Array();
            for(var j=1; j<=12; j++) {
                values['pay_money_months'][i][j] = fwt_pay_money;
                values['fwt_pay_money_total'] += fwt_pay_money;
            }
            for(var k=1; k<=2; k++) {
                values['pay_bonus_months'][i][k] = fwt_pay_bonus;
                values['fwt_pay_bonus_total'] += fwt_pay_bonus;
            }
            
             // 利率切替
            if(pay_debt_switch2 != 0 && pay_debt_switch2 == i) {
                fwt_pay_money = values['fwt_pay_money3'];
                fwt_pay_bonus = values['fwt_pay_bonus3'];
            }
            else if(pay_debt_switch1 != 0 && pay_debt_switch1 == i) {
                fwt_pay_money = values['fwt_pay_money2'];
                fwt_pay_bonus = values['fwt_pay_bonus2'];
            }
       }
        
        // 返済総額
        values['fwt_pay_total'] = values['fwt_pay_money_total'] + values['fwt_pay_bonus_total'];
        
        // 年間総額（平均）
        values['fwt_pay_total_year'] = Math.round(values['fwt_pay_total'] / values['fwt_borrow_year']);
        
        // 年収に対しての支払負担率
        if(values['fwt_income'] > 0) {
            values['fwt_rate_income'] = values['fwt_pay_total'] / (values['fwt_income'] * values['fwt_borrow_year'] * 10000) * 100;
        }
        
        return values;
    },
    // 入力フォーム出力
    putForm: function() {
    	var query = window.location.search.toQueryParams();
		if(query["pc"]){price = query["pc"]}else{price = 0;}
        var html = '';
        html += '<h4>借入金額から毎月の返済金額をシミュレーションします</h4>';
        html += '<div id="fwt_explane">';
        html += '<p>※下記フォームを入力し、「計算」ボタンをクリックしてください。<br>※計算結果は概算です。あくまで参考としてご利用ください。</p>';
        html += '</div>';
        html += '<form name="fwt_hensaiForm" id="fwt_hensaiForm">';
        html += '<div id="contact-form">';
        html += '<table>';
        html += '<tr>';
        html += '<th>借入予定金額<span class="req">必須</span></th>';
        html += '<td><input type="text" name="fwt_borrow_money" id="fwt_borrow_money" value="' + price + '" /> 万円<div class="fwt_note">1～999999</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th class="longtxt">内、ボーナスで返済する割合又は金額</th>';
        html += '<td>'+this.makeBorrowBonusRate('fwt_borrow_bonus_rate')+' ％ 又は <input type="text" name="fwt_borrow_bonus" id="fwt_borrow_bonus" value="" /> 万円<div class="fwt_note">1～999999</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>返済期間<span class="req">必須</span></th>';
        html += '<td>'+this.makeBorrowYear('fwt_borrow_year')+' 年</td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>当初金利<span class="req">必須</span></th>';
        html += '<td><input type="text" name="fwt_debt_rate1" id="fwt_debt_rate1" value="" /> ％ <select name="fwt_debt_rate_year1" id="fwt_debt_rate_year1">'+this.makeDebtRateYearOption()+'</select><div class="fwt_note">0.01～99.99</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>第２金利</th>';
        html += '<td><input type="text" name="fwt_debt_rate2" id="fwt_debt_rate2" value="" disabled="disabled" /> ％ <select name="fwt_debt_rate_year2" id="fwt_debt_rate_year2" disabled="disabled">'+this.makeDebtRateYearOption2()+'</select><div class="fwt_note">0.01～99.99</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>第３金利</th>';
        html += '<td><input type="text" name="fwt_debt_rate3" id="fwt_debt_rate3" value="" disabled="disabled" /> ％ <select name="fwt_debt_rate_year3" id="fwt_debt_rate_year3" disabled="disabled">'+this.makeDebtRateYearOption3()+'</select><div class="fwt_note">0.01～99.99</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>返済方式</th>';
        html += '<td><input type="radio" name="fwt_pay_type" id="fwt_pay_type1" value="1" checked="checked" /><label for="fwt_pay_type1"> 元利均等</label><input type="radio" name="fwt_pay_type" id="fwt_pay_type2" value="2" /><label for="fwt_pay_type2"> 元金均等</label></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>年収</th>';
        html += '<td><input type="text" name="fwt_income" id="fwt_income" value="" /> 万円<div class="fwt_note">1～999999</div></td>';
        html += '</tr>';
        html += '</table>';
				html += '<div class="submit"><input type="button" name="fwt_clear" id="fwt_clear" value="クリア" /> <input type="button" name="fwt_calc" id="fwt_calc" value="計算" /></div>';
        html += '</div>';
        html += '<div id="fwt_results">';
        html += '</div>';
        html += '<div id="fwt_buttons2"><input type="button" name="fwt_show_detail" id="fwt_show_detail" value="さらに月別返済を詳細表示する" /></div>';
        html += '<div id="fwt_detail_results">';
        html += '</div>';
        //html += '<div id="fwt_gotop"><a href="javascript:void(0);">↑ページ上へ戻る</a></div>';
        html += '</form>';
        Element.update($(this.elm),html);
    },
    // 入力フォームの利率期間
    makeDebtRateYearOption: function() {
        
        var max = 25;
        if($('fwt_borrow_year')) {
            max = $('fwt_borrow_year').value;
        }
        
        var html = '';
        html += '<option value="999">全期間</option>';
        for(var i=1; i < max; i++) {
            html += '<option value="'+i+'">'+i+'年間</option>';
        }
        return html;
    },
    makeDebtRateYearOption2: function() {
        if($('fwt_debt_rate_year2')) {
            $('fwt_debt_rate2').disabled = true;
            $('fwt_debt_rate_year2').disabled = true;
        }
        html = '<option value="999">残期間</option>';
        if($('fwt_borrow_year') && $('fwt_debt_rate_year1')) {
            if($('fwt_debt_rate_year1').value != 999) {
                var max = $('fwt_borrow_year').value - $('fwt_debt_rate_year1').value;
                if(max > 0) {
                    for(var i=1; i < max; i++) {
                        html += '<option value="'+i+'">'+i+'年間</option>';
                    }
                    $('fwt_debt_rate2').disabled = false;
                    $('fwt_debt_rate_year2').disabled = false;
                }
            }
        }
        return html;
    },
    makeDebtRateYearOption3: function() {
        if($('fwt_debt_rate_year3')) {
            $('fwt_debt_rate3').disabled = true;
            $('fwt_debt_rate_year3').disabled = true;
        }
        html = '<option value="999">残期間</option>';
        if($('fwt_borrow_year') && $('fwt_debt_rate_year1') && $('fwt_debt_rate_year2')) {
            var max = $('fwt_borrow_year').value - $('fwt_debt_rate_year1').value - $('fwt_debt_rate_year2').value;
            if(max > 0) {
                $('fwt_debt_rate3').disabled = false;
                $('fwt_debt_rate_year3').disabled = false;
            }
        }
        return html;
    },
    // 入力フォームのボーナス返済率
    makeBorrowBonusRate: function(name) {
        var html = '';
        if(name) {
            html += '<select name="'+name+'" id="'+name+'">';
            html += '<option value=""></option>';
            for(var i=0; i<=100; i=i+5) {
                html += '<option value="'+i+'">'+i+'</option>';
            }
            html += '</select>';
        }
        return html;
    },
    // 入力フォームの返済期間選択肢（年）
    makeBorrowYear: function(name) {
        var html = '';
        if(name) {
            html += '<select name="'+name+'" id="'+name+'">';
            for(var i=0; i<=40; i++) {
                if(i == 25) {
                    html += '<option value="'+i+'" selected="selected">'+i+'</option>';
                }
                else {
                    html += '<option value="'+i+'">'+i+'</option>';
                }
            }
            html += '</select>';
        }
        return html;
    },
    // 結果出力
    putResults: function() {
        var note = '';
        if(this.results['fwt_pay_type'] == 1) {
            note = '（初回）';
        }
        
        var fwt_borrow_year = 0;
        if(this.results['fwt_pay_bonus_total'] > 0) {
            fwt_borrow_year = this.results['fwt_borrow_year']*2;
        }
        
        var html = '';
        html += '<p id="fwt_caption_result">シミュレーション結果</p>';
        html += '<div id="fwt_pays">';
        html += '<table>';
        html += '<tr>';
        html += '<th class="c1">&nbsp;</th><th class="c2">返済金額<br />（毎月）</th><th class="c3">返済金額<br />（ボーナス1回分）</th>';
        html += '</tr>';
        html += '<tr>';
        html += '<td>当初金利'+this.results['fwt_debt_rate_year1_show']+'</td><td>'+this.addComma(this.results['fwt_pay_money1'])+' 円'+note+'</td><td>'+this.addComma(this.results['fwt_pay_bonus1'])+' 円'+note+'</td>';
        html += '</tr>';
        if(this.results['fwt_pay_money2'] > 0) {
        html += '<tr>';
        html += '<td>第２金利'+this.results['fwt_debt_rate_year2_show']+'</td><td>'+this.addComma(this.results['fwt_pay_money2'])+' 円'+note+'</td><td>'+this.addComma(this.results['fwt_pay_bonus2'])+' 円'+note+'</td>';
        html += '</tr>';
        }
        if(this.results['fwt_pay_money3'] > 0) {
        html += '<tr>';
        html += '<td>第３金利'+this.results['fwt_debt_rate_year3_show']+'</td><td>'+this.addComma(this.results['fwt_pay_money3'])+' 円'+note+'</td><td>'+this.addComma(this.results['fwt_pay_bonus3'])+' 円'+note+'</td>';
        html += '</tr>';
        }
        html += '</table>';
        html += '</div>';
        html += '<table>';
        html += '<tr>';
        html += '<th>返済金額（合計）</th>';
        html += '<td><div id="fwt_pay_total">'+this.addComma(Math.round(this.results['fwt_pay_total']/10000))+' 万円</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>返済金額（月払分）</th>';
        html += '<td><div id="fwt_pay_money_total">'+this.addComma(Math.round(this.results['fwt_pay_money_total']/10000))+' 万円</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>返済金額（ボーナス分）</th>';
        html += '<td><div>'+this.addComma(Math.round(this.results['fwt_pay_bonus_total']/10000))+' 万円</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>利息（合計）</th>';
        html += '<td><div id="fwt_debt_total">'+this.addComma(Math.round((this.results['fwt_pay_total']-this.results['fwt_borrow_money']-this.results['fwt_borrow_bonus'])/10000))+' 万円</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>月払回数</th>';
        html += '<td><div id="fwt_pay_m">'+this.addComma(this.results['fwt_borrow_year']*12)+' 回</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>ボーナス払回数</th>';
        html += '<td><div id="fwt_pay_b">'+this.addComma(fwt_borrow_year)+' 回</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>年間返済金額（平均）</th>';
        html += '<td><div id="fwt_pay_total_year">'+this.addComma(Math.round(this.results['fwt_pay_total_year']/10000))+' 万円</div></td>';
        html += '</tr>';
        html += '<tr>';
        html += '<th>年収に対しての支払負担率（平均）</th>';
        html += '<td><div id="fwt_rate_income">'+this.addComma(Math.round(this.results['fwt_rate_income']*100)/100)+' ％</div></td>';
        html += '</tr>';
        html += '</table>';
        Element.update($('fwt_results'),html);
        
        // 詳細
        html = '<p id="fwt_caption_detail">月別返済（詳細）</p>';
        html = '<p>※ボーナスは各年6月・12月支払いとみなしています。</p>';
        for(var i=1; i <= this.results['fwt_borrow_year']; i++) {
            
            var pay_bonus = 0;
            var pay_money_total = 0;
            var pay_bonus_total = 0;
            
            html += '<div class="fwt_caption_detail_table">'+i+'年目</div>';
            html += '<table>';
            html += '<tr>';
            html += '<th>&nbsp;</th><th>返済金額<br />（毎月）</th><th>返済金額<br />（ボーナス1回分）</th><th>返済金額<br />（小計）</th>';
            html += '</tr>';
            for(var j=1; j<=12; j++) {
                
                // ボーナス払
                pay_bonus = 0;
                if(j == 6) {
                    pay_bonus = this.results['pay_bonus_months'][i][1];
                }
                else if(j == 12) {
                    pay_bonus = this.results['pay_bonus_months'][i][2];
                }
                
                // 計
                pay_money_total += this.results['pay_money_months'][i][j];
                pay_bonus_total += pay_bonus;
                
                html += '<tr>';
                html += '<td>'+j+'ヶ月目</td><td>'+this.addComma(this.results['pay_money_months'][i][j])+' 円</td><td>'+this.addComma(pay_bonus)+' 円</td><td>'+this.addComma(this.results['pay_money_months'][i][j]+pay_bonus)+' 円</td>';
                html += '</tr>';
            }
            html += '<tr>';
            html += '<td>計</td><td>'+this.addComma(pay_money_total)+' 円</td><td>'+this.addComma(pay_bonus_total)+' 円</td><td>'+this.addComma(pay_money_total+pay_bonus_total)+' 円</td>';
            html += '</tr>';
            html += '</table>';
        }
        Element.update($('fwt_detail_results'),html);
        
        // 詳細表示用ボタン表示
        $('fwt_buttons2').style.display = 'block';
        
    },
    // 結果クリア
    clearResult: function() {
        Element.update($('fwt_results'),'');
        this.results = {};
    },
    // 入力値クリア
    clearForm: function() {
        $('fwt_borrow_money').value = '';
        $('fwt_borrow_bonus').value = '';
        $('fwt_borrow_bonus_rate').selectedIndex = 0;
        $('fwt_borrow_year').selectedIndex = 25;
        $('fwt_debt_rate1').value = '';
        $('fwt_debt_rate2').value = '';
        $('fwt_debt_rate3').value = '';
        $('fwt_debt_rate_year1').selectedIndex = 0;
        this.makeDebtRateYearOption2();
        $('fwt_debt_rate_year2').selectedIndex = 0;
        this.makeDebtRateYearOption3();
        $('fwt_debt_rate_year3').selectedIndex = 0;
        $('fwt_pay_type1').checked = true;
        $('fwt_pay_type2').checked = false;
        $('fwt_income').value = '';
    },
    // 事前処理（計算・入力値チェックの前）
    calcBegin: function() {
        $('fwt_borrow_money').value = this.replaceZennumToHannum($('fwt_borrow_money').value);
        $('fwt_borrow_bonus_rate').value = this.replaceZennumToHannum($('fwt_borrow_bonus_rate').value);
        $('fwt_borrow_bonus').value = this.replaceZennumToHannum($('fwt_borrow_bonus').value);
        $('fwt_borrow_year').value = this.replaceZennumToHannum($('fwt_borrow_year').value);
        $('fwt_debt_rate1').value = this.replaceZennumToHannum($('fwt_debt_rate1').value);
        $('fwt_debt_rate2').value = this.replaceZennumToHannum($('fwt_debt_rate2').value);
        $('fwt_debt_rate3').value = this.replaceZennumToHannum($('fwt_debt_rate3').value);
        $('fwt_debt_rate_year1').value = this.replaceZennumToHannum($('fwt_debt_rate_year1').value);
        $('fwt_debt_rate_year2').value = this.replaceZennumToHannum($('fwt_debt_rate_year2').value);
        $('fwt_debt_rate_year3').value = this.replaceZennumToHannum($('fwt_debt_rate_year3').value);
        $('fwt_income').value = this.replaceZennumToHannum($('fwt_income').value);
    },
    // 入力値チェック
    checkForm: function() {
        this.errors = [];
        this.error_elm = '';
        var name = '';
        
        // 借入予定金額
        name = '「借入予定金額」';
        if($('fwt_borrow_money').value == '') {
            this.errors.push(name+'を入力してください。');
            this.errorFocus('fwt_borrow_money');
        }
        else {
            var err = this.checkNumber($('fwt_borrow_money').value,6,0);
            if(err) {
                this.errors.push(name+'は'+err);
                this.errorFocus('fwt_borrow_money');
            }
        }
        
        // 内、ボーナスで返済する金額
        name = '「内、ボーナスで返済する金額」';
        if($('fwt_borrow_bonus').value != '') {
            var err = this.checkNumber($('fwt_borrow_bonus').value,6,0);
            if(err) {
                this.errors.push(name+'は'+err);
                this.errorFocus('fwt_borrow_bonus');
            }
        }
        if($('fwt_borrow_bonus').value != '' && $('fwt_borrow_money').value != '') {
            if(parseInt($('fwt_borrow_bonus').value) > parseInt($('fwt_borrow_money').value)) {
                this.errors.push(name+'は借入予定金額以下で入力してください。');
                this.errorFocus('fwt_borrow_bonus');
            }
        }
        
        // 内、ボーナスで返済する割合又は金額
        name = '「内、ボーナスで返済する割合又は金額」';
        if($('fwt_borrow_bonus_rate').value != '' && $('fwt_borrow_bonus').value != '') {
            this.errors.push(name+'はいずれか一方のみを入力してください。');
            this.errorFocus('fwt_borrow_bonus');
        }
        
        // 返済期間
        name = '「返済期間」';
        if($('fwt_borrow_year').value == 0) {
            this.errors.push(name+'を入力してください。');
            this.errorFocus('fwt_borrow_year');
        }
        
        // 当初金利
        name = '「当初金利」';
        if($('fwt_debt_rate1').value == '') {
            this.errors.push(name+'を入力してください。');
            this.errorFocus('fwt_debt_rate1');
        }
        else {
            var err = this.checkNumber($('fwt_debt_rate1').value,2,2);
            if(err) {
                this.errors.push(name+'は'+err);
                this.errorFocus('fwt_debt_rate1');
            }
        }
        // 第２金利
        name = '「第２金利」';
        if($('fwt_debt_rate_year1').value != '999') {
            if($('fwt_debt_rate2').value == '') {
                this.errors.push(name+'を入力してください。');
                this.errorFocus('fwt_debt_rate2');
            }
            else {
                var err = this.checkNumber($('fwt_debt_rate2').value,2,2);
                if(err) {
                    this.errors.push(name+'は'+err);
                    this.errorFocus('fwt_debt_rate2');
                }
            }
        }
        // 第３金利
        name = '「第３金利」';
        if($('fwt_debt_rate_year2').value != '999') {
            if($('fwt_debt_rate3').value == '') {
                this.errors.push(name+'を入力してください。');
                this.errorFocus('fwt_debt_rate3');
            }
            else {
                var err = this.checkNumber($('fwt_debt_rate3').value,2,2);
                if(err) {
                    this.errors.push(name+'は'+err);
                    this.errorFocus('fwt_debt_rate3');
                }
            }
        }
        
        // 年収
        name = '「年収」';
        if($('fwt_income').value != '') {
            var err = this.checkNumber($('fwt_income').value,6,0);
            if(err) {
                this.errors.push(name+'は'+err);
                this.errorFocus('fwt_income');
            }
        }
    },
    // 数値チェック
    checkNumber: function(val,int_length,dec_length) {
        // 空
        if(val == '') {
            return '数値を入力してください。';
        }
        // 数字とカンマ以外
        if(val.match(/[^0-9|^.]/g)) {
            return '数値を入力してください。';
        }
        // カンマの数
        var cnt = 0;
        for(var i=0; i<val.length; i++) {
            if(val.charAt(i) == '.') {
                cnt++;
            }
        }
        if(cnt > 1) {
            return '正しい数値を入力してください。';
        }
        // 先頭カンマ
        if(val.charAt(0) == '.') {
            return '正しい数値を入力してください。';
        }
        // 整数部分の桁数
        if(int_length > 0) {
            var int_str = '';
            if(cnt == 1) {
                int_str = val.substring(0, val.indexOf('.'));
            }
            else {
                int_str = val;
            }
            if(int_str.length > int_length) {
                return '整数部分を'+int_length+'桁以下で入力してください。';
            }
        }
        // 小数部分の桁数
        if(dec_length > 0) {
            var dec_str = '';
            if(cnt == 1) {
                var idx = val.indexOf('.');
                dec_str = val.substring(idx);
                if(dec_str.length-1 > dec_length) {
                    return '小数部分を'+dec_length+'桁以下で入力してください。';
                }
            }
        }
        else {
            if(cnt == 1) {
                return '整数を入力してください。';
            }
        }
        return false;
    },
    // 全角数字＆ドットを半角に置換
    replaceZennumToHannum: function(chars) {
        var zennums = new Array('０','１','２','３','４','５','６','７','８','９','．');
        var hannums = new Array(0,1,2,3,4,5,6,7,8,9,'.');
        for(var i=0; i<11; i++) {
            chars = chars.replace(new RegExp(zennums[i],'g'),hannums[i]); 
        }
        return chars;
    },
    // カンマ付加
    addComma: function(str) {
        var num = new String(str).replace(/,/g, "");
        while(num != (num = num.replace(/^(-?\d+)(\d{3})/, "$1,$2")));
        return num;
    },
    // アラート出力
    alerts: function() {
        if(this.errors.length > 0) {
            var txt = '';
            for(var i=0; i<this.errors.length; i++) {
                txt += this.errors[i]+"\n";
            }
            if(this.error_elm != '') {
                $(this.error_elm).focus();
            }
            alert(txt);
        }
    },
    // エラー要素へのフォーカス
    errorFocus: function(elm) {
        if(this.error_elm == '') {
            this.error_elm = elm;
        }
    }
});

document.observe('dom:loaded', function() {
    
    // 出力スペースの要素名
    var elm = 'fwt_hensai';
    
    if($(elm)) {
        new hensai(elm);
    }
});



