<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<link href="<?php echo bloginfo('template_directory');?>/style.css" rel="stylesheet" type="text/css" >
<![endif]-->

<?php if(is_page("11159")):?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/sim/hensai.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/sim/prototype.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/sim/hensai.js"></script>
<?php endif;?>
<?php wp_head(); ?>

<!--スマートフォン振り分け -->
<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=0.5,user-scalable=yes,initial-scale=1.0" />



<!-- main stylesheet -->


<!--smartphone-->
<link href="<?php echo bloginfo('template_directory');?>/style_s.css" rel="stylesheet" type="text/css" media="only screen and (max-width: 820px)" >


<!--retina-->
<link href="<?php echo bloginfo('template_directory');?>/style_s.css" rel="stylesheet" type="text/css" media="only screen and (max-device-width: 820px) and (-webkit-device-pixel-ratio: 2)" >


<!--tablet,PC-->
<link href="<?php echo bloginfo('template_directory');?>/style.css" rel="stylesheet" type="text/css" media="only screen and (min-width: 821px)" >

<link href="<?php echo bloginfo('template_directory');?>/style.css" rel="stylesheet" type="text/css" media="print" >
<link href="<?php echo bloginfo('template_directory');?>/print.css" rel="stylesheet" type="text/css" media="print" >
<link href="<?php echo bloginfo('template_directory');?>/css/custom_style.css" rel="stylesheet" type="text/css" >



<?php if(is_smartphone()){?>

<!-- main script xxx-->

<script src="<?php echo bloginfo('template_directory');?>/js/main_s.js" type="text/javascript"></script>

<?php }else{?>

<script src="<?php echo bloginfo('template_directory');?>/js/main_pc.js" type="text/javascript"></script>
<script src="<?php echo bloginfo('template_directory');?>/js/jquery.flatheights.js" type="text/javascript"></script>
<!--test-->

<?php }?>

<!-- flexslider用スタイル-->
<link href="<?php echo bloginfo('template_directory');?>/js/jquery.flexslider/flexslider.css" rel="stylesheet" type="text/css" >

<!--[if (lt IE 9)&(!IEMobile)]>
<link rel="stylesheet" href="<?php echo bloginfo('template_directory');?>/style.css.php?dir=<?php echo $inst_dir;?>" media="all">
<![endif]-->

<!--スマートフォン振り分けend -->

<!--WPテーマカスタマイザー出力 -->

 <?php mytheme_customize_css(); ?>

<!--WPテーマカスタマイザー出力end -->
</head>

<body <?php body_class(); ?>>

<!-- facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1&appId=484207311607131";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	
<div id="page" class="hfeed">
	<header id="masthead" class="site-header site" role="banner">
		<div id="logo_area">
			<?php if(get_option('expression_option_logo') != ''){	?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
      <?php echo '<img src="'.get_option('expression_option_logo').'" alt="'.get_bloginfo('name').'" />'; ?>
			</a>
      <?php
			}else{
			?>
			<h1 class="ex_site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
			<?php bloginfo( 'name' ); ?></a></h1>
			<?php
			}
			?>
				<!-- <img src="<?php echo get_template_directory_uri()?>/images/expression_logo.png"> -->
		</div>
  <div class="s_navi spnone">
  <!--add Navigation for Headermenu-->
  <?php wp_nav_menu( array(
          'theme_location'=>'headernavi', 
          'container'     =>'',
          'menu_class'    =>'',
          'items_wrap'    =>'<ul class="s_list cf">%3$s</ul>'));
  ?>
  </div>
		<hgroup>
			<h2 class="ex_site-description"><?php bloginfo( 'description' ); ?></h2>
		</hgroup>

	<div class="login_area">
		<p>ようこそ！ <span><?php if(is_user_logged_in()){ the_author();} else {echo 'ゲスト';}; ?></span> さん</p>
		<ul class="cf">
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>wp-content/plugins/fudoumodule/kaiin/wp-login.php?action=login&amp;KeepThis=true&amp;TB_iframe=true&amp;height=300&amp;width=350" class="thickbox"><img src="<?php echo bloginfo('template_directory');?>/images/btn_login.png" class="spnone" alt="ログイン" /><img src="<?php echo bloginfo('template_directory');?>/images/btn_login_sp.png" class="pcnone" alt="ログイン" /></a></li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>?page_id=11134%20class="><img src="<?php echo bloginfo('template_directory');?>/images/btn_kaiin.png" alt="新規会員登録" class="spnone" /><img src="<?php echo bloginfo('template_directory');?>/images/btn_kaiin_sp.png" class="pcnone" alt="新規会員登録" /></a></li>
		</ul>
	</div>
	<div class="contact_area spnone">
		<ul class="cf">
			<li><img src="<?php echo bloginfo('template_directory');?>/images/img_num.png" alt="お気軽にお問い合わせ下さい" /></li>
			<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>/?page_id=54"><img src="<?php echo bloginfo('template_directory');?>/images/btn_contact.png" alt="お問い合わせ資料請求はこちら" /></a></li>
		</ul>
	</div>


<?php if(get_option('expression_option_share_btn_top')=='yes'){?>
	<div id="site-share">
		<!-- original twitter btn -->
		<!--
	<a target="_blank" href="http://twitter.com/share
	?url=<?php echo urlencode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>
	&text=<?php echo get_bloginfo('title');?>
	">
	<img src="<?php echo get_bloginfo('template_directory');?>/images/share_t.gif" /></a>
-->
<!-- standard twitter btn -->
	<a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja">ツイート</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
	<!-- original fb button-->
	<!--
	<a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);?>&amp;t=<?php echo get_bloginfo('title');?>">
			<img src="<?php echo get_bloginfo('template_directory');?>/images/share_f.gif" />
	</a>
	-->
	<!-- standard fb button-->
	<div class="fb-like" data-href="<?php echo get_bloginfo('url'); ?>" data-send="false" data-width="100" data-colorscheme="light" data-layout="button_count" data-action="like" data-show-faces="false" data-font="segoe ui"></div>
	


	</div>

	<?php } ?>
<div id="site-text">
	<?php if(get_option('expression_option_open') != ''){?>
	<div id="site-open">
		<?php echo get_option('expression_option_open'); ?>
	</div>
	<?php } ?>
	<?php if(get_option('expression_option_tel') != ''){?>
	<div id="site-tel">
		<span class="prefix">TEL</span><span class="no"><?php echo get_option('expression_option_tel'); ?></span>
	</div>
	<?php } ?>
</div>




		<?php $header_image = get_header_image();
		if ( ! empty( $header_image ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $header_image ); ?>" class="header-image" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" /></a>
		<?php endif; ?>
	</header><!-- #masthead -->

<!-- #site-navigation -->
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<!--<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>-->
			<div class="spnone">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu cf spnone' ) ); ?>
			</div>
			<div class="pcnone">
			<?php wp_nav_menu( array( 'theme_location' => 'primary-sp', 'menu_class' => 'nav-menu cf pcnone' ) ); ?>
			</div>
		</nav>


<!-- top slideshow -->

				<?php if((is_front_page())){ 
				if(get_option('expression_slideshow_display')=='yes'){
				 ?>
				 <script type="text/javascript">
				 /* script for top slider */

						jQuery(window).load(function() {

						//flexslider

						  jQuery('#top_slider').flexslider({
						    animation: "<?php echo (get_option('expression_slideshow_type'))?:get_option('expression_slideshow_type');'slide' ?>",
						    controlNav: true,
						    directionNav: <?php echo (is_smartphone())?'false':'true'; ?>,
						    animationLoop: true,
						    slideshow: true,
						    smoothHeight: true,
						    slideshowSpeed: <?php echo (get_option('expression_slideshow_time'))?:get_option('expression_slideshow_time');'2000' ?>,
						    animationSpeed: <?php echo (is_smartphone())?'800':'1800';?>
						  });

						});
				 </script>

					<div class="slider-wrapper theme-default">
						<div class="slider-back">
							<div id="top_slider" class="flexslider">
								<ul class="slides">

				<?php 
				$i = 1;
				for($i=1 ; $i<7 ; $i++){

					if(get_option('expression_slideshow_'.$i) != ''){
						$link = get_option('expression_slideshow_link_'.$i);
						echo '<li>';
						if($link != '')echo '<a href="'.$link.'">';
						if(!is_smartphone()){
							echo '<img src="'. get_option('expression_slideshow_'.$i).'" alt="" />';
						}else{
							$aid = get_id_from_filename(get_option('expression_slideshow_'.$i));
							$arr = wp_get_attachment_image_src( $aid, 'large');
							echo '<img src="'. $arr[0].'" alt="" />';
						}
							echo (get_option('expression_slideshow_cap_'.$i))?'<div class="slide_cap">'.get_option('expression_slideshow_cap_'.$i).'</div>':'';
						if($link != '')echo '</a>';
						echo '</li>';
					}

					}
				
		?>
							</ul>
						</div>
					</div>
				</div>
		<?php
			}
		}
			?>

	<div id="main" class="wrapper site">


